import { Component, EventEmitter, Output } from '@angular/core';

/**
 * modal used by all CRUD actions that aren't actual CRUD
 */
@Component({
  selector: 'app-modal-action',
  templateUrl: './modal-action.component.html',
  styleUrls: ['./modal-action.component.scss']
})
export class ModalActionComponent {

  /**
   * emit confirm value
   */
  @Output() confirm: EventEmitter<boolean> = new EventEmitter();

  constructor() { }

  /**
   * emit confirmation value
   * @param confirm confirmation value
   */
  confirmAction(confirm: boolean){
    this.confirm.emit(confirm);
  }
}
