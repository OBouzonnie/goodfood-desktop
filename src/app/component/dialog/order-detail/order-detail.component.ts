import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Order } from 'src/app/models/apis/models';
import { DeliveryType } from 'src/app/models/enums/Delivery';
import { OrderDetailTableMapper } from 'src/app/models/table/mappers/OrderDetailTableMapper/OrderDetailTableMapper';
import { OrderInfoTableMapper } from 'src/app/models/table/mappers/OrderInfoTableMapper/OrderInfoTableMapper';
import { OrderDetailTableModel } from 'src/app/models/table/resources/OrderDetailTableModel';
import formatUtils from 'src/app/utils/format.utils';

/**
 * modal displaying an order informations
 */
@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.scss']
})
export class OrderDetailComponent {

  /**
   * formatting tools
   */
  formatUtils = formatUtils;

  /**
   * delivery type possible values
   */
  deliveryType = DeliveryType;

  /**
   * Define table columns
   */
  displayedColumns: string[] = [];

   /**
    * Table Data content
    */
  dataSource: OrderDetailTableModel[] = [];

   /**
    * Table Head content
    */
  head: any;

  /**
   * map order details informations for rendering
   */
  protected tableMapper: OrderDetailTableMapper = new OrderDetailTableMapper();

  /**
   * map order information for rendering
   */
  infoMapper: OrderInfoTableMapper|null = null;

  /**
   * information table title
   */
  infoTitle: string = 'dialog.order.info';

  /**
   * class constructor
   * @param data the order
   */
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: Order
  ) {
    if(this.data && this.data.details){
      this.infoMapper = new OrderInfoTableMapper(data);
      this.tableMapper.addMultipleRows(this.data.details);
      this.renderTable();
    }
  }

  /**
   * render the order detail table
   */
  renderTable(){
    this.displayedColumns = this.tableMapper.getColumns();
    this.head = this.tableMapper.head;
    this.dataSource = this.tableMapper.rows;
  }

}
