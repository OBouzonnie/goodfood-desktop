import { Component, EventEmitter, Output } from '@angular/core';

/**
 * modal displaying a confirmation message when a customer pick recipe from a different franchise that the one a cart is open for
 */
@Component({
  selector: 'app-modal-cart',
  templateUrl: './modal-cart.component.html',
  styleUrls: ['./modal-cart.component.scss']
})
export class ModalCartComponent {

  /**
   * confirm event
   */
  @Output() confirm: EventEmitter<boolean> = new EventEmitter();

  constructor() { }

  /**
   * emit confirmation
   * @param confirm confirm value
   */
  confirmNewCart(confirm: boolean){
    this.confirm.emit(confirm);
  }
}
