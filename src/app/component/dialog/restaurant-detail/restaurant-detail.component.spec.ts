import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TranslateModule } from '@ngx-translate/core';
import { AppModule } from 'src/app/app.module';
import { Restaurant } from 'src/app/models/apis/models';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { ShopModule } from 'src/app/modules/shop/shop.module';
import { MockService } from 'src/app/services/mock/mock.service';

import { RestaurantDetailComponent } from './restaurant-detail.component';

describe('RestaurantDetailComponent', () => {
  let component: RestaurantDetailComponent;
  let fixture: ComponentFixture<RestaurantDetailComponent>;
  let template: HTMLElement;
  let mockService: MockService;
  let restaurant: Restaurant;

  beforeEach(async () => {
    mockService = new MockService();
    restaurant = mockService.mockRestaurant();
    await TestBed.configureTestingModule({
      imports: [AppModule, SharedModule, ShopModule, TranslateModule],
      declarations: [ RestaurantDetailComponent ],
      providers: [{provide : MAT_DIALOG_DATA, useValue: restaurant}]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RestaurantDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    template = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render restaurant data', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(template.textContent).toContain(restaurant.name);
    })
  }));
});
