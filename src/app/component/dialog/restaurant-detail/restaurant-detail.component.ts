import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Restaurant } from 'src/app/models/apis/models';
import { CatalogService } from 'src/app/services/catalog/catalog.service';
import formatUtils from 'src/app/utils/format.utils';

/**
 * modal displaying a restaurant informations
 */
@Component({
  selector: 'app-restaurant-detail',
  templateUrl: './restaurant-detail.component.html',
  styleUrls: ['./restaurant-detail.component.scss']
})
export class RestaurantDetailComponent {

  /**
   * formatting tools
   */
  formatUtils = formatUtils;

  /**
   * class constructor
   * @param data the restaurant
   * @param catalogService the catalog service
   */
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: Restaurant,
    public catalogService: CatalogService
  ) {}
}
