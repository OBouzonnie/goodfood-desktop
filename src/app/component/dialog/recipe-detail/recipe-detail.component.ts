import { Component, EventEmitter, Inject, Output } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Recipe } from 'src/app/models/apis/models';
import { ApiUrl } from 'src/app/models/enums/ApiUrl';
import { RestaurantRecipeModel } from 'src/app/models/models/RestaurantRecipeModel';
import { CatalogService } from 'src/app/services/catalog/catalog.service';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import { environment } from 'src/environments/environment';

/**
 * Display a recipe details and a "add to cart" btn
 */
@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.scss']
})
export class RecipeDetailComponent {

  /**
   * image encoding
   */
  dwlImg: string = environment.core.dwlImg;

  /**
   * add to cart event
   */
  @Output() toCart: EventEmitter<RestaurantRecipeModel> = new EventEmitter();

  /**
   * the active recipe
   */
  recipe?: Recipe;

  /**
   * class constructor
   * @param data the restaurant recipe data
   * @param baseRepo the repository service
   * @param catalogService the catalog service
   */
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: RestaurantRecipeModel,
    public baseRepo: BaseRepositoryService,
    public catalogService: CatalogService
  ) {
    if (data.id) this.get(ApiUrl.RECIPE, data.id);
  }

  /**
   * gte the recipe additionnal data
   * @param api the api url
   * @param id the recipe id
   */
  protected get(api: string, id: string){
    this.baseRepo.get(api, id)
    .then(res => {
      this.recipe = res.body;
    })
    .catch(err => {
      console.error(err);
    });
  }

  /**
   * emit the add to cart event
   * @param data the restaurant recipe
   */
  addToCart(data: RestaurantRecipeModel){
    if(data){
      this.toCart.emit(data);
    }
  }
}
