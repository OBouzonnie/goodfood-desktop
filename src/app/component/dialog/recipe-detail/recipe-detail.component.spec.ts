import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TranslateModule } from '@ngx-translate/core';
import { AppModule } from 'src/app/app.module';
import { Recipe } from 'src/app/models/apis/models';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { ShopModule } from 'src/app/modules/shop/shop.module';
import { MockService } from 'src/app/services/mock/mock.service';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';

import { RecipeDetailComponent } from './recipe-detail.component';

describe('RecipeDetailComponent', () => {
  let component: RecipeDetailComponent;
  let fixture: ComponentFixture<RecipeDetailComponent>;
  let template: HTMLElement;
  let mockService: MockService;
  let recipes: Recipe[];
  let baseRepo: BaseRepositoryService;
  let getSpy: jasmine.Spy;
  let index: number;

  beforeEach(async () => {
    mockService = new MockService();
    recipes = mockService.getRecipes();
    index = Math.floor(Math.random() * recipes.length);
    await TestBed.configureTestingModule({
      imports: [AppModule, SharedModule, ShopModule, TranslateModule],
      declarations: [ RecipeDetailComponent ],
      providers: [{provide : MAT_DIALOG_DATA, useValue: recipes[index]}]
    })
    .compileComponents();
  });

  beforeEach(() => {
    baseRepo = TestBed.inject(BaseRepositoryService);
    getSpy = spyOn(baseRepo, 'get').and.callThrough().and.returnValue(Promise.resolve({body: recipes[index]}));
    fixture = TestBed.createComponent(RecipeDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    template = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render recipe data', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(template.textContent).toContain(recipes[index].name);
      recipes[index].ingredients?.forEach(i => expect(template.textContent).toContain(i.name));
    })
  }));
});
