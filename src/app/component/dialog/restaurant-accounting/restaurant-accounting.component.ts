import { Component, HostListener, Inject, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Order } from 'src/app/models/apis/models';
import { RestaurantStats } from 'src/app/models/apis/models/restaurant-stats';
import { ApiUrl } from 'src/app/models/enums/ApiUrl';
import { AccountingOrderTableMapper } from 'src/app/models/table/mappers/AccountingOrderTableMapper/AccountingOrderTableMapper copy';
import { MiniAccountingOrderTableMapper } from 'src/app/models/table/mappers/AccountingOrderTableMapper/MiniAccountingOrderTableMapper';
import { FranchiseStatsTableMapper } from 'src/app/models/table/mappers/FranchiseStatsTableMapper/FranchiseStatsTableMapper';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import { environment } from 'src/environments/environment';

/**
 * modal displaying a restaurant statistics information
 */
@Component({
  selector: 'app-restaurant-accounting',
  templateUrl: './restaurant-accounting.component.html',
  styleUrls: ['./restaurant-accounting.component.scss']
})
export class RestaurantAccountingComponent implements OnInit, OnChanges {

  /**
   * table/accordeon display toggle value
   */
  breakpoint: number = 600;

  /**
   * screen width
   */
  public innerWidth: any;

  /**
   * detect screen size
   */
  @HostListener('window:resize')
  onResize() {
    this.innerWidth = window.innerWidth;
  }

  /**
   * franchise statistics
   */
  stats: RestaurantStats|null = null;

  /**
   * franchises orders
   */
  orders: Order[] = [];

  /**
   * map the franchise statistics for rendering
   */
  statsMapper: FranchiseStatsTableMapper|null = null;

  /**
   * map the franchise orders for big screen
   */
  tableMapper: AccountingOrderTableMapper = new AccountingOrderTableMapper();

  /**
   * map the franchise orders for small screen
   */
  miniTableMapper: MiniAccountingOrderTableMapper = new MiniAccountingOrderTableMapper();

  /**
   * stat table title
   */
  statsTitle: string = 'resource.stats';

  /**
   * class constructor
   * @param id the franchise id
   * @param baseRepo the repository service
   */
  constructor(
    @Inject(MAT_DIALOG_DATA) public id: string,
    private baseRepo: BaseRepositoryService
  ) {
    if(this.id){
      this.getStats();
      this.getOrders();
    }
  }

  ngOnInit(){
    this.innerWidth = window.innerWidth;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes) {
      this.innerWidth = window.innerWidth;
    }
  }

  /**
   * get franchise stats
   */
  getStats(){
    if(this.id){
      const url = ApiUrl.RESTAURANT_STATS.replace(environment.core.idParam, this.id);
      this.baseRepo.get(url).then(res => {
        this.stats = res.body;
        if (this.stats) this.statsMapper = new FranchiseStatsTableMapper(this.stats);
      })
    }
  }

  /**
   * get franchise orders
   */
  getOrders(){
    if(this.id){
      const url = ApiUrl.RESTAURANT_ORDER + this.id;
      this.baseRepo.get(url).then(res => {
        this.orders = res.body;
        this.tableMapper.addMultipleRows(res.body);
        this.miniTableMapper.addMultipleRows(res.body);
      })
    }
  }

}
