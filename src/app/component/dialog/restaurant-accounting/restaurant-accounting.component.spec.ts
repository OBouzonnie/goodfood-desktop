import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { AppModule } from 'src/app/app.module';
import { RestaurantAccountingComponent } from './restaurant-accounting.component';

describe('RestaurantAccountingComponent', () => {
  let component: RestaurantAccountingComponent;
  let fixture: ComponentFixture<RestaurantAccountingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule, TranslateModule],
      declarations: [ RestaurantAccountingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RestaurantAccountingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
