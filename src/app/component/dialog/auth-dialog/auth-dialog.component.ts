import { Component, Inject } from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MsgDialog } from 'src/app/models/type/MsgDialog';

/**
 * Dialog Pop-Up displaying validators errors messages for authentification process
 */
@Component({
  selector: 'app-auth-dialog',
  templateUrl: './auth-dialog.component.html',
  styleUrls: ['./auth-dialog.component.scss']
})
export class AuthDialogComponent {

  /**
   * class constructor
   * @param data data provided to the modal
   */
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: MsgDialog
  ) {}
}
