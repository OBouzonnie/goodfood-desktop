import { Component, Inject } from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MsgDialog } from 'src/app/models/type/MsgDialog';

/**
 * Dialog Pop-Up displaying confirmation of successfull operation
 */
@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent {

  /**
   * class constructor
   * @param data data provided to the modal
   */
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: MsgDialog
  ) {}
}
