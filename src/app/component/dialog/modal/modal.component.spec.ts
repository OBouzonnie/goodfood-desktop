import { ComponentFixture, TestBed } from '@angular/core/testing';
import faker from '@faker-js/faker';
import { AppModule } from 'src/app/app.module';

import { ModalComponent } from './modal.component';

describe('ModalComponent', () => {
  let component: ModalComponent;
  let fixture: ComponentFixture<ModalComponent>;
  let template: HTMLElement;
  let bool: boolean;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule],
      declarations: [ ModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    template = fixture.nativeElement;
    bool = faker.datatype.boolean();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
