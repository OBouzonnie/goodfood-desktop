import { Component, EventEmitter, Input, Output } from '@angular/core';

/**
 * generic modal used by all crud forms
 */
@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent{

  /**
   * modal title
   */
  @Input() title: string = '';

  /**
   * define modal state
   * forms MUST be disabled for a true value
   */
  @Input() readonly: boolean = false;

  /**
   * confirmation event
   */
  @Output() confirm: EventEmitter<boolean> = new EventEmitter();

  constructor() { }

  /**
   * emit confirmation to the active form
   * @param confirm confirmation value
   */
  submission(confirm: boolean){
    this.confirm.emit(confirm);
  }
}
