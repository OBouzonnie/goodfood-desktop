import { Component, EventEmitter, Inject, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { Order } from 'src/app/models/apis/models';
import { OrderStatus } from 'src/app/models/enums/OrderStatus';
import { SnackbarService } from 'src/app/services/snackbar/snackbar.service';

/**
 * Modal handling the status changes of a franchise order
 */
@Component({
  selector: 'app-franchise-order',
  templateUrl: './franchise-order.component.html',
  styleUrls: ['./franchise-order.component.scss']
})
export class FranchiseOrderComponent {

  /**
   * Emit the new order status
   */
  @Output() submitted: EventEmitter<OrderStatus|null> = new EventEmitter();

  /**
   * return the modal form controls
   */
  get f(): any {return this.form.controls;}

  /**
   * modal form
   */
  form: FormGroup = this.fb.group({
    status: [null, Validators.required]
  });

  /**
   * modal title
   */
  title: string = this.translate.instant('dialog.status.title');

  /**
   * possible status
   */
  statusEnum = OrderStatus;

  /**
   * message displayed by the modal
   */
  msg: string = '';

  /**
   * class constructor
   * @param fb angular form builder
   * @param snackBarService allow the display of a snackbar popup for error mssage related to form validation
   * @param data the targeted order
   * @param translate internationalization tool
   */
  constructor(
    public fb: FormBuilder,
    public snackBarService: SnackbarService,
    @Inject(MAT_DIALOG_DATA) public data: Order,
    private translate: TranslateService
  ) {
    if(data && data.status && data.id){
      this.title += ' ' + data.id;
      this.msg = this.setMsg(data.status);
    }
  }

  /**
   * set the displayed message
   * @param status the actual status of the order
   * @returns the message
   */
  setMsg(status: OrderStatus): string{
    switch(status){
      case OrderStatus.PLACED: return this.translate.instant('dialog.status.approve');
      case OrderStatus.APPROVED: return this.translate.instant('dialog.status.ready');
      case OrderStatus.PROVIDED: return this.translate.instant('dialog.status.delivered');
      default: return '';
    }
  }

  /**
   * set the status emitted if the confirm btn is clicked
   * @param status the actual order status
   * @returns the new order status
   */
  setEmittedStatus(status: OrderStatus): OrderStatus|null{
    switch(status){
      case OrderStatus.PLACED: return OrderStatus.APPROVED;
      case OrderStatus.APPROVED: return OrderStatus.PROVIDED;
      case OrderStatus.PROVIDED: return OrderStatus.DELIVERED;
      default: return null;
    }
  };

  /**
   * handle confirm click event
   * @param event click event
   */
  handleModalConfirm(event: boolean): void{
    if (event && this.data.status) {
      const status = this.setEmittedStatus(this.data.status);
      this.submitted.emit(status);
      return;
    }
    this.submitted.emit(null);
  }

}
