import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Restaurant } from 'src/app/models/apis/models';
import { ApiUrl } from 'src/app/models/enums/ApiUrl';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';
import { TokenService } from 'src/app/services/token/token.service';
import { BackOfficeComponent } from '../back-office.component';

/**
 * Back Office Franchise parent component
 */
@Component({
  selector: 'app-back-office-franchise',
  templateUrl: '../back-office.component.html',
  styleUrls: ['../back-office.component.scss']
})
export class BackOfficeFranchiseComponent extends BackOfficeComponent {

  /**
   * api called to fetch franchise data
   */
  restaurantApi: ApiUrl = ApiUrl.RESTAURANT;

  /**
   * franchise data
   */
  restaurant?: Restaurant;

  /**
   * class constructor
   * @param tokenService allow access to jwt token payload
   * @param dialog material modals
   * @param baseRepo application repository
   * @param spinner allow to toggle on/off the application spinner
   */
  constructor(
    protected override dialog: MatDialog,
    protected override baseRepo: BaseRepositoryService,
    protected tokenService: TokenService,
    public override spinner: SpinnerService
  ) {
    super(dialog, baseRepo, spinner);
    this.franchise = this.getFranchise();
    this.getRestaurant();
  }

  /**
   * Get the active franchise ID from the jwt token
   * @returns the active franchise ID
   */
  getFranchise(): string|undefined{
    const id = this.tokenService.getFranchise();
    if(!id) return undefined;
    return id;
  }

  /**
   * Get the active franchise data
   */
  getRestaurant(){
    if(this.franchise) this.getResource(this.restaurantApi, this.franchise).then((res: any) => {
      this.restaurant = res.body;
      this.setActivesResources();
    }).catch(e => console.error(e));
  }

  /**
   * Set the franchise actives resources
   */
  setActivesResources(){
    this.actives = [];
  }

  /**
   * refresh data after every api calls
   */
  override refreshData(){
    this.getAllResources(this.apiUrl);
    this.getRestaurant();
  }

}
