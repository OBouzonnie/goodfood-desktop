import { Component } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { PurchaseComponent } from 'src/app/component/form/resource/purchase/purchase.component';
import { Purchase } from 'src/app/models/apis/models';
import { ApiUrl } from 'src/app/models/enums/ApiUrl';
import { CRUD } from 'src/app/models/enums/CRUD';
import { PurchaseTableMapper } from 'src/app/models/table/mappers/PurchaseTableMapper/PurchaseTableMapper';
import { PurchaseTableModel } from 'src/app/models/table/resources/PurchaseTableModel';
import { CrudConfig } from 'src/app/models/type/CrudConfig';
import { CrudTarget } from 'src/app/models/type/CrudTarget';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';
import { TokenService } from 'src/app/services/token/token.service';
import { environment } from 'src/environments/environment';
import { BackOfficeFranchiseComponent } from '../back-office-franchise.component';

/**
 * Target of the /franchise/:uid/purchase route
 */
@Component({
  selector: 'app-back-office-franchise-purchase',
  templateUrl: '../../back-office.component.html',
  styleUrls: ['../../back-office.component.scss']
})
export class BackOfficeFranchisePurchaseComponent extends BackOfficeFranchiseComponent {

  /**
   * crud api url
   */
  override apiUrl: string = ApiUrl.RESTAURANT_PURCHASE;

  /**
   * Content of the h1
   */
  override title: string = 'resource.purchase';

  /**
   * Resources displayed
   */
  override resources: Purchase[] = [];

  /**
   * Mapped resources for display
   */
  override dataSource: PurchaseTableModel[] = [];

  /**
   * CRUD actions allowed
   */
  override crudConfig: CrudConfig = [
    CRUD.CREATE,
    CRUD.READ
  ];

  /**
   * Map Table content from provided resources
   */
  override tableMapper: PurchaseTableMapper = new PurchaseTableMapper();

  /**
   * class constructor
   * @param tokenService allow access to jwt token payload
   * @param dialog material modals
   * @param baseRepo application repository
   * @param spinner allow to toggle on/off the application spinner
   */
  constructor(
  protected override dialog: MatDialog,
  protected override baseRepo: BaseRepositoryService,
  protected override tokenService: TokenService,
  public override spinner: SpinnerService
  ) {
    super(dialog, baseRepo, tokenService, spinner);
    if (this.franchise) this.apiUrl = this.apiUrl.replace(environment.core.idParam, this.franchise);
  }

  /**
   * Open the CRUD form modal handling Create, Read & Update actions
   * @param crudTarget contains information about the resource targeted by the crud action
   * @returns the modal reference
   */
  override openCRUmodal(crudTarget: CrudTarget): MatDialogRef<PurchaseComponent> {
    return this.dialog.open(PurchaseComponent, {data: crudTarget});
  }

}
