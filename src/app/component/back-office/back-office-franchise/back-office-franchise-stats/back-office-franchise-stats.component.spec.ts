import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppModule } from 'src/app/app.module';
import { MaterialModule } from 'src/app/modules/materials/material.module';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { BackOfficeFranchiseStatsComponent } from './back-office-franchise-stats.component';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';

describe('BackOfficeFranchiseStatsComponent', () => {
  let component: BackOfficeFranchiseStatsComponent;
  let fixture: ComponentFixture<BackOfficeFranchiseStatsComponent>;
  let spinner: SpinnerService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule, SharedModule, MaterialModule],
      declarations: [ BackOfficeFranchiseStatsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    spinner = TestBed.inject(SpinnerService);
    spyOn(spinner, 'increaseCount');
    spyOn(spinner, 'decreaseCount');
    fixture = TestBed.createComponent(BackOfficeFranchiseStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
