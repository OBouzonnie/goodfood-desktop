import { Component } from '@angular/core';
import { RestaurantStats } from 'src/app/models/apis/models/restaurant-stats';
import { ApiUrl } from 'src/app/models/enums/ApiUrl';
import { FranchiseStatsTableMapper } from 'src/app/models/table/mappers/FranchiseStatsTableMapper/FranchiseStatsTableMapper';
import { Logo } from 'src/app/models/type/Logo';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';
import { TokenService } from 'src/app/services/token/token.service';
import formatUtils from 'src/app/utils/format.utils';
import { environment } from 'src/environments/environment';

/**
 * Target of the /franchise/:uid/stats route
 */
@Component({
  selector: 'app-back-office-franchise-stats',
  templateUrl: './back-office-franchise-stats.component.html',
  styleUrls: ['./back-office-franchise-stats.component.scss']
})
export class BackOfficeFranchiseStatsComponent {

  /**
   * provided formatting tools for displayed values
   */
  formatUtils = formatUtils;

  /**
   * the franchise ID
   */
  franchise?: string;

  /**
   * the franchise statistics
   */
  stats?: RestaurantStats;

  /**
   * map franchise stats for display
   */
  statsMapper: FranchiseStatsTableMapper|null = null;

  /**
   * content of the h1
   */
  statsTitle: string = 'resource.stats';

  /**
   * GOOd FOOD logo img attributes
   */
  logo: Logo = {
    url:'assets/logo/logo-good_food-white.svg',
    alt:'good food logo'
  }

  /**
   * Handle injected services and instanciation
   */
   constructor(
    protected baseRepo: BaseRepositoryService,
    private tokenService: TokenService,
    public spinner: SpinnerService
  ) {
    this.franchise  = this.getFranchise();
    this.getStats();
  }

  /**
   * Get the franchise ID from the jwt token
   * @returns the franchise ID
   */
  getFranchise(): string|undefined{
    const id = this.tokenService.getFranchise();
    if(!id) return undefined;
    return id;
  }

  /**
   * Get the franchise statistics
   */
  getStats(){
    if(this.franchise){
      const url = ApiUrl.RESTAURANT_STATS.replace(environment.core.idParam, this.franchise);
      this.baseRepo.get(url).then(res => {
        this.stats = res.body;
        if (this.stats) this.statsMapper = new FranchiseStatsTableMapper(this.stats);
      })
    }
  }

}
