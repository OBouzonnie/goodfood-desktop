import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { AppModule } from 'src/app/app.module';
import { BaseModel, Restaurant } from 'src/app/models/apis/models';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { MockService } from 'src/app/services/mock/mock.service';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';
import { BackOfficeFranchiseDiscountComponent } from './back-office-franchise-discount.component';

describe('BackOfficeFranchiseDiscountComponent', () => {
  let component: BackOfficeFranchiseDiscountComponent;
  let fixture: ComponentFixture<BackOfficeFranchiseDiscountComponent>;
  let template: HTMLElement;
  let mockService: MockService;
  let resources: BaseModel[];
  let baseRepo: BaseRepositoryService;
  let restaurant: Restaurant;
  let spinner: SpinnerService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule, SharedModule],
      declarations: [ BackOfficeFranchiseDiscountComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    spinner = TestBed.inject(SpinnerService);
    spyOn(spinner, 'increaseCount');
    spyOn(spinner, 'decreaseCount');
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 300000;
    mockService = new MockService();
    resources = mockService.getDiscounts();
    restaurant = mockService.mockRestaurant();
    baseRepo = TestBed.inject(BaseRepositoryService);
    spyOn(baseRepo, 'getAll').and.callThrough().and.returnValue(Promise.resolve({body: resources}));
    spyOn(baseRepo, 'get').and.callThrough().and.returnValue(Promise.resolve({body: restaurant}));
    fixture = TestBed.createComponent(BackOfficeFranchiseDiscountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    template = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render all data', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      component.dataSource.forEach(d => {
        expect(template.textContent).toContain(d.value);
        expect(template.textContent).toContain(d.start);
        expect(template.textContent).toContain(d.end);
        expect(template.textContent).toContain(d.target);
      });
    });
  }));
});
