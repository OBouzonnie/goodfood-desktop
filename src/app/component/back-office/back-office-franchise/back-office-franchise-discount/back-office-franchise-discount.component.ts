import { Component } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DiscountComponent } from 'src/app/component/form/resource/discount/discount.component';
import { Discount, RestaurantRecipe } from 'src/app/models/apis/models';
import { ApiUrl } from 'src/app/models/enums/ApiUrl';
import { CRUD } from 'src/app/models/enums/CRUD';
import { DiscountTableMapper } from 'src/app/models/table/mappers/DiscountTableMapper/DiscountTableMapper';
import { DiscountTableModel } from 'src/app/models/table/resources/DiscountTableModel';
import { CrudConfig } from 'src/app/models/type/CrudConfig';
import { CrudTarget } from 'src/app/models/type/CrudTarget';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';
import { TokenService } from 'src/app/services/token/token.service';
import { BackOfficeFranchiseComponent } from '../back-office-franchise.component';

/**
 * Target of the /franchise/:uid/discount route
 */
@Component({
  selector: 'app-franchise-discount',
  templateUrl: '../../back-office.component.html',
  styleUrls: ['../../back-office.component.scss']
})
export class BackOfficeFranchiseDiscountComponent extends BackOfficeFranchiseComponent {

  /**
   * crud api url
   */
  override apiUrl: ApiUrl = ApiUrl.DISCOUNT;

  /**
   * Resources displayed
   */
  override resources: Discount[] = [];

  /**
   * the franchise woned recipes
   */
  restaurantRecipes: string[] = [];

  /**
   * Content of the h1
   */
  override title: string = 'resource.discount';

  /**
   * Mapped resources for display
   */
  override dataSource: DiscountTableModel[] = [];

  /**
   * CRUD actions allowed
   */
  override crudConfig: CrudConfig = [
    CRUD.CREATE,
    CRUD.ADD
  ];

  /**
   * Map Table content from provided resources
   */
  override tableMapper: DiscountTableMapper = new DiscountTableMapper();

  /**
   * class constructor
   * @param tokenService allow access to jwt token payload
   * @param dialog material modals
   * @param baseRepo application repository
   * @param spinner allow to toggle on/off the application spinner
   */
  constructor(
    protected override dialog: MatDialog,
    protected override baseRepo: BaseRepositoryService,
    protected override tokenService: TokenService,
    public override spinner: SpinnerService
  ) {
    super(dialog, baseRepo, tokenService, spinner);
  }

  /**
   * CRUD url
   * @returns returns the CRUD url
   */
  override crudUrl(): string {
    return ApiUrl.RESTAURANT + '/' + this.franchise +  this.apiUrl;
  }

  /**
   * Open the CRUD form modal handling Create, Read & Update actions
   * @param crudTarget contains information about the resource targeted by the crud action
   * @returns the modal reference
   */
  override openCRUmodal(crudTarget: CrudTarget): MatDialogRef<DiscountComponent> {
    return this.dialog.open(DiscountComponent, {data: crudTarget});
  }

  /**
   * Set the franchise actives resources
   */
  override setActivesResources(){
    this.actives = [];
    if(this.restaurant?.discount?.id){
      this.actives.push(this.restaurant.discount.id);
    }
    if(this.restaurant?.recipes){
      this.restaurant.recipes.forEach((r: RestaurantRecipe) => {
        if(r.id) this.restaurantRecipes.push(r.id);
        if(r.discount && r.discount.id) this.actives.push(r.discount.id);
      })
    }
  }

  /**
   * CRUD Create method
   */
  override crudCreate(){
    const ref = this.openCRUmodal({action: CRUD.CREATE, uid: '', franchise: this.franchise, actives: this.restaurantRecipes});
    ref.componentInstance.submitted.subscribe((confirm: boolean) => {
      if (confirm) this.postResource(this.crudUrl(), this.standardise(ref.componentInstance.getBody()));
      ref.componentInstance.submitted.unsubscribe();
      ref.close();
    });
  }

}
