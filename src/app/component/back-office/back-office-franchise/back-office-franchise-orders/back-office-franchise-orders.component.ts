import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { OrderDetailComponent } from 'src/app/component/dialog/order-detail/order-detail.component';
import { FranchiseOrderComponent } from 'src/app/component/dialog/franchise-order/franchise-order.component';
import { ApiUrl } from 'src/app/models/enums/ApiUrl';
import { CRUD } from 'src/app/models/enums/CRUD';
import { OrderTableMapper } from 'src/app/models/table/mappers/OrderTableMapper/OrderTableMapper';
import { CrudConfig } from 'src/app/models/type/CrudConfig';
import { CrudTarget } from 'src/app/models/type/CrudTarget';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import { BackOfficeFranchiseComponent } from '../back-office-franchise.component';
import { OrderStatus } from 'src/app/models/enums/OrderStatus';
import { Order } from 'src/app/models/apis/models';
import { DeliveryType } from 'src/app/models/enums/Delivery';
import { TokenService } from 'src/app/services/token/token.service';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';

/**
 * Target of the /franchise/:uid/order route
 */
@Component({
  selector: 'app-back-office-franchise-orders',
  templateUrl: '../../back-office.component.html',
  styleUrls: ['../../back-office.component.scss']
})
export class BackOfficeFranchiseOrdersComponent extends BackOfficeFranchiseComponent implements OnInit, OnDestroy{

  /**
   * crud api url
   */
  override apiUrl: string = ApiUrl.RESTAURANT_ORDER;

  /**
   * Content of the h1
   */
  override title: string = 'resource.order';

  /**
   * CRUD actions allowed
   */
  override crudConfig: CrudConfig = [
    CRUD.MANAGE,
    CRUD.READ
  ]

  /**
   * Resources displayed
   */
  override resources: Order[] = [];

  /**
   * Map Table content from provided resources
   */
  override tableMapper: OrderTableMapper = new OrderTableMapper(true);

  /**
   * Refresh interval
   */
  interval: any = null;

  /**
   * class constructor
   * @param tokenService allow access to jwt token payload
   * @param translate allow internationalization
   * @param dialog material modals
   * @param baseRepo application repository
   * @param spinner allow to toggle on/off the application spinner
   */
  constructor(
    public translate: TranslateService,
    protected override dialog: MatDialog,
    protected override baseRepo: BaseRepositoryService,
    protected override tokenService: TokenService,
    public override spinner: SpinnerService
  ) {
    super(dialog, baseRepo, tokenService, spinner);
  }

  override ngOnInit(): void{
    this.innerWidth = window.innerWidth;
    this.apiUrl +=  this.franchise;
    this.getAllResources(this.apiUrl);
    this.interval = setInterval(() => {
      this.getAllResources(this.apiUrl);
    },30000)
  }

  ngOnDestroy(): void {
    if(this.interval) clearInterval(this.interval);
  }

  /**
   * CRUD url
   * @returns returns the CRUD url
   */
  override crudUrl(): string {
    return ApiUrl.ORDER;
  }

  /**
   * open the Read or Update modal and call the manage status method on an Update confirmation
   * @param event contains information about the resource targeted by the crud action
   */
  override crudAction(event: CrudTarget): void{
    if(event.action == CRUD.READ && this.resources.length > 0){
      const order = this.resources.find(o => o.id === event.uid);
      if (order) this.dialog.open(OrderDetailComponent, {data: order});
    }
    if(event.action == CRUD.MANAGE && this.resources.length > 0){
      const order = this.resources.find(o => o.id === event.uid);
      if (order){
        const ref = this.dialog.open(FranchiseOrderComponent, {data: order});
        ref.componentInstance.submitted.subscribe((status: OrderStatus|null) => {
          if(status) this.manageStatus(status, event.uid);
          ref.componentInstance.submitted.unsubscribe();
          ref.close();
        })
      }
    }
  }

  /**
   * calls the actionResource method with the correct api action url
   * @param status the new delivery status
   * @param id the order updated ID
   */
  manageStatus(status: OrderStatus, id: string){
    switch(status){
      case OrderStatus.APPROVED:
        this.actionResource(this.crudUrl(), id, ApiUrl.ACTION_APPROVE);
        break;
      case OrderStatus.PROVIDED:
        this.actionResource(this.crudUrl(), id, ApiUrl.ACTION_PROVIDE);
        break;
      case OrderStatus.DELIVERED:
        this.actionResource(this.crudUrl(), id, ApiUrl.ACTION_DELIVER);
        break;
    }
  }

  /**
   * Call an api url in order to fetch all resources
   * @param url the url to call
   */
  override getAllResources(url: string){
    console.log(`GET ${url}`);
    this.spinner.increaseCount();
    this.baseRepo.getAll(url).then(res => {
      const all = res.body;
      if (all) this.resources = all.filter((o: Order) => o.delivery?.type == DeliveryType.TAKEAWAY || o.delivery?.type == DeliveryType.DELIVER && o.isPayed == true);
      if(this.resources) this.tableMapper.addMultipleRows(this.resources);
      this.renderTable();
      this.spinner.decreaseCount();
    })
    .catch(err => {
      console.error(err);
      this.spinner.decreaseCount();
    });
  }

}
