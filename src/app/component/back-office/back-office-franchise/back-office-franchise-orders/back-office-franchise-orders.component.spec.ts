import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { AppModule } from 'src/app/app.module';
import { BackOfficeFranchiseOrdersComponent } from './back-office-franchise-orders.component';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';
import { Order } from 'src/app/models/apis/models';
import { MockService } from 'src/app/services/mock/mock.service';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { MaterialModule } from 'src/app/modules/materials/material.module';
import { DeliveryType } from 'src/app/models/enums/Delivery';
import { OrderStatus } from 'src/app/models/enums/OrderStatus';

describe('BackOfficeFranchiseOrdersComponent', () => {
  let component: BackOfficeFranchiseOrdersComponent;
  let fixture: ComponentFixture<BackOfficeFranchiseOrdersComponent>;
  let spinner: SpinnerService;
  let orders: Order[];
  let mockService: MockService;
  let baseRepo: BaseRepositoryService;
  let template: HTMLElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule, SharedModule, MaterialModule],
      declarations: [ BackOfficeFranchiseOrdersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    mockService = TestBed.inject(MockService);
    orders = [
      mockService.mockOrder(OrderStatus.PLACED, DeliveryType.DELIVER),
      mockService.mockOrder(OrderStatus.PLACED, DeliveryType.DELIVER, false),
      mockService.mockOrder(OrderStatus.APPROVED, DeliveryType.DELIVER),
      mockService.mockOrder(OrderStatus.PROVIDED, DeliveryType.DELIVER),
      mockService.mockOrder(OrderStatus.DELIVERED, DeliveryType.DELIVER),
      mockService.mockOrder(OrderStatus.PLACED, DeliveryType.TAKEAWAY),
      mockService.mockOrder(OrderStatus.APPROVED, DeliveryType.TAKEAWAY),
      mockService.mockOrder(OrderStatus.PROVIDED, DeliveryType.TAKEAWAY)
    ];
    spinner = TestBed.inject(SpinnerService);
    baseRepo = TestBed.inject(BaseRepositoryService);
    spyOn(spinner, 'increaseCount');
    spyOn(spinner, 'decreaseCount');
    spyOn(baseRepo, 'getAll').and.callThrough().and.returnValue(Promise.resolve({body: orders}));
    spyOn(baseRepo, 'action');
    fixture = TestBed.createComponent(BackOfficeFranchiseOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    template = fixture.nativeElement;
    clearInterval(component.interval);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render all payed delivery', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      orders.forEach(o => {
        if (o.isPayed && o.delivery?.type === DeliveryType.DELIVER) expect(template.textContent).toContain(o.id);
      })
    })
  }))

  it('should render all takeway orders', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      orders.forEach(o => {
        if (o.delivery?.type === DeliveryType.TAKEAWAY) expect(template.textContent).toContain(o.id);
      })
    })
  }))

  it('should not render unpayed delivery order', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      orders.forEach(o => {
        if (!o.isPayed && o.delivery?.type === DeliveryType.DELIVER) expect(template.textContent).not.toContain(o.id);
      })
    })
  }))

  it('placed & payed delivery order should be eligible to become approved', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      const actions = template.querySelectorAll('app-crud-actions');
      const action = actions[6];
      const management = action.querySelector(`#crud-manage-${orders[0].id}`) as HTMLButtonElement;
      expect(management).toBeTruthy();
      management?.click();
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        const modal = document.querySelector('app-franchise-order');
        expect(modal).toBeTruthy();
        expect(modal?.textContent).toContain('dialog.status.approve');
      })
    })
  }));

  it('approved delivery order should be eligible to become provided', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      const actions = template.querySelectorAll('app-crud-actions');
      const action = actions[5];
      const management = action.querySelector(`#crud-manage-${orders[2].id}`) as HTMLButtonElement;
      expect(management).toBeTruthy();
      management?.click();
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        const modal = document.querySelector('app-franchise-order');
        expect(modal).toBeTruthy();
        expect(modal?.textContent).toContain('dialog.status.ready');
      })
    })
  }));

  it('provided delivery order should be eligible to become delivered', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      const actions = template.querySelectorAll('app-crud-actions');
      const action = actions[4];
      const management = action.querySelector(`#crud-manage-${orders[3].id}`) as HTMLButtonElement;
      expect(management).toBeTruthy();
      management?.click();
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        const modal = document.querySelector('app-franchise-order');
        expect(modal).toBeTruthy();
        expect(modal?.textContent).toContain('dialog.status.delivered');
      })
    })
  }));

  it('delivered delivery order should NOT provide a management button', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      const actions = template.querySelectorAll('app-crud-actions');
      const action = actions[3];
      const management = action.querySelector(`#crud-manage-${orders[4].id}`) as HTMLButtonElement;
      expect(management).toBeFalsy();
    })
  }));

  it('placed takeaway order should be eligible to become approved', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      const actions = template.querySelectorAll('app-crud-actions');
      const action = actions[2];
      const management = action.querySelector(`#crud-manage-${orders[5].id}`) as HTMLButtonElement;
      expect(management).toBeTruthy();
      management?.click();
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        const modal = document.querySelector('app-franchise-order');
        expect(modal).toBeTruthy();
        expect(modal?.textContent).toContain('dialog.status.approve');
      })
    })
  }));

  it('approved takeaway order should be eligible to become provided', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      const actions = template.querySelectorAll('app-crud-actions');
      const action = actions[1];
      const management = action.querySelector(`#crud-manage-${orders[6].id}`) as HTMLButtonElement;
      expect(management).toBeTruthy();
      management?.click();
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        const modal = document.querySelector('app-franchise-order');
        expect(modal).toBeTruthy();
        expect(modal?.textContent).toContain('dialog.status.ready');
      })
    })
  }));

  it('provided takeaway order should NOT provide a management button', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      const actions = template.querySelectorAll('app-crud-actions');
      const action = actions[0];
      const management = action.querySelector(`#crud-manage-${orders[7].id}`) as HTMLButtonElement;
      expect(management).toBeFalsy();
    })
  }));

});
