import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppModule } from 'src/app/app.module';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';
import { BackOfficeFranchiseComponent } from './back-office-franchise.component';

describe('BackOfficeFranchiseComponent', () => {
  let component: BackOfficeFranchiseComponent;
  let fixture: ComponentFixture<BackOfficeFranchiseComponent>;
  let spinner: SpinnerService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule, SharedModule],
      declarations: [ BackOfficeFranchiseComponent ],
    })
    .compileComponents();
  });

  beforeEach(() => {
    spinner = TestBed.inject(SpinnerService);
    spyOn(spinner, 'increaseCount');
    spyOn(spinner, 'decreaseCount');
    fixture = TestBed.createComponent(BackOfficeFranchiseComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
