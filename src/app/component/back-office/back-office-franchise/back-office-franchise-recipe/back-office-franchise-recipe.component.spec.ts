import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import faker from '@faker-js/faker';
import { AppModule } from 'src/app/app.module';
import { Ingredient, Recipe, Restaurant } from 'src/app/models/apis/models';
import { MaterialModule } from 'src/app/modules/materials/material.module';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { MockService } from 'src/app/services/mock/mock.service';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';
import { BackOfficeFranchiseRecipeComponent } from './back-office-franchise-recipe.component';

describe('BackOfficeFranchiseRecipeComponent', () => {
  let component: BackOfficeFranchiseRecipeComponent;
  let fixture: ComponentFixture<BackOfficeFranchiseRecipeComponent>;
  let template: HTMLElement;
  let uid: string;
  let mockService: MockService;
  let resources: Recipe[];
  let recipe: Recipe;
  let baseRepo: BaseRepositoryService;
  let ing: Ingredient[];
  let getAllSpy: jasmine.Spy;
  let restaurant: Restaurant;
  let spinner: SpinnerService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule, SharedModule, MaterialModule],
      declarations: [ BackOfficeFranchiseRecipeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    spinner = TestBed.inject(SpinnerService);
    spyOn(spinner, 'increaseCount');
    spyOn(spinner, 'decreaseCount');
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 300000;
    mockService = new MockService();
    ing = mockService.getIngredients();
    resources = mockService.getRecipes();
    restaurant = mockService.mockRestaurant();
    baseRepo = TestBed.inject(BaseRepositoryService);
    getAllSpy = spyOn(baseRepo, 'getAll').and.callThrough().and.returnValue(Promise.resolve({body: resources}));
    spyOn(baseRepo, 'get').and.callThrough().and.returnValue(Promise.resolve({body: restaurant}));
    fixture = TestBed.createComponent(BackOfficeFranchiseRecipeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    template = fixture.nativeElement;
    uid = faker.datatype.uuid();
    recipe = resources[Math.floor(Math.random() * resources.length)];
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render all data', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      const img = template.querySelectorAll('img');
      const src: string[] = [];
      img.forEach(img => {
        const i: HTMLImageElement = img as HTMLImageElement;
        src.push(i.src);
      });
      component.dataSource.forEach(d => {
        expect(template.textContent).toContain(d.name);
        expect(template.textContent).toContain(d.price);
        expect(src).toContain('unsafe:data:image/jpeg;base64,' + d.photo);
      });
    })
  }));
});
