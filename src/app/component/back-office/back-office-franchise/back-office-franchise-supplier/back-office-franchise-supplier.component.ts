import { Component } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { SupplierComponent } from 'src/app/component/form/resource/supplier/supplier.component';
import { Supplier } from 'src/app/models/apis/models';
import { ApiUrl } from 'src/app/models/enums/ApiUrl';
import { SupplierTableMapper } from 'src/app/models/table/mappers/SupplierTableMapper/SupplierTableMapper';
import { SupplierTableModel } from 'src/app/models/table/resources/SupplierTableModel';
import { CrudTarget } from 'src/app/models/type/CrudTarget';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';
import { TokenService } from 'src/app/services/token/token.service';
import { BackOfficeFranchiseComponent } from '../back-office-franchise.component';

/**
 * Target of the /franchise/:uid/discount route
 */
@Component({
  selector: 'app-franchise-supplier',
  templateUrl: '../../back-office.component.html',
  styleUrls: ['../../back-office.component.scss']
})
export class BackOfficeFranchiseSupplierComponent extends BackOfficeFranchiseComponent {

  /**
   * crud api url
   */
  override apiUrl: string = ApiUrl.SUPPLIER;

  /**
   * Content of the h1
   */
  override title: string = 'resource.supplier';

  /**
   * Resources displayed
   */
  override resources: Supplier[] = [];


  /**
   * Mapped resources for display
   */
  override dataSource: SupplierTableModel[] = [];

  /**
   * Map Table content from provided resources
   */
  override tableMapper: SupplierTableMapper = new SupplierTableMapper();

  /**
   * class constructor
   * @param tokenService allow access to jwt token payload
   * @param dialog material modals
   * @param baseRepo application repository
   * @param spinner allow to toggle on/off the application spinner
   */
  constructor(
    protected override dialog: MatDialog,
    protected override baseRepo: BaseRepositoryService,
    protected override tokenService: TokenService,
    public override spinner: SpinnerService
  ) {
    super(dialog, baseRepo, tokenService, spinner);
  }

  /**
   * CRUD url
   * @returns returns the CRUD url
   */
  override crudUrl(): string {
    return ApiUrl.RESTAURANT + '/' + this.franchise +  this.apiUrl;
  }

  /**
   * Open the CRUD form modal handling Create, Read & Update actions
   * @param crudTarget contains information about the resource targeted by the crud action
   * @returns the modal reference
   */
  override openCRUmodal(crudTarget: CrudTarget): MatDialogRef<SupplierComponent> {
    console.log("openCRUmodal for supplier");
    return this.dialog.open(SupplierComponent, {data: crudTarget});
  }

  /**
   * Add the isStandard attribute to the resource
   * @param body the resource targeted
   * @returns the modified resource
   */
  override standardise(body: Supplier): Supplier{
    body.isStandard = false;
    return body;
  }

}
