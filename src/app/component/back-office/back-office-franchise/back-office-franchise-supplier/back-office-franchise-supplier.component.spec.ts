import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import faker from '@faker-js/faker';
import { AppModule } from 'src/app/app.module';
import { BaseModel, Ingredient, Restaurant, Supplier } from 'src/app/models/apis/models';
import { MaterialModule } from 'src/app/modules/materials/material.module';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { MockService } from 'src/app/services/mock/mock.service';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';
import { BackOfficeFranchiseSupplierComponent } from './back-office-franchise-supplier.component';

describe('BackOfficeFranchiseSupplierComponent', () => {
  let component: BackOfficeFranchiseSupplierComponent;
  let fixture: ComponentFixture<BackOfficeFranchiseSupplierComponent>;
  let template: HTMLElement;
  let mockService: MockService;
  let resources: BaseModel[];
  let baseRepo: BaseRepositoryService;
  let ing: Ingredient[];
  let getAllSpy: jasmine.Spy;
  let uid: string;
  let resource: Supplier;
  let restaurant: Restaurant;
  let spinner: SpinnerService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule, SharedModule, MaterialModule],
      declarations: [ BackOfficeFranchiseSupplierComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    spinner = TestBed.inject(SpinnerService);
    spyOn(spinner, 'increaseCount');
    spyOn(spinner, 'decreaseCount');
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 300000;
    mockService = new MockService();
    ing = mockService.getIngredients();
    resources = mockService.getSuppliers();
    restaurant = mockService.mockRestaurant();
    baseRepo = TestBed.inject(BaseRepositoryService);
    getAllSpy = spyOn(baseRepo, 'getAll').and.callThrough().and.returnValue(Promise.resolve({body: resources}));
    spyOn(baseRepo, 'get').and.callThrough().and.returnValue(Promise.resolve({body: restaurant}));
    fixture = TestBed.createComponent(BackOfficeFranchiseSupplierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    template = fixture.nativeElement;
    uid = faker.datatype.uuid();
    resource = resources[Math.floor(Math.random() * resources.length)];
    console.log(resource)
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render all data', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      component.dataSource.forEach(s => {
        expect(template.textContent).toContain(s.email);
        expect(template.textContent).toContain(s.phone);
        expect(template.textContent).toContain(s.name);
      });
    });
  }));
});
