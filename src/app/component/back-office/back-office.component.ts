import { Component, HostListener, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ApiUrl } from 'src/app/models/enums/ApiUrl';
import { CRUD } from 'src/app/models/enums/CRUD';
import { CrudTarget } from 'src/app/models/type/CrudTarget';
import { TableMapper } from 'src/app/models/table/mappers/TableMapper';
import { TableModel } from 'src/app/models/table/resources/TableModel';
import { ModalActionComponent } from '../dialog/modal-action/modal-action.component';
import { ResourceComponent } from '../form/resource/resource.component';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import { BaseModel } from 'src/app/models/apis/models';
import { CrudConfig } from 'src/app/models/type/CrudConfig';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';

/**
 * Base Office first parent
 * Display a table component for large screen and an accordeon component for small ones
 * Own the back-office html template used by all back-office containers components
 */
@Component({
  selector: 'app-back-office',
  templateUrl: './back-office.component.html',
  styleUrls: ['./back-office.component.scss']
})
export class BackOfficeComponent implements OnInit, OnChanges {

  /**
   * active franchise
   */
  franchise?: string;

  /**
   * available CRUD actions
   */
  crudEnum = CRUD;

  /**
   * crud api url
   */
  apiUrl: string = ApiUrl.NONE;

  /**
   * switching point for table & accordeon components
   */
  breakpoint: number = 800;

  /**
   * screen width
   */
  public innerWidth: any;

  /**
   * Content of the h1
   */
  title: string = '';

  /**
   * Resources displayed
   */
  resources: BaseModel[] = [];

  /**
   * Resources already possessed by the active franchise
   */
  actives: string[] = [];

  /**
   * Define table columns
   */
  displayedColumns: string[] = [];

  /**
   * Mapped resources for display
   */
  dataSource: TableModel[] = [];

  /**
   * Table Head content
   */
  head: any;

  /**
   * CRUD actions allowed
   */
  crudConfig: CrudConfig = [
    CRUD.CREATE,
    CRUD.READ,
    CRUD.UPDATE,
    CRUD.DELETE
  ]

  /**
   * Map Table content from provided resources
   */
  protected tableMapper: TableMapper = new TableMapper();

  /**
   * Detect screen size
   */
  @HostListener('window:resize')
  onResize() {
    this.innerWidth = window.innerWidth;
  }

  /**
   * class constructor
   * @param dialog material modals
   * @param baseRepo application repository
   * @param spinner allow to toggle on/off the application spinner
   */
  constructor(
    protected dialog: MatDialog,
    protected baseRepo: BaseRepositoryService,
    public spinner: SpinnerService
  ) {}

  ngOnInit(){
    this.innerWidth = window.innerWidth;
    this.getAllResources(this.apiUrl);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes) {
      this.innerWidth = window.innerWidth;
    }
  }

  /**
   * Set table & accordeon data
   */
  renderTable(){
    this.displayedColumns = [...this.tableMapper.getColumns()];
    this.head = this.tableMapper.head;
    this.dataSource = this.tableMapper.rows;
  }

  /**
   * CRUD url
   * @returns returns the CRUD url
   */
  crudUrl(): string {
    return this.apiUrl;
  }

  /**
   * calls the CRUD methods matching the CRUD action applied to the target
   * @param event contains information about the resource targeted by the crud action
   */
  crudAction(event: CrudTarget): void{
    switch(event.action){
      case CRUD.READ:
        this.crudRead(event);
        break;
      case CRUD.UPDATE:
        this.crudUpdate(event);
        break;
      case CRUD.DELETE:
        this.crudDelete(event);
        break;
      case CRUD.STANDARDISE:
        this.restAction(event, ApiUrl.ACTION_STANDARDISE);
        break;
      case CRUD.ADD:
        this.restAction(event, ApiUrl.ACTION_ADD);
        break;
      case CRUD.REMOVE:
        this.restAction(event, ApiUrl.ACTION_REMOVE);
        break;
    }
  }

  /**
   * Open the CRUD form modal handling Create, Read & Update actions
   * @param crudTarget contains information about the resource targeted by the crud action
   * @returns the modal reference
   */
  openCRUmodal(crudTarget: CrudTarget): MatDialogRef<ResourceComponent> {
    return this.dialog.open(ResourceComponent, {data: crudTarget});
  }

  /**
   * Open the CRUD form modal handling Delete actions
   * @returns the modal reference
   */
  openDmodal(): MatDialogRef<ModalActionComponent> {
    return this.dialog.open(ModalActionComponent);
  }

  /**
   * Add attributes to a resource
   * @param body the resource targeted
   * @returns the modified resource
   */
  standardise(body: BaseModel): BaseModel{
    return body;
  }

  /**
   * CRUD Create method
   */
  crudCreate(){
    const ref = this.openCRUmodal({action: CRUD.CREATE, uid: '', franchise: this.franchise});
    ref.componentInstance.submitted.subscribe((confirm: boolean) => {
      if (confirm) this.postResource(this.crudUrl(), this.standardise(ref.componentInstance.getBody()));
      ref.componentInstance.submitted.unsubscribe();
      ref.close();
    });
  }

  /**
   * CRUD Read method
   */
  crudRead(crudTarget: CrudTarget) {
    this.getResource(this.apiUrl, crudTarget.uid).then(res => {
      if (res.status === 200) crudTarget.resource = res.body;
      const ref = this.openCRUmodal(crudTarget);
      ref.componentInstance.submitted.subscribe((confirm: BaseModel) => {
        if (!confirm) {
          ref.componentInstance.submitted.unsubscribe();
          ref.close();
        }
      });
    });
  }

  /**
   * CRUD Update method
   */
  crudUpdate(crudTarget: CrudTarget) {
    this.getResource(this.apiUrl, crudTarget.uid).then(res => {
      if (res.status === 200) crudTarget.resource = res.body;
      const ref = this.openCRUmodal(crudTarget);
      ref.componentInstance.submitted.subscribe((confirm: BaseModel) => {
        if (confirm) this.putResource(this.crudUrl(), this.standardise(ref.componentInstance.getBody()));
        ref.componentInstance.submitted.unsubscribe();
        ref.close();
      });
    });
  }

  /**
   * CRUD Delete method
   */
  crudDelete(crudTarget: CrudTarget) {
    const ref = this.openDmodal();
    ref.componentInstance.confirm.subscribe((confirm: boolean) => {
      if (confirm) this.deleteResource(this.crudUrl(), crudTarget.uid);
      ref.componentInstance.confirm.unsubscribe();
      ref.close();
    });
  }

  /**
   * Other CRUD actions method
   * @param crudTarget contains information on the targeted resource
   * @param actionUrl the action url
   */
  restAction(crudTarget: CrudTarget, actionUrl: ApiUrl){
    const ref = this.openDmodal();
    ref.componentInstance.confirm.subscribe((confirm: boolean) => {
      if (confirm) this.actionResource(this.crudUrl(), crudTarget.uid, actionUrl);
      ref.componentInstance.confirm.unsubscribe();
      ref.close();
    });
  }

  /**
   * Call an api url in order to fetch all resources
   * @param url the url to call
   */
  getAllResources(url: string){
    console.log(`GET ${url}`);
    this.spinner.increaseCount();
    this.baseRepo.getAll(url).then(res => {
      this.resources = res.body;
      if(this.resources) this.tableMapper.addMultipleRows(this.resources);
      this.renderTable();
      this.spinner.decreaseCount();
    })
    .catch(err => {
      console.error(err);
      this.spinner.decreaseCount();
    });
  }

  /**
   * Calls the repository for a POST HTTP request
   * @param url the api url to call
   * @param body the resource to create
   */
  postResource(url: string, body: BaseModel) {
    console.log(`POST ${url} with body ${body}`);
    this.baseRepo.post(url, body).then(res => {
      this.refreshData();
    });
  }

  /**
   * Calls the repository for a GET HTTP request
   * @param url the api url to call
   * @param uid the resource to read ID
   * @returns the promise
   */
  getResource(url: string, uid: string): Promise<any> {
    console.log(`GET ${url}/${uid}`);
    return this.baseRepo.get(url, uid);
  }

  /**
   * Calls the repository for a PUT HTTP request
   * @param url the api url to call
   * @param body the resource to update
   */
  putResource(url: string, body: BaseModel){
    console.log(`PUT ${url}/${body.id} with body ${body}`);
    this.baseRepo.put(url, body).then(res => {
      this.refreshData();
    });
  }

  /**
   * Calls the repository for a DELETE HTTP request
   * @param url the api url to call
   * @param uid the resource to delete
   */
  deleteResource(url: string, uid: string){
    console.log(`DELETE ${url}/${uid}`);
    this.baseRepo.delete(url, uid).then(res => {
      this.refreshData();
    });
  }

  /**
   * Calls the repository for an action api call, with a POST HTTP request
   * @param url the api url to call
   * @param id the resource targeted by the action ID
   * @param action the api action url
   */
  actionResource(url: string, id: string, action: ApiUrl) {
    const actionUrl = url + '/' + id + action;
    console.log(`POST ${actionUrl}`);
    this.baseRepo.action(actionUrl).then(res => {
      this.refreshData();
    });
  }

  /**
   * refresh data after every api calls
   */
  refreshData(){
    this.getAllResources(this.apiUrl);
  }

}
