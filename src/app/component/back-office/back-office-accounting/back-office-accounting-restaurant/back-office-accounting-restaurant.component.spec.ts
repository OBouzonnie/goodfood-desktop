import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppModule } from 'src/app/app.module';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';
import { BackOfficeAccountingRestaurantComponent } from './back-office-accounting-restaurant.component';

describe('BackOfficeAccountingRestaurantComponent', () => {
  let component: BackOfficeAccountingRestaurantComponent;
  let fixture: ComponentFixture<BackOfficeAccountingRestaurantComponent>;
  let spinner: SpinnerService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule],
      declarations: [ BackOfficeAccountingRestaurantComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    spinner = TestBed.inject(SpinnerService);
    spyOn(spinner, 'increaseCount');
    spyOn(spinner, 'decreaseCount');
    fixture = TestBed.createComponent(BackOfficeAccountingRestaurantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
