import { Component } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { RestaurantAccountingComponent } from 'src/app/component/dialog/restaurant-accounting/restaurant-accounting.component';
import { CRUD } from 'src/app/models/enums/CRUD';
import { RestaurantTableMapper } from 'src/app/models/table/mappers/RestaurantTableMapper/RestaurantTableMapper';
import { CrudConfig } from 'src/app/models/type/CrudConfig';
import { CrudTarget } from 'src/app/models/type/CrudTarget';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';
import { BackOfficeGroupRestaurantComponent } from '../../back-office-group/back-office-group-restaurant/back-office-group-restaurant.component';

/**
 * Back Office Accounting only component
 */
@Component({
  selector: 'app-back-office-accounting-restaurant',
  templateUrl: '../../back-office.component.html',
  styleUrls: ['../../back-office.component.scss']
})
export class BackOfficeAccountingRestaurantComponent extends BackOfficeGroupRestaurantComponent {

  /**
   * Content of the h1
   */
  override title: string = 'resource.accounting';

  /**
   * CRUD actions allowed
   */
  override crudConfig: CrudConfig = [
    CRUD.READ
  ]

  /**
   * Map Table content from provided resources
   */
  override tableMapper: RestaurantTableMapper = new RestaurantTableMapper(true);

  /**
   * class constructor
   * @param translate allow internationalization
   * @param dialog material modals
   * @param baseRepo application repository
   * @param spinner allow to toggle on/off the application spinner
   */
  constructor(
    public override translate: TranslateService,
    public override dialog: MatDialog,
    protected override baseRepo: BaseRepositoryService,
    public override spinner: SpinnerService
  ) {
    super(translate, dialog, baseRepo, spinner);
  }

  /**
   * CRUD Read method
   */
  override crudRead(crudTarget: CrudTarget) {
    console.log(crudTarget)
    this.openModal(crudTarget.uid);
  }

  /**
   * Open the modal displaying the franchise informations
   * @param id the franchise ID
   * @returns the modal ref
   */
  openModal(id: string): MatDialogRef<RestaurantAccountingComponent> {
    return this.dialog.open(RestaurantAccountingComponent, {data : id});
  }

}
