import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import faker from '@faker-js/faker';
import { AppModule } from 'src/app/app.module';
import { Ingredient } from 'src/app/models/apis/models';
import { BaseModel } from 'src/app/models/apis/models';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { MockService } from 'src/app/services/mock/mock.service';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import formatUtils from 'src/app/utils/format.utils';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';
import { BackOfficeGroupRecipeComponent } from './back-office-group-recipe.component';

describe('BackOfficeGroupRecipeComponent', () => {
  let component: BackOfficeGroupRecipeComponent;
  let fixture: ComponentFixture<BackOfficeGroupRecipeComponent>;
  let template: HTMLElement;
  let uid: string;
  let mockService: MockService;
  let resources: BaseModel[];
  let baseRepo: BaseRepositoryService;
  let ing: Ingredient[];
  let getAllSpy: jasmine.Spy;
  let spinner: SpinnerService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule, SharedModule, HttpClientTestingModule],
      declarations: [ BackOfficeGroupRecipeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    spinner = TestBed.inject(SpinnerService);
    spyOn(spinner, 'increaseCount');
    spyOn(spinner, 'decreaseCount');
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 300000;
    mockService = new MockService();
    ing = mockService.getIngredients();
    resources = mockService.getRecipes();
    baseRepo = TestBed.inject(BaseRepositoryService);
    getAllSpy = spyOn(baseRepo, 'getAll').and.callThrough().and.returnValue(Promise.resolve({body: resources}));
    fixture = TestBed.createComponent(BackOfficeGroupRecipeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    template = fixture.nativeElement;
    uid = faker.datatype.uuid();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  /**
   * Integration Testing - Mapping
   */
  it('#displayedColumns should be RecipeTableMapper columns', waitForAsync(() => {
    fixture.whenStable().then(() => {
      expect(component.displayedColumns).toContain('name');
      expect(component.displayedColumns).toContain('photo');
      expect(component.displayedColumns).toContain('price');
      expect(component.displayedColumns).toContain('uid');
    });
  }));

  it('#head should be RecipeTableMapper head', waitForAsync(() => {
    fixture.whenStable().then(() => {
      expect(component.head).toEqual({
        name: 'resource.recipe',
        photo: 'table.photo',
        price: 'table.price',
        count: 'table.usage',
        logo: '',
        uid: ''
      });
    });
  }));

  it('#dataSource should correctly map Recipes Resources', waitForAsync(() => {
    fixture.whenStable().then(() => {
      component.dataSource.forEach((d, i) => {
        const sp = component.resources[i].standardPrice;
        const p = component.resources[i].photo;
        expect(d.name).toBe(component.resources[i].name);
        expect(d.price).toBe(sp ? formatUtils.toEuroString(sp) : '');
        expect(d.photo).toBe(p ? p : '');
        expect(d.uid).toBe(component.resources[i].id);
        expect(d.collapse).toBeTrue();
      });
    });
  }));

  /**
   * Integration Testing - Rendering
   */
  it('should render all data', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      const img = template.querySelectorAll('img');
      const src: string[] = [];
      img.forEach(img => {
        const i: HTMLImageElement = img as HTMLImageElement;
        src.push(i.src);
      });
      component.dataSource.forEach(d => {
        expect(template.textContent).toContain(d.name);
        expect(template.textContent).toContain(d.price);
        expect(src).toContain('unsafe:data:image/jpeg;base64,' + d.photo);
      });
    });
  }));

  /**
   * Integration Testing - CRUD btn
   */
  it('form submission for a crud create action', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      getAllSpy.and.callThrough().and.returnValue(Promise.resolve({body: ing}));
      const btn = template.querySelector('#crud-create-btn');
      btn?.dispatchEvent(new Event('click'));
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        const modal = document.querySelector('app-modal');
        const form = modal?.querySelector('form');
        const confirm = modal?.querySelector('#confirm-modal-btn');
        const ingBtn = form?.querySelector('#ing-add-btn');
        ingBtn?.dispatchEvent(new Event('click'));
        const name = form?.querySelector('#name') as HTMLInputElement;
        const price = form?.querySelector('#price') as HTMLInputElement;
        let ingId = form?.querySelector('#ingId0') as HTMLElement;
        const ingQty = form?.querySelector('#ingQty0') as HTMLInputElement;
        ingId?.click();
        fixture.whenStable().then(() => {
          fixture.detectChanges();
          spyOn(baseRepo, 'post').and.callThrough();
          const options = document.querySelectorAll('mat-option');
          const o = Math.floor(Math.random() * (options.length - 1));
          const option = options[o];
          option.dispatchEvent(new Event('click'));
          name.value = faker.name.lastName();
          name.dispatchEvent(new Event('input'));
          price.value = (Math.floor(Math.random() * 1000) / 100).toString();
          price.dispatchEvent(new Event('input'));
          ingQty.value = Math.floor(Math.random() * 100).toString();
          ingQty?.dispatchEvent(new Event('input'));
          fixture.detectChanges();
          confirm?.dispatchEvent(new Event('click'));
          fixture.detectChanges();
          expect(baseRepo.post).toHaveBeenCalled();
        });
      })
    });
  }));

  it('form submission for a crud update action', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      const crudBtns = template.querySelectorAll('.crud-update-btn');
      const btn = crudBtns[Math.floor(Math.random() * crudBtns.length)];
      const id = btn.getAttribute('id')?.replace('crud-update-', '');
      const recipe = component.resources.find(i => i.id === id);
      spyOn(component, 'getResource').and.callThrough().and.returnValue(Promise.resolve({status: 200, body: recipe}));
      getAllSpy.and.callThrough().and.returnValue(Promise.resolve({body: ing}));
      btn.dispatchEvent(new Event('click'));
      fixture.whenStable().then(() => {
        spyOn(baseRepo, 'put').and.callThrough();
        fixture.detectChanges();
        const modal = document.querySelector('app-modal');
        const form = modal?.querySelector('form');
        const confirm = modal?.querySelector('#confirm-modal-btn');
        const name = form?.querySelector('#name') as HTMLInputElement;
        const price = form?.querySelector('#price') as HTMLInputElement;
        name.value = faker.name.lastName();
        name.dispatchEvent(new Event('input'));
        fixture.detectChanges();
        price.value = (Math.floor(Math.random() * 1000) / 100).toString();
        price.dispatchEvent(new Event('input'));
        fixture.detectChanges();
        confirm?.dispatchEvent(new Event('click'));
        fixture.detectChanges();
        expect(baseRepo.put).toHaveBeenCalled();
      });
    });
  }));

  it('submission for a crud delete action', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      spyOn(baseRepo, 'delete').and.callThrough();
      const crudBtns = template.querySelectorAll('.crud-delete-btn');
      const btn = crudBtns[Math.floor(Math.random() * crudBtns.length)];
      btn.dispatchEvent(new Event('click'));
      const modal = document.querySelector('app-modal-action');
      const confirm = modal?.querySelector('#confirm-modal-btn');
      confirm?.dispatchEvent(new Event('click'));
      expect(baseRepo.delete).toHaveBeenCalled();
    });
  }));

  it('set form value for a crud update action', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      const crudBtns = template.querySelectorAll('.crud-update-btn');
      const btn = crudBtns[Math.floor(Math.random() * crudBtns.length)];
      const id = btn.getAttribute('id')?.replace('crud-update-', '');
      const recipe = component.resources.find(i => i.id === id);
      spyOn(component, 'getResource').and.callThrough().and.returnValue(Promise.resolve({status: 200, body: recipe}));
      getAllSpy.and.callThrough().and.returnValue(Promise.resolve({body: ing}));
      btn.dispatchEvent(new Event('click'));
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        template = fixture.nativeElement;
        const modal = document.querySelector('app-modal');
        const form = modal?.querySelector('form');
        const name = form?.querySelector('#name') as HTMLInputElement;
        const price = form?.querySelector('#price') as HTMLInputElement;
        const recipePrice = recipe?.standardPrice? formatUtils.toDecimalString(recipe.standardPrice) : '';
        expect(name.value === recipe?.name).toBeTrue();
        expect(price.value === recipePrice).toBeTrue();
      });
    });
  }));

  it('set form value for a crud read action', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      template = fixture.nativeElement;
      const crudBtns = template.querySelectorAll('.crud-read-btn');
      const btn = crudBtns[Math.floor(Math.random() * crudBtns.length)];
      const id = btn.getAttribute('id')?.replace('crud-read-', '');
      const recipe = component.resources.find(i => i.id === id);
      spyOn(component, 'getResource').and.callThrough().and.returnValue(Promise.resolve({status: 200, body: recipe}));
      getAllSpy.and.callThrough().and.returnValue(Promise.resolve({body: ing}));
      btn.dispatchEvent(new Event('click'));
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        template = fixture.nativeElement;
        const modal = document.querySelector('app-modal');
        const form = modal?.querySelector('form');
        const name = form?.querySelector('#name') as HTMLInputElement;
        const price = form?.querySelector('#price') as HTMLInputElement;
        const recipePrice = recipe?.standardPrice? formatUtils.toDecimalString(recipe.standardPrice) : '';
        expect(name.value === recipe?.name).toBeTrue();
        expect(price.value === recipePrice).toBeTrue();
      });
    })
  }));
});
