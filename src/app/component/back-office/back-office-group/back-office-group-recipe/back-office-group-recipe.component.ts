import { Component } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { RecipeComponent } from 'src/app/component/form/resource/recipe/recipe.component';
import { Ingredient, Recipe } from 'src/app/models/apis/models';
import { ApiUrl } from 'src/app/models/enums/ApiUrl';
import { CRUD } from 'src/app/models/enums/CRUD';
import { RecipeTableMapper } from 'src/app/models/table/mappers/RecipeTableMapper/RecipeTableMapper';
import { RecipeTableModel } from 'src/app/models/table/resources/RecipeTableModel';
import { CrudConfig } from 'src/app/models/type/CrudConfig';
import { CrudTarget } from 'src/app/models/type/CrudTarget';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';
import { BackOfficeGroupComponent } from '../back-office-group.component';

/**
 * Target of the /group/recipe route
 */
@Component({
  selector: 'app-group-recipe',
  templateUrl: '../../back-office.component.html',
  styleUrls: ['../../back-office.component.scss']
})
export class BackOfficeGroupRecipeComponent extends BackOfficeGroupComponent {

  /**
   * crud api url
   */
  override apiUrl: ApiUrl = ApiUrl.RECIPE;

  override breakpoint: number = 650;

  /**
   * Content of the h1
   */
  override title: string = 'resource.recipe';

  /**
   * Resources displayed
   */
  override resources: Recipe[] = [];

  ings: Ingredient[] = [];

  /**
   * Mapped resources for display
   */
  override dataSource: RecipeTableModel[] = [];

  /**
   * CRUD actions allowed
   */
  override crudConfig: CrudConfig = [
    CRUD.CREATE,
    CRUD.READ,
    CRUD.UPDATE,
    CRUD.DELETE,
    CRUD.STANDARDISE
  ]

  /**
   * Map Table content from provided resources
   */
  override tableMapper: RecipeTableMapper = new RecipeTableMapper();

  /**
   * class constructor
   * @param translate allow internationalization
   * @param dialog material modals
   * @param baseRepo application repository
   * @param spinner allow to toggle on/off the application spinner
   */
   constructor(
    public translate: TranslateService,
    protected override dialog: MatDialog,
    protected override baseRepo: BaseRepositoryService,
    public override spinner: SpinnerService
  ) {
    super(dialog, baseRepo, spinner);
  }

  /**
   * Open the CRUD form modal handling Create, Read & Update actions
   * @param crudTarget contains information about the resource targeted by the crud action
   * @returns the modal reference
   */
  override openCRUmodal(crudTarget: CrudTarget): MatDialogRef<RecipeComponent> {
    return this.dialog.open(RecipeComponent, {data: crudTarget});
  }

  /**
   * Add the isStandard attribute to the resource
   * @param body the resource targeted
   * @returns the modified resource
   */
  override standardise(body: Recipe): Recipe{
    body.isStandard = true;
    return body;
  }

}
