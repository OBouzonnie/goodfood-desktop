import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';
import { BackOfficeComponent } from '../back-office.component';

/**
 * Back Office Group parent component
 */
@Component({
  selector: 'app-back-office-group',
  templateUrl: '../back-office.component.html',
  styleUrls: ['../back-office.component.scss']
})
export class BackOfficeGroupComponent extends BackOfficeComponent {

  /**
   * class constructor
   * @param dialog material modals
   * @param baseRepo application repository
   * @param spinner allow to toggle on/off the application spinner
   */
  constructor(
    protected override dialog: MatDialog,
    protected override baseRepo: BaseRepositoryService,
    public override spinner: SpinnerService
  ) {super(dialog, baseRepo, spinner);}

}
