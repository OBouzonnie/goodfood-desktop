import { Component } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { SupplierComponent } from 'src/app/component/form/resource/supplier/supplier.component';
import { Supplier } from 'src/app/models/apis/models';
import { ApiUrl } from 'src/app/models/enums/ApiUrl';
import { CRUD } from 'src/app/models/enums/CRUD';
import { SupplierTableMapper } from 'src/app/models/table/mappers/SupplierTableMapper/SupplierTableMapper';
import { SupplierTableModel } from 'src/app/models/table/resources/SupplierTableModel';
import { CrudConfig } from 'src/app/models/type/CrudConfig';
import { CrudTarget } from 'src/app/models/type/CrudTarget';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';
import { BackOfficeGroupComponent } from '../back-office-group.component';
/**
 * Target of the /group/supplier route
 */
@Component({
  selector: 'app-group-supplier',
  templateUrl: '../../back-office.component.html',
  styleUrls: ['../../back-office.component.scss']
})
export class BackOfficeGroupSupplierComponent extends BackOfficeGroupComponent {

  /**
   * crud api url
   */
  override apiUrl: ApiUrl = ApiUrl.SUPPLIER;

  /**
   * Resources displayed
   */
  override resources: Supplier[] = [];

  /**
   * Content of the h1
   */
  override title: string = 'resource.supplier';

  /**
   * Mapped resources for display
   */
  override dataSource: SupplierTableModel[] = [];

  /**
   * CRUD actions allowed
   */
  override crudConfig: CrudConfig = [
    CRUD.CREATE,
    CRUD.READ,
    CRUD.UPDATE,
    CRUD.DELETE,
    CRUD.STANDARDISE
  ]


  /**
   * Map Table content from provided resources
   */
  override tableMapper: SupplierTableMapper = new SupplierTableMapper();

  /**
   * class constructor
   * @param dialog material modals
   * @param baseRepo application repository
   * @param spinner allow to toggle on/off the application spinner
   */
  constructor(
    protected override dialog: MatDialog,
    protected override baseRepo: BaseRepositoryService,
    public override spinner: SpinnerService
  ) {super(dialog, baseRepo, spinner);}

  /**
   * Open the CRUD form modal handling Create, Read & Update actions
   * @param crudTarget contains information about the resource targeted by the crud action
   * @returns the modal reference
   */
  override openCRUmodal(crudTarget: CrudTarget): MatDialogRef<SupplierComponent> {
    return this.dialog.open(SupplierComponent, {data: crudTarget});
  }

  /**
   * Add the isStandard attribute to the resource
   * @param body the resource targeted
   * @returns the modified resource
   */
  override standardise(body: Supplier): Supplier{
    body.isStandard = true;
    return body;
  }

}
