import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import faker from '@faker-js/faker';
import { AppModule } from 'src/app/app.module';
import { BaseModel, Ingredient, Supplier } from 'src/app/models/apis/models';
import { MaterialModule } from 'src/app/modules/materials/material.module';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { MockService } from 'src/app/services/mock/mock.service';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';
import { BackOfficeGroupSupplierComponent } from './back-office-group-supplier.component';

describe('BackOfficeGroupSupplierComponent', () => {
  let component: BackOfficeGroupSupplierComponent;
  let fixture: ComponentFixture<BackOfficeGroupSupplierComponent>;
  let template: HTMLElement;
  let mockService: MockService;
  let resources: BaseModel[];
  let baseRepo: BaseRepositoryService;
  let ing: Ingredient[];
  let getAllSpy: jasmine.Spy;
  let uid: string;
  let resource: Supplier;
  let spinner: SpinnerService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule, SharedModule, MaterialModule],
      declarations: [ BackOfficeGroupSupplierComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    spinner = TestBed.inject(SpinnerService);
    spyOn(spinner, 'increaseCount');
    spyOn(spinner, 'decreaseCount');
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 300000;
    mockService = new MockService();
    ing = mockService.getIngredients();
    resources = mockService.getSuppliers();
    baseRepo = TestBed.inject(BaseRepositoryService);
    getAllSpy = spyOn(baseRepo, 'getAll');
    getAllSpy.and.callThrough().and.returnValue(Promise.resolve({body: resources}));
    fixture = TestBed.createComponent(BackOfficeGroupSupplierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    template = fixture.nativeElement;
    uid = faker.datatype.uuid();
    resource = resources[Math.floor(Math.random() * resources.length)];
    console.log(resource)
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  /**
   * Integration Testing - Mapping
   */
  it('#displayedColumns should be SupplierTableMapper columns', waitForAsync(() => {
    fixture.whenStable().then(() => {
      expect(component.displayedColumns).toContain('name');
      expect(component.displayedColumns).toContain('email');
      expect(component.displayedColumns).toContain('phone');
      expect(component.displayedColumns).toContain('uid');
    });
  }));

  it('#head should be SupplierTableMapper head', waitForAsync(() => {
    fixture.whenStable().then(() => {
      expect(component.head).toEqual({
        name: 'resource.supplier',
        email: 'table.email',
        phone:'table.phone',
        logo: '',
        uid: ''
      });
    });
  }));

  it('#dataSource should correctly map Suppliers Resources', waitForAsync(() => {
    fixture.whenStable().then(() => {
      component.dataSource.forEach((d, i) => {
        const e = component.resources[i].email;
        const p = component.resources[i].phone;
        expect(d.name).toBe(component.resources[i].name);
        expect(d.email).toBe(e ? e : '');
        expect(d.phone).toBe(p ? p : '');
        expect(d.uid).toBe(component.resources[i].id);
        expect(d.collapse).toBeTrue();
      });
    });
  }));

  it('should render all data', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      component.dataSource.forEach(s => {
        expect(template.textContent).toContain(s.email);
        expect(template.textContent).toContain(s.phone);
        expect(template.textContent).toContain(s.name);
      });
    });
  }));

  /**
   * Integration Testing - CRUD btn
   */
  it('form submission for a crud create action', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      getAllSpy.and.callThrough().and.returnValue(Promise.resolve({body: ing}));
      const btn = template.querySelector('#crud-create-btn');
      btn?.dispatchEvent(new Event('click'));
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        const modal = document.querySelector('app-modal');
        const form = modal?.querySelector('form');
        const confirm = modal?.querySelector('#confirm-modal-btn');
        const ingBtn = form?.querySelector('#ing-add-btn');
        ingBtn?.dispatchEvent(new Event('click'));
        const name = form?.querySelector('#name') as HTMLInputElement;
        const email = form?.querySelector('#email') as HTMLInputElement;
        const phone = form?.querySelector('#phone') as HTMLInputElement;
        let ingId = form?.querySelector('#ingId0') as HTMLElement;
        const ingPrice = form?.querySelector('#ingPrice0') as HTMLInputElement;
        ingId?.click();
        fixture.whenStable().then(() => {
          fixture.detectChanges();
          spyOn(component, 'postResource');
          const options = document.querySelectorAll('mat-option');
          const o = Math.floor(Math.random() * options.length);
          const option = options[o];
          option.dispatchEvent(new Event('click'));
          name.value = faker.name.lastName();
          name.dispatchEvent(new Event('input'));
          phone.value = Math.floor((Math.random() * 1000000000)).toString();
          phone.dispatchEvent(new Event('input'));
          email.value = faker.internet.email();
          email.dispatchEvent(new Event('input'));
          ingPrice.value = (Math.floor(Math.random() * 1000) / 100).toString();
          ingPrice?.dispatchEvent(new Event('input'));
          fixture.detectChanges();
          console.log(confirm)
          confirm?.dispatchEvent(new Event('click'));
          fixture.detectChanges();
          expect(component.postResource).toHaveBeenCalled();
        });
      })
    });
  }));

  it('form submission for a crud update action', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      const crudBtns = template.querySelectorAll('.crud-update-btn');
      const btn = crudBtns[Math.floor(Math.random() * crudBtns.length)];
      const id = btn.getAttribute('id')?.replace('crud-update-', '');
      const supplier = component.resources.find(i => i.id === id);
      spyOn(component, 'getResource').and.callThrough().and.returnValue(Promise.resolve({status: 200, body: supplier}));
      getAllSpy.and.callThrough().and.returnValue(Promise.resolve({body: ing}));
      btn.dispatchEvent(new Event('click'));
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        const modal = document.querySelector('app-modal');
        const form = modal?.querySelector('form');
        const confirm = modal?.querySelector('#confirm-modal-btn');
        const name = form?.querySelector('#name') as HTMLInputElement;
        const email = form?.querySelector('#email') as HTMLInputElement;
        const phone = form?.querySelector('#phone') as HTMLInputElement;
        let ingId = form?.querySelector('#ingId0') as HTMLElement;
        const ingPrice = form?.querySelector('#ingPrice0') as HTMLInputElement;
        ingId?.click();
        fixture.whenStable().then(() => {
          fixture.detectChanges();
          spyOn(component, 'putResource');
          const options = document.querySelectorAll('mat-option');
          const o = Math.floor(Math.random() * options.length);
          const option = options[o];
          option.dispatchEvent(new Event('click'));
          name.value = faker.name.lastName();
          name.dispatchEvent(new Event('input'));
          phone.value = Math.floor(Math.random() * 1000000000).toString();
          phone.dispatchEvent(new Event('input'));
          email.value = faker.internet.email();
          email.dispatchEvent(new Event('input'));
          ingPrice.value = (Math.floor(Math.random() * 1000) / 100).toString();
          ingPrice?.dispatchEvent(new Event('input'));
          fixture.detectChanges();
          confirm?.dispatchEvent(new Event('click'));
          fixture.detectChanges();
          expect(component.putResource).toHaveBeenCalled();
        });
      });
    });
  }));

  it('submission for a crud delete action', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      spyOn(component, 'deleteResource');
      const crudBtns = template.querySelectorAll('.crud-delete-btn');
      const btn = crudBtns[Math.floor(Math.random() * crudBtns.length)];
      btn.dispatchEvent(new Event('click'));
      const modal = document.querySelector('app-modal-action');
      const confirm = modal?.querySelector('#confirm-modal-btn');
      confirm?.dispatchEvent(new Event('click'));
      expect(component.deleteResource).toHaveBeenCalled();
    });
  }));

  it('set form value for a crud update action', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      const crudBtns = template.querySelectorAll('.crud-update-btn');
      const btn = crudBtns[Math.floor(Math.random() * crudBtns.length)];
      console.log('res', resource)
      spyOn(component, 'getResource').and.callThrough().and.returnValue(Promise.resolve({status: 200, body: resource}));
      getAllSpy.and.callThrough().and.returnValue(Promise.resolve({body: ing}));
      btn.dispatchEvent(new Event('click'));
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        template = fixture.nativeElement;
        const modal = document.querySelector('app-modal');
        const form = modal?.querySelector('form');
        const name = form?.querySelector('#name') as HTMLInputElement;
        const email = form?.querySelector('#email') as HTMLInputElement;
        const phone = form?.querySelector('#phone') as HTMLInputElement;
        expect(name.value == resource?.name).toBeTrue();
        expect(email.value == resource?.email).toBeTrue();
        expect(phone.value == resource?.phone).toBeTrue();
      });
    });
  }));

  it('set form value for a crud read action', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      template = fixture.nativeElement;
      const crudBtns = template.querySelectorAll('.crud-read-btn');
      const btn = crudBtns[Math.floor(Math.random() * crudBtns.length)];
      spyOn(component, 'getResource').and.callThrough().and.returnValue(Promise.resolve({status: 200, body: resource}));
      getAllSpy.and.callThrough().and.returnValue(Promise.resolve({body: ing}));
      btn.dispatchEvent(new Event('click'));
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        template = fixture.nativeElement;
        const modal = document.querySelector('app-modal');
        const form = modal?.querySelector('form');
        const name = form?.querySelector('#name') as HTMLInputElement;
        const email = form?.querySelector('#email') as HTMLInputElement;
        const phone = form?.querySelector('#phone') as HTMLInputElement;
        expect(name.value == resource?.name).toBeTrue();
        expect(email.value == resource?.email).toBeTrue();
        expect(phone.value == resource?.phone).toBeTrue();
      });
    })
  }));
});
