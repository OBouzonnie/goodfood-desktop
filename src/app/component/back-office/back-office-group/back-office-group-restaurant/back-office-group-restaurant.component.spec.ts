import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import faker from '@faker-js/faker';
import { AppModule } from 'src/app/app.module';
import { BaseModel } from 'src/app/models/apis/models';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { MockService } from 'src/app/services/mock/mock.service';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import testUtils from 'src/test/utils/test.utils';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';
import { BackOfficeGroupRestaurantComponent } from './back-office-group-restaurant.component';

describe('BackOfficeGroupRestaurantComponent', () => {
  let component: BackOfficeGroupRestaurantComponent;
  let fixture: ComponentFixture<BackOfficeGroupRestaurantComponent>;
  let template: HTMLElement;
  let uid: string;
  let mockService: MockService;
  let resources: BaseModel[];
  let baseRepo: BaseRepositoryService;
  let spinner: SpinnerService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule, SharedModule],
      declarations: [ BackOfficeGroupRestaurantComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    spinner = TestBed.inject(SpinnerService);
    spyOn(spinner, 'increaseCount');
    spyOn(spinner, 'decreaseCount');
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 300000;
    mockService = new MockService();
    resources = mockService.getRestaurants();
    baseRepo = TestBed.inject(BaseRepositoryService);
    spyOn(baseRepo, 'getAll').and.callThrough().and.returnValue(Promise.resolve({body: resources}));
    fixture = TestBed.createComponent(BackOfficeGroupRestaurantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    template = fixture.nativeElement;
    uid = faker.datatype.uuid();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  /**
   * Integration Testing - Mapping
   */
  it('#displayedColumns should be RestaurantTableMapper columns', waitForAsync(() => {
    fixture.whenStable().then(() => {
      expect(component.displayedColumns).toContain('name');
      expect(component.displayedColumns).toContain('email');
      expect(component.displayedColumns).toContain('phone');
      expect(component.displayedColumns).toContain('uid');
    });
  }));

  it('#head should be RestaurantTableMapper head', waitForAsync(() => {
    fixture.whenStable().then(() => {
      expect(component.head).toEqual({
        name: 'resource.restaurant',
        email: 'table.email',
        phone: 'table.phone',
        uid: ''
      });
    });
  }));

  it('#dataSource should correctly map Restaurants Resources', waitForAsync(() => {
    fixture.whenStable().then(() => {
      component.dataSource.forEach((d, i) => {
        const p = component.resources[i].phone;
        expect(d.name).toBe(component.resources[i].name);
        if (component.resources[i].phone) expect(d.phone).toBe(p ? p : '');
        expect(d.uid).toBe(component.resources[i].id);
        expect(d.collapse).toBeTrue();
      });
    });
  }));

  /**
   * Integration Testing - Rendering
   */
  it('should render all data', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      component.dataSource.forEach(d => {
        expect(template.textContent).toContain(d.name);
        expect(template.textContent).toContain(d.phone);
        expect(template.textContent).toContain(d.email);
      });
    });
  }));

  it('should create a crud buttons for all element', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      template = fixture.nativeElement;
      const btns = template.querySelectorAll('button');
      const id: string[] = [];
      btns.forEach(btn => {
        id.push(btn.id);
      })
      component.dataSource.forEach(d => {
        expect(id).toContain('crud-read-'+ d.uid);
        expect(id).toContain('crud-update-'+ d.uid);
        expect(id).toContain('crud-delete-'+ d.uid);
      });
    });
  }));

  /**
   * Integration Testing - CRUD btn
   */
  it('form submission for a crud create action', waitForAsync(() => {
    fixture.whenStable().then(() => {
      spyOn(component, 'postResource');
      const btn = template.querySelector('#crud-create-btn');
      btn?.dispatchEvent(new Event('click'));
      const modal = document.querySelector('app-modal');
      const form = modal?.querySelector('form');
      const confirm = modal?.querySelector('#confirm-modal-btn');
      const name = form?.querySelector('#name') as HTMLInputElement;
      const email = form?.querySelector('#email') as HTMLInputElement;
      const firstname = form?.querySelector('#firstname') as HTMLInputElement;
      const lastname = form?.querySelector('#lastname') as HTMLInputElement;
      const number = form?.querySelector('#number') as HTMLInputElement;
      const street = form?.querySelector('#street') as HTMLInputElement;
      const postal = form?.querySelector('#postal') as HTMLInputElement;
      const city = form?.querySelector('#city') as HTMLInputElement;
      const phone = form?.querySelector('#phone') as HTMLInputElement;
      const open0 = form?.querySelector('#open0') as HTMLInputElement;
      const open1 = form?.querySelector('#open1') as HTMLInputElement;
      const open2 = form?.querySelector('#open2') as HTMLInputElement;
      const open3 = form?.querySelector('#open3') as HTMLInputElement;
      const open4 = form?.querySelector('#open4') as HTMLInputElement;
      const open5 = form?.querySelector('#open5') as HTMLInputElement;
      const open6 = form?.querySelector('#open6') as HTMLInputElement;
      const close0 = form?.querySelector('#close0') as HTMLInputElement;
      const close1 = form?.querySelector('#close1') as HTMLInputElement;
      const close2 = form?.querySelector('#close2') as HTMLInputElement;
      const close3 = form?.querySelector('#close3') as HTMLInputElement;
      const close4 = form?.querySelector('#close4') as HTMLInputElement;
      const close5 = form?.querySelector('#close5') as HTMLInputElement;
      const close6 = form?.querySelector('#close6') as HTMLInputElement;
      name.value = faker.name.lastName();
      name.dispatchEvent(new Event('input'));
      email.value = faker.internet.email();
      email.dispatchEvent(new Event('input'));
      firstname.value = faker.name.firstName();
      firstname.dispatchEvent(new Event('input'));
      lastname.value = faker.name.lastName();
      lastname.dispatchEvent(new Event('input'));
      number.value = Math.floor(Math.random() * 100).toString();
      number.dispatchEvent(new Event('input'));
      street.value = faker.address.streetName();
      street.dispatchEvent(new Event('input'));
      postal.value = testUtils.randomStringifiedNumber(100000);
      postal.dispatchEvent(new Event('input'));
      city.value = faker.address.city();
      city.dispatchEvent(new Event('input'));
      phone.value = Math.floor(Math.random() * 10000000000).toString();
      phone.dispatchEvent(new Event('input'));
      open0.value = testUtils.randomTime();
      open0.dispatchEvent(new Event('input'));
      open1.value = testUtils.randomTime();
      open1.dispatchEvent(new Event('input'));
      open2.value = testUtils.randomTime();
      open2.dispatchEvent(new Event('input'));
      open3.value = testUtils.randomTime();
      open3.dispatchEvent(new Event('input'));
      open4.value = testUtils.randomTime();
      open4.dispatchEvent(new Event('input'));
      open5.value = testUtils.randomTime();
      open5.dispatchEvent(new Event('input'));
      open6.value = testUtils.randomTime();
      open6.dispatchEvent(new Event('input'));
      close0.value = testUtils.randomTime();
      close0.dispatchEvent(new Event('input'));
      close1.value = testUtils.randomTime();
      close1.dispatchEvent(new Event('input'));
      close2.value = testUtils.randomTime();
      close2.dispatchEvent(new Event('input'));
      close3.value = testUtils.randomTime();
      close3.dispatchEvent(new Event('input'));
      close4.value = testUtils.randomTime();
      close4.dispatchEvent(new Event('input'));
      close5.value = testUtils.randomTime();
      close5.dispatchEvent(new Event('input'));
      close6.value = testUtils.randomTime();
      close6.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      confirm?.dispatchEvent(new Event('click'));
      fixture.detectChanges();
      expect(component.postResource).toHaveBeenCalled();
    });
  }));

  it('form submission for a crud update action', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      template = fixture.nativeElement;
      spyOn(component, 'putResource');
      const crudBtns = template.querySelectorAll('.crud-update-btn');
      const btn = crudBtns[Math.floor(Math.random() * crudBtns.length)];
      const id = btn.getAttribute('id')?.replace('crud-update-', '');
      const restaurant = component.resources.find(i => i.id === id);
      spyOn(component, 'getResource').and.callThrough().and.returnValue(Promise.resolve({status: 200, body: restaurant}));
      btn.dispatchEvent(new Event('click'));
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        template = fixture.nativeElement;
        const modal = document.querySelector('app-modal');
        const form = modal?.querySelector('form');
        const confirm = modal?.querySelector('#confirm-modal-btn');
        const name = form?.querySelector('#name') as HTMLInputElement;
        const email = form?.querySelector('#email') as HTMLInputElement;
        const firstname = form?.querySelector('#firstname') as HTMLInputElement;
        const lastname = form?.querySelector('#lastname') as HTMLInputElement;
        const number = form?.querySelector('#number') as HTMLInputElement;
        const street = form?.querySelector('#street') as HTMLInputElement;
        const postal = form?.querySelector('#postal') as HTMLInputElement;
        const city = form?.querySelector('#city') as HTMLInputElement;
        const phone = form?.querySelector('#phone') as HTMLInputElement;
        const open0 = form?.querySelector('#open0') as HTMLInputElement;
        const open1 = form?.querySelector('#open1') as HTMLInputElement;
        const open2 = form?.querySelector('#open2') as HTMLInputElement;
        const open3 = form?.querySelector('#open3') as HTMLInputElement;
        const open4 = form?.querySelector('#open4') as HTMLInputElement;
        const open5 = form?.querySelector('#open5') as HTMLInputElement;
        const open6 = form?.querySelector('#open6') as HTMLInputElement;
        const close0 = form?.querySelector('#close0') as HTMLInputElement;
        const close1 = form?.querySelector('#close1') as HTMLInputElement;
        const close2 = form?.querySelector('#close2') as HTMLInputElement;
        const close3 = form?.querySelector('#close3') as HTMLInputElement;
        const close4 = form?.querySelector('#close4') as HTMLInputElement;
        const close5 = form?.querySelector('#close5') as HTMLInputElement;
        const close6 = form?.querySelector('#close6') as HTMLInputElement;
        name.value = faker.name.lastName();
        name.dispatchEvent(new Event('input'));
        email.value = faker.internet.email();
        email.dispatchEvent(new Event('input'));
        firstname.value = faker.name.firstName();
        firstname.dispatchEvent(new Event('input'));
        lastname.value = faker.name.lastName();
        lastname.dispatchEvent(new Event('input'));
        number.value = Math.floor(Math.random() * 100).toString();
        number.dispatchEvent(new Event('input'));
        street.value = faker.address.streetName();
        street.dispatchEvent(new Event('input'));
        postal.value = testUtils.randomStringifiedNumber(100000);
        postal.dispatchEvent(new Event('input'));
        city.value = faker.address.city();
        city.dispatchEvent(new Event('input'));
        phone.value = Math.floor(Math.random() * 10000000000).toString();
        phone.dispatchEvent(new Event('input'));
        open0.value = testUtils.randomTime();
        open0.dispatchEvent(new Event('input'));
        open1.value = testUtils.randomTime();
        open1.dispatchEvent(new Event('input'));
        open2.value = testUtils.randomTime();
        open2.dispatchEvent(new Event('input'));
        open3.value = testUtils.randomTime();
        open3.dispatchEvent(new Event('input'));
        open4.value = testUtils.randomTime();
        open4.dispatchEvent(new Event('input'));
        open5.value = testUtils.randomTime();
        open5.dispatchEvent(new Event('input'));
        open6.value = testUtils.randomTime();
        open6.dispatchEvent(new Event('input'));
        close0.value = testUtils.randomTime();
        close0.dispatchEvent(new Event('input'));
        close1.value = testUtils.randomTime();
        close1.dispatchEvent(new Event('input'));
        close2.value = testUtils.randomTime();
        close2.dispatchEvent(new Event('input'));
        close3.value = testUtils.randomTime();
        close3.dispatchEvent(new Event('input'));
        close4.value = testUtils.randomTime();
        close4.dispatchEvent(new Event('input'));
        close5.value = testUtils.randomTime();
        close5.dispatchEvent(new Event('input'));
        close6.value = testUtils.randomTime();
        close6.dispatchEvent(new Event('input'));
        fixture.detectChanges();
        confirm?.dispatchEvent(new Event('click'));
        fixture.detectChanges();
        expect(component.putResource).toHaveBeenCalled();
      })
    });
  }));

  it('submission for a crud delete action', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      template = fixture.nativeElement;
      spyOn(component, 'deleteResource');
      const crudBtns = template.querySelectorAll('.crud-delete-btn');
      const btn = crudBtns[Math.floor(Math.random() * crudBtns.length)];
      btn.dispatchEvent(new Event('click'));
      const modal = document.querySelector('app-modal-action');
      const confirm = modal?.querySelector('#confirm-modal-btn');
      confirm?.dispatchEvent(new Event('click'));
      expect(component.deleteResource).toHaveBeenCalled();
    })
  }));

  it('set form value for a crud update action', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      template = fixture.nativeElement;
      const crudBtns = template.querySelectorAll('.crud-update-btn');
      const btn = crudBtns[Math.floor(Math.random() * crudBtns.length)];
      const id = btn.getAttribute('id')?.replace('crud-update-', '');
      const restaurant = component.resources.find(i => i.id === id);
      spyOn(component, 'getResource').and.callThrough().and.returnValue(Promise.resolve({status: 200, body: restaurant}));
      btn.dispatchEvent(new Event('click'));
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        template = fixture.nativeElement;
        const modal = document.querySelector('app-modal');
        const form = modal?.querySelector('form');
        const name = form?.querySelector('#name') as HTMLInputElement;
        const firstname = form?.querySelector('#firstname') as HTMLInputElement;
        const lastname = form?.querySelector('#lastname') as HTMLInputElement;
        const email = form?.querySelector('#email') as HTMLInputElement;
        const phone = form?.querySelector('#phone') as HTMLInputElement;
        const number = form?.querySelector('#number') as HTMLInputElement;
        const street = form?.querySelector('#street') as HTMLInputElement;
        const postal = form?.querySelector('#postal') as HTMLInputElement;
        const city = form?.querySelector('#city') as HTMLInputElement;
        expect(name.value === restaurant?.name).toBeTrue();
        expect(firstname.value === restaurant?.owner?.firstName).toBeTrue();
        expect(lastname.value === restaurant?.owner?.lastName).toBeTrue();
        expect(email.value === restaurant?.owner?.email).toBeTrue();
        expect(phone.value === restaurant?.phone).toBeTrue();
        expect(number.value === restaurant?.address?.number).toBeTrue();
        expect(street.value === restaurant?.address?.street).toBeTrue();
        expect(postal.value === restaurant?.address?.postal).toBeTrue();
        expect(city.value === restaurant?.address?.city).toBeTrue();
      })
    });
  }));

  it('set form value for a crud read action', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      template = fixture.nativeElement;
      const crudBtns = template.querySelectorAll('.crud-read-btn');
      const btn = crudBtns[Math.floor(Math.random() * crudBtns.length)];
      const id = btn.getAttribute('id')?.replace('crud-read-', '');
      const restaurant = component.resources.find(i => i.id === id);
      spyOn(component, 'getResource').and.callThrough().and.returnValue(Promise.resolve({status: 200, body: restaurant}));
      btn.dispatchEvent(new Event('click'));
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        template = fixture.nativeElement;
        const modal = document.querySelector('app-modal');
        const form = modal?.querySelector('form');
        const name = form?.querySelector('#name') as HTMLInputElement;
        const firstname = form?.querySelector('#firstname') as HTMLInputElement;
        const lastname = form?.querySelector('#lastname') as HTMLInputElement;
        const email = form?.querySelector('#email') as HTMLInputElement;
        const phone = form?.querySelector('#phone') as HTMLInputElement;
        const number = form?.querySelector('#number') as HTMLInputElement;
        const street = form?.querySelector('#street') as HTMLInputElement;
        const postal = form?.querySelector('#postal') as HTMLInputElement;
        const city = form?.querySelector('#city') as HTMLInputElement;
        expect(name.value === restaurant?.name).toBeTrue();
        expect(firstname.value === restaurant?.owner?.firstName).toBeTrue();
        expect(lastname.value === restaurant?.owner?.lastName).toBeTrue();
        expect(email.value === restaurant?.owner?.email).toBeTrue();
        expect(phone.value === restaurant?.phone).toBeTrue();
        expect(number.value === restaurant?.address?.number).toBeTrue();
        expect(street.value === restaurant?.address?.street).toBeTrue();
        expect(postal.value === restaurant?.address?.postal).toBeTrue();
        expect(city.value === restaurant?.address?.city).toBeTrue();
      })
    });
  }));
});
