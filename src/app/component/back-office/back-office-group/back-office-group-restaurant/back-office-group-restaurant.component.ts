import { Component } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { RestaurantComponent } from 'src/app/component/form/resource/restaurant/restaurant.component';
import { Restaurant } from 'src/app/models/apis/models';
import { ApiUrl } from 'src/app/models/enums/ApiUrl';
import { RestaurantTableMapper } from 'src/app/models/table/mappers/RestaurantTableMapper/RestaurantTableMapper';
import { RestaurantTableModel } from 'src/app/models/table/resources/RestaurantTableModel';
import { CrudTarget } from 'src/app/models/type/CrudTarget';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';
import { BackOfficeGroupComponent } from '../back-office-group.component';

/**
 * Target of the /group/restaurant route
 */
@Component({
  selector: 'app-group-restaurant',
  templateUrl: '../../back-office.component.html',
  styleUrls: ['../../back-office.component.scss']
})
export class BackOfficeGroupRestaurantComponent extends BackOfficeGroupComponent {

  /**
   * crud api url
   */
  override apiUrl: ApiUrl = ApiUrl.RESTAURANT;

  /**
   * Content of the h1
   */
  override title: string = 'resource.restaurant';

  /**
   * Resources displayed
   */
  override resources: Restaurant[] = [];

  /**
   * Mapped resources for display
   */
  override dataSource: RestaurantTableModel[] = [];

  /**
   * Map Table content from provided resources
   */
  override tableMapper: RestaurantTableMapper = new RestaurantTableMapper();

  /**
   * class constructor
   * @param translate allow internationalization
   * @param dialog material modals
   * @param baseRepo application repository
   * @param spinner allow to toggle on/off the application spinner
   */
  constructor(
    public translate: TranslateService,
    public override dialog: MatDialog,
    protected override baseRepo: BaseRepositoryService,
    public override spinner: SpinnerService
  ) {
    super(dialog, baseRepo, spinner);
  }

  /**
   * Open the CRUD form modal handling Create, Read & Update actions
   * @param crudTarget contains information about the resource targeted by the crud action
   * @returns the modal reference
   */
  override openCRUmodal(crudTarget: CrudTarget): MatDialogRef<RestaurantComponent> {
    return this.dialog.open(RestaurantComponent, {data: crudTarget});
  }

}
