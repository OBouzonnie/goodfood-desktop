import { Component } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { IngredientComponent } from 'src/app/component/form/resource/ingredient/ingredient.component';
import { Ingredient } from 'src/app/models/apis/models';
import { ApiUrl } from 'src/app/models/enums/ApiUrl';
import { CRUD } from 'src/app/models/enums/CRUD';
import { IngredientTableMapper } from 'src/app/models/table/mappers/IngredientTableMapper/IngredientTableMapper';
import { IngredientTableModel } from 'src/app/models/table/resources/IngredientTableModel';
import { CrudConfig } from 'src/app/models/type/CrudConfig';
import { CrudTarget } from 'src/app/models/type/CrudTarget';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';
import { BackOfficeGroupComponent } from '../back-office-group.component';

/**
 * Target of the /group/ingredient route
 */
@Component({
  selector: 'app-group-ingredient',
  templateUrl: '../../back-office.component.html',
  styleUrls: ['../../back-office.component.scss']
})
export class BackOfficeGroupIngredientComponent extends BackOfficeGroupComponent  {

  /**
   * crud api url
   */
  override apiUrl: ApiUrl = ApiUrl.INGREDIENT;

  /**
   * Content of the h1
   */
  override title: string = 'resource.ingredients';

  /**
   * Resources displayed
   */
  override resources: Ingredient[] = [];

  /**
   * Mapped resources for display
   */
  override dataSource: IngredientTableModel[] = [];

  /**
   * CRUD actions allowed
   */
  override crudConfig: CrudConfig = [
    CRUD.CREATE,
    CRUD.UPDATE,
    CRUD.DELETE
  ]

  /**
   * Map Table content from provided resources
   */
  override tableMapper: IngredientTableMapper = new IngredientTableMapper();

  /**
   * class constructor
   * @param translate allow internationalization
   * @param dialog material modals
   * @param baseRepo application repository
   * @param spinner allow to toggle on/off the application spinner
   */
   constructor(
    public translate: TranslateService,
    protected override dialog: MatDialog,
    protected override baseRepo: BaseRepositoryService,
    public override spinner: SpinnerService
  ) {
    super(dialog, baseRepo, spinner);
  }

  /**
   * Open the CRUD form modal handling Create, Read & Update actions
   * @param crudTarget contains information about the resource targeted by the crud action
   * @returns the modal reference
   */
  override openCRUmodal(crudTarget: CrudTarget): MatDialogRef<IngredientComponent> {
    return this.dialog.open(IngredientComponent, {data: crudTarget});
  }

}
