import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import faker from '@faker-js/faker';
import { AppModule } from 'src/app/app.module';
import { BaseModel } from 'src/app/models/apis/models';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { MockService } from 'src/app/services/mock/mock.service';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';
import { BackOfficeGroupIngredientComponent } from './back-office-group-ingredient.component';

describe('BackOfficeGroupIngredientComponent', () => {
  let component: BackOfficeGroupIngredientComponent;
  let fixture: ComponentFixture<BackOfficeGroupIngredientComponent>;
  let template: HTMLElement;
  let uid: string;
  let mockService: MockService;
  let resources: BaseModel[];
  let baseRepo: BaseRepositoryService;
  let spinner: SpinnerService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule, SharedModule],
      declarations: [ BackOfficeGroupIngredientComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    spinner = TestBed.inject(SpinnerService);
    spyOn(spinner, 'increaseCount');
    spyOn(spinner, 'decreaseCount');
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 300000;
    mockService = new MockService();
    resources = mockService.getIngredients();
    baseRepo = TestBed.inject(BaseRepositoryService);
    spyOn(baseRepo, 'getAll').and.callThrough().and.returnValue(Promise.resolve({body: resources}));
    fixture = TestBed.createComponent(BackOfficeGroupIngredientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    template = fixture.nativeElement;
    uid = faker.datatype.uuid();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  /**
   * Integration Testing - Mapping
   */
  it('#displayedColumns should be IngredientTableMapper columns', waitForAsync(() => {
    fixture.whenStable().then(() => {
      expect(component.displayedColumns).toContain('name');
      expect(component.displayedColumns).toContain('allergens');
      expect(component.displayedColumns).toContain('uid');
    });
  }));

  it('#head should be IngredientTableMapper head', waitForAsync(() => {
    fixture.whenStable().then(() => {
      expect(component.head).toEqual({
        name: 'resource.ingredients',
        allergens: 'table.allergens',
        uid: ''
      });
    });
  }));

  it('#dataSource should correctly map ingredients Resources', waitForAsync(() => {
    fixture.whenStable().then(() => {
      component.dataSource.forEach((d, i) => {
        expect(d.name).toBe(component.resources[i].name);
        expect(d.uid).toBe(component.resources[i].id);
        expect(d.collapse).toBeTrue();
      })
    });
  }));

  /**
   * Integration Testing - Rendering
   */
  it('should render all data', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      component.dataSource.forEach(d => {
        expect(template.textContent).toContain(d.name);
      });
    });
  }));

  /**
   * Integration Testing - CRUD btn
   */
  it('form submission for a crud create action', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      spyOn(component, 'postResource');
      const btn = template.querySelector('#crud-create-btn');
      btn?.dispatchEvent(new Event('click'));
      const modal = document.querySelector('app-modal');
      const form = modal?.querySelector('form');
      const confirm = modal?.querySelector('#confirm-modal-btn');
      const ingBtn = form?.querySelector('#ing-add-btn');
      ingBtn?.dispatchEvent(new Event('click'));
      const name = form?.querySelector('#name') as HTMLInputElement;
      let allergen = form?.querySelector('#allergen0') as HTMLInputElement;
      name.value = faker.name.lastName();
      name.dispatchEvent(new Event('input'));
      allergen.value = faker.name.lastName();
      allergen.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      confirm?.dispatchEvent(new Event('click'));
      fixture.detectChanges();
      expect(component.postResource).toHaveBeenCalled();
    });
  }));

  it('form submission for a crud update action', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      const crudBtns = template.querySelectorAll('.crud-update-btn');
      const btn = crudBtns[Math.floor(Math.random() * crudBtns.length)];
      const id = btn.getAttribute('id')?.replace('crud-update-', '');
      const ingredient = component.resources.find(i => i.id === id);
      spyOn(component, 'getResource').and.callThrough().and.returnValue(Promise.resolve({status: 200, body: ingredient}));
      btn.dispatchEvent(new Event('click'));
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        const modal = document.querySelector('app-modal');
        const form = modal?.querySelector('form');
        const confirm = modal?.querySelector('#confirm-modal-btn');
        const name = form?.querySelector('#name') as HTMLInputElement;
        name.value = faker.name.lastName();
        name.dispatchEvent(new Event('input'));
        fixture.detectChanges();
        spyOn(component, 'putResource');
        confirm?.dispatchEvent(new Event('click'));
        fixture.detectChanges();
        expect(component.putResource).toHaveBeenCalled();
      });
    });
  }));

  it('submission for a crud delete action', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      spyOn(component, 'deleteResource');
      const crudBtns = template.querySelectorAll('.crud-delete-btn');
      const btn = crudBtns[Math.floor(Math.random() * crudBtns.length)];
      btn.dispatchEvent(new Event('click'));
      const modal = document.querySelector('app-modal-action');
      const confirm = modal?.querySelector('#confirm-modal-btn');
      confirm?.dispatchEvent(new Event('click'));
      expect(component.deleteResource).toHaveBeenCalled();
    });
  }));

  it('set form value for a crud update action', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      const crudBtns = template.querySelectorAll('.crud-update-btn');
      const btn = crudBtns[Math.floor(Math.random() * crudBtns.length)];
      const id = btn.getAttribute('id')?.replace('crud-update-', '');
      const ingredient = component.resources.find(i => i.id === id);
      spyOn(component, 'getResource').and.callThrough().and.returnValue(Promise.resolve({status: 200, body: ingredient}));
      btn.dispatchEvent(new Event('click'));
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        template = fixture.nativeElement;
        const modal = document.querySelector('app-modal');
        const form = modal?.querySelector('form');
        const name = form?.querySelector('#name') as HTMLInputElement;
        expect(name.value === ingredient?.name).toBeTrue();
      });
    });
  }));
});
