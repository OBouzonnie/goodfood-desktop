import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import faker from '@faker-js/faker';
import * as moment from 'moment';
import { AppModule } from 'src/app/app.module';
import { BaseModel } from 'src/app/models/apis/models';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { MockService } from 'src/app/services/mock/mock.service';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import { BackOfficeGroupDiscountComponent } from './back-office-group-discount.component';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';

describe('BackOfficeGroupDiscountComponent', () => {
  let component: BackOfficeGroupDiscountComponent;
  let fixture: ComponentFixture<BackOfficeGroupDiscountComponent>;
  let template: HTMLElement;
  let uid: string;
  let baseRepo: BaseRepositoryService;
  let getAllSpy: jasmine.Spy;
  let mockService: MockService;
  let resources: BaseModel[];
  let spinner: SpinnerService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule, SharedModule],
      declarations: [ BackOfficeGroupDiscountComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    spinner = TestBed.inject(SpinnerService);
    spyOn(spinner, 'increaseCount');
    spyOn(spinner, 'decreaseCount');
    mockService = new MockService();
    resources = mockService.getDiscounts();
    baseRepo = TestBed.inject(BaseRepositoryService);
    getAllSpy = spyOn(baseRepo, 'getAll').and.callThrough().and.returnValue(Promise.resolve({body: resources}));
    fixture = TestBed.createComponent(BackOfficeGroupDiscountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    template = fixture.nativeElement;
    uid = faker.datatype.uuid();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  /**
   * Integration Testing - Mapping
   */
  it('#displayedColumns should be DiscountTableMapper columns', waitForAsync(() => {
    fixture.whenStable().then(() => {
      expect(component.displayedColumns).toContain('target');
      expect(component.displayedColumns).toContain('start');
      expect(component.displayedColumns).toContain('end');
      expect(component.displayedColumns).toContain('value');
      expect(component.displayedColumns).toContain('uid');
    });
  }));

  it('#head should be DiscountTableMapper head', waitForAsync(() => {
    fixture.whenStable().then(() => {
      expect(component.head).toEqual({
        target: 'resource.recipe',
        start: 'table.start',
        end: 'table.end',
        value: 'table.value',
        uid: ''
      });
    });
  }));

  it('#dataSource should correctly map Suppliers Resources', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      component.dataSource.forEach((d, i) => {
        const v = component.resources[i].value?.toString();
        const e = component.resources[i].end;
        const s = component.resources[i].start;
        const t = component.resources[i].recipeName;
        console.log(d, component.resources[i])
        expect(d.value).toBe(v ? v : '');
        expect(d.end).toBe(e ? e : '');
        expect(d.start).toBe(s ? s : '');
        expect(d.target).toBe(t ? t : '--');
        expect(d.uid).toBe(component.resources[i].id);
        expect(d.collapse).toBeTrue();
      });
    });
  }));

  it('should render all data', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      component.dataSource.forEach(d => {
        expect(template.textContent).toContain(d.value);
        expect(template.textContent).toContain(d.start);
        expect(template.textContent).toContain(d.end);
        expect(template.textContent).toContain(d.target);
      });
    });
  }));

  it('form submission for a crud create action', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      const btn = template.querySelector('#crud-create-btn');
      btn?.dispatchEvent(new Event('click'));
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        const modal = document.querySelector('app-modal');
        const form = modal?.querySelector('form');
        const confirm = modal?.querySelector('#confirm-modal-btn');
        const value = form?.querySelector('#value') as HTMLInputElement;
        const end = form?.querySelector('#end') as HTMLInputElement;
        const start = form?.querySelector('#start') as HTMLInputElement;
        let recipe = form?.querySelector('#recipe') as HTMLElement;
        recipe?.click();
        fixture.whenStable().then(() => {
          fixture.detectChanges();
          spyOn(component, 'postResource');
          value.value = Math.floor(Math.random() * 100).toString();
          value.dispatchEvent(new Event('input'));
          end.value = moment().format('MM/DD/YYYY');
          end.dispatchEvent(new Event('input'));
          start.value = moment().format('MM/DD/YYYY');
          start.dispatchEvent(new Event('input'));
          fixture.detectChanges();
          confirm?.dispatchEvent(new Event('click'));
          fixture.detectChanges();
          expect(component.postResource).toHaveBeenCalled();
        });
      })
    });
  }));
});
