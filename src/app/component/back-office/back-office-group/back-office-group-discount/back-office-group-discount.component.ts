import { Component } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DiscountComponent } from 'src/app/component/form/resource/discount/discount.component';
import { Discount } from 'src/app/models/apis/models/discount';
import { ApiUrl } from 'src/app/models/enums/ApiUrl';
import { CRUD } from 'src/app/models/enums/CRUD';
import { DiscountTableMapper } from 'src/app/models/table/mappers/DiscountTableMapper/DiscountTableMapper';
import { DiscountTableModel } from 'src/app/models/table/resources/DiscountTableModel';
import { CrudConfig } from 'src/app/models/type/CrudConfig';
import { CrudTarget } from 'src/app/models/type/CrudTarget';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';
import { BackOfficeGroupComponent } from '../back-office-group.component';

/**
 * Target of the /group/discount route
 */
@Component({
  selector: 'app-group-discount',
  templateUrl: '../../back-office.component.html',
  styleUrls: ['../../back-office.component.scss']
})
export class BackOfficeGroupDiscountComponent extends BackOfficeGroupComponent {

  /**
   * crud api url
   */
  override apiUrl: ApiUrl = ApiUrl.DISCOUNT;

  /**
   * Resources displayed
   */
  override resources: Discount[] = [];

  /**
   * Content of the h1
   */
  override title: string = 'resource.discount';

  /**
   * Mapped resources for display
   */
  override dataSource: DiscountTableModel[] = [];

  /**
   * CRUD actions allowed
   */
  override crudConfig: CrudConfig = [CRUD.CREATE];

  /**
   * Map Table content from provided resources
   */
  override tableMapper: DiscountTableMapper = new DiscountTableMapper();

  /**
   * class constructor
   * @param dialog material modals
   * @param baseRepo application repository
   * @param spinner allow to toggle on/off the application spinner
   */
  constructor(
    protected override dialog: MatDialog,
    protected override baseRepo: BaseRepositoryService,
    public override spinner: SpinnerService
  ) {super(dialog, baseRepo, spinner);}

  /**
   * Open the CRUD form modal handling Create, Read & Update actions
   * @param crudTarget contains information about the resource targeted by the crud action
   * @returns the modal reference
   */
  override openCRUmodal(crudTarget: CrudTarget): MatDialogRef<DiscountComponent> {
    return this.dialog.open(DiscountComponent, {data: crudTarget});
  }


}
