import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppModule } from 'src/app/app.module';
import { BackOfficeGroupAdminComponent } from './back-office-group-admin.component';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';

describe('BackOfficeGroupAdminComponent', () => {
  let component: BackOfficeGroupAdminComponent;
  let fixture: ComponentFixture<BackOfficeGroupAdminComponent>;
  let spinner: SpinnerService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule],
      declarations: [ BackOfficeGroupAdminComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    spinner = TestBed.inject(SpinnerService);
    spyOn(spinner, 'increaseCount');
    spyOn(spinner, 'decreaseCount');
    fixture = TestBed.createComponent(BackOfficeGroupAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
