import { Component } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { AdminComponent } from 'src/app/component/form/resource/admin/admin.component';
import { Admin } from 'src/app/models/apis/models';
import { ApiUrl } from 'src/app/models/enums/ApiUrl';
import { CRUD } from 'src/app/models/enums/CRUD';
import { AdminTableMapper } from 'src/app/models/table/mappers/AdminTableMapper/AdminTableMapper';
import { CrudConfig } from 'src/app/models/type/CrudConfig';
import { CrudTarget } from 'src/app/models/type/CrudTarget';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';
import { BackOfficeGroupComponent } from '../back-office-group.component';

/**
 * Target of the /group/admin route
 */
@Component({
  selector: 'app-back-office-group-admin',
  templateUrl: '../../back-office.component.html',
  styleUrls: ['../../back-office.component.scss']
})
export class BackOfficeGroupAdminComponent extends BackOfficeGroupComponent {

  /**
   * crud api url
   */
  override apiUrl: ApiUrl = ApiUrl.ADMIN;

  /**
   * Resources displayed
   */
  override resources: Admin[] = [];

  /**
   * Map Table content from provided resources
   */
  override tableMapper: AdminTableMapper = new AdminTableMapper();

  /**
   * Content of the h1
   */
  override title: string = 'resource.admin';

  /**
   * CRUD actions allowed
   */
  override crudConfig: CrudConfig = [
    CRUD.CREATE,
    CRUD.DELETE
  ];

  /**
   * class constructor
   * @param dialog material modals
   * @param baseRepo application repository
   * @param spinner allow to toggle on/off the application spinner
   */
   constructor(
    protected override dialog: MatDialog,
    protected override baseRepo: BaseRepositoryService,
    public override spinner: SpinnerService
  ) {super(dialog, baseRepo, spinner);}

  /**
   * Open the CRUD form modal handling Create, Read & Update actions
   * @param crudTarget contains information about the resource targeted by the crud action
   * @returns the modal reference
   */
  override openCRUmodal(crudTarget: CrudTarget): MatDialogRef<AdminComponent> {
    return this.dialog.open(AdminComponent, {data: crudTarget});
  }

}
