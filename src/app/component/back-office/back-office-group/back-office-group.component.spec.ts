import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppModule } from 'src/app/app.module';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';
import { BackOfficeGroupComponent } from './back-office-group.component';

describe('BackOfficeGroupComponent', () => {
  let component: BackOfficeGroupComponent;
  let fixture: ComponentFixture<BackOfficeGroupComponent>;
  let spinner: SpinnerService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule, SharedModule],
      declarations: [ BackOfficeGroupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    spinner = TestBed.inject(SpinnerService);
    spyOn(spinner, 'increaseCount');
    spyOn(spinner, 'decreaseCount');
    fixture = TestBed.createComponent(BackOfficeGroupComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
