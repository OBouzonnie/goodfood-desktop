import { Component } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { RestaurantDetailComponent } from 'src/app/component/dialog/restaurant-detail/restaurant-detail.component';
import { Restaurant } from 'src/app/models/apis/models';
import { ApiUrl } from 'src/app/models/enums/ApiUrl';
import { CRUD } from 'src/app/models/enums/CRUD';
import { RestaurantEvalTableMapper } from 'src/app/models/table/mappers/RestaurantEvalTableMapper/RestaurantEvalTableMapper';
import { CrudConfig } from 'src/app/models/type/CrudConfig';
import { CrudTarget } from 'src/app/models/type/CrudTarget';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';
import { BackOfficeGroupComponent } from '../../back-office-group/back-office-group.component';

/**
 * Back Office Community only component
 */
@Component({
  selector: 'app-back-office-community-restaurant',
  templateUrl: '../../back-office.component.html',
  styleUrls: ['../../back-office.component.scss']
})
export class BackOfficeCommunityRestaurantComponent extends BackOfficeGroupComponent {

  override title = 'resource.feedbacks'

  /**
   * crud api url
   */
  override apiUrl: ApiUrl = ApiUrl.RESTAURANT;

  /**
   * CRUD actions allowed
   */
  override crudConfig: CrudConfig = [
    CRUD.READ
  ]

  /**
   * Resources displayed
   */
  override resources: Restaurant[] = [];

  /**
   * Map Table content from provided resources
   */
   override tableMapper: RestaurantEvalTableMapper = new RestaurantEvalTableMapper();

  /**
   * class constructor
   * @param translate allow internationalization
   * @param dialog material modals
   * @param baseRepo application repository
   * @param spinner allow to toggle on/off the application spinner
   */
  constructor(
    public translate: TranslateService,
    public override dialog: MatDialog,
    protected override baseRepo: BaseRepositoryService,
    public override spinner: SpinnerService
  ) {
    super(dialog, baseRepo, spinner);
  }

  /**
   * Open the modal displaying the franchise informations
   * @param data the franchise data
   * @returns the modal ref
   */
  openModal(data: Restaurant): MatDialogRef<RestaurantDetailComponent> {
    return this.dialog.open(RestaurantDetailComponent, {data: data});
  }

  /**
   * CRUD Read method
   */
  override crudRead(crudTarget: CrudTarget) {
    this.getResource(this.apiUrl, crudTarget.uid).then(res => {
      if (res.status === 200) crudTarget.resource = res.body;
      if (crudTarget.resource) this.openModal(crudTarget.resource);
    });
  }

  /**
   * Call an api url in order to fetch all resources
   * @param url the url to call
   */
  override getAllResources(url: string){
    console.log(`GET ${url}`);
    this.spinner.increaseCount();
    this.baseRepo.getAll(url).then(res => {
      this.resources = res.body;
      if(this.resources) {
        this.resources = this.resources.filter(r => r.feedbacks && r.feedbacks.length > 0)
        this.tableMapper.addMultipleRows(this.resources);
      }
      this.renderTable();
      this.spinner.decreaseCount();
    })
    .catch(err => {
      console.error(err);
      this.spinner.decreaseCount();
    });
  }

}
