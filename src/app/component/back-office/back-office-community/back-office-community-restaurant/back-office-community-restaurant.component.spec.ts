import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppModule } from 'src/app/app.module';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';
import { BackOfficeCommunityRestaurantComponent } from './back-office-community-restaurant.component';

describe('BackOfficeCommunityRestaurantComponent', () => {
  let component: BackOfficeCommunityRestaurantComponent;
  let fixture: ComponentFixture<BackOfficeCommunityRestaurantComponent>;
  let spinner: SpinnerService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule],
      declarations: [ BackOfficeCommunityRestaurantComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    spinner = TestBed.inject(SpinnerService);
    spyOn(spinner, 'increaseCount');
    spyOn(spinner, 'decreaseCount');
    fixture = TestBed.createComponent(BackOfficeCommunityRestaurantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
