import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import faker from '@faker-js/faker';
import { AppModule } from 'src/app/app.module';
import { CRUD } from 'src/app/models/enums/CRUD';
import { BaseModel } from 'src/app/models/apis/models';
import { CrudTarget } from 'src/app/models/type/CrudTarget';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { MockService } from 'src/app/services/mock/mock.service';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';

import { BackOfficeComponent } from './back-office.component';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';

describe('BackOfficeComponent', () => {
  let component: BackOfficeComponent;
  let fixture: ComponentFixture<BackOfficeComponent>;
  let template: HTMLElement;
  let uid: string;
  let mockService: MockService;
  let resources: BaseModel[];
  let baseRepo: BaseRepositoryService;
  let spinner: SpinnerService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule, SharedModule],
      declarations: [ BackOfficeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 300000;
    const resourceType = Math.floor(Math.random() * 3);
    baseRepo = TestBed.inject(BaseRepositoryService);
    spinner = TestBed.inject(SpinnerService);
    spyOn(spinner, 'increaseCount');
    spyOn(spinner, 'decreaseCount');
    mockService = new MockService();
    if (resourceType === 0) resources = mockService.getRestaurants();
    if (resourceType === 1) resources = mockService.getRecipes();
    if (resourceType === 2) resources = mockService.getIngredients();
    spyOn(baseRepo, 'getAll').and.callThrough().and.returnValue(Promise.resolve({body : resources}));
    spyOn(baseRepo, 'get').and.callThrough().and.returnValue(Promise.resolve({}));
    fixture = TestBed.createComponent(BackOfficeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    template = fixture.nativeElement;
    uid = faker.datatype.uuid();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  /**
   * Methods
   */
  it('#crudAction should call crudRead with a CRUD.READ action', () => {
    spyOn(component, 'crudRead');
    component.crudAction({action: CRUD.READ, uid: uid});
    expect(component.crudRead).toHaveBeenCalledWith({action: CRUD.READ, uid: uid});
  });

  it('#crudAction should call crudDelete with a CRUD.DELETE action', () => {
    spyOn(component, 'crudDelete');
    component.crudAction({action: CRUD.DELETE, uid: uid});
    expect(component.crudDelete).toHaveBeenCalledWith({action: CRUD.DELETE, uid: uid});
  });

  it('#crudAction should call crudUpdate with a CRUD.UPDATE action', () => {
    spyOn(component, 'crudUpdate');
    component.crudAction({action: CRUD.UPDATE, uid: uid});
    expect(component.crudUpdate).toHaveBeenCalledWith({action: CRUD.UPDATE, uid: uid});
  });

  /**
   * Rendering
   */
  it('should render app-back-office-page-content', () => {
    const element = template.querySelector('h1')
    expect(element).toBeTruthy();
  });

  it('should render app-table for large screen', () => {
    component.innerWidth = 2000;
    fixture.detectChanges();
    template = fixture.nativeElement;
    const element = template.querySelector('app-table');
    const altElement = template.querySelector('app-accordion');
    const innerElement = template.querySelector('table');
    const innerAltElement = template.querySelector('mat-accordion');
    expect(element).toBeTruthy();
    expect(altElement).toBeFalsy();
    expect(innerElement).toBeTruthy();
    expect(innerAltElement).toBeFalsy();
  });

  it('should render app-accordion for small screen', () => {
    component.innerWidth = 320;
    fixture.detectChanges();
    template = fixture.nativeElement;
    const element = template.querySelector('app-accordion');
    const altElement = template.querySelector('app-table');
    const innerElement = template.querySelector('mat-accordion');
    const innerAltElement = template.querySelector('table');
    expect(element).toBeTruthy();
    expect(altElement).toBeFalsy();
    expect(innerElement).toBeTruthy();
    expect(innerAltElement).toBeFalsy();
  });

  /**
   * Integration Testing - Delete Modal by methods
   */

  it('#crudDelete should pop delete modal', waitForAsync(() => {
    const crudTarget: CrudTarget = {action: CRUD.DELETE, uid: uid};
    component.crudDelete(crudTarget);
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      template = fixture.nativeElement;
      const modal = document.querySelector('app-modal');
      expect(modal).toBeTruthy();
      const btns = modal?.querySelectorAll('button');
      expect(btns?.length).toBe(2);
    })
  }));

  /**
   * Integration Testing - Create Modal by methods
   */

   it('#crudCreate should pop modal', waitForAsync(() => {
    component.crudCreate();
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      template = fixture.nativeElement;
      const modal = document.querySelector('app-modal');
      expect(modal).toBeTruthy();
      const btns = modal?.querySelectorAll('button');
      expect(btns?.length).toBe(2);
    })
  }));

  /**
   * Integration Testing - Update Modal by methods
   */

   it('#crudUpdate should pop modal', waitForAsync(() => {
    const crudTarget: CrudTarget = {action: CRUD.UPDATE, uid: uid};
    component.crudUpdate(crudTarget);
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      template = fixture.nativeElement;
      const modal = document.querySelector('app-modal');
      expect(modal).toBeTruthy();
      const btns = modal?.querySelectorAll('button');
      expect(btns?.length).toBe(2);
    })
  }));

  /**
   * Integration Testing - Read Modal by methods
   */

   it('#crudRead should pop modal', waitForAsync(() => {
    const crudTarget: CrudTarget = {action: CRUD.READ, uid: uid};
    component.crudRead(crudTarget);
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      template = fixture.nativeElement;
      const modal = document.querySelector('app-modal');
      expect(modal).toBeTruthy();
      const btns = modal?.querySelectorAll('button');
      expect(btns?.length).toBe(1);
    })
  }));
});
