import { Component } from '@angular/core';
import { Logo } from 'src/app/models/type/Logo';

/**
 * Component displaying a big GOOD FOOD logo as a home page for all back offices modules
 */
@Component({
  selector: 'app-back-office-home',
  templateUrl: './back-office-home.component.html',
  styleUrls: ['./back-office-home.component.scss']
})
export class BackOfficeHomeComponent {

  /**
   * the GOOD FOOD img attributes
   */
  logo: Logo = {
    url:'assets/logo/logo-good_food-primary.svg',
    alt:'good food logo'
  }

  constructor() { }

}
