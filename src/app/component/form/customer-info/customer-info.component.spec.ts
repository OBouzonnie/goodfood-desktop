import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TranslateModule } from '@ngx-translate/core';
import * as moment from 'moment';
import { AppModule } from 'src/app/app.module';
import { CustomerInformation } from 'src/app/models/apis/models';
import { MockService } from 'src/app/services/mock/mock.service';

import { CustomerInfoComponent } from './customer-info.component';

describe('CustomerInfoComponent', () => {
  let component: CustomerInfoComponent;
  let fixture: ComponentFixture<CustomerInfoComponent>;
  let template: HTMLElement;
  let mockService: MockService;
  let info : CustomerInformation

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule, TranslateModule],
      declarations: [ CustomerInfoComponent ],
      providers: [
        FormBuilder,
        { provide: MAT_DIALOG_DATA, useValue: {} }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    template = fixture.nativeElement;
    mockService = new MockService();
    info = mockService.getCustomerInfo();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  /**
   * Rendering
   */
   it('should render 4 inputs by default', () => {
    const form = template.querySelector('form');
    const inputs = form?.querySelectorAll('input');
    expect(inputs).toBeTruthy();
    expect(inputs?.length).toBe(4);
  });

  it('inputs should be empty for no data', () => {
    const form = template.querySelector('form');
    const inputs = form?.querySelectorAll('input');
    expect(inputs).toBeTruthy();
    inputs?.forEach(i => {
      expect(i.textContent).toBe('');
    })
  });

  it('setFormValues should set form values', () => {
    component.setFormValues(info);
    expect(component.f.birthdate.value).toEqual(moment(info.birthDate, 'DD/MM/YYYY'));
    expect(component.f.firstname.value).toBe(info.firstName);
    expect(component.f.lastname.value).toBe(info.lastName);
    expect(component.f.phone.value).toBe(info.phone);
  });
});
