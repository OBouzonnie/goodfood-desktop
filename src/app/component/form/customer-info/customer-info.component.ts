import { Component, EventEmitter, Inject, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { CustomerInformation } from 'src/app/models/apis/models';
import { SnackBarCSS } from 'src/app/models/enums/SnackBarCSS';
import { IForm } from 'src/app/models/interfaces/form.interface';
import { SnackbarService } from 'src/app/services/snackbar/snackbar.service';
import formUtil from 'src/app/utils/form.util';

/**
 * the customer form
 */
@Component({
  selector: 'app-customer-info',
  templateUrl: './customer-info.component.html',
  styleUrls: ['./customer-info.component.scss']
})
export class CustomerInfoComponent implements IForm {

  /**
   * submission event
   */
  @Output() submitted: EventEmitter<boolean|null> = new EventEmitter();

  /**
   * form inputs
   */
  get f(): any {return this.form.controls;}

  /**
   * angular form
   */
  form: FormGroup = this.fb.group({
    birthdate: [null],
    firstname: [null, formUtil.words],
    lastname: [null, formUtil.words],
    phone: [null, formUtil.nbrs]
  });

  /**
    * form title
    */
  title: string = 'form.info.title';

  /**
   * class constructor
   * @param fb angular form builder
   * @param snackBarService allow access to the material snackbar pop-up
   * @param translate internationalization tool
   * @param data the customer information
   */
  constructor(
    public fb: FormBuilder,
    public snackBarService: SnackbarService,
    public translate: TranslateService,
    @Inject(MAT_DIALOG_DATA) public data?: CustomerInformation,
  ) {
    if (this.data) this.setFormValues(this.data);
  }

  /**
   * handle confirmation event
   * @param event confirmation event
   */
  handleModalConfirm(event: boolean): void{
    if (event) {
      this.submitForm();
      return;
    }
    this.submitted.emit(null);
  }

  /**
   * set form values
   * @param info customer informations
   */
  setFormValues(info: CustomerInformation){
    this.f.birthdate.setValue(moment(info.birthDate, 'DD/MM/YYYY'));
    this.f.firstname.setValue(info.firstName);
    this.f.lastname.setValue(info.lastName);
    this.f.phone.setValue(info.phone);
  }

  /**
   * form submission
   */
  submitForm(): void{
    if (!this.form.valid){
      const errors: string[] = this.getErrorsMsg();
      if (errors.length > 0) this.snackBarService.openSnackbar(errors.join('\n'), SnackBarCSS.ERROR);
    }
    if(this.form.valid){
      this.submitted.emit(true);
    }
  }

  /**
   * compile validation errors
   * @returns an array of validation errors
   */
  getErrorsMsg(): string[]{
    const errors: string[] = [];
    if(
      this.f['birthdate'].errors?.required ||
      this.f['firstname'].errors?.required ||
      this.f['lastname'].errors?.required ||
      this.f['phone'].errors?.required
    ) errors.push(this.translate.instant('errors.required'));
    if(
      this.f['firstname'].errors?.maxlength ||
      this.f['lastname'].errors?.maxlength
    ) errors.push(this.translate.instant('errors.txtLength'));
    if(
      this.f['firstname'].errors?.pattern ||
      this.f['lastname'].errors?.pattern
    ) errors.push(this.translate.instant('errors.forbiddenChars'));
    if (this.f['phone'].errors?.pattern) errors.push(this.translate.instant('errors.phone'));
    return errors;
  }

  /**
   * create the request body
   * @returns the request body
   */
  getBody(): any{
    return {
      birthDate: moment(this.f.birthdate.value._d).format('DD/MM/YYYY'),
      firstName: this.f.firstname.value,
      lastName: this.f.lastname.value,
      phone: this.f.phone.value
    }
  }
}
