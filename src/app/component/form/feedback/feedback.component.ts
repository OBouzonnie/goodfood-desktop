import { Component, Input } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Feedback } from 'src/app/models/apis/models';
import { ApiUrl } from 'src/app/models/enums/ApiUrl';
import { SnackBarCSS } from 'src/app/models/enums/SnackBarCSS';
import { Logo } from 'src/app/models/type/Logo';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import { SnackbarService } from 'src/app/services/snackbar/snackbar.service';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';
import formUtil from 'src/app/utils/form.util';
import { environment } from 'src/environments/environment';

/**
 * the feedback form
 */
@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss']
})
export class FeedbackComponent {

  /**
   * the franchise the order is adress to
   */
  @Input() franchise?: string;

  /**
   * number of active star
   */
  eval: number = 0;

  /**
   * number of active star hovered
   */
  hover: number = 0;

  /**
   * the form submission state
   */
  hasPostedFeedback: boolean = false;

  /**
   * form inputs
   */
  get f(): any {return this.form.controls;}

  /**
   * angular form
   */
  form: FormGroup = this.fb.group({
    comment: [null, formUtil.textarea]
  });

  /**
   * the GOOD FOOD logo img attributes
   */
  logo: Logo = {
    url:'assets/logo/logo-good_food-primary.svg',
    alt:'good food logo'
  }

  /**
   * class constructor
   * @param fb angular form builder
   * @param snackBarService allow access to the material snackbar pop-up
   * @param translate internationalization tool
   * @param baseRepo the repository service
   * @param spinner allow access to the application spinner state
   */
  constructor(
    public fb: FormBuilder,
    public snackBarService: SnackbarService,
    public translate: TranslateService,
    private baseRepo: BaseRepositoryService,
    public spinner: SpinnerService
  ) { }

  /**
   * form submission
   */
  submitForm(): void{
    if (!this.form.valid || this.eval == 0){
      const errors: string[] = this.getErrorsMsg();
      if (errors.length > 0) this.snackBarService.openSnackbar(errors.join('\n'), SnackBarCSS.ERROR);
    }
    if(this.form.valid && this.franchise && !this.hasPostedFeedback && this.eval > 0){
      const feedback = {
        comment: this.f.comment.value,
        eval: this.eval
      };
      const url = ApiUrl.RESTAURANT_FEEDBACK.replace(environment.core.idParam, this.franchise);
      this.post(url, feedback);
    }
  }

  /**
   * compile validation errors
   * @returns an array of validation errors
   */
  getErrorsMsg(): string[]{
    const errors: string[] = [];
    if(this.f['comment'].errors?.maxLength) errors.push(this.translate.instant('errors.lgTxtLength'));
    if(this.f['comment'].errors?.pattern) errors.push(this.translate.instant('errors.forbiddenChars'));
    if(this.eval == 0) errors.push(this.translate.instant('errors.eval'));
    return errors;
  }

  /**
   * set the customer evaluation value
   * @param count number of stars selected
   */
  evaluate(count: number){
    if(!this.hasPostedFeedback) this.eval = count;
  }

  /**
   * set the hovered stars
   * @param count number of star hovered
   */
  hoverEval(count: number){
    if(!this.hasPostedFeedback) this.hover = count;
  }

  /**
   * calls the repository to post the request
   * @param url api url
   * @param body the customer feedback
   */
  post(url: string, body: Feedback) {
    console.log(`POST ${url} with body ${body}`);
    this.spinner.increaseCount();
    this.baseRepo.post(url, body).then(res => {
      this.hasPostedFeedback = true;
      this.form.disable();
      this.spinner.decreaseCount();
    })
    .catch(err => {
      console.error(err);
      this.spinner.decreaseCount();
    });
  }

}
