import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import faker from '@faker-js/faker';
import { AppModule } from 'src/app/app.module';
import { CRUD } from 'src/app/models/enums/CRUD';

import { ResourceComponent } from './resource.component';

describe('ResourceComponent', () => {
  let component: ResourceComponent;
  let fixture: ComponentFixture<ResourceComponent>;
  let template: HTMLElement;
  let uid: string;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule],
      declarations: [ ResourceComponent ],
      providers: [
        FormBuilder,
        { provide: MAT_DIALOG_DATA, useValue: {} }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResourceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    template = fixture.nativeElement;
    uid = faker.datatype.uuid();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  /**
   * Methods
   */
  it('#On Init should set modal for a CRUD.READ state', () => {
    spyOn(component, 'setFormValues');
    component.data = {action: CRUD.READ, uid: uid, resource: {id: uid}};
    component.ngOnInit();
    expect(component.readonly).toBeTrue();
    expect(component.form.disabled).toBeTrue();
    expect(component.setFormValues).toHaveBeenCalled();
  });

  it('#On Init should set modal for a CRUD.UPDATE state', () => {
    spyOn(component, 'setFormValues');
    component.data = {action: CRUD.UPDATE, uid: uid, resource: {id: uid}};
    component.ngOnInit();
    expect(component.readonly).toBeFalse();
    expect(component.form.disabled).toBeFalse();
    expect(component.setFormValues).toHaveBeenCalled();
  });

  it('#On Init should set modal for a CRUD.CREATE state', () => {
    spyOn(component, 'setFormValues');
    component.data = {action: CRUD.CREATE, uid: ''};
    component.ngOnInit();
    expect(component.readonly).toBeFalse();
    expect(component.form.disabled).toBeFalse();
    expect(component.setFormValues).not.toHaveBeenCalled();
  });

  /**
   * Rendering
   */
   it('should render a modal', () => {
    const element = template.querySelector('app-modal');
    const innerElement = template.querySelector('h3');
    expect(element).toBeTruthy();
    expect(innerElement).toBeTruthy();
  });
});
