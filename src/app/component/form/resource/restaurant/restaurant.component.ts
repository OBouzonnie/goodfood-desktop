import { Component, Inject } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { Restaurant } from 'src/app/models/apis/models';
import { CRUD } from 'src/app/models/enums/CRUD';
import { CrudTarget } from 'src/app/models/type/CrudTarget';
import { SnackbarService } from 'src/app/services/snackbar/snackbar.service';
import FormUtil from 'src/app/utils/form.util';
import { ResourceComponent } from '../resource.component';

/**
 * restaurant form
 */
@Component({
  selector: 'app-restaurant',
  templateUrl: './restaurant.component.html',
  styleUrls: ['./restaurant.component.scss']
})
export class RestaurantComponent extends ResourceComponent {

  /**
   * form title
   */
  override title: string = 'resource.restaurant';

  /**
   * the restaurant address ID
   */
  addressId: string = '';

  /**
   * class constructor
   * @param fb angular form builder
   * @param snackBarService allow acces to the application snackbar
   * @param data crud action to perform and related informations
   * @param translate internationalization tool
   */
  constructor(
    public override fb: FormBuilder,
    public override snackBarService: SnackbarService,
    @Inject(MAT_DIALOG_DATA) public override data: CrudTarget,
    private translate: TranslateService
  ) {
    super(fb, snackBarService, data);
    if (!this.readonly) this.setOpeningHours();
    this.f['name'].addValidators(Validators.required);
    this.f['number'].addValidators(Validators.required);
    this.f['street'].addValidators(Validators.required);
    this.f['postal'].addValidators(Validators.required);
    this.f['city'].addValidators(Validators.required);
    this.f['phone'].addValidators(Validators.required);
  }

  /**
   * set the opening hours form values for a crud READ or UPDATE state
   * @param r the restaurant
   */
  setOpeningHours(r?: Restaurant){
    this.f.openingHours.clear();
    for (let i = 0; i < 7; i++){
      this.f.openingHours.push(this.fb.group({
        day: [this.week[i]],
        open: [r?.openDateTime ? r.openDateTime[i] : null],
        close: [r?.closeDateTime ? r.closeDateTime[i] : null]
      }))
    }
  }

  /**
   * labels for each day of the week
   */
  week= ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];

  /**
   * the resource form
   */
  override form = this.fb.group({
    name: [null, FormUtil.txt],
    email: [null, FormUtil.email],
    firstname: [null, FormUtil.txt],
    lastname: [null, FormUtil.txt],
    number: [null, FormUtil.nbrs],
    street: [null, FormUtil.txt],
    postal: [null, FormUtil.nbrs],
    city: [null, FormUtil.txt],
    phone: [null, FormUtil.nbrs],
    openingHours: this.fb.array([])
  });
  get openingHours() {return this.f.openingHours.controls}

  /**
   * set form values for read and update crud states
   * @param restaurant restaurant to display
   */
  override setFormValues(restaurant: Restaurant){
    this.setOpeningHours(restaurant);
    this.f.name.setValue(restaurant.name);
    this.f.email.setValue(restaurant.owner?.email);
    this.f.firstname.setValue(restaurant.owner?.firstName);
    this.f.lastname.setValue(restaurant.owner?.lastName);
    this.f.number.setValue(restaurant.address?.number);
    this.f.street.setValue(restaurant.address?.street);
    this.f.postal.setValue(restaurant.address?.postal);
    this.f.city.setValue(restaurant.address?.city);
    this.f.phone.setValue(restaurant.phone);
    if (this.data.action === CRUD.READ) this.f.openingHours.disable();
  }

  /**
   * compile validation errors in a array
   * @returns an array of error messages
   */
  override getErrorsMsg(): string[]{
    const errors: string[] = [];
    if(
      this.f['name'].errors?.required ||
      this.f['number'].errors?.required ||
      this.f['street'].errors?.required ||
      this.f['postal'].errors?.required ||
      this.f['city'].errors?.required ||
      this.f['phone'].errors?.required
    ) errors.push(this.translate.instant('errors.required'));
    if(
      this.f['name'].errors?.maxlength ||
      this.f['email'].errors?.maxlength ||
      this.f['firstname'].errors?.maxlength ||
      this.f['lastname'].errors?.maxlength ||
      this.f['street'].errors?.maxlength ||
      this.f['city'].errors?.maxlength
    ) errors.push(this.translate.instant('errors.txtLength'));
    if(
      this.f['name'].errors?.pattern ||
      this.f['firstname'].errors?.pattern ||
      this.f['lastname'].errors?.pattern ||
      this.f['street'].errors?.pattern ||
      this.f['city'].errors?.pattern
    ) errors.push(this.translate.instant('errors.forbiddenChars'));
    if (this.f['email'].errors?.pattern || this.f['email'].errors?.email) errors.push(this.translate.instant('errors.email'));
    if (this.f['number'].errors?.pattern || this.f['postal'].errors?.pattern || this.f['phone'].errors?.pattern) errors.push(this.translate.instant('errors.nbrs'));
    return errors;
  }

  /**
   * create the request body
   * @returns the request body as a restaurant
   */
  override getBody(): Restaurant{
    const body: Restaurant = {
      id: this.data.resource ? this.data.resource.id : undefined,
      name: this.f['name'].value,
      phone: this.f['phone'].value,
      owner: {
        email: this.f['email'].value,
        firstName: this.f['firstname'].value,
        lastName: this.f['lastname'].value
      },
      address: {
      city: this.f['city'].value,
      number: this.f['number'].value,
      postal: this.f['postal'].value,
      street: this.f['street'].value,
      },
      openDateTime: [],
      closeDateTime: []
    };
    this.form.controls['openingHours'].value.forEach((oh: any) => {
      body.openDateTime?.push(oh.open);
      body.closeDateTime?.push(oh.close);
    });
    return body
  }
}
