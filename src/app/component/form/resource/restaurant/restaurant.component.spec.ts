import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AppModule } from 'src/app/app.module';
import { Restaurant } from 'src/app/models/apis/models';
import { CRUD } from 'src/app/models/enums/CRUD';
import { MockService } from 'src/app/services/mock/mock.service';

import { RestaurantComponent } from './restaurant.component';

describe('RestaurantComponent', () => {
  let component: RestaurantComponent;
  let fixture: ComponentFixture<RestaurantComponent>;
  let template: HTMLElement;
  let mockService: MockService;
  let resources: Restaurant[];
  let index: number;
  let restaurant: Restaurant;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule],
      declarations: [ RestaurantComponent ],
      providers: [
        FormBuilder,
        { provide: MAT_DIALOG_DATA, useValue: {} }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RestaurantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    template = fixture.nativeElement;
    mockService = new MockService();
    resources = mockService.getRestaurants();
    index = Math.floor(Math.random() * resources.length);
    restaurant = resources[index];
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  /**
   * Rendering
   */
  it('should render 23 inputs by default', () => {
    const form = template.querySelector('form');
    const inputs = form?.querySelectorAll('input');
    expect(inputs).toBeTruthy();
    expect(inputs?.length).toBe(23);
  });

  it('should render a disabled form for CRUD.READ state', () => {
    component.data = {action: CRUD.READ, uid: resources[index].id || '', resource: resources[index]};
    component.ngOnInit();
    fixture.detectChanges();
    template = fixture.nativeElement;
    const form = template.querySelector('form');
    const inputs = form?.querySelectorAll('input');
    inputs?.forEach(i => {
      expect(i.disabled).toBeTruthy();
    })
  });

  it('should render an enabled form for CRUD.UPDATE state', () => {
    component.data = {action: CRUD.UPDATE, uid: resources[index].id || '', resource: resources[index]};
    component.ngOnInit();
    fixture.detectChanges();
    template = fixture.nativeElement;
    const form = template.querySelector('form');
    const inputs = form?.querySelectorAll('input');
    inputs?.forEach(i => {
      expect(i.disabled).toBeFalsy();
    })
  });

  it('should render an enabled form for CRUD.CREATE state', () => {
    component.data = {action: CRUD.CREATE, uid: ''};
    component.ngOnInit();
    fixture.detectChanges();
    template = fixture.nativeElement;
    const form = template.querySelector('form');
    const inputs = form?.querySelectorAll('input');
    inputs?.forEach(i => {
      expect(i.disabled).toBeFalsy();
    })
  });

  /**
   * Methods
   */

  it('setFormValues should set form values with hours enable by default', () => {
    component.setFormValues(restaurant);
    expect(component.f.name.value).toBe(restaurant.name);
    expect(component.f.email.value).toBe(restaurant.owner?.email);
    expect(component.f.firstname.value).toBe(restaurant.owner?.firstName);
    expect(component.f.lastname.value).toBe(restaurant.owner?.lastName);
    expect(component.f.number.value).toBe(restaurant.address?.number);
    expect(component.f.street.value).toBe(restaurant.address?.street);
    expect(component.f.postal.value).toBe(restaurant.address?.postal);
    expect(component.f.city.value).toBe(restaurant.address?.city);
    expect(component.f.phone.value).toBe(restaurant.phone);
    for(let i = 0; i < component.week.length; i++){
      expect(component.f.openingHours.controls[i].controls.day.value).toBe(component.week[i]);
      if (restaurant.openDateTime) expect(component.f.openingHours.controls[i].controls.open.value).toBe(restaurant.openDateTime[i]);
      if (restaurant.closeDateTime) expect(component.f.openingHours.controls[i].controls.close.value).toBe(restaurant.closeDateTime[i]);
    };
    expect(component.f.openingHours.disabled).toBeFalse();
    expect(component.form.disabled).toBeFalse();
  });

  it('setFormValues should set form values and hours disabled for CRUD.READ state', () => {
    component.data.action = CRUD.READ;
    component.setFormValues(restaurant);
    expect(component.f.name.value).toBe(restaurant.name);
    expect(component.f.email.value).toBe(restaurant.owner?.email);
    expect(component.f.firstname.value).toBe(restaurant.owner?.firstName);
    expect(component.f.lastname.value).toBe(restaurant.owner?.lastName);
    expect(component.f.number.value).toBe(restaurant.address?.number);
    expect(component.f.street.value).toBe(restaurant.address?.street);
    expect(component.f.postal.value).toBe(restaurant.address?.postal);
    expect(component.f.city.value).toBe(restaurant.address?.city);
    expect(component.f.phone.value).toBe(restaurant.phone);
    for(let i = 0; i < component.week.length; i++){
      expect(component.f.openingHours.controls[i].controls.day.value).toBe(component.week[i]);
      if (restaurant.openDateTime) expect(component.f.openingHours.controls[i].controls.open.value).toBe(restaurant.openDateTime[i]);
      if (restaurant.closeDateTime) expect(component.f.openingHours.controls[i].controls.close.value).toBe(restaurant.closeDateTime[i]);
    };
    expect(component.f.openingHours.disabled).toBeTrue();
  });
});
