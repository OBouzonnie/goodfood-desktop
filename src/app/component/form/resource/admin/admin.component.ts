import { Component, Inject, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { Admin, Restaurant } from 'src/app/models/apis/models';
import { ApiUrl } from 'src/app/models/enums/ApiUrl';
import { Roles, rolesValues } from 'src/app/models/enums/Roles';
import { SnackBarCSS } from 'src/app/models/enums/SnackBarCSS';
import { CrudTarget } from 'src/app/models/type/CrudTarget';
import { RoleOption } from 'src/app/models/type/RoleOption';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import { SnackbarService } from 'src/app/services/snackbar/snackbar.service';
import formUtil from 'src/app/utils/form.util';
import { ResourceComponent } from '../resource.component';

/**
 * admin form
 */
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent extends ResourceComponent {

  /**
   * the available roles for this application
   */
  roles: RoleOption[] = rolesValues().map(r => {return {label: 'enum.' + r, role: r}});

  /**
   * form title
   */
  override title: string = 'resource.admin';

  /**
   * the catalog of restaurants
   */
  franchises: Restaurant[] = [];

  /**
   * the restaurant api url
   */
  franchiseUrl: ApiUrl = ApiUrl.RESTAURANT;

  /**
   * is the admin a franchise admin
   */
  hasFranchise: boolean = false;

  /**
   * the resource form
   */
  override form = this.fb.group({
    login: [null, formUtil.email],
    password: [null, formUtil.password],
    confirm: [null, formUtil.password],
    franchise: [null],
    permissions: this.fb.array([])
  });

  /**
   * get permissions form controls
   */
  get perm() {return this.form.controls['permissions'] as FormArray};

  /**
   * class constructor
   * @param fb angular form builder
   * @param snackBarService allow acces to the application snackbar
   * @param data crud action to perform and related informations
   * @param translate internationalization tool
   * @param baseRepo the repository service
   */
  constructor(
    public override fb: FormBuilder,
    public override snackBarService: SnackbarService,
    @Inject(MAT_DIALOG_DATA) public override data: CrudTarget,
    private translate: TranslateService,
    private baseRepo: BaseRepositoryService
  ) {
    super(fb, snackBarService, data);
    this.f['permissions'].addValidators(Validators.required);
    this.getRestaurant();
  }

  /**
   * get the catalog of active restaurant
   */
  getRestaurant(){
    this.baseRepo.getAll(this.franchiseUrl).then((res: any) => {
      this.franchises = res.body;
      this.franchises = this.franchises.filter(f => f.isActive == true);
    });
  }

  /**
   * toggle the franchise input of the form
   */
  toggleFranchiseInput(){
    const perm = this.f.permissions.value.find((p: any) => p.permId == Roles.FRANCHISE);
    if (perm) {
      this.hasFranchise = true;
      this.f['franchise'].addValidators(Validators.required);
    } else {
      this.hasFranchise = false;
      this.f['franchise'].clearValidators();
    };
  }

  /**
   * add one permission input to the form
   * @param i the permission
   */
  addPerm(i?: any){
    const perm: FormGroup = this.fb.group({
      permId: [i ? i : '', Validators.required]
    });
    this.f.permissions.push(perm);
    if (this.readonly) this.f.permissions.disable();
  }

  /**
   * remove one permission inputs
   * @param i the permission input index
   */
  deletePerm(i: number){
    this.f.permissions.removeAt(i);
  }

  /**
   * remove all permission inputs from the form
   */
  cleanPerm(){
    this.f.permissions.clear();
  }

  /**
   * set form values for read and update crud states
   * @param admin admin to display
   */
  override setFormValues(admin: Admin){
    this.f.login.setValue(admin.login);
    this.f.password.setValue(admin.password);
    this.f.franchise.setValue(admin.franchise);
    admin.permission.forEach((p) => {
      this.addPerm(p);
    });
  }

  /**
   * compile validation errors in a array
   * @returns an array of error messages
   */
  override getErrorsMsg(): string[]{
    const errors: string[] = [];
    if(
      this.f['login'].errors?.required ||
      this.f['password'].errors?.required ||
      this.f['franchise'].errors?.required
    ) errors.push(this.translate.instant('errors.required'));
    if(this.f['permissions'].errors?.required) errors.push(this.translate.instant('errors.perm'));
    if (
      this.f['password']?.errors?.pattern ||
      this.f['login']?.errors?.pattern
    ) errors.push(this.translate.instant('auth.msg.special'));
    if (this.f['password']?.errors?.minlength) errors.push(this.translate.instant('auth.msg.pwd.length'));
    if (this.f['password']?.errors?.pattern) errors.push(this.translate.instant('auth.msg.pwd.pattern'));
    if (this.f['password']?.errors?.maxlength ||
    this.f['login']?.errors?.maxlength
    ) errors.push(this.translate.instant('auth.msg.length'));
    if (this.f.password.value != this.f.confirm.value) errors.push(this.translate.instant('auth.msg.confirm'));
    return errors;
  }

  /**
   * form submission
   */
  override submitForm(): void{
    if (!this.form.valid || this.f.password.value != this.f.confirm.value){
      const errors: string[] = this.getErrorsMsg();
      if (errors.length > 0) this.snackBarService.openSnackbar(errors.join('\n'), SnackBarCSS.ERROR);
      return;
    }
    if(this.form.valid){
      const body = this.getBody();
      this.submitted.emit(body);
    }
  }

  /**
   * create the request body
   * @returns the request body as an admin credentials
   */
  override getBody(): Admin {
    const body: Admin = {
      id: this.data.resource ? this.data.resource.id : undefined,
      login: this.f['login'].value,
      password: this.f['password'].value,
      franchise: this.f['franchise'].value,
      permission: []
    }
    this.perm.value.forEach((v: any) => {
      body.permission.push(v.permId);
    })
    return body;
  }

}
