import { Component, Inject } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { Address } from 'src/app/models/apis/models';
import { CrudTarget } from 'src/app/models/type/CrudTarget';
import { SnackbarService } from 'src/app/services/snackbar/snackbar.service';
import formUtil from 'src/app/utils/form.util';
import { ResourceComponent } from '../resource.component';

/**
 * address form
 */
@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss']
})
export class AddressComponent extends ResourceComponent {

  /**
   * form title
   */
  override title: string = 'resource.address';

  /**
   * the resource form
   */
  override form = this.fb.group({
    city: [null, formUtil.txt],
    number: [null, formUtil.nbrs],
    postal: [null, formUtil.nbrs],
    street: [null, formUtil.txt],
  });

  /**
   * class constructor
   * @param fb angular form builder
   * @param snackBarService allow acces to the application snackbar
   * @param data crud action to perform and related informations
   * @param translate internationalization tool
   */
  constructor(
    public override fb: FormBuilder,
    public override snackBarService: SnackbarService,
    @Inject(MAT_DIALOG_DATA) public override data: CrudTarget,
    public translate: TranslateService
  ) {
    super(fb, snackBarService, data);
  }

  /**
   * compile validation errors in a array
   * @returns an array of error messages
   */
  override getErrorsMsg(): string[]{
    const errors: string[] = [];
    if(
      this.f['number'].errors?.required ||
      this.f['street'].errors?.required ||
      this.f['postal'].errors?.required ||
      this.f['city'].errors?.required
    ) errors.push(this.translate.instant('errors.required'));
    if(
      this.f['street'].errors?.maxlength ||
      this.f['city'].errors?.maxlength
    ) errors.push(this.translate.instant('errors.txtLength'));
    if(
      this.f['street'].errors?.pattern ||
      this.f['city'].errors?.pattern
    ) errors.push(this.translate.instant('errors.forbiddenChars'));
    if (this.f['number'].errors?.pattern || this.f['postal'].errors?.pattern) errors.push(this.translate.instant('errors.nbrs'));
    return errors;
  }

  /**
   * create the request body
   * @returns the request body as an adress
   */
  override getBody(): Address{
    const body: Address = {
      city: this.f['city'].value,
      number: this.f['number'].value,
      postal: this.f['postal'].value,
      street: this.f['street'].value,
    };
    return body
  }

}
