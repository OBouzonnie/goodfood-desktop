import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import faker from '@faker-js/faker';
import { AppModule } from 'src/app/app.module';
import { Ingredient, Recipe } from 'src/app/models/apis/models';
import { CRUD } from 'src/app/models/enums/CRUD';
import { MockService } from 'src/app/services/mock/mock.service';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import formatUtils from 'src/app/utils/format.utils';

import { RecipeComponent } from './recipe.component';

describe('RecipeComponent', () => {
  let component: RecipeComponent;
  let fixture: ComponentFixture<RecipeComponent>;
  let template: HTMLElement;
  let mockService: MockService;
  let resources: Recipe[];
  let ing: Ingredient[];
  let index: number;
  let baseRepo: BaseRepositoryService;
  let getIngSpy: jasmine.Spy;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule],
      declarations: [ RecipeComponent ],
      providers: [
        FormBuilder,
        { provide: MAT_DIALOG_DATA, useValue: {} }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    mockService = new MockService();
    ing = mockService.getIngredients();
    baseRepo = TestBed.inject(BaseRepositoryService);
    getIngSpy = spyOn(baseRepo, 'getAll').and.callThrough().and.returnValue(Promise.resolve({body: ing}));
    fixture = TestBed.createComponent(RecipeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    template = fixture.nativeElement;
    mockService = new MockService();
    resources = mockService.getRecipes();
    index = Math.floor(Math.random() * resources.length);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  /**
   * Rendering
   */

  it('should render 3 inputs by default', () => {
    const form = template.querySelector('form');
    const inputs = form?.querySelectorAll('input');
    expect(inputs).toBeTruthy();
    expect(inputs?.length).toBe(3);
  });

  it('should render additionnal inputs and selects for a CRUD.READ state', () => {
    component.data = {action: CRUD.READ, uid: resources[index].id || '', resource: resources[index]};
    component.ngOnInit();
    fixture.detectChanges();
    template = fixture.nativeElement;
    const form = template.querySelector('form');
    const inputs = form?.querySelectorAll('input');
    const selects = form?.querySelectorAll('mat-select');
    const l = resources[index].ingredients?.length;
    expect(inputs?.length).toBe(3 + (l ? l : 0));
    expect(selects?.length).toBe(resources[index].ingredients?.length);
  });

  it('should render additionnal inputs and selects for a CRUD.UPDATE state', () => {
    component.data = {action: CRUD.UPDATE, uid: resources[index].id || '', resource: resources[index]};
    component.ngOnInit();
    fixture.detectChanges();
    template = fixture.nativeElement;
    const form = template.querySelector('form');
    const inputs = form?.querySelectorAll('input');
    const selects = form?.querySelectorAll('mat-select');
    const l = resources[index].ingredients?.length;
    expect(inputs?.length).toBe(3 + (l ? l : 0));
    expect(selects?.length).toBe(resources[index].ingredients?.length);
  });

  it('should not render additionnal inputs and selects for a CRUD.CREATE state', () => {
    component.data = {action: CRUD.CREATE, uid: ''};
    component.ngOnInit();
    fixture.detectChanges();
    template = fixture.nativeElement;
    const form = template.querySelector('form');
    const inputs = form?.querySelectorAll('input');
    const selects = form?.querySelectorAll('mat-select');
    expect(inputs?.length).toBe(3);
    expect(selects?.length).toBe(0);
  });

  it('should not render any ingredients btn for a CRUD.READ state', () => {
    component.data = {action: CRUD.READ, uid: resources[index].id || '', resource: resources[index]};
    component.ngOnInit();
    fixture.detectChanges();
    template = fixture.nativeElement;
    const form = template.querySelector('form');
    const addBtn = form?.querySelector('#ing-add-btn');
    const delAllBtn = form?.querySelector('#ing-delete-all-btn');
    const delBtns = form?.querySelectorAll('.ing-delete-btn');
    expect(addBtn).toBeFalsy();
    expect(delAllBtn).toBeFalsy();
    expect(delBtns?.length).toBe(0);
  });

  it('should render ingredients btn for a CRUD.UPDATE state', () => {
    component.data = {action: CRUD.UPDATE, uid: resources[index].id || '', resource: resources[index]};
    component.ngOnInit();
    fixture.detectChanges();
    template = fixture.nativeElement;
    const form = template.querySelector('form');
    const addBtn = form?.querySelector('#ing-add-btn');
    const delAllBtn = form?.querySelector('#ing-delete-all-btn');
    const delBtns = form?.querySelectorAll('.ing-delete-btn');
    expect(addBtn).toBeTruthy();
    expect(delAllBtn).toBeTruthy();
    expect(delBtns?.length).not.toBe(0);
  });

  it('should render ingredients btn for a CRUD.CREATE state', () => {
    component.data = {action: CRUD.CREATE, uid: ''};
    component.ngOnInit();
    fixture.detectChanges();
    template = fixture.nativeElement;
    const form = template.querySelector('form');
    const addBtn = form?.querySelector('#ing-add-btn');
    const delAllBtn = form?.querySelector('#ing-delete-all-btn');
    const delBtns = form?.querySelectorAll('.ing-delete-btn');
    expect(addBtn).toBeTruthy();
    expect(delAllBtn).toBeTruthy();
    expect(delBtns?.length).toBe(0);
  });

  it('should render a disabled form for CRUD.READ state', () => {
    component.data = {action: CRUD.READ, uid: resources[index].id || '', resource: resources[index]};
    component.ngOnInit();
    fixture.detectChanges();
    template = fixture.nativeElement;
    const form = template.querySelector('form');
    const inputs = form?.querySelectorAll('input');
    const selects = form?.querySelectorAll('mat-select');
    inputs?.forEach(i => {
      expect(i.disabled || i.hidden).toBeTruthy();
    });
    selects?.forEach(s => {
      expect(s.ariaDisabled).toBe('true');
    });
  });

  it('should render an enabled form for CRUD.UPDATE state', () => {
    component.data = {action: CRUD.UPDATE, uid: resources[index].id || '', resource: resources[index]};
    component.ngOnInit();
    fixture.detectChanges();
    template = fixture.nativeElement;
    const form = template.querySelector('form');
    const inputs = form?.querySelectorAll('input');
    const selects = form?.querySelectorAll('mat-select');
    inputs?.forEach(i => {
      expect(i.disabled).toBeFalsy();
    });
    selects?.forEach(s => {
      expect(s.ariaDisabled).toBe('false');
    });
  });

  it('should render an enabled form for CRUD.CREATE state', () => {
    component.data = {action: CRUD.CREATE, uid: ''};
    component.ngOnInit();
    fixture.detectChanges();
    template = fixture.nativeElement;
    const form = template.querySelector('form');
    const inputs = form?.querySelectorAll('input');
    const selects = form?.querySelectorAll('mat-select');
    inputs?.forEach(i => {
      expect(i.disabled).toBeFalsy();
    });
    selects?.forEach(s => {
      expect(s.ariaDisabled).toBe('false');
    });
  });

  /**
   * Ingredients inputs
   */
  it('does not render ingredients inputs at init for a CRUD.CREATE state', () => {
    component.data = {action: CRUD.CREATE, uid: ''};
    component.ngOnInit();
    fixture.detectChanges();
    template = fixture.nativeElement;
    const ingFormFields = template.querySelectorAll('.ing-form-field');
    expect(ingFormFields.length).toBe(0);
  });

  /**
   * Interaction
   */

   it('should add inputs when add ingredients btn is clicked', () => {
    component.ngOnInit();
    fixture.detectChanges();
    template = fixture.nativeElement;
    let form = template.querySelector('form');
    let btn = template.querySelector('#ing-add-btn');
    let ings = form?.querySelectorAll('.ing-form-field');
    const ingsCount = ings?.length;
    btn?.dispatchEvent(new Event('click'));
    fixture.detectChanges();
    form = template.querySelector('form');
    template = fixture.nativeElement;
    ings = form?.querySelectorAll('.ing-form-field');
    const ingsDelta = ings && (ingsCount || ingsCount == 0) ?  ings.length - ingsCount : 0;
    expect(ingsDelta > 0).toBeTrue();
  });

  it('should remove inputs when delete all ingredients btn is clicked', () => {
    component.data = {action: CRUD.UPDATE, uid: resources[index].id || '', resource: resources[index]};
    component.ngOnInit();
    fixture.detectChanges();
    template = fixture.nativeElement;
    let form = template.querySelector('form');
    let btn = template.querySelector('#ing-delete-all-btn');
    let ings = form?.querySelectorAll('.ing-form-field');
    expect(ings?.length).not.toBe(0);
    btn?.dispatchEvent(new Event('click'));
    fixture.detectChanges();
    form = template.querySelector('form');
    template = fixture.nativeElement;
    ings = form?.querySelectorAll('.ing-form-field');
    expect(ings?.length).toBe(0);
  });

  it('should remove inputs when delete ingredients btn is clicked', () => {
    component.data = {action: CRUD.UPDATE, uid: resources[index].id || '', resource: resources[index]};
    component.ngOnInit();
    fixture.detectChanges();
    template = fixture.nativeElement;
    let form = template.querySelector('form');
    let btn = template.querySelectorAll('.ing-delete-btn');
    let ings = form?.querySelectorAll('.ing-form-field');
    const ingsCount = ings?.length;
    const i = ingsCount ? Math.floor(Math.random() * ingsCount) : -1;
    btn[i]?.dispatchEvent(new Event('click'));
    fixture.detectChanges();
    form = template.querySelector('form');
    template = fixture.nativeElement;
    ings = form?.querySelectorAll('.ing-form-field');
    const ingsDelta = ings && (ingsCount || ingsCount == 0) ?  ings.length - ingsCount : 0;
    expect(ingsDelta < 0).toBeTrue();
  });

  /**
   * Methods
   */

  it('setFormValues should set form values', () => {
    expect(component.f.name.value).toBeNull();
    expect(component.f.price.value).toBeNull();
    expect(component.f.ingredients.length).toBe(0);
    const i = Math.floor(Math.random() * resources.length);
    const recipe = resources[i];
    component.setFormValues(recipe);
    expect(component.f.name.value).toBe(recipe.name);
    expect(component.f.price.value).toBe(recipe.standardPrice ? formatUtils.toDecimalString(recipe.standardPrice) : '');
    expect(component.f.ingredients.length).toBe(recipe.ingredients?.length);
    component.f.ingredients.value.forEach((ing: any, i: number) => {
      if (recipe.ingredients) {
        expect(ing.ingId).toBe(recipe.ingredients[i].id);
        expect(ing.ingQty).toBe(recipe.ingredients[i].quantity);
      }
    });
  });

  it('addIng should increase ingredients form array length', () => {
    expect(component.ing.controls.length).toBe(0);
    const uid = faker.datatype.uuid();
    const qty = Math.floor(Math.random()* 100);
    const loop = Math.floor(Math.random()* 10);
    for (let i = 0; i < loop; i++){
      component.addIng({id: uid, quantity: qty });
    }
    expect(component.ing.controls.length).toBe(loop);
  });

  it('deleteIng should reduce ingredients form array length', () => {
    expect(component.ing.controls.length).toBe(0);
    const uid = faker.datatype.uuid();
    const qty = Math.floor(Math.random()* 100) + 1;
    const loop = Math.floor(Math.random()* 10) + 1;
    for (let i = 0; i < loop; i++){
      component.addIng({id: uid, quantity: qty });
    }
    const index = Math.floor(Math.random() * loop);
    component.deleteIng(index);
    expect(component.ing.controls.length).toBe(loop - 1);
  });

  it('cleanIng should reset ingredients form array length', () => {
    expect(component.ing.controls.length).toBe(0);
    const uid = faker.datatype.uuid();
    const qty = Math.floor(Math.random()* 100);
    const loop = Math.floor(Math.random()* 10);
    for (let i = 0; i < loop; i++){
      component.addIng({id: uid, quantity: qty });
    }
    expect(component.ing.controls.length).toBe(loop);
    component.cleanIng();
    expect(component.ing.controls.length).toBe(0);
  });

  it('getIngredients should provide ingredients data', waitForAsync(() => {
    expect(component.ingredients.length).toBe(0);
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(component.ingredients.length).not.toBe(0);
    })
  }));

  /**
   * Form submission
   */
  it('invalid form submission should not emit a submitted event', () => {
    spyOn(component.submitted, 'emit');
    component.submitForm();
    expect(component.submitted.emit).not.toHaveBeenCalled();
  });

  it('valid form submission should emit a submitted event', () => {
    spyOn(component.submitted, 'emit');
    const i = Math.floor(Math.random() * resources.length);
    const recipe = resources[i];
    component.f['name'].setValue(faker.name.lastName());
    component.f['price'].setValue((Math.floor(Math.random() * 1000) / 100).toString());
    recipe.ingredients?.forEach(ing => {
      component.addIng(ing);
    });
    component.submitForm();
    expect(component.submitted.emit).toHaveBeenCalled();
  });
});
