import { Component, Inject, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { Ingredient, Recipe } from 'src/app/models/apis/models';
import { ApiUrl } from 'src/app/models/enums/ApiUrl';
import { MIME } from 'src/app/models/enums/MIME';
import { SnackBarCSS } from 'src/app/models/enums/SnackBarCSS';
import { CrudTarget } from 'src/app/models/type/CrudTarget';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import { SnackbarService } from 'src/app/services/snackbar/snackbar.service';
import FormUtil from 'src/app/utils/form.util';
import formatUtils from 'src/app/utils/format.utils';
import { ResourceComponent } from '../resource.component';
import {environment} from 'src/environments/environment';

/**
 * recipe form
 */
@Component({
  selector: 'app-recipe',
  templateUrl: './recipe.component.html',
  styleUrls: ['./recipe.component.scss']
})
export class RecipeComponent extends ResourceComponent implements OnInit {

  /**
   * the recipe image encoding
   */
  dwlImg: string = environment.core.dwlImg;

  /**
   * the recipe image file as a binary string
   */
  image?: string;

  /**
   * form title
   */
  override title: string = 'resource.recipe';

  /**
   * ingredient api url
   */
  ingUrl: ApiUrl = ApiUrl.INGREDIENT;

  /**
   * the ingredient catalog
   */
  ingredients: Ingredient[] = [];

  /**
   * class constructor
   * @param fb angular form builder
   * @param snackBarService allow acces to the application snackbar
   * @param data crud action to perform and related informations
   * @param translate internationalization tool
   * @param baseRepo the repository service
   */
  constructor(
    public override fb: FormBuilder,
    public override snackBarService: SnackbarService,
    @Inject(MAT_DIALOG_DATA) public override data: CrudTarget,
    private translate: TranslateService,
    private baseRepo: BaseRepositoryService
  ) {
    super(fb, snackBarService, data);
    this.f['name'].addValidators(Validators.required);
    this.f['price'].addValidators(Validators.required);
    this.f['ingredients'].addValidators(Validators.required);
    this.getIngredients();
  }

  /**
   * the resource form
   */
  override form = this.fb.group({
    name: [null, FormUtil.txt],
    price: [null, FormUtil.prices],
    ingredients: this.fb.array([])
  });
  get ing() {return this.form.controls['ingredients'] as FormArray};

  /**
   * set form values for read and update crud states
   * @param recipe recipe to display
   */
  override setFormValues(recipe: Recipe){
    this.f.name.setValue(recipe.name);
    this.f.price.setValue(recipe.standardPrice ? formatUtils.toDecimalString(recipe.standardPrice) : recipe.standardPrice);
    recipe.ingredients?.forEach((i) => {
      this.addIng(i)
    });
    if (recipe.photo) this.image = recipe.photo;
  }

  /**
   * compile validation errors in a array
   * @returns an array of error messages
   */
  override getErrorsMsg(): string[]{
    const checkIng = this.checkIngredientsInputs();
    const errors: string[] = [];
    if(
      this.f['name'].errors?.required ||
      this.f['price'].errors?.required
    ) errors.push(this.translate.instant('errors.required'));
    if(checkIng.requiredError) errors.push(this.translate.instant('errors.ing'));
    if(this.f['name'].errors?.pattern) errors.push(this.translate.instant('errors.forbiddenChars'));
    if(this.f['name'].errors?.maxlength) errors.push(this.translate.instant('errors.txtLength'));
    if(this.f['price'].errors?.pattern) errors.push(this.translate.instant('errors.prices'));
    if(checkIng.patternError) errors.push(this.translate.instant('errors.nbrs'));
    return errors;
  }

  /**
   * validate the ingredients inputs
   * @returns an object of validation errors
   */
  checkIngredientsInputs(): any {
    let valid: any = {};
    if (this.f['ingredients'].errors?.required) valid.requiredError = true;
    this.f['ingredients'].controls.forEach((i: any) => {
      if (i.controls.ingId.errors?.required || i.controls.ingQty.errors?.required) valid.requiredError = true;
      if (i.controls.ingQty.errors?.pattern) valid.patternError = true;
    });
    return valid;
  }

  /**
   * create the request body
   * @returns the request body as a recipe
   */
  override getBody(): Recipe {
    const body: Recipe = {
      id: this.data.resource ? this.data.resource.id : undefined,
      name: this.f['name'].value,
      ingredients: [],
      photo: this.image ? this.image : '',
      standardPrice: formatUtils.toCentInteger(this.f['price'].value),
    }
    this.ing.value.forEach((v: any) => {
      body.ingredients?.push({
        id: v.ingId,
        quantity: parseInt(v.ingQty)
      })
    })
    return body;
  }

  /**
   * add a set of ingredient inputs to the form
   * @param i the ingredient inputs values
   */
  addIng(i?: any){
    const ing: FormGroup = this.fb.group({
      ingId: [i ? i.id : '', Validators.required],
      ingQty: [i ? i.quantity : 1, FormUtil.nbrs]
    });
    this.f.ingredients.push(ing);
    if (this.readonly) this.f.ingredients.disable();
  }

  /**
   * remove one ingredient inputs from the form
   * @param i the index of the ingredient in the form
   */
  deleteIng(i: number){
    this.f.ingredients.removeAt(i);
  }

  /**
   * remove all ingredient inputs from the form
   */
  cleanIng(){
    this.f.ingredients.clear();
  }

  /**
   * get ingredient catalog
   */
  getIngredients(){
    this.baseRepo.getAll(this.ingUrl).then((res: any) => this.ingredients = res.body);
  }

  /**
   * encoded images to binary string
   */
  onFileSelected() {
    const inputNode: any = document.querySelector('#file');

    const errors: string[] = [];

    if(inputNode.files[0].type != MIME.JPG) errors.push(this.translate.instant('errors.imgType'));
    if(inputNode.files[0].size > 500000) errors.push(this.translate.instant('errors.imgSize'));

    if(errors.length > 0) return this.snackBarService.openSnackbar(errors.join('\n'), SnackBarCSS.ERROR);

    if (typeof (FileReader) !== 'undefined') {
      const reader = new FileReader();

      reader.onload = (e: any) => {
        this.image = btoa(e.target.result);
      };

      reader.readAsBinaryString(inputNode.files[0]);
    }
  }
}
