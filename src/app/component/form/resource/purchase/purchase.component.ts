import { Component, Inject } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { Purchase, PurchaseDetail, Supplier, SupplierIngredient } from 'src/app/models/apis/models';
import { CreatePurchase } from 'src/app/models/apis/models/create-purchase';
import { ApiUrl } from 'src/app/models/enums/ApiUrl';
import { PurchaseDetailTableMapper } from 'src/app/models/table/mappers/PurchaseDetailTableMapper.ts/PurchaseDetailTableMapper';
import { SupplierInfoTableMapper } from 'src/app/models/table/mappers/SupplierInfoTableMapper/SupplierInfoTableMapper';
import { CrudTarget } from 'src/app/models/type/CrudTarget';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import { SnackbarService } from 'src/app/services/snackbar/snackbar.service';
import formUtil from 'src/app/utils/form.util';
import { ResourceComponent } from '../resource.component';

/**
 * purchase form
 */
@Component({
  selector: 'app-purchase',
  templateUrl: './purchase.component.html',
  styleUrls: ['./purchase.component.scss']
})
export class PurchaseComponent extends ResourceComponent {

  /**
   * form title
   */
  override title: string = 'resource.purchase';

  /**
   * the supplier api url
   */
  supUrl: ApiUrl = ApiUrl.SUPPLIER;

  /**
   * the catalog of suppliers
   */
  suppliers: Supplier[] = [];

  /**
   * the active supplier ingredient catalog
   */
  ingredients: SupplierIngredient[] = [];

  /**
   * map the purchase details for rendering
   */
  tableMapper: PurchaseDetailTableMapper = new PurchaseDetailTableMapper();

  /**
   * map the information for rendering
   */
  infoMapper: SupplierInfoTableMapper = new SupplierInfoTableMapper();

  /**
   * title of the informations table
   */
  infoTitle: string = 'dialog.purchase.title';

  /**
   * class constructor
   * @param fb angular form builder
   * @param snackBarService allow acces to the application snackbar
   * @param data crud action to perform and related informations
   * @param translate internationalization tool
   * @param baseRepo the repository service
   */
  constructor(
    public override fb: FormBuilder,
    public override snackBarService: SnackbarService,
    @Inject(MAT_DIALOG_DATA) public override data: CrudTarget,
    private translate: TranslateService,
    private baseRepo: BaseRepositoryService
  ) {
    super(fb, snackBarService, data);
    this.getSuppliers();
  }

  /**
   * the resource form
   */
  override form = this.fb.group({
    supplier: [null, Validators.required],
    ingredients: this.fb.array([])
  });
  get ing() {return this.form.controls['ingredients'] as FormArray};

  /**
   * set form values for read and update crud states
   * @param purchase purchase to display
   */
  override setFormValues(purchase: Purchase){
    this.infoMapper.updateData(purchase.supplier);
    this.f.supplier.setValue(purchase.supplier.id);
    purchase.PurchaseDetails?.forEach((i) => {
      this.addIng(purchase.supplier, i)
    });
  }

  /**
   * compile validation errors in a array
   * @returns an array of error messages
   */
  override getErrorsMsg(): string[]{
    const checkIng = this.checkIngredientsInputs();
    const errors: string[] = [];
    if(this.f['supplier'].errors?.required) errors.push(this.translate.instant('errors.required'));
    if(checkIng.requiredError) errors.push(this.translate.instant('errors.ing'));
    if(checkIng.patternError) errors.push(this.translate.instant('errors.nbrs'));
    return errors;
  }

  /**
   * ingredient inputs validation
   * @returns an object of validation errors
   */
  checkIngredientsInputs(): any {
    let valid: any = {};
    if (this.f['ingredients'].errors?.required) valid.requiredError = true;
    this.f['ingredients'].controls.forEach((i: any) => {
      if (i.controls.ingId.errors?.required || i.controls.ingQty.errors?.required) valid.requiredError = true;
      if (i.controls.ingQty.errors?.pattern) valid.patternError = true;
    });
    return valid;
  }

  /**
   * create the request body
   * @returns the request body as a created purchase
   */
  override getBody(): CreatePurchase {
    const body: CreatePurchase = {
      supplier: this.f['supplier'].value,
      PurchaseDetails: []
    }
    this.ing.value.forEach((v: any) => {
      body.PurchaseDetails?.push({
        ingredientSupplierId: v.ingId.id,
        price: v.ingId.price,
        quantity: v.ingQty
      })
    })
    return body;
  }

  /**
   * create a set of ingredient inputs
   * @param supplier the supplier the purchase is adress to
   * @param detail one of purchase details of the order
   */
  addIng(supplier?: Supplier, detail?: PurchaseDetail){
    const ingID = supplier?.ingredients?.find(i => i.id == detail?.ingredientSupplierId);
    const ing: FormGroup = this.fb.group({
      ingId: [ingID ? ingID : '', Validators.required],
      ingQty: [detail?.quantity ? detail.quantity : 1, formUtil.nbrs]
    });
    this.f.ingredients.push(ing);
    if (this.readonly) this.f.ingredients.disable();
    this.updateTable();
  }

  /**
   * remove a set on ingredient inputs
   * @param i the ingredients set of inputs index
   */
  deleteIng(i: number){
    this.f.ingredients.removeAt(i);
    this.updateTable();
  }

  /**
   * remove all ingredients inputs from the form
   */
  cleanIng(){
    this.f.ingredients.clear();
    this.updateTable();
  }

  /**
   * get the supplier catalog
   */
  getSuppliers(){
    this.baseRepo.getAll(this.supUrl).then((res: any) => {
      this.suppliers = res.body;
      this.suppliers = this.suppliers.filter(s => s.isActive == true);
    });
  }

  /**
   * update the supplier the order is address to if the user change it
   * @param supID the supplier ID
   */
  getSupplierInfo(supID?: string){
    this.cleanIng();
    this.updateTable();
    const supplier = this.suppliers.find(s => s.id == supID);
    if (supplier && supplier.ingredients) {
      this.ingredients = supplier.ingredients;
      this.infoMapper.updateData(supplier);
    }
  }

  /**
   * update the table displaying the ordered ingredients
   */
  updateTable(){
    this.tableMapper.addMultipleRows(this.ing.value);
  }

}
