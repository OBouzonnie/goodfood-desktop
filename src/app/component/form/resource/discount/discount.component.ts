import { Component, Inject, OnInit } from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";
import { MAT_DIALOG_DATA } from "@angular/material/dialog";
import { TranslateService } from "@ngx-translate/core";
import * as moment from "moment";
import { Discount, Recipe } from "src/app/models/apis/models";
import { ApiUrl } from "src/app/models/enums/ApiUrl";
import { CrudTarget } from "src/app/models/type/CrudTarget";
import { BaseRepositoryService } from "src/app/services/repository/base-repository.service";
import { SnackbarService } from "src/app/services/snackbar/snackbar.service";
import formUtil from "src/app/utils/form.util";
import { ResourceComponent } from "../resource.component";

/**
 * discount form
 */
@Component({
  selector: 'app-discount',
  templateUrl: './discount.component.html',
  styleUrls: ['./discount.component.scss']
})
export class DiscountComponent extends ResourceComponent implements OnInit {

  /**
   * form title
   */
  override title: string = 'resource.discount';

  /**
   * the existing discounts
   */
  discounts: Discount[] = [];

  /**
   * the recipe api url
   */
  recUrl: ApiUrl = ApiUrl.RECIPE;

  /**
   * the catalog of recipe
   */
  recipes: Recipe[] = [];

  /**
   * class constructor
   * @param fb angular form builder
   * @param snackBarService allow acces to the application snackbar
   * @param data crud action to perform and related informations
   * @param translate internationalization tool
   * @param baseRepo the repository service
   */
  constructor(
    public override fb: FormBuilder,
    public override snackBarService: SnackbarService,
    @Inject(MAT_DIALOG_DATA) public override data: CrudTarget,
    private translate: TranslateService,
    private baseRepo: BaseRepositoryService
  ) {
    super(fb, snackBarService, data);
    this.f['value'].addValidators(Validators.required);
    this.f['start'].addValidators(Validators.required);
    this.f['end'].addValidators(Validators.required);
    this.getRecipes();
  }

  /**
   * the resource form
   */
  override form = this.fb.group({
    recipe: [null],
    value: [null, formUtil.nbrs],
    start: [null],
    end: [null]
  });

  /**
   * set form values for read and update crud states
   * @param discount discount to display
   */
  override setFormValues(discount: Discount){
    this.f.start.setValue(moment(discount.start, 'YYYY-MM-DD'));
    this.f.end.setValue(moment(discount.end, 'YYYY-MM-DD'));
    this.f.recipe.setValue(discount.recipe);
    this.f.value.setValue(discount.value);
  }

  /**
   * compile validation errors in a array
   * @returns an array of error messages
   */
  override getErrorsMsg(): string[]{
    const errors: string[] = [];
    if(
      this.f['value'].errors?.required ||
      this.f['start'].errors?.required ||
      this.f['end'].errors?.required
    ) errors.push(this.translate.instant('errors.required'));
    if(this.f['value'].errors?.pattern) errors.push(this.translate.instant('errors.nbrs'));
    return errors;
  }

  /**
   * create the request body
   * @returns the request body as a discount
   */
  override getBody(): Discount {
    const body: Discount = {
      id: this.data.resource ? this.data.resource.id : undefined,
      recipe: this.f['recipe'].value,
      value: parseInt(this.f['value'].value),
      end:  moment(this.f.end.value._d).format('DD/MM/YYYY'),
      start:  moment(this.f.start.value._d).format('DD/MM/YYYY'),
    };
    return body;
  }

  /**
   * get the recipe catalog
   */
  getRecipes(){
    this.baseRepo.getAll(this.recUrl).then((res: any) => {
      this.recipes = res.body;
      this.recipes = this.recipes.filter(r => r.isActive);
      if(!this.data.franchise) this.recipes = this.recipes.filter(r => r.isStandard);
      if(this.data.franchise && this.data.actives) {
        const arr: any = [];
        this.recipes.forEach((r: Recipe) => {
          if(r.id && this.data.actives?.includes(r.id)){
            arr.push(r);
          }
        })
        this.recipes = arr;
      }
    });
  }

}
