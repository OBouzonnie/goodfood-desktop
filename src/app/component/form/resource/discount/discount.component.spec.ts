import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AppModule } from 'src/app/app.module';
import { Discount, Recipe } from 'src/app/models/apis/models';
import { CRUD } from 'src/app/models/enums/CRUD';
import { MockService } from 'src/app/services/mock/mock.service';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';

import { DiscountComponent } from './discount.component';

describe('DiscountComponent', () => {
  let component: DiscountComponent;
  let fixture: ComponentFixture<DiscountComponent>;
  let template: HTMLElement;
  let mockService: MockService;
  let resources: Discount[];
  let recipes: Recipe[];
  let index: number;
  let baseRepo: BaseRepositoryService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
        imports: [AppModule],
        declarations: [ DiscountComponent ],
        providers: [
          FormBuilder,
          { provide: MAT_DIALOG_DATA, useValue: {} }
        ]
      })
      .compileComponents();
  });

  beforeEach(() => {
    mockService = new MockService();
    recipes = mockService.getRecipes();
    baseRepo = TestBed.inject(BaseRepositoryService);
    spyOn(baseRepo, 'getAll').and.callThrough().and.returnValue(Promise.resolve({body: recipes}));
    fixture = TestBed.createComponent(DiscountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    template = fixture.nativeElement;
    mockService = new MockService();
    resources = mockService.getDiscounts();
    index = Math.floor(Math.random() * resources.length);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  /**
   * Rendering
   */
  it('should render 3 inputs & 1 select by default', () => {
    const form = template.querySelector('form');
    const inputs = form?.querySelectorAll('input');
    const selects = form?.querySelectorAll('mat-select');
    expect(inputs).toBeTruthy();
    expect(selects).toBeTruthy();
    expect(inputs?.length).toBe(3);
    expect(selects?.length).toBe(1);
  });

  it('should render an enabled form for CRUD.CREATE state', () => {
    component.data = {action: CRUD.CREATE, uid: ''};
    component.ngOnInit();
    fixture.detectChanges();
    template = fixture.nativeElement;
    const form = template.querySelector('form');
    const inputs = form?.querySelectorAll('input');
    inputs?.forEach(i => {
      expect(i.disabled).toBeFalsy();
    });
  });

});
