import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BaseModel } from 'src/app/models/apis/models';
import { CRUD } from 'src/app/models/enums/CRUD';
import { SnackBarCSS } from 'src/app/models/enums/SnackBarCSS';
import { IForm } from 'src/app/models/interfaces/form.interface';
import { CrudTarget } from 'src/app/models/type/CrudTarget';
import { SnackbarService } from 'src/app/services/snackbar/snackbar.service';

/**
 * resources form parent class
 */
@Component({
  selector: 'app-resource',
  templateUrl: './resource.component.html',
  styleUrls: ['./resource.component.scss']
})
export class ResourceComponent implements OnInit, IForm {

  /**
   * class constructor
   * @param fb angular form builder
   * @param snackBarService allow the use of the application snackbar
   * @param data the crud action to perform and related data
   */
  constructor(
    public fb: FormBuilder,
    public snackBarService: SnackbarService,
    @Inject(MAT_DIALOG_DATA) public data: CrudTarget
  ) { }

  /**
   * form submission event
   */
  @Output() submitted: EventEmitter<BaseModel|null> = new EventEmitter();

  /**
   * return the form controls
   */
  get f(): any {return this.form.controls;}

  /**
   * the resource form
   */
  form: FormGroup = this.fb.group({});

  /**
   * the form title
   */
  title: string = '';

  /**
   * readonly status - form is disabled for a true value
   */
  readonly: boolean = false;

  ngOnInit(){
    if (this.data) {
      if (this.data.action === CRUD.READ) this.readonly = true;
      if (this.readonly) this.form.disable();
      if (this.data.resource) this.setFormValues(this.data.resource);
    }
  }

  /**
   * set form values for read and update crud states
   * @param resource resource to display
   */
  setFormValues(resource: BaseModel){
    console.log(resource);
  }

  /**
   * emit a null submitted event or execute the form submission
   * @param event confirmation event
   */
  handleModalConfirm(event: boolean): void{
    if (event) {
      this.submitForm();
      return;
    }
    this.submitted.emit(null);
  }

  /**
   * form submission
   */
  submitForm(): void{
    if (!this.form.valid){
      const errors: string[] = this.getErrorsMsg();
      if (errors.length > 0) this.snackBarService.openSnackbar(errors.join('\n'), SnackBarCSS.ERROR);
    }
    if(this.form.valid){
      const body = this.getBody();
      this.submitted.emit(body);
    }
  }

  /**
   * compile validation errors in a array
   * @returns an array of error messages
   */
  getErrorsMsg(): string[]{
    const errors: string[] = [];
    return errors;
  }

  /**
   * create the request body
   * @returns the request body
   */
  getBody(): any{
    return {}
  }
}
