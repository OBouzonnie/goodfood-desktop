import { Component, Inject, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { Ingredient } from 'src/app/models/apis/models';
import { CrudTarget } from 'src/app/models/type/CrudTarget';
import { SnackbarService } from 'src/app/services/snackbar/snackbar.service';
import formUtil from 'src/app/utils/form.util';
import { ResourceComponent } from '../resource.component';

/**
 * ingredient form
 */
@Component({
  selector: 'app-ingredient',
  templateUrl: './ingredient.component.html',
  styleUrls: ['./ingredient.component.scss']
})
export class IngredientComponent extends ResourceComponent implements OnInit {

  /**
   * form title
   */
  override title: string = 'resource.ingredients';

  /**
   * class constructor
   * @param fb angular form builder
   * @param snackBarService allow acces to the application snackbar
   * @param data crud action to perform and related informations
   * @param translate internationalization tool
   */
  constructor(
    public override fb: FormBuilder,
    public override snackBarService: SnackbarService,
    @Inject(MAT_DIALOG_DATA) public override data: CrudTarget,
    private translate: TranslateService,
  ) {
    super(fb, snackBarService, data);
    this.f['name'].addValidators(Validators.required);
  }

  /**
   * the resource form
   */
  override form = this.fb.group({
    name: [null, formUtil.txt],
    allergens: this.fb.array([])
  });

  /**
   * get allergens form controls
   */
  get allergens() {return this.form.controls['allergens'] as FormArray};

  /**
   * set form values for read and update crud states
   * @param ing ingredient to display
   */
  override setFormValues(ing: Ingredient){
    this.f.name.setValue(ing.name);
    ing.allergen?.forEach((a) => {
      this.addAllergen(a);
    });
  }

  /**
   * compile validation errors in a array
   * @returns an array of error messages
   */
  override getErrorsMsg(): string[]{
    const errors: string[] = [];
    const allergensValidation: any = this.checkAllergensInputs();
    if(this.f['name'].errors?.required || !allergensValidation.required) errors.push(this.translate.instant('errors.required'));
    if(this.f['name'].errors?.pattern || !allergensValidation.pattern) errors.push(this.translate.instant('errors.forbiddenChars'));
    if(this.f['name'].errors?.maxlength || !allergensValidation.length) errors.push(this.translate.instant('errors.txtLength'));
    return errors;
  }

  /**
   * validate allergen inputs
   * @returns an object of validation errors
   */
  checkAllergensInputs(): any {
    let requiredIsValid: boolean = true;
    let patternIsValid: boolean = true;
    let lengthIsValid: boolean = true;
    this.f['allergens'].controls.forEach((i: any) => {
      if (i.controls.allergen.errors?.required) requiredIsValid = false;
      if (i.controls.allergen.errors?.pattern) patternIsValid = false;
      if (i.controls.allergen.errors?.maxlength) lengthIsValid = false;
    });
    return {required: requiredIsValid, pattern: patternIsValid, length : lengthIsValid};
  }

  /**
   * add one set of allergen inputs to the form
   * @param a the allergen
   */
  addAllergen(a?: string){
    const allergen: FormGroup = this.fb.group({allergen : [a ? a : '', formUtil.txt]});
    allergen.controls['allergen'].addValidators(Validators.required);
    this.f.allergens.push(allergen);
    if (this.readonly) this.f.allergens.disable();
  }

  /**
   * remove one set of allergen inputs from the form
   * @param i the allergen inputs index
   */
  deleteAllergen(i: number){
    this.f.allergens.removeAt(i);
  }

  /**
   * remove all allergen inputs from the form
   */
  cleanAllergens(){
    this.f.allergens.clear();
  }

  /**
   * create the request body
   * @returns the request body as an ingredient
   */
  override getBody(): Ingredient {
    const body: Ingredient = {
      id: this.data.resource ? this.data.resource.id : undefined,
      name: this.f['name'].value,
      allergen: [],
    }
    this.allergens.value.forEach((v: any) => {
      console.log(v)
      body.allergen?.push(v.allergen);
    })
    return body;
  }


}
