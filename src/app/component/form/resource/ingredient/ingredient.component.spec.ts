import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import faker from '@faker-js/faker';
import { AppModule } from 'src/app/app.module';
import { Ingredient } from 'src/app/models/apis/models';
import { CRUD } from 'src/app/models/enums/CRUD';
import { MockService } from 'src/app/services/mock/mock.service';

import { IngredientComponent } from './ingredient.component';

describe('IngredientComponent', () => {
  let component: IngredientComponent;
  let fixture: ComponentFixture<IngredientComponent>;
  let template: HTMLElement;
  let mockService: MockService;
  let resources: Ingredient[];
  let index: number;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule],
      declarations: [ IngredientComponent ],
      providers: [
        FormBuilder,
        { provide: MAT_DIALOG_DATA, useValue: {} }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IngredientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    template = fixture.nativeElement;
    mockService = new MockService();
    resources = mockService.getIngredients();
    index = Math.floor(Math.random() * resources.length);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  /**
   * Rendering
   */

  it('should render 1 input by default', () => {
    const form = template.querySelector('form');
    const inputs = form?.querySelectorAll('input');
    expect(inputs).toBeTruthy();
    expect(inputs?.length).toBe(1);
  });

  it('should render additionnal inputs and selects for a CRUD.READ state', () => {
    component.data = {action: CRUD.READ, uid: resources[index].id || '', resource: resources[index]};
    component.ngOnInit();
    fixture.detectChanges();
    template = fixture.nativeElement;
    const form = template.querySelector('form');
    const inputs = form?.querySelectorAll('input');
    const length = resources[index].allergen;
    expect(inputs?.length).toBe(1 + (length ? length.length : 0));
  });

  it('should render additionnal inputs and selects for a CRUD.UPDATE state', () => {
    component.data = {action: CRUD.UPDATE, uid: resources[index].id || '', resource: resources[index]};
    component.ngOnInit();
    fixture.detectChanges();
    template = fixture.nativeElement;
    const form = template.querySelector('form');
    const inputs = form?.querySelectorAll('input');
    const length = resources[index].allergen;
    expect(inputs?.length).toBe(1 + (length ? length.length : 0));
  });

  it('should not render additionnal inputs and selects for a CRUD.CREATE state', () => {
    component.data = {action: CRUD.CREATE, uid: ''};
    component.ngOnInit();
    fixture.detectChanges();
    template = fixture.nativeElement;
    const form = template.querySelector('form');
    const inputs = form?.querySelectorAll('input');
    expect(inputs?.length).toBe(1);
  });

  it('should not render any ingredients btn for a CRUD.READ state', () => {
    component.data = {action: CRUD.READ, uid: resources[index].id || '', resource: resources[index]};
    component.ngOnInit();
    fixture.detectChanges();
    template = fixture.nativeElement;
    const form = template.querySelector('form');
    const addBtn = form?.querySelector('#ing-add-btn');
    const delAllBtn = form?.querySelector('#ing-delete-all-btn');
    const delBtns = form?.querySelectorAll('.ing-delete-btn');
    expect(addBtn).toBeFalsy();
    expect(delAllBtn).toBeFalsy();
    expect(delBtns?.length).toBe(0);
  });

  it('should render ingredients btn for a CRUD.UPDATE state', () => {
    component.data = {action: CRUD.UPDATE, uid: resources[index].id || '', resource: resources[index]};
    component.ngOnInit();
    fixture.detectChanges();
    template = fixture.nativeElement;
    const form = template.querySelector('form');
    const addBtn = form?.querySelector('#ing-add-btn');
    const delAllBtn = form?.querySelector('#ing-delete-all-btn');
    const delBtns = form?.querySelectorAll('.ing-delete-btn');
    expect(addBtn).toBeTruthy();
    expect(delAllBtn).toBeTruthy();
    expect(delBtns?.length).not.toBe(0);
  });

  it('should render ingredients btn for a CRUD.CREATE state', () => {
    component.data = {action: CRUD.CREATE, uid: ''};
    component.ngOnInit();
    fixture.detectChanges();
    template = fixture.nativeElement;
    const form = template.querySelector('form');
    const addBtn = form?.querySelector('#ing-add-btn');
    const delAllBtn = form?.querySelector('#ing-delete-all-btn');
    const delBtns = form?.querySelectorAll('.ing-delete-btn');
    expect(addBtn).toBeTruthy();
    expect(delAllBtn).toBeTruthy();
    expect(delBtns?.length).toBe(0);
  });

  it('should render a disabled form for CRUD.READ state', () => {
    component.data = {action: CRUD.READ, uid: resources[index].id || '', resource: resources[index]};
    component.ngOnInit();
    fixture.detectChanges();
    template = fixture.nativeElement;
    const form = template.querySelector('form');
    const inputs = form?.querySelectorAll('input');
    inputs?.forEach(i => {
      expect(i.disabled).toBeTruthy();
    });
  });

  it('should render an enabled form for CRUD.UPDATE state', () => {
    component.data = {action: CRUD.UPDATE, uid: resources[index].id || '', resource: resources[index]};
    component.ngOnInit();
    fixture.detectChanges();
    template = fixture.nativeElement;
    const form = template.querySelector('form');
    const inputs = form?.querySelectorAll('input');
    inputs?.forEach(i => {
      expect(i.disabled).toBeFalsy();
    });
  });

  it('should render an enabled form for CRUD.CREATE state', () => {
    component.data = {action: CRUD.CREATE, uid: ''};
    component.ngOnInit();
    fixture.detectChanges();
    template = fixture.nativeElement;
    const form = template.querySelector('form');
    const inputs = form?.querySelectorAll('input');
    inputs?.forEach(i => {
      expect(i.disabled).toBeFalsy();
    });
  });

  /**
   * Allergens inputs
   */
  it('does not render allergens inputs at init for a CRUD.CREATE state', () => {
    component.data = {action: CRUD.CREATE, uid: ''};
    component.ngOnInit();
    fixture.detectChanges();
    template = fixture.nativeElement;
    const ingFormFields = template.querySelectorAll('.ing-form-field');
    expect(ingFormFields.length).toBe(0);
  });

  /**
   * Interaction
   */

   it('should add inputs when add allergens btn is clicked', () => {
    component.ngOnInit();
    fixture.detectChanges();
    template = fixture.nativeElement;
    let form = template.querySelector('form');
    let btn = template.querySelector('#ing-add-btn');
    let ings = form?.querySelectorAll('.ing-form-field');
    const ingsCount = ings?.length;
    btn?.dispatchEvent(new Event('click'));
    fixture.detectChanges();
    form = template.querySelector('form');
    template = fixture.nativeElement;
    ings = form?.querySelectorAll('.ing-form-field');
    const ingsDelta = ings && (ingsCount || ingsCount == 0) ?  ings.length - ingsCount : 0;
    expect(ingsDelta > 0).toBeTrue();
  });

  it('should remove inputs when delete all ingredients btn is clicked', () => {
    component.data = {action: CRUD.UPDATE, uid: resources[index].id || '', resource: resources[index]};
    component.ngOnInit();
    fixture.detectChanges();
    template = fixture.nativeElement;
    let form = template.querySelector('form');
    let btn = template.querySelector('#ing-delete-all-btn');
    let ings = form?.querySelectorAll('.ing-form-field');
    expect(ings?.length).not.toBe(0);
    btn?.dispatchEvent(new Event('click'));
    fixture.detectChanges();
    form = template.querySelector('form');
    template = fixture.nativeElement;
    ings = form?.querySelectorAll('.ing-form-field');
    expect(ings?.length).toBe(0);
  });

  it('should remove inputs when delete all ingredients btn is clicked', () => {
    component.data = {action: CRUD.UPDATE, uid: resources[index].id || '', resource: resources[index]};
    component.ngOnInit();
    fixture.detectChanges();
    template = fixture.nativeElement;
    let form = template.querySelector('form');
    let btn = template.querySelector('#ing-delete-all-btn');
    let ings = form?.querySelectorAll('.ing-form-field');
    expect(ings?.length).not.toBe(0);
    btn?.dispatchEvent(new Event('click'));
    fixture.detectChanges();
    form = template.querySelector('form');
    template = fixture.nativeElement;
    ings = form?.querySelectorAll('.ing-form-field');
    expect(ings?.length).toBe(0);
  });

  it('should remove inputs when delete ingredients btn is clicked', () => {
    component.data = {action: CRUD.UPDATE, uid: resources[index].id || '', resource: resources[index]};
    component.ngOnInit();
    fixture.detectChanges();
    template = fixture.nativeElement;
    let form = template.querySelector('form');
    let btn = template.querySelectorAll('.ing-delete-btn');
    let ings = form?.querySelectorAll('.ing-form-field');
    const ingsCount = ings?.length;
    const i = ingsCount ? Math.floor(Math.random() * ingsCount) : -1;
    btn[i]?.dispatchEvent(new Event('click'));
    fixture.detectChanges();
    form = template.querySelector('form');
    template = fixture.nativeElement;
    ings = form?.querySelectorAll('.ing-form-field');
    const ingsDelta = ings && (ingsCount || ingsCount == 0) ?  ings.length - ingsCount : 0;
    expect(ingsDelta < 0).toBeTrue();
  });

  /**
   * Methods
   */

  it('setFormValues should set form values', () => {
    expect(component.f.name.value).toBeNull();
    const i = Math.floor(Math.random() * resources.length);
    const ing = resources[i];
    component.setFormValues(ing);
    expect(component.f.name.value).toBe(ing.name);
    component.f.allergens.value.forEach((a: any, i: number) => {
      const allergen = ing.allergen ? ing.allergen : null;
      expect(a.allergen).toBe(allergen ? allergen[i] : null);
    });
  });

  it('addAllergen should increase allergen form array length', () => {
    expect(component.allergens.controls.length).toBe(0);
    const allergen = faker.word.noun();
    const loop = Math.floor(Math.random()* 10);
    for (let i = 0; i < loop; i++){
      component.addAllergen(allergen);
    }
    expect(component.allergens.controls.length).toBe(loop);
  });

  /**
   * Form submission
   */
  it('invalid form submission should not emit a submitted event', () => {
    spyOn(component.submitted, 'emit');
    component.submitForm();
    expect(component.submitted.emit).not.toHaveBeenCalled();
  });

  it('valid form submission should emit a submitted event', () => {
    spyOn(component.submitted, 'emit');
    const i = Math.floor(Math.random() * resources.length);
    const ing = resources[i];
    component.f['name'].setValue(faker.name.lastName());
    ing.allergen?.forEach(a => {
      component.addAllergen(a);
    });
    component.submitForm();
    expect(component.submitted.emit).toHaveBeenCalled();
  });
});
