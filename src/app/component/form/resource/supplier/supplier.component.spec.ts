import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AppModule } from 'src/app/app.module';
import { Supplier } from 'src/app/models/apis/models';
import { CRUD } from 'src/app/models/enums/CRUD';
import { MockService } from 'src/app/services/mock/mock.service';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';

import { SupplierComponent } from './supplier.component';

describe('SupplierComponent', () => {
  let component: SupplierComponent;
  let fixture: ComponentFixture<SupplierComponent>;
  let template: HTMLElement;
  let mockService: MockService;
  let resources: Supplier[];
  let ingredients: number[];
  let index: number;
  let baseRepo: BaseRepositoryService;
  let getIngSpy: jasmine.Spy;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
        imports: [AppModule],
        declarations: [ SupplierComponent ],
        providers: [
          FormBuilder,
          { provide: MAT_DIALOG_DATA, useValue: {} }
        ]
      })
      .compileComponents();
  });

  beforeEach(() => {
    mockService = new MockService();
    baseRepo = TestBed.inject(BaseRepositoryService);
    getIngSpy = spyOn(baseRepo, 'getAll').and.callThrough().and.returnValue(Promise.resolve({body: ingredients}));
    fixture = TestBed.createComponent(SupplierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    template = fixture.nativeElement;
    mockService = new MockService();
    resources = mockService.getSuppliers();
    index = Math.floor(Math.random() * resources.length);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  /**
   * Rendering
   */

  it('should render 3 inputs by default', () => {
    const form = template.querySelector('form');
    const inputs = form?.querySelectorAll('input');
    expect(inputs).toBeTruthy();
    expect(inputs?.length).toBe(3);
  });

  it('should render a disabled form for CRUD.READ state', () => {
    component.data = {action: CRUD.READ, uid: resources[index].id || '', resource: resources[index]};
    component.ngOnInit();
    fixture.detectChanges();
    template = fixture.nativeElement;
    const form = template.querySelector('form');
    const inputs = form?.querySelectorAll('input');
    inputs?.forEach(i => {
      expect(i.disabled).toBeTruthy();
    });
  });

  it('should render an enabled form for CRUD.UPDATE state', () => {
    component.data = {action: CRUD.UPDATE, uid: resources[index].id || '', resource: resources[index]};
    component.ngOnInit();
    fixture.detectChanges();
    template = fixture.nativeElement;
    const form = template.querySelector('form');
    const inputs = form?.querySelectorAll('input');
    inputs?.forEach(i => {
      expect(i.disabled).toBeFalsy();
    });
  });

  it('should render an enabled form for CRUD.CREATE state', () => {
    component.data = {action: CRUD.CREATE, uid: ''};
    component.ngOnInit();
    fixture.detectChanges();
    template = fixture.nativeElement;
    const form = template.querySelector('form');
    const inputs = form?.querySelectorAll('input');
    inputs?.forEach(i => {
      expect(i.disabled).toBeFalsy();
    });
  });

  /**
   * Methods
   */
  it('setFormValues should set form values', () => {
    expect(component.f.email.value).toBeNull();
    expect(component.f.phone.value).toBeNull();
    expect(component.f.name.value).toBeNull();
    expect(component.f.ingredients.length).toBe(0);
    const i = Math.floor(Math.random() * resources.length);
    const supplier = resources[i];
    component.setFormValues(supplier);
    expect(component.f.email.value).toBe(supplier.email);
    expect(component.f.phone.value).toBe(supplier.phone);
    expect(component.f.name.value).toBe(supplier.name);
    expect(component.f.ingredients.length).not.toBe(0);
  });

});
