import { Component, Inject, OnInit } from '@angular/core';
import { ResourceComponent } from '../resource.component';
import { TranslateService } from '@ngx-translate/core';
import { CrudTarget } from 'src/app/models/type/CrudTarget';
import { SnackbarService } from 'src/app/services/snackbar/snackbar.service';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Supplier } from 'src/app/models/apis/models/supplier';
import formUtil from 'src/app/utils/form.util';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import { ApiUrl } from 'src/app/models/enums/ApiUrl';
import { Ingredient } from 'src/app/models/apis/models';
import formatUtils from 'src/app/utils/format.utils';

/**
 * supplier form
 */
@Component({
  selector: 'app-supplier',
  templateUrl: './supplier.component.html',
  styleUrls: ['./supplier.component.scss']
})
export class SupplierComponent extends ResourceComponent implements OnInit {

  /**
   * form title
   */
  override title: string = 'resource.supplier';

  /**
   * ingredient api url
   */
  ingUrl: ApiUrl = ApiUrl.INGREDIENT;

  /**
   * ingredients catalog
   */
  ingredients: Ingredient[] = [];

  /**
   * class constructor
   * @param fb angular form builder
   * @param snackBarService allow acces to the application snackbar
   * @param data crud action to perform and related informations
   * @param translate internationalization tool
   * @param baseRepo the repository service
   */
  constructor(
    public override fb: FormBuilder,
    public override snackBarService: SnackbarService,
    @Inject(MAT_DIALOG_DATA) public override data: CrudTarget,
    private translate: TranslateService,
    private baseRepo: BaseRepositoryService
  ) {

    super(fb, snackBarService, data);
    this.f['email'].addValidators(Validators.required);
    this.f['name'].addValidators(Validators.required);
    this.getIngredients();
  }

  /**
   * the resource form
   */
  override form = this.fb.group({
    email: [null, formUtil.email],
    name: [null, formUtil.txt],
    phone: [null, formUtil.txt],
    ingredients: this.fb.array([])
  });


  /**
   * set form values for read and update crud states
   * @param supplier supplier to display
   */
  override setFormValues(supplier: Supplier){
    this.f.email.setValue(supplier.email);
    this.f.name.setValue(supplier.name);
    this.f.phone.setValue(supplier.phone);
    supplier.ingredients?.forEach((a) => {
      this.addIng(a);
    });
  }

  /**
   * compile validation errors in a array
   * @returns an array of error messages
   */
  override getErrorsMsg(): string[]{
    const checkIng = this.checkIngredientsInputs();
    const errors: string[] = [];
    if(
      this.f['email'].errors?.required ||
      this.f['name'].errors?.required ||
      this.f['phone'].errors?.required
    ) errors.push(this.translate.instant('errors.required'));
    if(checkIng.requiredError) errors.push(this.translate.instant('errors.ing'));
    if(
      this.f['email'].errors?.maxlength ||
      this.f['name'].errors?.maxlength ||
      this.f['phone'].errors?.maxlength
    ) errors.push(this.translate.instant('errors.txtLength'));
    if(
      this.f['email'].errors?.pattern ||
      this.f['name'].errors?.pattern ||
      this.f['phone'].errors?.pattern
    ) errors.push(this.translate.instant('errors.forbiddenChars'));
    if(checkIng.patternError) errors.push(this.translate.instant('errors.prices'));
    return errors;
  }

  /**
   * ingredients input validation
   * @returns an object of active errors
   */
  checkIngredientsInputs(): any {
    let valid: any = {};
    if (this.f['ingredients'].errors?.required) valid.requiredError = true;
    this.f['ingredients'].controls.forEach((i: any) => {
      if (i.controls.ingId.errors?.required || i.controls.ingPrice.errors?.required) valid.requiredError = true;
      if (i.controls.ingPrice.errors?.pattern) valid.patternError = true;
    });
    return valid;
  }

  /**
   * get ingredients form controls
   */
  get ing() {return this.form.controls['ingredients'] as FormArray};

  /**
   * create the request body
   * @returns the request body as a supplier
   */
  override getBody(): Supplier {
    const body: Supplier = {
      id: this.data.resource ? this.data.resource.id : undefined,
      name: this.f['name'].value,
      email: this.f['email'].value,
      phone: this.f['phone'].value,
      ingredients: [],
    }
    this.ing.value.forEach((v: any) => {
        body.ingredients?.push({
          id: v.ingId,
          price: formatUtils.toCentInteger(v.ingPrice)
        })
      })
    return body;
  }

  /**
   * add an ingredient inputs to the form
   * @param i ingredient field values
   */
  addIng(i?: any){
    const ing: FormGroup = this.fb.group({
      ingId: [i ? i.id : '', Validators.required],
      ingPrice: [i ? formatUtils.toDecimalString(i.price) : undefined, formUtil.prices]
    });
    this.f.ingredients.push(ing);
    if (this.readonly) this.f.ingredients.disable();
  }

  /**
   * remove an ingredient inputs from the form
   * @param i the index of the ingredient inputs
   */
  deleteIng(i: number){
    this.f.ingredients.removeAt(i);
  }

  /**
   * remove all ingredients inputs from the form
   */
  cleanIng(){
    this.f.ingredients.clear();
  }

  /**
   * get the ingredient catalog
   */
  getIngredients(){
    this.baseRepo.getAll(this.ingUrl).then((res: any) => this.ingredients = res.body);
  }

}
