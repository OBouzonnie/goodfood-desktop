import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TranslateModule } from '@ngx-translate/core';
import { AppModule } from 'src/app/app.module';
import { CRUD } from 'src/app/models/enums/CRUD';
import { MockService } from 'src/app/services/mock/mock.service';

import { OrderComponent } from './order.component';

describe('OrderComponent', () => {
  let component: OrderComponent;
  let fixture: ComponentFixture<OrderComponent>;
  let template: HTMLElement;
  let mockService: MockService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule, TranslateModule],
      declarations: [ OrderComponent ],
      providers: [
        FormBuilder,
        { provide: MAT_DIALOG_DATA, useValue: {} }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    template = fixture.nativeElement;
    mockService = new MockService();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render all addresses', waitForAsync(() => {
    component.addresses = mockService.getAddresses();
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      component.addresses.forEach(a => {
        expect(template.textContent).toContain(a.city);
        expect(template.textContent).toContain(a.postal);
        expect(template.textContent).toContain(a.number);
        expect(template.textContent).toContain(a.street);
      });
    });
  }));

  it('should render one radio for each address', waitForAsync(() => {
    component.addresses = mockService.getAddresses();
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      const radio = template.querySelectorAll('mat-radio-button');
      expect(radio.length).toBe(component.addresses.length)
    });
  }));

  it('should not render radio for no address', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      const radio = template.querySelectorAll('mat-radio-button');
      expect(radio.length).toBe(0);
    });
  }));

  it('create address btn should emit CRUD create action', waitForAsync(() => {
    spyOn(component.crudAddress, 'emit');
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      const btn = template.querySelector('#crud-create-btn');
      btn?.dispatchEvent(new Event('click'));
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        expect(component.crudAddress.emit).toHaveBeenCalledWith({action: CRUD.CREATE, uid: ''})
      });
    });
  }));

  it('delete address btn should emit CRUD delete action', waitForAsync(() => {
    component.addresses = mockService.getAddresses();
    const i = Math.floor(Math.random() * component.addresses.length);
    spyOn(component.crudAddress, 'emit');
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      const id = component.addresses[i].id;
      const btn = template.querySelector(`#crud-delete-btn${id}`) as HTMLElement;
      btn?.click();
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        expect(component.crudAddress.emit).toHaveBeenCalledWith({action: CRUD.DELETE, uid: id || ''})
      });
    });
  }));
});
