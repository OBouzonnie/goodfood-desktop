import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Address, Order, Restaurant } from 'src/app/models/apis/models';
import { CRUD } from 'src/app/models/enums/CRUD';
import { DeliveryType } from 'src/app/models/enums/Delivery';
import { SnackBarCSS } from 'src/app/models/enums/SnackBarCSS';
import { IForm } from 'src/app/models/interfaces/form.interface';
import { CrudTarget } from 'src/app/models/type/CrudTarget';
import { SnackbarService } from 'src/app/services/snackbar/snackbar.service';

/**
 * the order form
 */
@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements IForm {

  /**
   * the adress crud event
   */
  @Output() crudAddress: EventEmitter<CrudTarget> = new EventEmitter();

  /**
   * the submission event
   */
  @Output() submitted: EventEmitter<any|null> = new EventEmitter();

  /**
   * the delivery mode options
   */
  deliveryMode = DeliveryType;

  /**
   * the adress state - no adress necessary if false
   */
  deliveryAddressNeeded: boolean = true;

  /**
   * user addresses
   */
  @Input() addresses: Address[] = [];

  /**
   * the franchise the order is adress to
   */
  @Input() franchise: Restaurant|null = null;

  /**
   * the form title
   */
  title: string = 'form.order.title';

  /**
   * return the form controls
   */
  get f(): any {return this.form.controls;}

  /**
   * the order form
   */
  form: FormGroup = this.fb.group({
    address: [null],
    type: [null, Validators.required]
  });

  /**
   *
   * @param snackBarService allow acces to the snackbar pop-up
   * @param fb angular form builder
   * @param translate internationalization tool
   */
  constructor(
    public snackBarService: SnackbarService,
    public fb: FormBuilder,
    public translate: TranslateService
  ) {
  }

  /**
   * emit a create adress event
   */
  newAddress(){
    this.crudAddress.emit({action: CRUD.CREATE, uid: ''});
  }

  /**
   * remove an adress
   * @param id the targeted for deletion adress ID
   */
  deleteAddress(id?: string){
    if(id) this.crudAddress.emit({action: CRUD.DELETE, uid: id});
  }

  /**
   * form submission
   */
  submitForm() {
    if (!this.form.valid || (!this.form.controls['address'].value && this.form.controls['type'].value == DeliveryType.DELIVER)){
      const errors: string[] = this.getErrorsMsg();
      if (errors.length > 0) this.snackBarService.openSnackbar(errors.join('\n'), SnackBarCSS.ERROR);
    }
    if(this.form.valid && ((this.form.controls['address'].value && this.form.controls['type'].value == DeliveryType.DELIVER) || this.form.controls['type'].value == DeliveryType.TAKEAWAY)){
      const body = this.getBody();
      this.submitted.emit(body);
    }
  }

  /**
   * compile validation errors
   * @returns an array of validation errors
   */
  getErrorsMsg() {
    const errors: string[] = [];
    if (this.f.address.errors?.required || (!this.form.controls['address'].value && this.form.controls['type'].value == DeliveryType.DELIVER)) errors.push(this.translate.instant('errors.order_address'));
    if (this.f.type.errors?.required) errors.push(this.translate.instant('errors.order_delivery'));
    return errors;
  }

  /**
   * create the request body
   * @returns the request body as an order
   */
  getBody(): Order {
    const order: Order = {
      delivery: {
        type: this.f.type.value
      }
    }
    if(order.delivery) {
      switch(order.delivery.type){
        case DeliveryType.TAKEAWAY:
          if(this.franchise && this.franchise.address){
            order.address = {
              number: this.franchise.address.number,
              street : this.franchise.address.street,
              postal: this.franchise.address.postal,
              city: this.franchise.address.city
            }
          }
          break;
        case DeliveryType.DELIVER:
          const address = this.addresses.find(a => a.id === this.f.address.value);
          if(address){
            order.address = {
              number: address.number,
              street : address.street,
              postal: address.postal,
              city: address.city
            }
          }
          break;

      }
    }
    return order;
  }

  /**
   * change the delivery mode
   * @param event the delivery select change event
   */
  deliveryModeToggle(event: any){
    this.form.controls['address'].clearValidators();
    if(event.value == DeliveryType.DELIVER){
      this.deliveryAddressNeeded = true;
      this.form.controls['address'].addValidators(Validators.required);
      console.log(event, this.deliveryAddressNeeded)
    }
    if(event.value == DeliveryType.TAKEAWAY) {
      this.deliveryAddressNeeded = false;
      this.form.controls['address'].clearValidators();
    }
  }

}
