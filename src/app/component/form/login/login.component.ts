import { Component, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import FormUtil from 'src/app/utils/form.util';
import {MatDialog} from '@angular/material/dialog';
import { AuthDialogComponent } from 'src/app/component/dialog/auth-dialog/auth-dialog.component';
import {TranslateService} from '@ngx-translate/core';
import { Credential } from 'src/app/models/type/Credential';
import { ForgottenPwdComponent } from '../forgotten-pwd/forgotten-pwd.component';

/**
 * Login form
 */
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  /**
   * is this the back office access page ?
   */
  @Input() backOffice: boolean = false;

  /**
   * Emits a boolean is the login process is successfull
   */
  @Output() submitted: EventEmitter<Credential> = new EventEmitter();

  /**
   * form inputs
   */
  public loginForm = this.fb.group({
    email: [null, FormUtil.email],
    password: [null, FormUtil.password]
  });

  /**
   * Errors messages to be displayed if the submitted form isn't valid
   */
  public errors: string[] = [];

  /**
   * class constructor
   * @param fb angular form builder
   * @param dialog material modal component
   * @param translate internationalization tool
   */
  constructor(
    private fb: FormBuilder,
    private dialog: MatDialog,
    public translate: TranslateService
  ) { }

  /**
   * Handle the form submission
   */
  onSubmit(): void{
    //if the form is not valid, display the needed error messages for the user
    if(!this.loginForm.valid){
      if (this.loginForm.get('password')?.errors?.['pattern'] || this.loginForm.get('email')?.errors?.['pattern']) this.errors.push(this.translate.instant('auth.msg.special'));
      if (this.loginForm.get('password')?.errors?.['minlength']) this.errors.push(this.translate.instant('auth.msg.pwd.length'));
      if (this.loginForm.get('password')?.errors?.['pattern']) this.errors.push(this.translate.instant('auth.msg.pwd.pattern'));
      if (this.loginForm.get('password')?.errors?.['maxlength'] || this.loginForm.get('email')?.errors?.['maxlength']) this.errors.push(this.translate.instant('auth.msg.length'));
      if (this.errors.length > 0) {
        const errorModal = this.dialog.open(AuthDialogComponent, {data: {errors: this.errors}});
        errorModal.afterClosed().subscribe(() => {
          this.errors = [];
        });
      }
    }

    //if the form is valid, emit the credentials
    if(this.loginForm.valid) {
      const credential: Credential = {
        username: this.loginForm.controls['email'].value,
        password: this.loginForm.controls['password'].value
      }
      this.submitted.emit(credential);
    }
  }

  /**
   * open the reset password modal
   */
  resetPwd(){
    this.dialog.open(ForgottenPwdComponent, {data: this.backOffice});
  }
}
