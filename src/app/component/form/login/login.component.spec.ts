import { ComponentFixture, TestBed, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { AppModule } from 'src/app/app.module';

import { LoginComponent } from './login.component';

import { faker } from '@faker-js/faker';
import TestUtil from 'src/test/utils/test.utils';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let template: HTMLElement;
  let email: string = faker.internet.email();
  let pwd: string = faker.internet.password() + Math.floor(Math.random() * 10);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule],
      declarations: [ LoginComponent ],
      providers: [{ provide: ComponentFixtureAutoDetect, useValue: true }]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    template = fixture.nativeElement;
    spyOn(component.submitted, 'emit');
  });

  /*
   * Test component creation
  **/
  it('should create', () => {
    expect(component).toBeTruthy();
  });

  /*
   * Test onSubmit()
  **/
  it('validation success', () => {

    component.loginForm.get('email')?.setValue(email);
    component.loginForm.get('password')?.setValue(pwd);
    expect(component.loginForm.valid).toBeTrue();
    component.onSubmit();
    expect(component.submitted.emit).toHaveBeenCalledWith({username: email, password: pwd});
    if (!component.loginForm.valid) console.log('email  ', email, ' & password ', pwd);
  });

  it('validation fails on empty email', () => {
    component.loginForm.get('password')?.setValue(pwd);
    expect(component.loginForm.valid).toBeFalse();
    component.onSubmit();
    expect(component.submitted.emit).not.toHaveBeenCalled();
  });

  it('validation fails on empty password', () => {
    component.loginForm.get('email')?.setValue(email);
    expect(component.loginForm.valid).toBeFalse();
    component.onSubmit();
    expect(component.submitted.emit).not.toHaveBeenCalled();
  });

  it('validation fails on wrong email syntax', () => {
    component.loginForm.get('email')?.setValue(pwd);
    component.loginForm.get('password')?.setValue(pwd);
    expect(component.loginForm.valid).toBeFalse();
    component.onSubmit();
    expect(component.submitted.emit).not.toHaveBeenCalled();
  });

  it('validation fails on pwd w/o number', () => {
    component.loginForm.get('email')?.setValue(email);
    component.loginForm.get('password')?.setValue(pwd.replace(/[0-9]/g, ''));
    expect(component.loginForm.valid).toBeFalse();
    component.onSubmit();
    expect(component.submitted.emit).not.toHaveBeenCalled();
  });

  it('validation fails on pwd w/o lowercase', () => {
    component.loginForm.get('email')?.setValue(email);
    component.loginForm.get('password')?.setValue(pwd.replace(/[a-z]/g, ''));
    expect(component.loginForm.valid).toBeFalse();
    component.onSubmit();
    expect(component.submitted.emit).not.toHaveBeenCalled();
  });

  it('validation fails on pwd w/o uppercase', () => {
    component.loginForm.get('email')?.setValue(email);
    component.loginForm.get('password')?.setValue(pwd.replace(/[A-Z]/g, ''));
    expect(component.loginForm.valid).toBeFalse();
    component.onSubmit();
    expect(component.submitted.emit).not.toHaveBeenCalled();
  });

  it('validation fails on email with special char', () => {
    const fbdnEmail = TestUtil.useSpecialChar(email);
    component.loginForm.get('email')?.setValue(fbdnEmail.str);
    component.loginForm.get('password')?.setValue(pwd);
    expect(component.loginForm.valid).toBeFalse();
    component.onSubmit();
    expect(component.submitted.emit).not.toHaveBeenCalled();
    if(component.loginForm.valid) console.log('email validation fails on special char : ', fbdnEmail.char)
  });

  it('validation fails on pwd with special char', () => {
    const fbdnPwd = TestUtil.useSpecialChar(pwd);

    component.loginForm.get('email')?.setValue(email);
    component.loginForm.get('password')?.setValue(fbdnPwd.str);
    expect(component.loginForm.valid).toBeFalse();
    component.onSubmit();
    expect(component.submitted.emit).not.toHaveBeenCalled();
    if(component.loginForm.valid) console.log('password validation fails on special char : ', fbdnPwd.char)
  });

  it('validation fails on space in email', () => {
    const spacedEmail = TestUtil.useSpace(email);

    component.loginForm.get('email')?.setValue(spacedEmail);
    component.loginForm.get('password')?.setValue(pwd);
    expect(component.loginForm.valid).toBeFalse();
    component.onSubmit();
    expect(component.submitted.emit).not.toHaveBeenCalled();
    if(component.loginForm.valid) console.log('email validation fails on space : ', spacedEmail)
  });

  it('validation fails on space in pwd', () => {
    const spacedPwd = TestUtil.useSpace(pwd);

    component.loginForm.get('email')?.setValue(email);
    component.loginForm.get('password')?.setValue(spacedPwd);
    expect(component.loginForm.valid).toBeFalse();
    component.onSubmit();
    expect(component.submitted.emit).not.toHaveBeenCalled();
    if(component.loginForm.valid) console.log('password validation fails on space : ', spacedPwd)
  });

  it('validation fails on script in email', () => {
    component.loginForm.get('email')?.setValue(TestUtil.useScript());
    component.loginForm.get('password')?.setValue(pwd);
    expect(component.loginForm.valid).toBeFalse();
    component.onSubmit();
    expect(component.submitted.emit).not.toHaveBeenCalled();
  });

  it('validation fails on script in pwd', () => {
    component.loginForm.get('email')?.setValue(email);
    component.loginForm.get('password')?.setValue(TestUtil.useScript());
    expect(component.loginForm.valid).toBeFalse();
    component.onSubmit();
    expect(component.submitted.emit).not.toHaveBeenCalled();
  });

  it('validation fails on short pwd', () => {
    component.loginForm.get('email')?.setValue(email);
    component.loginForm.get('password')?.setValue(TestUtil.cutPwd(pwd));
    expect(component.loginForm.valid).toBeFalse();
    component.onSubmit();
    expect(component.submitted.emit).not.toHaveBeenCalled();
  });

});
