import { Component, Inject } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { ApiUrl } from 'src/app/models/enums/ApiUrl';
import { SnackBarCSS } from 'src/app/models/enums/SnackBarCSS';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import { SnackbarService } from 'src/app/services/snackbar/snackbar.service';
import formUtil from 'src/app/utils/form.util';

/**
 * the forgotten-password form
 */
@Component({
  selector: 'app-forgotten-pwd',
  templateUrl: './forgotten-pwd.component.html',
  styleUrls: ['./forgotten-pwd.component.scss']
})
export class ForgottenPwdComponent {

  /**
   * form inputs
   */
  get f(): any {return this.form.controls;}

  /**
   * angular form
   */
  public form= this.fb.group({
     email: [null, formUtil.email]
   });

   /**
    * form title
    */
  title: string = 'form.pwd.forgotten';

  /**
   * class constructor
   * @param fb angular form builder
   * @param snackBarService allow access to the material snackbar pop-up
   * @param translate internationalization tool
   * @param baseRepo the repository service
   * @param ref the modal reference
   * @param backOffice is the form from the backoffice ?
   */
  constructor(
    public fb: FormBuilder,
    public snackBarService: SnackbarService,
    public translate: TranslateService,
    private baseRepo: BaseRepositoryService,
    private ref: MatDialogRef<ForgottenPwdComponent>,
    @Inject(MAT_DIALOG_DATA) public backOffice: boolean
  ) {}

  /**
   * handle confirmation event
   * @param event confirmation event
   */
  handleModalConfirm(event: boolean): void{
    if (event) return this.submitForm();
    this.ref.close();
  }

  /**
   * form submission
   */
  submitForm(): void{
    let url: string;
    if (!this.form.valid){
      const errors: string[] = this.getErrorsMsg();
      if (errors.length > 0) this.snackBarService.openSnackbar(errors.join('\n'), SnackBarCSS.ERROR);
    }
    if(this.form.valid){
      if(this.backOffice){
        url = ApiUrl.ADMIN_RESET_PWD;
      } else {
        url = ApiUrl.CUSTOMER_RESET_PWD;
      }
      this.post(url);
    }
  }

  /**
   * calls the repository to post the request
   * @param url api url
   */
  post(url: string): void{
    this.baseRepo.post(url, this.getBody())
      .then(r => {
        this.snackBarService.openSnackbar(this.translate.instant('auth.msg.pwd.reset'), SnackBarCSS.SUCCESS);
        this.ref.close();
      })
      .catch(e => {
        console.error(e);
        this.snackBarService.openSnackbar(this.translate.instant('errors.default'), SnackBarCSS.ERROR);
      })
  }

  /**
   * create the request body
   * @returns the request body as an email object
   */
  getBody(): {email: string}{
    return {
      email: this.f.email.value
    };
  }

  /**
   * compile validation errors
   * @returns an array of validation errors
   */
  getErrorsMsg(): string[]{
    const errors: string[] = [];

    if (this.f.email.errors?.required) errors.push(this.translate.instant('errors.required'));
    if (this.f.email.errors?.pattern) errors.push(this.translate.instant('auth.msg.special'));
    if (this.f.email.errors?.maxlength) errors.push(this.translate.instant('auth.msg.length'));

    return errors;
  }

}
