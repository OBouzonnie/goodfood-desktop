import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { AppModule } from 'src/app/app.module';
import { MaterialModule } from 'src/app/modules/materials/material.module';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { ForgottenPwdComponent } from './forgotten-pwd.component';

describe('ForgottenPwdComponent', () => {
  let component: ForgottenPwdComponent;
  let fixture: ComponentFixture<ForgottenPwdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule, SharedModule, MaterialModule, MatDialogModule],
      declarations: [ ForgottenPwdComponent ],
      providers: [
        { provide: MatDialogRef, useValue: {} }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ForgottenPwdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
