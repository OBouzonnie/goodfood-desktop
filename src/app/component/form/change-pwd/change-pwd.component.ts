import { Component, Inject } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { ApiUrl } from 'src/app/models/enums/ApiUrl';
import { SnackBarCSS } from 'src/app/models/enums/SnackBarCSS';
import { ChangePwd } from 'src/app/models/type/ChangePwd';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import { SnackbarService } from 'src/app/services/snackbar/snackbar.service';
import { TokenService } from 'src/app/services/token/token.service';
import formUtil from 'src/app/utils/form.util';

/**
 * change password form
 */
@Component({
  selector: 'app-change-pwd',
  templateUrl: './change-pwd.component.html',
  styleUrls: ['./change-pwd.component.scss']
})
export class ChangePwdComponent {

  /**
   * form inputs
   */
  get f(): any {return this.form.controls;}

  /**
   * angular form
   */
  public form= this.fb.group({
    oldPassword: [null, formUtil.password],
    newPassword: [null, formUtil.password],
    confirmNew: [null, formUtil.password]
  });

  /**
    * form title
    */
  title: string = 'form.pwd.title';

  /**
   * class constructor
   * @param fb angular form builder
   * @param snackBarService allow access to the material snackbar pop-up
   * @param translate internationalization tool
   * @param baseRepo the repository service
   * @param tokenService allow access to the jwt token
   * @param ref the modal reference
   * @param backOffice is the form from the backoffice ?
   */
  constructor(
    public fb: FormBuilder,
    public snackBarService: SnackbarService,
    public translate: TranslateService,
    private tokenService: TokenService,
    private baseRepo: BaseRepositoryService,
    private ref: MatDialogRef<ChangePwdComponent>,
    @Inject(MAT_DIALOG_DATA) public backOffice: boolean
  ) {}

  /**
   * handle confirmation event
   * @param event confirmation event
   */
  handleModalConfirm(event: boolean): void{
    if (event) return this.submitForm();
    this.ref.close();
  }

  /**
   * form submission
   */
  submitForm(): void{
    const user = this.tokenService.getUser();
    let url: string;
    if (!this.form.valid || this.f.newPassword.value != this.f.confirmNew.value){
      const errors: string[] = this.getErrorsMsg();
      if (errors.length > 0) this.snackBarService.openSnackbar(errors.join('\n'), SnackBarCSS.ERROR);
      return;
    }
    if(this.form.valid && user && this.f.newPassword.value == this.f.confirmNew.value){
      if(this.backOffice){
        url = ApiUrl.ADMIN_PWD;
      } else {
        url = ApiUrl.CUSTOMER_PWD;
      }
      this.post(url + user);
    }
  }

  /**
   * create the request body
   * @returns the request body as a change password object
   */
  getBody(): ChangePwd{
    return {
      oldPassword: this.f.oldPassword.value,
      newPassword: this.f.newPassword.value
    }
  }

  /**
   * calls the repository to post the request
   * @param url api url
   */
  post(url: string): void{
    this.baseRepo.post(url, this.getBody())
      .then(r => {
        this.snackBarService.openSnackbar(this.translate.instant('auth.msg.pwd.changed'), SnackBarCSS.SUCCESS);
        this.ref.close();
      })
      .catch(e => {
        console.error(e);
        this.snackBarService.openSnackbar(this.translate.instant('errors.default'), SnackBarCSS.ERROR);
      })
  }

  /**
   * compile validation errors
   * @returns an array of validation errors
   */
  getErrorsMsg(): string[]{
    const errors: string[] = [];

    if (
      this.f.confirmNew.errors?.required ||
      this.f.newPassword.errors?.required ||
      this.f.oldPassword.errors?.required
    ) errors.push(this.translate.instant('errors.required'));

    if (
      this.f.confirmNew.errors?.pattern ||
      this.f.newPassword.errors?.pattern ||
      this.f.oldPassword.errors?.pattern
    ) errors.push(this.translate.instant('auth.msg.special'));

    if (
      this.f.confirmNew.errors?.minlength ||
      this.f.newPassword.errors?.minlength ||
      this.f.oldPassword.errors?.minlength
    ) errors.push(this.translate.instant('auth.msg.pwd.length'));

    if (
      this.f.confirmNew.errors?.pattern ||
      this.f.newPassword.errors?.pattern ||
      this.f.oldPassword.errors?.pattern

    ) errors.push(this.translate.instant('auth.msg.pwd.pattern'));

    if (
      this.f.confirmNew.errors?.maxlength ||
      this.f.newPassword.errors?.maxlength ||
      this.f.oldPassword.errors?.maxlength
    ) errors.push(this.translate.instant('auth.msg.length'));


    if(this.f.newPassword.value != this.f.confirmNew.value) errors.push(this.translate.instant('auth.msg.confirm'));

    return errors;
  }

}
