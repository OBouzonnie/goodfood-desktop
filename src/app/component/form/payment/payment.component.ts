import { Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { SnackBarCSS } from 'src/app/models/enums/SnackBarCSS';
import { PaymentService } from 'src/app/services/payment/payment-service.service';
import { SnackbarService } from 'src/app/services/snackbar/snackbar.service';
import formUtil from 'src/app/utils/form.util';

/**
 * Mock a payment form
 */
@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent {

  /**
   * expiration format
   */
  readonly expInitValue = '  /  '

  /**
   * confirmation event
   */
  @Output() payed: EventEmitter<string> = new EventEmitter();

  /**
   * loading state of the form
   */
  isLoading: boolean = false;

  /**
   * return the form controls
   */
  get f(): any {return this.form.controls;}

  /**
   * the payment form
   */
  form: FormGroup = this.fb.group({
    cardNumber: [null, formUtil.creditCardNbrs],
    expirationDate: [null, formUtil.creditCardExp],
    code: [null, formUtil.creditCardCode],
  });

  /**
   * the form title
   */
  title: string = 'form.creditCard.title';

  /**
   *
   * @param fb angular form builder
   * @param snackBarService allow access to the snackbar pop-up
   * @param translate internationalization tool
   * @param paymentService mock up the payment process
   */
  constructor(
    public fb: FormBuilder,
    public snackBarService: SnackbarService,
    public translate: TranslateService,
    private paymentService: PaymentService
  ) {}

  /**
   * form submission
   */
  submitForm(): void{
    if (!this.form.valid){
      const errors: string[] = this.getErrorsMsg();
      if (errors.length > 0) this.snackBarService.openSnackbar(errors.join('\n'), SnackBarCSS.ERROR);
    }
    if(this.form.valid){
      this.isLoading = true;
      this.paymentService.pay().then((res: any) => {
        this.isLoading = false;
        this.payed.emit(res);
      })
    }
  }

  /**
   * compile the validation error messages
   * @returns an array of validation errors
   */
  getErrorsMsg(): string[]{
    const errors: string[] = [];
    if(
      this.f['cardNumber'].errors?.required ||
      this.f['expirationDate'].errors?.required ||
      this.f['code'].errors?.required
    ) errors.push(this.translate.instant('errors.required'));
    if (this.f['cardNumber'].errors?.pattern) errors.push(this.translate.instant('errors.creditCardNbr'));
    if (this.f['expirationDate'].errors?.pattern) errors.push(this.translate.instant('errors.creditCardExp'));
    if (this.f['code'].errors?.pattern) errors.push(this.translate.instant('errors.creditCardCode'));
    return errors;
  }

  /**
   * handle the card number formatting while the user is typing
   */
  updateCardNbr(){
    const arr = this.sanitizeInput(this.f.cardNumber.value);
    let val: string = '';
    for(let i = 0; i < arr.length; i++){
      val += arr[i];
      if(i != 0 && i != (arr.length - 1) && (i + 1) % 4 == 0) val += ' ';
    }
    this.f.cardNumber.setValue(val.slice(0,19));
  }

  /**
   * handle the expiration formatting while the user is typing
   */
  updateCardExp(){
    const arr = this.sanitizeInput(this.f.expirationDate.value)
    if (arr.length > 2) arr.splice(2,0,'/');
    this.f.expirationDate.setValue(arr.slice(0,5).join(''));
  }

  /**
   * handle the code formatting while the user is typing
   */
  updateCardCode(){
    const arr = this.sanitizeInput(this.f.code.value)
    this.f.code.setValue(arr.slice(0,3).join(''));
  }

  /**
   * remove any non digit char the user might input
   * @param str the input string
   * @returns an array of char
   */
  sanitizeInput(str: string): string[]{
    return str.replace(/\D/g, '').split('');
  }

}
