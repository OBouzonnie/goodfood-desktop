import { Component, Output, EventEmitter } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import FormUtil from 'src/app/utils/form.util';
import {TranslateService} from '@ngx-translate/core';
import { AuthDialogComponent } from 'src/app/component/dialog/auth-dialog/auth-dialog.component';
import { ConfirmDialogComponent } from 'src/app/component/dialog/confirm-dialog/confirm-dialog.component';
import * as moment from 'moment';
import { AuthService } from 'src/app/services/auth/auth.service';

/**
 * Register form
 */
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {

  /**
   * Emits a boolean is the registration process is successfull
   */
  @Output() submitted: EventEmitter<boolean> = new EventEmitter();

  /**
   * form inputs
   */
  public registerForm= this.fb.group({
    email: [null, FormUtil.email],
    password: [null, FormUtil.password],
    confirm: [null, FormUtil.password],
    rgpd: [null, Validators.required]
  });

  /**
   * Errors messages to be displayed if the submitted form isn't valid
   */
  private errors: string[] = [];

  /**
   * class constructor
   * @param fb angular form builder
   * @param dialog material dialog modals
   * @param translate internationalization tool
   * @param authService the login and registration service
   */
  constructor(
    private fb: FormBuilder,
    private dialog: MatDialog,
    private translate: TranslateService,
    private authService: AuthService
  ) { }

  /**
   * Handle the form submission
   */
  onSubmit(): void{
    //if the form is not valid, display the needed error messages for the user
    if(!this.registerForm.valid){
      if (this.registerForm.get('confirm')?.errors?.['pattern'] || this.registerForm.get('password')?.errors?.['pattern'] || this.registerForm.get('email')?.errors?.['pattern']) this.errors.push(this.translate.instant('auth.msg.special'));
      if (this.registerForm.get('confirm')?.errors?.['minlength'] || this.registerForm.get('password')?.errors?.['minlength']) this.errors.push(this.translate.instant('auth.msg.pwd.length'));
      if (this.registerForm.get('confirm')?.errors?.['pattern'] || this.registerForm.get('password')?.errors?.['pattern']) this.errors.push(this.translate.instant('auth.msg.pwd.pattern'));
      if (this.registerForm.get('confirm')?.errors?.['maxlength'] || this.registerForm.get('password')?.errors?.['maxlength'] || this.registerForm.get('email')?.errors?.['maxlength']) this.errors.push(this.translate.instant('auth.msg.length'));
      if (this.registerForm.get('rgpd')?.errors?.['required']) this.errors.push(this.translate.instant('auth.msg.rgpd'));
      if (this.errors.length > 0) {
        const errorModal = this.dialog.open(AuthDialogComponent, {data: {errors: this.errors}});
        errorModal.afterClosed().subscribe(() => {
          this.errors = [];
        });
      }
      return;
    }
    //if password and confirm don't match, display the needed error message to the user
    if (this.registerForm.get('password')?.value != this.registerForm.get('confirm')?.value) {
      this.errors.push(this.translate.instant('auth.msg.confirm'));
      if (this.errors.length > 0) {
        const errorModal = this.dialog.open(AuthDialogComponent, {data: {errors: this.errors}});
        errorModal.afterClosed().subscribe(() => {
          this.errors = [];
        });
      }
      return;
    }
    //if password matches and form is valid, just emit a true boolean atm
    if (this.registerForm.valid && this.registerForm.get('password')?.value == this.registerForm.get('confirm')?.value) {
      this.authService.register(this.getBody(), this.onRegisterResponse);
    }
  }

  /**
   * react according to the register api call response
   * @param res register api call response
   */
  onRegisterResponse = (res?: number) => {
    if(res === 200){
      this.dialog.open(ConfirmDialogComponent, {data: {msg: this.translate.instant('auth.title.success')}});
      this.submitted.emit(true);
      return;
    }
    if(res === 409){
      this.dialog.open(ConfirmDialogComponent, {data: {msg: this.translate.instant('auth.title.fail')}});
      return;
    }
    this.dialog.open(ConfirmDialogComponent, {data: {msg: this.translate.instant('errors.default')}});
  }

  /**
   * create the request body
   * @returns the request body
   */
  getBody(){
    return {
      email: this.registerForm.get('email')?.value,
      password: this.registerForm.get('password')?.value,
      rgpd: {
        hasConsentForDB: this.registerForm.get('rgpd')?.value,
        dbConsentDate: moment().format('DD/MM/YYYY')
      }
    }
  }

}
