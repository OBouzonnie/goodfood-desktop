import { ComponentFixture, TestBed, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { AppModule } from 'src/app/app.module';

import { RegisterComponent } from './register.component';

import { faker } from '@faker-js/faker';
import TestUtil from 'src/test/utils/test.utils';
import { AuthService } from 'src/app/services/auth/auth.service';

describe('RegisterComponent', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;
  let template: HTMLElement;
  let checkbox: HTMLElement;
  let authService: AuthService;

  let email: string = faker.internet.email();
  let pwd: string = faker.internet.password() + Math.floor(Math.random() * 10);
  let confirm: string = faker.internet.password() + Math.floor(Math.random() * 10);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule],
      declarations: [ RegisterComponent ],
      providers: [{ provide: ComponentFixtureAutoDetect, useValue: true }]
    })
    .compileComponents();
  });

  beforeEach(() => {
    authService = TestBed.inject(AuthService);
    spyOn(authService, 'register');
    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    template = fixture.nativeElement;
    checkbox = template.querySelector('#register-checkbox-input') as HTMLElement;
  });

  /*
   * Test component creation
  **/
  it('should create', () => {
    expect(component).toBeTruthy();
  });


  /*
   * Test onSubmit()
  **/
  it('validation success', () => {
    component.registerForm.get('email')?.setValue(email);
    component.registerForm.get('password')?.setValue(pwd);
    component.registerForm.get('confirm')?.setValue(pwd);
    checkbox.click();
    expect(component.registerForm.valid).toBeTrue();
    component.onSubmit();
    expect(authService.register).toHaveBeenCalledTimes(1);
    if (!component.registerForm.valid) console.log('email  ', email, ' & password ', pwd);
  });

  it('validation fails on empty email', () => {
    component.registerForm.get('password')?.setValue(pwd);
    component.registerForm.get('confirm')?.setValue(pwd);
    checkbox.click();
    expect(component.registerForm.valid).toBeFalse();
    component.onSubmit();
    expect(authService.register).not.toHaveBeenCalled();
  });

  it('validation fails on empty pwd', () => {
    component.registerForm.get('email')?.setValue(email);
    component.registerForm.get('confirm')?.setValue(pwd);
    checkbox.click();
    expect(component.registerForm.valid).toBeFalse();
    component.onSubmit();
    expect(authService.register).not.toHaveBeenCalled();
  });

  it('validation fails on empty confirm', () => {
    component.registerForm.get('email')?.setValue(email);
    component.registerForm.get('password')?.setValue(pwd);
    checkbox.click();
    expect(component.registerForm.valid).toBeFalse();
    component.onSubmit();
    expect(authService.register).not.toHaveBeenCalled();
  });

  it('validation fails on wrong email syntax', () => {
    component.registerForm.get('email')?.setValue(pwd);
    component.registerForm.get('password')?.setValue(pwd);
    component.registerForm.get('confirm')?.setValue(pwd);
    checkbox.click();
    expect(component.registerForm.valid).toBeFalse();
    component.onSubmit();
    expect(authService.register).not.toHaveBeenCalled();
  });

  it('validation fails on pwd w/o number', () => {
    component.registerForm.get('email')?.setValue(email);
    component.registerForm.get('password')?.setValue(pwd.replace(/[0-9]/g, ''));
    component.registerForm.get('confirm')?.setValue(pwd);
    checkbox.click();
    expect(component.registerForm.valid).toBeFalse();
    component.onSubmit();
    expect(authService.register).not.toHaveBeenCalled();
  });

  it('validation fails on confirm w/o number', () => {
    component.registerForm.get('email')?.setValue(email);
    component.registerForm.get('password')?.setValue(pwd);
    component.registerForm.get('confirm')?.setValue(pwd.replace(/[0-9]/g, ''));
    checkbox.click();
    expect(component.registerForm.valid).toBeFalse();
    component.onSubmit();
    expect(authService.register).not.toHaveBeenCalled();
  });

  it('validation fails on pwd w/o lowercase', () => {
    component.registerForm.get('email')?.setValue(email);
    component.registerForm.get('password')?.setValue(pwd.replace(/[a-z]/g, ''));
    component.registerForm.get('confirm')?.setValue(pwd);
    checkbox.click();
    expect(component.registerForm.valid).toBeFalse();
    component.onSubmit();
    expect(authService.register).not.toHaveBeenCalled();
  });

  it('validation fails on confirm w/o lowercase', () => {
    component.registerForm.get('email')?.setValue(email);
    component.registerForm.get('password')?.setValue(pwd);
    component.registerForm.get('confirm')?.setValue(pwd.replace(/[a-z]/g, ''));
    checkbox.click();
    expect(component.registerForm.valid).toBeFalse();
    component.onSubmit();
    expect(authService.register).not.toHaveBeenCalled();
  });

  it('validation fails on pwd w/o uppercase', () => {
    component.registerForm.get('email')?.setValue(email);
    component.registerForm.get('password')?.setValue(pwd.replace(/[A-Z]/g, ''));
    component.registerForm.get('confirm')?.setValue(pwd);
    checkbox.click();
    expect(component.registerForm.valid).toBeFalse();
    component.onSubmit();
    expect(authService.register).not.toHaveBeenCalled();
  });

  it('validation fails on confirm w/o uppercase', () => {
    component.registerForm.get('email')?.setValue(email);
    component.registerForm.get('password')?.setValue(pwd);
    component.registerForm.get('confirm')?.setValue(pwd.replace(/[A-Z]/g, ''));
    checkbox.click();
    expect(component.registerForm.valid).toBeFalse();
    component.onSubmit();
    expect(authService.register).not.toHaveBeenCalled();
  });

  it('validation fails on special char in email', () => {
    const fbdnEmail = TestUtil.useSpecialChar(email);

    component.registerForm.get('email')?.setValue(fbdnEmail.str);
    component.registerForm.get('password')?.setValue(pwd);
    component.registerForm.get('confirm')?.setValue(pwd);
    checkbox.click();
    expect(component.registerForm.valid).toBeFalse();
    component.onSubmit();
    expect(authService.register).not.toHaveBeenCalled();
    if(component.registerForm.valid) console.log('email validation fails on special char : ', fbdnEmail.char);
  });

  it('validation fails on special char in pwd', () => {
    const fbdnPwd = TestUtil.useSpecialChar(pwd);

    component.registerForm.get('email')?.setValue(email);
    component.registerForm.get('password')?.setValue(fbdnPwd.str);
    component.registerForm.get('confirm')?.setValue(pwd);
    checkbox.click();
    expect(component.registerForm.valid).toBeFalse();
    component.onSubmit();
    expect(authService.register).not.toHaveBeenCalled();
    if(component.registerForm.valid) console.log('password validation fails on special char : ', fbdnPwd.char);
  });

  it('validation fails on special char in confirm', () => {
    const fbdnPwd = TestUtil.useSpecialChar(pwd);

    component.registerForm.get('email')?.setValue(email);
    component.registerForm.get('password')?.setValue(pwd);
    component.registerForm.get('confirm')?.setValue(fbdnPwd.str);
    checkbox.click();
    expect(component.registerForm.valid).toBeFalse();
    component.onSubmit();
    expect(authService.register).not.toHaveBeenCalled();
    if(component.registerForm.valid) console.log('password validation fails on special char : ', fbdnPwd.char)
  });

  it('validation fails on space in email', () => {
    const spacedEmail = TestUtil.useSpace(email);

    component.registerForm.get('email')?.setValue(spacedEmail);
    component.registerForm.get('password')?.setValue(pwd);
    component.registerForm.get('confirm')?.setValue(pwd);
    checkbox.click();
    expect(component.registerForm.valid).toBeFalse();
    component.onSubmit();
    expect(authService.register).not.toHaveBeenCalled();
    if(component.registerForm.valid) console.log('email validation fails on space : ', spacedEmail);
  });

  it('validation fails on space in pwd', () => {
    const spacedPwd = TestUtil.useSpace(pwd);

    component.registerForm.get('email')?.setValue(email);
    component.registerForm.get('password')?.setValue(spacedPwd);
    component.registerForm.get('confirm')?.setValue(pwd);
    checkbox.click();
    expect(component.registerForm.valid).toBeFalse();
    component.onSubmit();
    expect(authService.register).not.toHaveBeenCalled();
    if(component.registerForm.valid) console.log('password validation fails on space : ', spacedPwd);
  });

  it('validation fails on space in confirm', () => {
    const spacedPwd = TestUtil.useSpace(pwd);

    component.registerForm.get('email')?.setValue(email);
    component.registerForm.get('password')?.setValue(pwd);
    component.registerForm.get('confirm')?.setValue(spacedPwd);
    checkbox.click();
    expect(component.registerForm.valid).toBeFalse();
    component.onSubmit();
    expect(authService.register).not.toHaveBeenCalled();
    if(component.registerForm.valid) console.log('password validation fails on space : ', spacedPwd);
  });

  it('validation fails on script in email', () => {
    component.registerForm.get('email')?.setValue(TestUtil.useScript());
    component.registerForm.get('password')?.setValue(pwd);
    component.registerForm.get('confirm')?.setValue(pwd);
    checkbox.click();
    expect(component.registerForm.valid).toBeFalse();
    component.onSubmit();
    expect(authService.register).not.toHaveBeenCalled();
  });

  it('validation fails on script in pwd', () => {
    component.registerForm.get('email')?.setValue(email);
    component.registerForm.get('password')?.setValue(TestUtil.useScript());
    component.registerForm.get('confirm')?.setValue(pwd);
    checkbox.click();
    expect(component.registerForm.valid).toBeFalse();
    component.onSubmit();
    expect(authService.register).not.toHaveBeenCalled();
  });

  it('validation fails on script in confirm', () => {
    component.registerForm.get('email')?.setValue(email);
    component.registerForm.get('password')?.setValue(pwd);
    component.registerForm.get('confirm')?.setValue(TestUtil.useScript());
    checkbox.click();
    expect(component.registerForm.valid).toBeFalse();
    component.onSubmit();
    expect(authService.register).not.toHaveBeenCalled();
  });

  it('validation fails on short pwd in pwd', () => {
    component.registerForm.get('email')?.setValue(email);
    component.registerForm.get('password')?.setValue(TestUtil.cutPwd(pwd));
    component.registerForm.get('confirm')?.setValue(pwd);
    checkbox.click();
    expect(component.registerForm.valid).toBeFalse();
    component.onSubmit();
    expect(authService.register).not.toHaveBeenCalled();
  });

  it('validation fails on short pwd in confirm', () => {
    component.registerForm.get('email')?.setValue(email);
    component.registerForm.get('password')?.setValue(pwd);
    component.registerForm.get('confirm')?.setValue(TestUtil.cutPwd(pwd));
    checkbox.click();
    expect(component.registerForm.valid).toBeFalse();
    component.onSubmit();
    expect(authService.register).not.toHaveBeenCalled();
  });

  it('validation fails on pwd mismatch', () => {
    component.registerForm.get('email')?.setValue(email);
    component.registerForm.get('password')?.setValue(pwd);
    component.registerForm.get('confirm')?.setValue(confirm);
    checkbox.click();
    component.onSubmit();
    expect(authService.register).not.toHaveBeenCalled();
  });

  it('validation fail without rgpd checked', () => {
    component.registerForm.get('email')?.setValue(email);
    component.registerForm.get('password')?.setValue(pwd);
    component.registerForm.get('confirm')?.setValue(pwd);
    component.onSubmit();
    expect(authService.register).not.toHaveBeenCalled();
  });

});
