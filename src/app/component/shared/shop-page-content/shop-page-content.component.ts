import { Component } from '@angular/core';
import { environment } from 'src/environments/environment';
/**
 * shop module header
 */
@Component({
  selector: 'app-shop-page-content',
  templateUrl: './shop-page-content.component.html',
  styleUrls: ['./shop-page-content.component.scss']
})
export class ShopPageContentComponent {

  /**
   * page title - content of the h1
   */
  title: string = environment.title;

  constructor() { }

}
