import { Component, Input, OnInit, OnChanges, SimpleChanges, HostListener } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Logo } from 'src/app/models/type/Logo';
import { NavLink } from 'src/app/models/type/NavLink';
import { AuthService } from 'src/app/services/auth/auth.service';
import { environment } from 'src/environments/environment';
import { ChangePwdComponent } from '../../form/change-pwd/change-pwd.component';

/**
 * Aside Navigation
 */
@Component({
  selector: 'app-aside-nav',
  templateUrl: './aside-nav.component.html',
  styleUrls: ['./aside-nav.component.scss']
})
export class AsideNavComponent implements OnInit, OnChanges {

  /**
   * switching point
   */
  breakpoint: number = 425;

  /**
   * allow navigation resize from this point
   */
  allowResize: number = 800;

  /**
   * screen size
   */
  public innerWidth: any;

  /**
   * define navigation size
   */
  minimized: boolean = true;

  /**
   * is the nav used in a back office module
   */
  @Input() backoffice: boolean = false;

  /**
   * Links displayed in the navigation
   */
  @Input() links: NavLink[] = [];

  /**
   * Target of the return link
   */
  @Input() home: string = '';

  /**
   * logo img attributes
   */
  logo: Logo = {
    url:'assets/logo/logo-white.svg',
    alt:'good food logo'
  }

  /**
   * detect screen size
   */
  @HostListener('window:resize')
  onResize() {
    this.innerWidth = window.innerWidth;
  }

  /**
   * class constructor
   * @param router angular router
   * @param authService customer authentication service
   * @param dialog material modal
   */
  constructor(
    public router: Router,
    private authService: AuthService,
    private dialog: MatDialog
  ) {}

  ngOnInit(){
    this.onResize();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes) {
      this.onResize();
    }
  }

  /**
   * reduce nav size
   */
  minimize(){
    if (this.innerWidth > this.allowResize) this.minimized = true;
  }

  /**
   * grow nav size
   */
  maximize(){
    if (this.innerWidth > this.allowResize) this.minimized = false;
  }

  /**
   * logout authenticated user
   */
  logout(){
    this.authService.logout();
    if(this.backoffice){
      this.router.navigateByUrl(environment.core.ROUTES.goodfood.url + '/' + environment.core.ROUTES.goodfood.modules.auth.url);
    } else {
      this.router.navigateByUrl(environment.core.ROUTES.default.modules.auth.url);
    }
  }

  /**
   * open the change password modal
   */
  changePwd(){
    this.dialog.open(ChangePwdComponent, {data: this.backoffice});
  }

}
