import { ComponentFixture, TestBed } from '@angular/core/testing';
import faker from '@faker-js/faker';
import { AppModule } from 'src/app/app.module';
import testUtils from 'src/test/utils/test.utils';

import { AsideNavComponent } from './aside-nav.component';

describe('AsideNavComponent', () => {
  let component: AsideNavComponent;
  let fixture: ComponentFixture<AsideNavComponent>;
  let template: HTMLElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule],
      declarations: [ AsideNavComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AsideNavComponent);
    component = fixture.componentInstance;
    component.links = testUtils.mockNavLinks(Math.floor(Math.random()*100));
    component.home = faker.datatype.uuid();
    fixture.detectChanges();
    template = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  /**
   * Rendering
   */
  it('should render an aside for large screen', () => {
    component.innerWidth = 2000;
    fixture.detectChanges();
    template = fixture.nativeElement;
    const aside = template.querySelector('aside');
    const footer = template.querySelector('footer');
    expect(aside).toBeTruthy();
    expect(footer).toBeFalsy();
  });

  it('should render a link for each nav link', () => {
    const element = template.querySelectorAll('.aside-nav-link');
    expect(element).toBeTruthy();
    expect(element.length).toBe(component.links.length);
    element.forEach( (l, i) => {
      const link = l as HTMLAnchorElement;
      expect(link.href).toContain(component.links[i].url);
    })
  });

  /**
   * Rendering small screen
   */
  it('should render a footer for small screen', () => {
    component.innerWidth = 320;
    fixture.detectChanges();
    template = fixture.nativeElement;
    const aside = template.querySelector('aside');
    const footer = template.querySelector('footer');
    expect(aside).toBeFalsy();
    expect(footer).toBeTruthy();
  });

  it('should render a link for each nav link for small screen', () => {
    component.innerWidth = 320;
    fixture.detectChanges();
    template = fixture.nativeElement;
    const element = template.querySelectorAll('.footer-nav-link');
    expect(element).toBeTruthy();
    expect(element.length).toBe(component.links.length + 3);
    element.forEach( (l, i) => {
      const link = l as HTMLAnchorElement;
      if (i !== 0 && i !== (element.length - 1) && i !== (element.length - 2)) expect(link.href).toContain(component.links[i - 1].url);
    })
  });
});
