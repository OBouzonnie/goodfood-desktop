import { Component, Input, OnInit } from '@angular/core';
import { InfoTableMapper } from 'src/app/models/table/mappers/InfoTableMapper';

/**
 * information table
 */
@Component({
  selector: 'app-info-table',
  templateUrl: './info-table.component.html',
  styleUrls: ['./info-table.component.scss']
})
export class InfoTableComponent {

  /**
   * table head title
   */
  @Input() title: string = '';

  /**
   * mapper for the displayed resource informations
   */
  @Input() tableMapper: InfoTableMapper|null = null;

  constructor() { }

}
