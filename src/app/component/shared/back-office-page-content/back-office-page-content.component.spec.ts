import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppModule } from 'src/app/app.module';
import { CRUD } from 'src/app/models/enums/CRUD';

import { BackOfficePageContentComponent } from './back-office-page-content.component';

describe('BackOfficePageContentComponent', () => {
  let component: BackOfficePageContentComponent;
  let fixture: ComponentFixture<BackOfficePageContentComponent>;
  let template: HTMLElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule],
      declarations: [ BackOfficePageContentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BackOfficePageContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    template = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  /**
   * Event emitter
   */

  it('create btn should emit create event', () => {
    spyOn(component.crud, 'emit');
    const btn = template.querySelector('button');
    btn?.dispatchEvent(new Event('click'));
    expect(component.crud.emit).toHaveBeenCalledWith(CRUD.CREATE);
  });
});
