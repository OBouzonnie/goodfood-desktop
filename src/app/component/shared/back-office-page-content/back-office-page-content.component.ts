import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CRUD } from 'src/app/models/enums/CRUD';

/**
 * Header of the CRUD pages
 */
@Component({
  selector: 'app-back-office-page-content',
  templateUrl: './back-office-page-content.component.html',
  styleUrls: ['./back-office-page-content.component.scss']
})
export class BackOfficePageContentComponent {

  /**
   * Content of the h1
   */
  @Input() title: string = '';

  /**
   * display state of the CREATE btn
   */
  @Input() crudAction: boolean = true;

  /**
   * crud action event
   */
   @Output() crud: EventEmitter<CRUD> = new EventEmitter();

  constructor() { }

  /**
   * emit the crud event
   */
  emitCrud(): void {
    this.crud.emit(CRUD.CREATE);
  }

}
