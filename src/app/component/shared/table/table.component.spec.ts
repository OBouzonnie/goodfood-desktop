import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppModule } from 'src/app/app.module';
import { TestingTableMapper } from 'src/test/mapper/TestingTableMapper';

import { TableComponent } from './table.component';

describe('TableComponent', () => {
  let component: TableComponent;
  let fixture: ComponentFixture<TableComponent>;
  let template: HTMLElement;
  let tableMapper: TestingTableMapper = new TestingTableMapper();

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule],
      declarations: [ TableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableComponent);
    component = fixture.componentInstance;
    component.head = tableMapper.head;
    component.displayedColumns = tableMapper.getColumns();
    component.dataSource = tableMapper.rows;
    fixture.detectChanges();
    template = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  /*
   * Test rendering
  **/

  it('should render html th elements', () => {
    const element = template.querySelectorAll('th');
    expect(element).toBeTruthy();
    expect(element.length).toBe(component.displayedColumns.length);
  });

  it('should render html tr elements', () => {
    const element = template.querySelectorAll('tr');
    expect(element).toBeTruthy();
    expect(element.length).toBe(component.dataSource.length + 1);
  });

  it('should render html td elements for each row', () => {
    const tr = template.querySelectorAll('tr');
    tr.forEach((tr, i) => {
      if (i !== 0) {
        const td = tr.querySelectorAll('td');
        expect(td.length).toBe(component.displayedColumns.length);
      };
    });
  });

  it('should render specific elements for specific columns', () => {
    const tr = template.querySelectorAll('tr');
    tr.forEach((tr, i) => {
      if (i !== 0) {
        const td = tr.querySelectorAll('td');
        td.forEach((td, j) => {
          if (component.displayedColumns[j] === component.COLUMN_UID){
            const crud = td.querySelector('app-crud-actions');
            expect(crud).toBeTruthy();
          } else if (component.displayedColumns[j] === component.COLUMN_PHOTO){
            const img = td.querySelector('img');
            expect(img).toBeTruthy();
            expect(img?.src).toContain(component.dataSource[i - 1].photo);
          } else {
            const span = td.querySelector('span');
            expect(span).toBeTruthy();
            const data = Object.values(component.dataSource[i - 1]);
            expect(span?.textContent).toBe(data[j]);
          }
        })
      };
    });
  });
});
