import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CrudTarget } from 'src/app/models/type/CrudTarget';
import { TableModel } from 'src/app/models/table/resources/TableModel';
import { environment } from 'src/environments/environment';
import { CrudConfig } from 'src/app/models/type/CrudConfig';

/**
 * generic table
 */
@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent{

  /**
   * image encoding
   */
  dwlImg: string = environment.core.dwlImg;

  /**
   * uid column
   */
  readonly COLUMN_UID = environment.core.TABLE.uid;

  /**
   * image column
   */
  readonly COLUMN_PHOTO = environment.core.TABLE.photo;

  /**
   * name column, usually first column
   */
  readonly COLUMN_NAME = environment.core.TABLE.name;

  /**
   * logo column
   */
  readonly COLUMN_STD = environment.core.TABLE.std;

  /**
   * evaluation column
   */
  readonly COLUMN_EVAL = environment.core.TABLE.eval;

  /**
   * Table Head content
   */
  @Input() head: any;

  /**
   * Define table columns
   */
  @Input() displayedColumns: string[] = [];

  /**
  * Table Data content
  */
  @Input() dataSource: TableModel[] = [];

  /**
   * allowed crud actions for the current instance
   */
  @Input() crudConfig: CrudConfig = [];

  /**
   * franchise ID
   */
  @Input() franchise?: string;

  /**
   * resources already owned by the franchise
   */
  @Input() actives: string[] = [];

  /**
   * crud action event
   */
  @Output() crud: EventEmitter<CrudTarget> = new EventEmitter();

  constructor() { }

  /**
   * emit a crud action event
   * @param event crud action event
   */
  crudHandler(event: CrudTarget){
    this.crud.emit(event);
  }

}
