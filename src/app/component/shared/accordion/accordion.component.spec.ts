import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppModule } from 'src/app/app.module';
import { CRUD } from 'src/app/models/enums/CRUD';
import { TestingTableMapper } from 'src/test/mapper/TestingTableMapper';

import { AccordionComponent } from './accordion.component';

describe('AccordionComponent', () => {
  let component: AccordionComponent;
  let fixture: ComponentFixture<AccordionComponent>;
  let template: HTMLElement;
  let tableMapper: TestingTableMapper = new TestingTableMapper();

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule],
      declarations: [ AccordionComponent ],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccordionComponent);
    component = fixture.componentInstance;
    component.head = tableMapper.head;
    component.displayedColumns = tableMapper.getColumns();
    component.dataSource = tableMapper.rows;
    fixture.detectChanges();
    template = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  /*
   * Test rendering
  **/

  it('should render mat-panel-title materials components with elements name in it', () => {
    const element = template.querySelectorAll('mat-panel-title');
    const p = template.querySelectorAll('p');
    expect(element.length).toBe(component.dataSource.length);
    expect(p.length).toBe(component.dataSource.length);
    for (let i = 0; i < component.dataSource.length; i++){
      expect(p[i].textContent).toBe(component.dataSource[i].name);
    }
  });

  it('should render mat-expansion-panel-body materials components without element.uid or element.name', () => {
    const element = template.querySelectorAll('.mat-expansion-panel-body');
    expect(element.length).toBe(component.dataSource.length);
    for (let i = 0; i < component.dataSource.length; i++){
      const e: HTMLElement = element[i] as HTMLElement;
      const ul: HTMLElement = e.querySelector('ul') as HTMLElement;
      expect(ul).toBeTruthy();
      const span = ul.querySelectorAll('span');
      expect(span.length).toBe(1);
      span.forEach(s => {
        expect(s.textContent).not.toContain(component.dataSource[i].uid);
        expect(s.textContent).not.toContain(component.dataSource[i].name);
        expect(s.textContent).not.toContain(component.dataSource[i].photo);
      })
    }
  });

  it('should render mat-expansion-panel-body materials with each one image element', () => {
    const element = template.querySelectorAll('.mat-expansion-panel-body');
    for (let i = 0; i < component.dataSource.length; i++){
      const e: HTMLElement = element[i] as HTMLElement;
      const ul: HTMLElement = e.querySelector('ul') as HTMLElement;
      const img = ul.querySelectorAll('img');
      expect(img.length).toBe(1);
      expect(img[0].src).toContain(component.dataSource[i].photo);
    }
  });

  it('should render mat-expansion-panel-body materials with each one span element', () => {
    const element = template.querySelectorAll('.mat-expansion-panel-body');
    for (let i = 0; i < component.dataSource.length; i++){
      const e: HTMLElement = element[i] as HTMLElement;
      const ul: HTMLElement = e.querySelector('ul') as HTMLElement;
      const span = ul.querySelectorAll('span');
      expect(span.length).toBe(1);
      expect(span[0].textContent).toContain(component.dataSource[i].desc);
    }
  });
});
