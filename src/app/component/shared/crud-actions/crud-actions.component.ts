import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CRUD } from 'src/app/models/enums/CRUD';
import { CrudConfig } from 'src/app/models/type/CrudConfig';
import { CrudTarget } from 'src/app/models/type/CrudTarget';
import { Orientation } from 'src/app/models/type/Orientation';

/**
 * crud actions btn used by table and accordeon component
 */
@Component({
  selector: 'app-crud-actions',
  templateUrl: './crud-actions.component.html',
  styleUrls: ['./crud-actions.component.scss']
})
export class CrudActionsComponent {

  /**
   * resource ID
   */
  @Input() uid: string = '';

  /**
   * btn orientation
   */
  @Input() orientation: Orientation = 'horizontal';

  /**
   * crud actions allowed
   */
  @Input() crudConfig: CrudConfig = [];

  /**
   * franchise ID
   */
  @Input() franchise?: string;

  /**
   * standard state of the resource
   */
  @Input() isStandard?: boolean;

  /**
   * franchise creator of the resource
   */
  @Input() creator?: string;

  /**
   * order status - used for order only
   */
  @Input() status?: string;

  /**
   * delivery state - used for order only
   */
  @Input() delivery?: string;

  /**
   * franchise own resources
   */
  @Input() actives: string[] = [];

  /**
   * Emits a CRUD action
   */
  @Output() crud: EventEmitter<CrudTarget> = new EventEmitter();

  /**
   * possible CRUD actions
   */
  crudEnum = CRUD;

  constructor() {}

  /**
   * emit a crud event
   * @param event crud event
   */
  emitCrud(event: CRUD): void{
    this.crud.emit({action: event, uid: this.uid})
  }

  /**
   * define the read btn display state
   * @returns read btn display state
   */
  includeReadAction(): boolean{
    return this.crudConfig.includes(CRUD.READ);
  }

  /**
   * define the update btn display state
   * @returns update btn display state
   */
  includeUpdateAction(): boolean{
    return this.crudConfig.includes(CRUD.UPDATE) && this.includeWriteAction();
  }

  /**
   * define the delete btn display state
   * @returns delete btn display state
   */
  includeDeleteAction(): boolean{
    return this.crudConfig.includes(CRUD.DELETE) && this.includeWriteAction();
  }

  /**
   * define the write right of the resource, used by create et update states
   * @returns the write right state
   */
  includeWriteAction(): boolean{
    return (
      !this.franchise && (this.isStandard === true || this.isStandard === undefined))
      ||
      (this.franchise === this.creator && !this.isStandard);
  }

  /**
   * define the standardise btn display state
   * @returns standardise btn display state
   */
  includeStandardiseAction(): boolean{
    return this.crudConfig.includes(CRUD.STANDARDISE) && !this.franchise && !this.isStandard;
  }

  /**
   * define the add btn display state
   * @returns add btn display state
   */
  includeAddAction(): boolean{
    return this.crudConfig.includes(CRUD.ADD) && !!this.franchise && !!this.isStandard && !this.actives.includes(this.uid);
  }

  /**
   * define the remove btn display state
   * @returns remove btn display state
   */
  includeRemoveAction(): boolean{
    return this.crudConfig.includes(CRUD.REMOVE) && !!this.franchise && this.actives.includes(this.uid) && (!!this.isStandard || !this.isStandard && this.franchise !== this.creator);
  }

  /**
   * define the manage btn display state
   * @returns manage btn display state
   */
  includeManageAction(): boolean{
    return this.crudConfig.includes(CRUD.MANAGE) && this.status != undefined &&
      (this.status != 'enum.delivered' && this.delivery == 'enum.deliver'
      ||
      this.status != 'enum.delivered' && this.status != 'enum.provided' && this.delivery == 'enum.takeaway');
  }

}
