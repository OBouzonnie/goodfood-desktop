import { ComponentFixture, TestBed } from '@angular/core/testing';
import faker from '@faker-js/faker';
import { AppModule } from 'src/app/app.module';
import { CRUD } from 'src/app/models/enums/CRUD';
import { OrderStatus } from 'src/app/models/enums/OrderStatus';

import { CrudActionsComponent } from './crud-actions.component';

describe('CrudActionsComponent', () => {
  let component: CrudActionsComponent;
  let fixture: ComponentFixture<CrudActionsComponent>;
  let template: HTMLElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule],
      declarations: [ CrudActionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudActionsComponent);
    component = fixture.componentInstance;
    component.uid = faker.datatype.uuid();
    fixture.detectChanges();
    template = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  /**
   * Rendering
   */

  //READ
  it('should render a crud read button when config include CRUD.READ', () => {
    component.crudConfig = [
      CRUD.READ,
    ];
    fixture.detectChanges();
    const btn = template.querySelector(`#crud-read-${component.uid}`);
    expect(btn).toBeTruthy();
  });

  it('should not render a crud read button when config does not include CRUD.READ', () => {
    component.crudConfig = [
      CRUD.CREATE,
      CRUD.UPDATE,
      CRUD.DELETE,
      CRUD.STANDARDISE,
      CRUD.ADD,
      CRUD.REMOVE
    ];
    fixture.detectChanges();
    const btn = template.querySelector(`#crud-read-${component.uid}`);
    expect(btn).toBeFalsy();
  });

  //UPDATE
  it('should render a crud update button for the back-office restaurant, ingredients and admin config', () => {
    component.crudConfig = [
      CRUD.UPDATE
    ];
    fixture.detectChanges();
    const btn = template.querySelector(`#crud-update-${component.uid}`);
    expect(btn).toBeTruthy();
  });

  it('should render a crud update button for the group back-office recipe and supplier standard config', () => {
    component.crudConfig = [
      CRUD.UPDATE
    ];
    component.isStandard = true;
    fixture.detectChanges();
    const btn = template.querySelector(`#crud-update-${component.uid}`);
    expect(btn).toBeTruthy();
  });

  it('should render a crud update button for the franchise back-office recipe and supplier none standard config', () => {
    component.crudConfig = [
      CRUD.UPDATE
    ];
    component.isStandard = false;
    const communID = faker.datatype.uuid();
    component.franchise = communID;
    component.creator = communID;
    fixture.detectChanges();
    const btn = template.querySelector(`#crud-update-${component.uid}`);
    expect(btn).toBeTruthy();
  });

  it('should not render a crud update button for the group back-office if the recipe is not stand', () => {
    component.crudConfig = [
      CRUD.UPDATE
    ];
    component.isStandard = true;
    fixture.detectChanges();
    const btn = template.querySelector(`#crud-update-${component.uid}`);
    expect(btn).toBeTruthy();
  });

  it('should not render a crud update button for the group back-office recipe and supplier none standard config', () => {
    component.crudConfig = [
      CRUD.UPDATE
    ];
    component.isStandard = false;
    component.creator = faker.datatype.uuid();
    fixture.detectChanges();
    const btn = template.querySelector(`#crud-update-${component.uid}`);
    expect(btn).toBeFalsy();
  });

  it('should not render a crud update button for the franchise back-office recipe and supplier standard config', () => {
    component.crudConfig = [
      CRUD.UPDATE
    ];
    component.isStandard = true;
    const communID = faker.datatype.uuid();
    component.franchise = communID;
    component.creator = communID;
    fixture.detectChanges();
    const btn = template.querySelector(`#crud-update-${component.uid}`);
    expect(btn).toBeFalsy();
  });

  it('should not render a crud update button for the franchise back-office recipe and supplier none standard config if the franchise is not the resource creator', () => {
    component.crudConfig = [
      CRUD.UPDATE
    ];
    component.isStandard = true;
    component.franchise = faker.datatype.uuid();
    component.creator = faker.datatype.uuid();
    fixture.detectChanges();
    const btn = template.querySelector(`#crud-update-${component.uid}`);
    expect(btn).toBeFalsy();
  });

  it('should not render a crud update button if the config does not include a CRUD.UPDATE', () => {
    component.crudConfig = [
      CRUD.CREATE,
      CRUD.READ,
      CRUD.DELETE,
      CRUD.STANDARDISE,
      CRUD.ADD,
      CRUD.REMOVE
    ];
    component.isStandard = true;
    fixture.detectChanges();
    const btn = template.querySelector(`#crud-update-${component.uid}`);
    expect(btn).toBeFalsy();
  });

  //DELETE
  it('should render a crud delete button for the back-office restaurant, ingredients and admin config', () => {
    component.crudConfig = [
      CRUD.DELETE
    ];
    fixture.detectChanges();
    const btn = template.querySelector(`#crud-delete-${component.uid}`);
    expect(btn).toBeTruthy();
  });

  it('should render a crud delete button for the group back-office recipe and supplier standard config', () => {
    component.crudConfig = [
      CRUD.DELETE
    ];
    component.isStandard = true;
    fixture.detectChanges();
    const btn = template.querySelector(`#crud-delete-${component.uid}`);
    expect(btn).toBeTruthy();
  });

  it('should render a crud delete button for the franchise back-office recipe and supplier none standard config', () => {
    component.crudConfig = [
      CRUD.DELETE
    ];
    component.isStandard = false;
    const communID = faker.datatype.uuid();
    component.franchise = communID;
    component.creator = communID;
    fixture.detectChanges();
    const btn = template.querySelector(`#crud-delete-${component.uid}`);
    expect(btn).toBeTruthy();
  });

  it('should not render a crud delete button for the group back-office if the recipe is not stand', () => {
    component.crudConfig = [
      CRUD.DELETE
    ];
    component.isStandard = true;
    fixture.detectChanges();
    const btn = template.querySelector(`#crud-delete-${component.uid}`);
    expect(btn).toBeTruthy();
  });

  it('should not render a crud delete button for the group back-office recipe and supplier none standard config', () => {
    component.crudConfig = [
      CRUD.DELETE
    ];
    component.isStandard = false;
    component.creator = faker.datatype.uuid();
    fixture.detectChanges();
    const btn = template.querySelector(`#crud-delete-${component.uid}`);
    expect(btn).toBeFalsy();
  });

  it('should not render a crud delete button for the franchise back-office recipe and supplier standard config', () => {
    component.crudConfig = [
      CRUD.DELETE
    ];
    component.isStandard = true;
    const communID = faker.datatype.uuid();
    component.franchise = communID;
    component.creator = communID;
    fixture.detectChanges();
    const btn = template.querySelector(`#crud-delete-${component.uid}`);
    expect(btn).toBeFalsy();
  });

  it('should not render a crud delete button for the franchise back-office recipe and supplier none standard config if the franchise is not the resource creator', () => {
    component.crudConfig = [
      CRUD.DELETE
    ];
    component.isStandard = true;
    component.franchise = faker.datatype.uuid();
    component.creator = faker.datatype.uuid();
    fixture.detectChanges();
    const btn = template.querySelector(`#crud-delete-${component.uid}`);
    expect(btn).toBeFalsy();
  });

  it('should not render a crud delete button if the config does not include a CRUD.UPDATE', () => {
    component.crudConfig = [
      CRUD.CREATE,
      CRUD.READ,
      CRUD.UPDATE,
      CRUD.STANDARDISE,
      CRUD.ADD,
      CRUD.REMOVE
    ];
    component.isStandard = true;
    fixture.detectChanges();
    const btn = template.querySelector(`#crud-delete-${component.uid}`);
    expect(btn).toBeFalsy();
  });

  //STANDARDISE

  it('should render a standardise button for a group none standard config', () => {
    component.crudConfig = [
      CRUD.STANDARDISE
    ];
    component.isStandard = false;
    fixture.detectChanges();
    const btn = template.querySelector(`#crud-standard-${component.uid}`);
    expect(btn).toBeTruthy();
  });

  it('should not render a standardise button for a group standard config', () => {
    component.crudConfig = [
      CRUD.STANDARDISE
    ];
    component.isStandard = true;
    fixture.detectChanges();
    const btn = template.querySelector(`#crud-standard-${component.uid}`);
    expect(btn).toBeFalsy();
  });

  it('should not render a standardise button for a franchise config', () => {
    component.crudConfig = [
      CRUD.STANDARDISE
    ];
    component.isStandard = Math.random() * 2 > 1;
    component.franchise = faker.datatype.uuid();
    fixture.detectChanges();
    const btn = template.querySelector(`#crud-standard-${component.uid}`);
    expect(btn).toBeFalsy();
  });

  it('should not render a standardise button without a CRUD.STANDARDISE config', () => {
    component.crudConfig = [
      CRUD.CREATE,
      CRUD.READ,
      CRUD.UPDATE,
      CRUD.DELETE,
      CRUD.ADD,
      CRUD.REMOVE
    ];
    component.isStandard = false;
    fixture.detectChanges();
    const btn = template.querySelector(`#crud-standard-${component.uid}`);
    expect(btn).toBeFalsy();
  });

  //ADD
  it('should render a add button for a franchise standard config without this recipe', () => {
    component.crudConfig = [
      CRUD.ADD
    ];
    component.isStandard = true;
    component.franchise = faker.datatype.uuid();
    fixture.detectChanges();
    const btn = template.querySelector(`#crud-add-${component.uid}`);
    expect(btn).toBeTruthy();
  });

  it('should not render a add button for a franchise standard config with this recipe', () => {
    component.crudConfig = [
      CRUD.ADD
    ];
    component.isStandard = true;
    component.franchise = faker.datatype.uuid();
    component.actives.push(component.uid);
    fixture.detectChanges();
    const btn = template.querySelector(`#crud-add-${component.uid}`);
    expect(btn).toBeFalsy();
  });

  it('should not render a add button for a franchise none standard config with this recipe', () => {
    component.crudConfig = [
      CRUD.ADD
    ];
    component.isStandard = false;
    component.franchise = faker.datatype.uuid();
    fixture.detectChanges();
    const btn = template.querySelector(`#crud-add-${component.uid}`);
    expect(btn).toBeFalsy();
  });

  it('should not render a add button for a group config', () => {
    component.crudConfig = [
      CRUD.ADD
    ];
    fixture.detectChanges();
    const btn = template.querySelector(`#crud-add-${component.uid}`);
    expect(btn).toBeFalsy();
  });

  it('should not render a add button without a CRUD.ADD config', () => {
    component.crudConfig = [
      CRUD.CREATE,
      CRUD.READ,
      CRUD.UPDATE,
      CRUD.DELETE,
      CRUD.STANDARDISE,
      CRUD.REMOVE
    ];
    component.isStandard = true;
    component.franchise = faker.datatype.uuid();
    fixture.detectChanges();
    const btn = template.querySelector(`#crud-add-${component.uid}`);
    expect(btn).toBeFalsy();
  });

  //REMOVE
  it('should render a remove button for a franchise standard config with this recipe', () => {
    component.crudConfig = [
      CRUD.REMOVE
    ];
    component.isStandard = true;
    component.franchise = faker.datatype.uuid();
    component.actives.push(component.uid);
    fixture.detectChanges();
    const btn = template.querySelector(`#crud-remove-${component.uid}`);
    expect(btn).toBeTruthy();
  });

  it('should not render a remove button for a franchise standard config without this recipe', () => {
    component.crudConfig = [
      CRUD.REMOVE
    ];
    component.isStandard = true;
    component.franchise = faker.datatype.uuid();
    fixture.detectChanges();
    const btn = template.querySelector(`#crud-remove-${component.uid}`);
    expect(btn).toBeFalsy();
  });

  it('should not render a remove button for a group config', () => {
    component.crudConfig = [
      CRUD.REMOVE
    ];
    fixture.detectChanges();
    const btn = template.querySelector(`#crud-remove-${component.uid}`);
    expect(btn).toBeFalsy();
  });

  it('should not render a remove button without a CRUD.ADD config', () => {
    component.crudConfig = [
      CRUD.CREATE,
      CRUD.READ,
      CRUD.UPDATE,
      CRUD.DELETE,
      CRUD.STANDARDISE,
      CRUD.ADD
    ];
    component.isStandard = true;
    component.franchise = faker.datatype.uuid();
    component.actives.push(component.uid);
    fixture.detectChanges();
    const btn = template.querySelector(`#crud-remove-${component.uid}`);
    expect(btn).toBeFalsy();
  });

  //MANAGE
  it('should render a manage button with a placed status and a deliver delivery', () => {
    component.crudConfig = [
      CRUD.READ,
      CRUD.MANAGE
    ];
    component.status = 'enum.placed';
    component.delivery = 'enum.deliver'
    fixture.detectChanges();
    const btn = template.querySelector(`#crud-manage-${component.uid}`);
    expect(btn).toBeTruthy();
  });

  it('should NOT render a manage button without a CRUD.MANAGE config', () => {
    component.crudConfig = [
      CRUD.READ
    ];
    component.status = 'enum.placed';
    component.delivery = 'enum.deliver'
    fixture.detectChanges();
    const btn = template.querySelector(`#crud-manage-${component.uid}`);
    expect(btn).toBeFalsy();
  });

  it('should render a manage button with a approved status and a deliver delivery', () => {
    component.crudConfig = [
      CRUD.READ,
      CRUD.MANAGE
    ];
    component.status = 'enum.approved';
    component.delivery = 'enum.deliver'
    fixture.detectChanges();
    const btn = template.querySelector(`#crud-manage-${component.uid}`);
    expect(btn).toBeTruthy();
  });

  it('should render a manage button with a provided status and a deliver delivery', () => {
    component.crudConfig = [
      CRUD.READ,
      CRUD.MANAGE
    ];
    component.status = 'enum.provided';
    component.delivery = 'enum.deliver'
    fixture.detectChanges();
    const btn = template.querySelector(`#crud-manage-${component.uid}`);
    expect(btn).toBeTruthy();
  });

  it('should NOT render a manage button with a delivered status and a deliver delivery', () => {
    component.crudConfig = [
      CRUD.READ,
      CRUD.MANAGE
    ];
    component.status = 'enum.delivered';
    component.delivery = 'enum.deliver'
    fixture.detectChanges();
    const btn = template.querySelector(`#crud-manage-${component.uid}`);
    expect(btn).toBeFalsy();
  });

  it('should render a manage button with a placed status and a takeaway delivery', () => {
    component.crudConfig = [
      CRUD.READ,
      CRUD.MANAGE
    ];
    component.status = 'enum.placed';
    component.delivery = 'enum.takeaway'
    fixture.detectChanges();
    const btn = template.querySelector(`#crud-manage-${component.uid}`);
    expect(btn).toBeTruthy();
  });

  it('should render a manage button with a approved status and a takeaway delivery', () => {
    component.crudConfig = [
      CRUD.READ,
      CRUD.MANAGE
    ];
    component.status = 'enum.approved';
    component.delivery = 'enum.takeaway'
    fixture.detectChanges();
    const btn = template.querySelector(`#crud-manage-${component.uid}`);
    expect(btn).toBeTruthy();
  });

  it('should NOT render a manage button with a provided status and a takeaway delivery', () => {
    component.crudConfig = [
      CRUD.READ,
      CRUD.MANAGE
    ];
    component.status = 'enum.provided';
    component.delivery = 'enum.takeaway'
    fixture.detectChanges();
    const btn = template.querySelector(`#crud-manage-${component.uid}`);
    expect(btn).toBeFalsy();
  });

  /**
   * Event emitter
   */
  it('read btn should emit read event', () => {
    component.crudConfig = [
      CRUD.READ,
    ];
    fixture.detectChanges();
    spyOn(component.crud, 'emit');
    const btn = template.querySelector(`#crud-read-${component.uid}`);
    btn?.dispatchEvent(new Event('click'));
    expect(component.crud.emit).toHaveBeenCalledWith({action: CRUD.READ, uid: component.uid});
  });

  it('update btn should emit update event', () => {
    component.crudConfig = [
      CRUD.UPDATE
    ];
    fixture.detectChanges();
    spyOn(component.crud, 'emit');
    const btn = template.querySelector(`#crud-update-${component.uid}`);
    btn?.dispatchEvent(new Event('click'));
    expect(component.crud.emit).toHaveBeenCalledWith({action: CRUD.UPDATE, uid: component.uid});
  });

  it('delete btn should emit delete event', () => {
    component.crudConfig = [
      CRUD.DELETE
    ];
    fixture.detectChanges();
    spyOn(component.crud, 'emit');
    const btn = template.querySelector(`#crud-delete-${component.uid}`);
    btn?.dispatchEvent(new Event('click'));
    expect(component.crud.emit).toHaveBeenCalledWith({action: CRUD.DELETE, uid: component.uid});
  });

  it('standardise btn should emit standardise event', () => {
    component.crudConfig = [
      CRUD.STANDARDISE
    ];
    component.isStandard = false;
    fixture.detectChanges();
    spyOn(component.crud, 'emit');
    const btn = template.querySelector(`#crud-standard-${component.uid}`);
    btn?.dispatchEvent(new Event('click'));
    expect(component.crud.emit).toHaveBeenCalledWith({action: CRUD.STANDARDISE, uid: component.uid});
  });

  it('add btn should emit add event', () => {
    component.crudConfig = [
      CRUD.ADD
    ];
    component.isStandard = true;
    component.franchise = faker.datatype.uuid();
    fixture.detectChanges();
    spyOn(component.crud, 'emit');
    const btn = template.querySelector(`#crud-add-${component.uid}`);
    btn?.dispatchEvent(new Event('click'));
    expect(component.crud.emit).toHaveBeenCalledWith({action: CRUD.ADD, uid: component.uid});
  });

  it('remove btn should emit remove event', () => {
    component.crudConfig = [
      CRUD.REMOVE
    ];
    component.isStandard = true;
    component.franchise = faker.datatype.uuid();
    component.actives.push(component.uid);
    fixture.detectChanges();
    spyOn(component.crud, 'emit');
    const btn = template.querySelector(`#crud-remove-${component.uid}`);
    btn?.dispatchEvent(new Event('click'));
    expect(component.crud.emit).toHaveBeenCalledWith({action: CRUD.REMOVE, uid: component.uid});
  });

  it('manage btn should emit manage event', () => {
    component.crudConfig = [
      CRUD.MANAGE
    ];
    component.status = OrderStatus.PLACED;
    component.delivery = 'enum.deliver'
    fixture.detectChanges();
    spyOn(component.crud, 'emit');
    const btn = template.querySelector(`#crud-manage-${component.uid}`);
    btn?.dispatchEvent(new Event('click'));
    expect(component.crud.emit).toHaveBeenCalledWith({action: CRUD.MANAGE, uid: component.uid});
  });

});
