import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Restaurant } from 'src/app/models/apis/models';
import { ApiUrl } from 'src/app/models/enums/ApiUrl';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';
import formatUtils from 'src/app/utils/format.utils';
import { environment } from 'src/environments/environment';
import { RestaurantDetailComponent } from '../../dialog/restaurant-detail/restaurant-detail.component';

/**
 * shop home page
 */
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {

  /**
   * formatting tools
   */
  formatUtils = formatUtils;

  /**
   * catalog route
   */
  catalog: string = ['', environment.core.ROUTES.default.modules.shop.childrens.catalog.url, ''].join('/');

  /**
   * restaurants to display
   */
  resources: Restaurant[] = [];

  /**
   * class constructor
   * @param baseRepo application repository
   * @param dialog material modal
   * @param spinner allow access to the application spinner state
   */
  constructor(
    private baseRepo: BaseRepositoryService,
    protected dialog: MatDialog,
    public spinner: SpinnerService
  ) {
    this.getAll(ApiUrl.RESTAURANT);
  }

  /**
   * get all restaurant
   * @param api api url
   */
  private getAll(api: string){
    this.spinner.increaseCount();
    this.baseRepo.getAll(api)
    .then(res => {
      this.resources = res.body;
      this.spinner.decreaseCount();
    })
    .catch(err => {
      console.error(err);
      this.spinner.decreaseCount();
    });
  }

  /**
   * open a restaurant detail modal
   * @param id restaurant id
   */
  openRestaurantModal(id: string|undefined){
    if (id) {
      const restaurant = this.resources.find(r => r.id === id);
      this.dialog.open(RestaurantDetailComponent, {data: restaurant});
    }
  }
}
