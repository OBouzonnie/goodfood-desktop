import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { AppModule } from 'src/app/app.module';
import { Restaurant } from 'src/app/models/apis/models';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { ShopModule } from 'src/app/modules/shop/shop.module';
import { MockService } from 'src/app/services/mock/mock.service';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';
import { HomeComponent } from './home.component';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let template: HTMLElement;
  let mockService: MockService;
  let resources: Restaurant[];
  let baseRepo: BaseRepositoryService;
  let getAllSpy: jasmine.Spy;
  let spinner: SpinnerService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule, SharedModule, ShopModule, TranslateModule],
      declarations: [ HomeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    spinner = TestBed.inject(SpinnerService);
    spyOn(spinner, 'increaseCount');
    spyOn(spinner, 'decreaseCount');
    baseRepo = TestBed.inject(BaseRepositoryService);
    mockService = new MockService();
    resources = mockService.getRestaurants();
    getAllSpy = spyOn(baseRepo, 'getAll').and.callThrough().and.returnValue(Promise.resolve({body: resources}));
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    template = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render a link for each restaurant', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      const card = template.querySelectorAll('mat-card');
      expect(card.length).toBe(resources.length);
      for (let i = 0; i < resources.length; i++){
        expect(card[i].textContent).toContain(resources[i].name);
      }
    });
  }));

  it('should NOT render a link for an inactive restaurant', waitForAsync(() => {
    const i = Math.trunc(Math.random() * resources.length);
    resources[i].isActive = false;
    fixture = TestBed.createComponent(HomeComponent);
    template = fixture.nativeElement;
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      const card = template.querySelectorAll('mat-card');
      expect(card.length).toBe(resources.length - 1);
    });
  }));
});
