import { Component, Input, OnInit } from '@angular/core';
import { Feedback } from 'src/app/models/apis/models';

/**
 * Restaurant feedbacks
 */
@Component({
  selector: 'app-restaurant-feedback',
  templateUrl: './restaurant-feedback.component.html',
  styleUrls: ['./restaurant-feedback.component.scss']
})
export class RestaurantFeedbackComponent {

  /**
   * restaurant feedback to display
   */
  @Input() feedback: Feedback|null = null;

  constructor() { }

}
