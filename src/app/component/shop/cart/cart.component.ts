import { Component } from '@angular/core';
import { CartService } from 'src/app/services/cart/cart.service';
import formatUtils from 'src/app/utils/format.utils';
import { environment } from 'src/environments/environment';

/**
 * application cart
 */
@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent {

  /**
   * cart title
   */
  title: string = 'cart.title';

  /**
   * api order url
   */
  orderUrl: string = ['', environment.core.ROUTES.default.modules.shop.childrens.order.url, ''].join('/');

  /**
   * formatting tools
   */
  formatUtils = formatUtils;

  /**
   * class constructor
   * @param cartService allow access to the locale storage cart
   */
  constructor(
    public cartService: CartService
  ) {}
}
