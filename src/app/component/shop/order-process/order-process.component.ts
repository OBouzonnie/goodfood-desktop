import { Component } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Address, Order, Restaurant } from 'src/app/models/apis/models';
import { ApiUrl } from 'src/app/models/enums/ApiUrl';
import { CRUD } from 'src/app/models/enums/CRUD';
import { DeliveryType } from 'src/app/models/enums/Delivery';
import { CartDetail } from 'src/app/models/interfaces/cart-detail.interface';
import { CrudTarget } from 'src/app/models/type/CrudTarget';
import { CartService } from 'src/app/services/cart/cart.service';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';
import { TokenService } from 'src/app/services/token/token.service';
import { environment } from 'src/environments/environment';
import { ModalActionComponent } from '../../dialog/modal-action/modal-action.component';
import { AddressComponent } from '../../form/resource/address/address.component';

/**
 * order creation page
 */
@Component({
  selector: 'app-order-process',
  templateUrl: './order-process.component.html',
  styleUrls: ['./order-process.component.scss']
})
export class OrderProcessComponent {

  /**
   * customer addresses
   */
  addresses: Address[] = [];

  /**
   * customer ID
   */
  customerId: string|null = null;

  /**
   * adress api url
   */
  addressUrl: string|null = null;

  /**
   * order api url
   */
  orderUrl: string|null = null;

  /**
   * processing state
   */
  processing: boolean = false;

  /**
   * customer cart
   */
  cart: CartDetail[] = [];

  /**
   * franchise concern
   */
  franchise: Restaurant|null;

  /**
   *
   * @param dialog material modals
   * @param tokenService allow access to the jwt token
   * @param baseRepo repository service
   * @param cartService allow access to the local storage cart
   * @param router angular router
   * @param spinner allow access to the spinner state
   */
  constructor(
    protected dialog: MatDialog,
    private tokenService: TokenService,
    protected baseRepo: BaseRepositoryService,
    public cartService: CartService,
    private router: Router,
    public spinner: SpinnerService
  ) {
    this.cart = this.cartService.getCart();
    this.franchise = this.cartService.getFranchise();
    if (!this.cart || this.cart.length <= 0) this.router.navigateByUrl(environment.core.ROUTES.default.modules.shop.childrens.home.url);
    this.customerId = this.tokenService.getUser();
    if (this.customerId) {
      this.addressUrl = ApiUrl.CUSTOMER_ADDRESS.replace(environment.core.idParam, this.customerId);
      this.orderUrl = ApiUrl.CUSTOMER_ORDER.replace(environment.core.idParam, this.customerId);
    }
    this.getAddresses();
  }

  /**
   * get customer addresses
   */
  getAddresses(): void {
    this.addresses = [];
    if (this.addressUrl) {
      this.spinner.increaseCount();
      this.baseRepo.getAll(this.addressUrl).then(res => {
        this.addresses = [...res.body];
        this.spinner.decreaseCount();
      })
      .catch(err => {
        console.error(err);
        this.spinner.decreaseCount();
      });
    }
  }

  /**
   * create the order
   * @param event order creation event
   */
  createOrder(event: any){
    if(event && this.orderUrl && this.customerId && this.franchise) {
      this.processing = true;
      event.details = [];
      this.cart.forEach(item => {
        event.details.push({
          recipe: item.recipe,
          quantity: item.quantity,
          price: item.price
        })
      })
      event.details = this.cart;
      event.customer = this.customerId;
      event.restaurant = this.franchise.id;
      this.post(this.orderUrl, event, this.handlePostOrderResponse);
    }
  }

  /**
   * handle order creation response
   * @param id order id
   * @param order the processed order
   */
  handlePostOrderResponse: Function = (id: string, order: Order) => {
    if(order.delivery){
      let url: string = '';
      switch(order.delivery?.type){
        case DeliveryType.TAKEAWAY:
          url = environment.core.ROUTES.default.modules.shop.childrens.recap.url;
          break;
        case DeliveryType.DELIVER:
          url = environment.core.ROUTES.default.modules.shop.childrens.payment.url;
          break;
      }
      this.router.navigateByUrl(url + '/' + id);
    }
  }

  /**
   * trigger the crud method related to the crud action perform
   * @param event crud action for this address
   */
  addressCrudAction(event: CrudTarget): void{
    if (this.customerId && this.addressUrl) {
      switch(event.action){
        case CRUD.CREATE:
          this.crudCreate(this.addressUrl);
          break;
        case CRUD.DELETE:
          this.crudDelete(this.addressUrl, event);
          break;
      }
    }
  }

  /**
   * create a resource
   * @param url api url
   */
  crudCreate(url: string){
    const ref = this.openCRUmodal({action: CRUD.CREATE, uid: ''});
    ref.componentInstance.submitted.subscribe((confirm: boolean) => {
      if (confirm) this.post(url, ref.componentInstance.getBody());
      ref.componentInstance.submitted.unsubscribe();
      ref.close();
      this.refreshAddresses();
    });
  }

  /**
   * delete a resource
   * @param url api url
   * @param crudTarget crud action and target
   */
  crudDelete(url: string, crudTarget: CrudTarget) {
    const ref = this.openDmodal();
    ref.componentInstance.confirm.subscribe((confirm: boolean) => {
      if (confirm) this.delete(url, crudTarget.uid);
      ref.componentInstance.confirm.unsubscribe();
      ref.close();
      this.refreshAddresses();
    });
  }

  /**
   * refresh address list timer
   */
  refreshAddresses(){
    setTimeout(() => {
      this.getAddresses();
    }, 1000);
  }

  /**
   * open the modal for Create, Read, Update
   * @param crudTarget crud action and target
   * @returns the modal reference
   */
  openCRUmodal(crudTarget: CrudTarget): MatDialogRef<AddressComponent> {
    return this.dialog.open(AddressComponent, {data: crudTarget});
  }

  /**
   * open the Delete modal
   * @returns the modal reference
   */
  openDmodal(): MatDialogRef<ModalActionComponent> {
    return this.dialog.open(ModalActionComponent);
  }

  /**
   * call the repository to process a post request
   * @param url api url
   * @param body the request body
   * @param callback callback execute after the response
   */
  post(url: string, body: Address|Order, callback?: Function) {
    console.log(`POST ${url} with body ${body}`);
    this.baseRepo.post(url, body).then(res => {
      if(callback && res.body?.id) callback(res.body.id, body);
      this.processing = false;
    })
    .catch(err => {
      console.error(err);
      this.processing = false;
    });;
  }

  /**
   * calls the repository to process a ut request
   * @param url api url
   * @param body request body
   */
  put(url: string, body: Order) {
    console.log(`POST ${url} with body ${body}`);
    this.baseRepo.post(url, body).then(res => {
      console.log(res);
    });
  }

  /**
   * calls the repository to process a delete request
   * @param url api url
   * @param uid the resource id
   */
  delete(url: string, uid: string){
    console.log(`DELETE ${url}/${uid}`);
    this.baseRepo.delete(url, uid).then(res => {
      console.log(res)
    });
  }

}
