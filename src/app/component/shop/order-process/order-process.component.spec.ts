import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TranslateModule } from '@ngx-translate/core';
import { AppModule } from 'src/app/app.module';
import { Address, Restaurant } from 'src/app/models/apis/models';
import { ShopModule } from 'src/app/modules/shop/shop.module';
import { CartService } from 'src/app/services/cart/cart.service';
import { MockService } from 'src/app/services/mock/mock.service';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';
import { TokenService } from 'src/app/services/token/token.service';
import { OrderProcessComponent } from './order-process.component';

describe('OrderProcessComponent', () => {
  let component: OrderProcessComponent;
  let fixture: ComponentFixture<OrderProcessComponent>;
  let template: HTMLElement;
  let mockService: MockService;
  let baseRepo: BaseRepositoryService;
  let tokenService: TokenService;
  let addresses: Address[];
  let spinner: SpinnerService;
  let cartService: CartService;
  let franchise: Restaurant;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule, ShopModule, TranslateModule],
      declarations: [ OrderProcessComponent ],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: {} }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    spinner = TestBed.inject(SpinnerService);
    spyOn(spinner, 'increaseCount');
    spyOn(spinner, 'decreaseCount');
    baseRepo = TestBed.inject(BaseRepositoryService);
    tokenService = TestBed.inject(TokenService);
    cartService = TestBed.inject(CartService);
    mockService = new MockService();
    addresses = mockService.getAddresses();
    franchise = mockService.mockRestaurant();
    tokenService.saveToken({token: "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJvbGJvdS5jZXNpQGdtYWlsLmNvbSIsInVzZXJJZCI6IjYyYmY1YmM2ODNkZDRiNGI1OGJkOGUzNSIsInJvbGUiOlt7ImF1dGhvcml0eSI6IlVTRVIifV0sImV4cCI6MTY1NjcxMTY1NH0.KgMoAAC6f11xhlI7K9B8XxFLlaefO46hdj5zwbthuYAGrSsWQzIAMOiqGvxRxz4pz2L4jHBfP4RiJ-gu3QR8SA"});
    spyOn(baseRepo, 'getAll').and.callThrough().and.returnValue(Promise.resolve({body : addresses}));
    spyOn(cartService, 'getFranchise').and.returnValue(franchise);
    fixture = TestBed.createComponent(OrderProcessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    template = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render app-order component', () => {
    const form = template.querySelector('#order-form')
    expect(form).toBeTruthy();
  });

  it('should render all addresses', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(baseRepo.getAll).toHaveBeenCalled();
      component.addresses.forEach(a => {
        expect(template.textContent).toContain(a.city);
        expect(template.textContent).toContain(a.postal);
        expect(template.textContent).toContain(a.number);
        expect(template.textContent).toContain(a.street);
      });
    });
  }));

  it('should create order for delivery', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      spyOn(component, 'post');
      const btn = template.querySelector('#submit-btn')as HTMLElement;
      const type = template.querySelector('#type') as HTMLElement;
      const radios = document.querySelectorAll('.mat-radio-input');
      const r = Math.floor(Math.random() * radios.length);
      const radio = radios[r] as HTMLElement;
      type?.click();
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        const options = document.querySelectorAll('mat-option');
        const option = options[0];
        option.dispatchEvent(new Event('click'));
        radio.click();
        btn?.click();
        fixture.detectChanges();
        expect(component.post).toHaveBeenCalled();
      });
    });
  }));

  it('should create order for takeaway', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      spyOn(component, 'post');
      const btn = template.querySelector('#submit-btn')as HTMLElement;
      const type = template.querySelector('#type') as HTMLElement;
      type?.click();
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        const options = document.querySelectorAll('mat-option');
        const option = options[1];
        option.dispatchEvent(new Event('click'));
        btn?.click();
        fixture.detectChanges();
        expect(component.post).toHaveBeenCalled();
      });
    });
  }));

  it('should not create order without a delivery type', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      spyOn(component, 'post');
      const btn = template.querySelector('#submit-btn')as HTMLElement;
      const radios = document.querySelectorAll('.mat-radio-input');
      const r = Math.floor(Math.random() * radios.length);
      const radio = radios[r] as HTMLElement;
      radio.click();
      btn?.click();
      fixture.detectChanges();
      expect(component.post).not.toHaveBeenCalled();
    });
  }));

  it('should not create order for a delivery type without an address', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      spyOn(component, 'post');
      const btn = template.querySelector('#submit-btn')as HTMLElement;
      const type = template.querySelector('#type') as HTMLElement;
      type?.click();
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        const options = document.querySelectorAll('mat-option');
        const option = options[0];
        option.dispatchEvent(new Event('click'));
        btn?.click();
        fixture.detectChanges();
        expect(component.post).not.toHaveBeenCalled();
      });
    });
  }));
});
