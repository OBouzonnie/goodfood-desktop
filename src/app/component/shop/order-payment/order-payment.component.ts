import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Order } from 'src/app/models/apis/models';
import { ApiUrl } from 'src/app/models/enums/ApiUrl';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import { environment } from 'src/environments/environment';

/**
 * Order payment page
 */
@Component({
  selector: 'app-order-payment',
  templateUrl: './order-payment.component.html',
  styleUrls: ['./order-payment.component.scss']
})
export class OrderPaymentComponent {

  /**
   * order processed
   */
  order: Order|null = null;

  /**
   * order id
   */
  id: string|null = null;

  /**
   * order api url
   */
  api: ApiUrl = ApiUrl.ORDER;

  /**
   * class constructor
   * @param baseRepo application repository
   * @param route angular activated route
   * @param router angular router
   */
  constructor(
    private baseRepo: BaseRepositoryService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.id = this.route.snapshot.paramMap.get('id');
    if (this.id) this.get(this.api, this.id);
  }

  /**
   * calls the repository to perform a get request
   * @param api api url
   * @param id resource id
   */
  private get(api: string, id: string){
    this.baseRepo.get(api, id)
    .then((res: any) => {
      this.order = res.body;
      if(!this.order) this.router.navigateByUrl(environment.core.ROUTES.default.modules.shop.childrens.home.url);
      if(this.order?.isPayed) this.router.navigateByUrl(environment.core.ROUTES.default.modules.shop.childrens.payment.url + '/'+ this.id);
    })
    .catch(err => {
      console.error(err);
    });
  }

  /**
   * process an order is payed event
   * @param event order is payed event
   */
  orderIsPayed(event: string){
    if(this.order && this.order.id) {
      const payUrl = ApiUrl.ORDER_PAY.replace(environment.core.idParam, this.order.id)
      this.baseRepo.post(payUrl, event)
      .then((res: any) => {
        const redirectUrl = environment.core.ROUTES.default.modules.shop.childrens.recap.url;
        if (this.order) this.router.navigateByUrl(redirectUrl + '/' + this.order.id);
      })
      .catch(err => {
        console.error(err);
      });
    }
  }

}
