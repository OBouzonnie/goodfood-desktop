import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import faker from '@faker-js/faker';
import { TranslateModule } from '@ngx-translate/core';
import { AppModule } from 'src/app/app.module';
import { CustomerInformation } from 'src/app/models/apis/models';
import { ShopModule } from 'src/app/modules/shop/shop.module';
import { MockService } from 'src/app/services/mock/mock.service';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import { TokenService } from 'src/app/services/token/token.service';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';
import { AccountComponent } from './account.component';

describe('AccountComponent', () => {
  let component: AccountComponent;
  let fixture: ComponentFixture<AccountComponent>;
  let template: HTMLElement;
  let mockService: MockService;
  let baseRepo: BaseRepositoryService;
  let tokenService: TokenService;
  let info: CustomerInformation;
  let spinner: SpinnerService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule, ShopModule, TranslateModule],
      declarations: [ AccountComponent ],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: {} }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    spinner = TestBed.inject(SpinnerService);
    spyOn(spinner, 'increaseCount');
    spyOn(spinner, 'decreaseCount');
    baseRepo = TestBed.inject(BaseRepositoryService);
    tokenService = TestBed.inject(TokenService);
    mockService = new MockService();
    info = mockService.getCustomerInfo();
    spyOn(baseRepo, 'get').and.callThrough().and.returnValue(Promise.resolve({body : info}));
    tokenService.saveToken({token: "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJvbGJvdS5jZXNpQGdtYWlsLmNvbSIsInVzZXJJZCI6IjYyYmY1YmM2ODNkZDRiNGI1OGJkOGUzNSIsInJvbGUiOlt7ImF1dGhvcml0eSI6IlVTRVIifV0sImV4cCI6MTY1NjcxMTY1NH0.KgMoAAC6f11xhlI7K9B8XxFLlaefO46hdj5zwbthuYAGrSsWQzIAMOiqGvxRxz4pz2L4jHBfP4RiJ-gu3QR8SA"});
    fixture = TestBed.createComponent(AccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    template = fixture.nativeElement;
    component.info = info
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('modal should display customer info for info', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      const btn = template.querySelector('#info-crud-btn');
      btn?.dispatchEvent(new Event('click'));
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        const modal = document.querySelector('app-modal');
        const form = modal?.querySelector('#customer-info-form');
        const firstname = form?.querySelector('#firstname') as HTMLInputElement;
        const lastname = form?.querySelector('#lastname') as HTMLInputElement;
        const phone = form?.querySelector('#phone') as HTMLInputElement;
        expect(firstname.value).toBe(info.firstName ? info.firstName : '');
        expect(lastname.value).toBe(info.lastName ? info.lastName : '');
        expect(phone.value).toBe(info.phone);
      })
    });
  }));

  it('form submission for no info', waitForAsync(() => {
    fixture.whenStable().then(() => {
      component.info = undefined;
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        spyOn(baseRepo, 'post').and.callThrough().and.returnValue(Promise.resolve({}));
        const btn = template.querySelector('#info-crud-btn');
        btn?.dispatchEvent(new Event('click'));
        fixture.whenStable().then(() => {
          fixture.detectChanges();
          const modal = document.querySelector('app-modal');
          const form = modal?.querySelector('form');
          const confirm = modal?.querySelector('#confirm-modal-btn');
          const firstname = form?.querySelector('#firstname') as HTMLInputElement;
          const lastname = form?.querySelector('#lastname') as HTMLInputElement;
          const phone = form?.querySelector('#phone') as HTMLInputElement;
          const birthdate = form?.querySelector('#birthdate') as HTMLInputElement;
          firstname.value = faker.name.lastName();
          firstname.dispatchEvent(new Event('input'));
          lastname.value = faker.name.lastName();
          lastname.dispatchEvent(new Event('input'));
          phone.value = Math.floor(Math.random() * 1000000).toString();
          phone.dispatchEvent(new Event('input'));
          birthdate.value = mockService.getDate();
          birthdate.dispatchEvent(new Event('input'));
          fixture.detectChanges();
          confirm?.dispatchEvent(new Event('click'));
          fixture.whenStable().then(() => {
            fixture.detectChanges();
            expect(baseRepo.post).toHaveBeenCalled();
          })
        })
      });
    });
  }));

  it('form submission for info', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      spyOn(baseRepo, 'put').and.callThrough().and.returnValue(Promise.resolve({}));
      const btn = template.querySelector('#info-crud-btn');
      btn?.dispatchEvent(new Event('click'));
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        const modal = document.querySelector('app-modal');
        const form = modal?.querySelector('form');
        const confirm = modal?.querySelector('#confirm-modal-btn');
        const firstname = form?.querySelector('#firstname') as HTMLInputElement;
        const lastname = form?.querySelector('#lastname') as HTMLInputElement;
        const phone = form?.querySelector('#phone') as HTMLInputElement;
        const birthdate = form?.querySelector('#birthdate') as HTMLInputElement;
        firstname.value = faker.name.lastName();
        firstname.dispatchEvent(new Event('input'));
        lastname.value = faker.name.lastName();
        lastname.dispatchEvent(new Event('input'));
        phone.value = Math.floor(Math.random() * 1000000).toString();
        phone.dispatchEvent(new Event('input'));
        birthdate.value = mockService.getDate();
        birthdate.dispatchEvent(new Event('input'));
        fixture.detectChanges();
        confirm?.dispatchEvent(new Event('click'));
        fixture.detectChanges();
        expect(baseRepo.put).toHaveBeenCalled();
      })
    });
  }));
});
