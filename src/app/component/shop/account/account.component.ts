import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CustomerInformation, Order } from 'src/app/models/apis/models';
import { ApiUrl } from 'src/app/models/enums/ApiUrl';
import { CRUD } from 'src/app/models/enums/CRUD';
import { CustomerInfoTableMapper } from 'src/app/models/table/mappers/CustomerInfoTableMapper/CustomerInfoTableMapper';
import { OrderTableMapper } from 'src/app/models/table/mappers/OrderTableMapper/OrderTableMapper';
import { TableModel } from 'src/app/models/table/resources/TableModel';
import { CrudConfig } from 'src/app/models/type/CrudConfig';
import { CrudTarget } from 'src/app/models/type/CrudTarget';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';
import { TokenService } from 'src/app/services/token/token.service';
import { environment } from 'src/environments/environment';
import { OrderDetailComponent } from '../../dialog/order-detail/order-detail.component';
import { CustomerInfoComponent } from '../../form/customer-info/customer-info.component';

/**
 * account page
 */
@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit, OnChanges {

  /**
   * customer informations
   */
  info?: CustomerInformation;

  /**
   * customer orders
   */
  orders?: Order[];

  /**
   * customer ID
   */
  customerId: string|null = null;

  /**
   * customer info api url
   */
  infoUrl: string|null = null;

  /**
   * order api url
   */
  orderUrl: string|null = null;

  /**
   * no data display value
   */
  noData: string = environment.core.noData;

  /**
   * toggling point between big and small screen
   */
  breakpoint: number = 600;

  /**
   * screen size
   */
  public innerWidth: any;

  /**
   * crud actions allowed
   */
  crudConfig: CrudConfig = [
    CRUD.READ
  ]

  /**
   * Define table columns
   */
  displayedColumns: string[] = [];

   /**
    * Table Data content
    */
  dataSource: TableModel[] = [];

   /**
    * Table Head content
    */
  head: any;

  /**
   * Map Table content from provided resources
   */
  protected tableMapper: OrderTableMapper = new OrderTableMapper(false);

  /**
   * map customer information for table display
   */
  infoMapper: CustomerInfoTableMapper|null = null;

  /**
   * customer info table title
   */
  infoTitle: string = 'dialog.order.info';

  /**
   * class constructor
   * @param dialog material dialog modal
   * @param tokenService allow access to the jwt token
   * @param baseRepo repository service
   * @param spinner allow access to the spinner state
   */
  constructor(
    private dialog: MatDialog,
    private tokenService: TokenService,
    protected baseRepo: BaseRepositoryService,
    public spinner: SpinnerService
  ) {
    this.customerId = this.tokenService.getUser();
    if (this.customerId) {
      this.infoUrl = ApiUrl.CUSTOMER + '/' + this.customerId + ApiUrl.CUSTOMER_INFORMATION;
      this.orderUrl = ApiUrl.CUSTOMER + '/' + this.customerId + ApiUrl.ORDER;
    }
    this.getCustomerInfo();
    this.getCustomerOrders();
  }

  ngOnInit(){
    this.innerWidth = window.innerWidth;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes) {
      this.innerWidth = window.innerWidth;
    }
  }

  /**
   * render the order table
   */
  renderTable(){
    this.displayedColumns = this.tableMapper.getColumns();
    this.head = this.tableMapper.head;
    this.dataSource = this.tableMapper.rows;
  }

  /**
   * handle customer info crud
   */
  infoCRUD(){
    const ref = this.dialog.open(CustomerInfoComponent, {data: this.info});
    ref.componentInstance.submitted.subscribe((confirm: boolean) => {
      if (confirm && this.infoUrl) {
        if (this.info) {
          this.putCustomerInfo(this.infoUrl, ref.componentInstance.getBody());
        } else {
          this.postCustomerInfo(this.infoUrl, ref.componentInstance.getBody());
        }
      }
      ref.componentInstance.submitted.unsubscribe();
      ref.close();
    });
  }

  /**
   * get customer informations
   */
  getCustomerInfo(){
    if (this.infoUrl) {
      this.spinner.increaseCount();
      this.baseRepo.get(this.infoUrl).then(res => {
        this.info = res.body;
        if (this.info) this.infoMapper = new CustomerInfoTableMapper(this.info);
        this.spinner.decreaseCount();
      })
      .catch(err => {
        console.error(err);
        this.spinner.decreaseCount();
      });
    }
  }

  /**
   * get customer orders
   */
  getCustomerOrders(){
    if (this.orderUrl) {
      this.spinner.increaseCount();
      this.baseRepo.get(this.orderUrl).then(res => {
        this.orders = res.body;
        if(this.orders) {
          this.tableMapper.addMultipleRows(this.orders);
        }
        this.renderTable();
        this.spinner.decreaseCount();
      })
      .catch(err => {
        console.error(err);
        this.spinner.decreaseCount();
      });;
    }
  }

  /**
   * update customer info
   * @param url api url
   * @param body request body
   */
  putCustomerInfo(url: string, body: CustomerInformation){
    this.baseRepo.put(url, body).then(res => {
      this.getCustomerInfo();
    });
  }

  /**
   * create customer info
   * @param url api url
   * @param body request body
   */
  postCustomerInfo(url: string, body: CustomerInformation){
    this.baseRepo.post(url, body).then(res => {
      this.getCustomerInfo();
    });
  }

  /**
   * handle order crud action - readonly
   * @param event order crud event
   */
  crudAction(event: CrudTarget): void{
    if(event.action == CRUD.READ && this.orders){
      const order = this.orders.find(o => o.id === event.uid);
      if (order) this.dialog.open(OrderDetailComponent, {data: order});
    }
  }
}
