import { Component, Input } from '@angular/core';
import { Address } from 'src/app/models/apis/models';
import { DeliveryType } from 'src/app/models/enums/Delivery';
import { CartDetail } from 'src/app/models/interfaces/cart-detail.interface';
import formatUtils from 'src/app/utils/format.utils';

/**
 * cart recap component
 */
@Component({
  selector: 'app-cart-recap',
  templateUrl: './cart-recap.component.html',
  styleUrls: ['./cart-recap.component.scss']
})
export class CartRecapComponent {

  /**
   * active cart franchise
   */
  @Input() franchise?: string|null = null;

  /**
   * cart details
   */
  @Input() details?: CartDetail[] = [];

  /**
   * cart total
   */
  @Input() total?: number|null = null;

  /**
   * cart active discount
   */
  @Input() discount?: number|null = null;

  /**
   * franchise address
   */
  @Input() address?: Address;

  /**
   * active delivery type
   */
  @Input() delivery?: "deliver" | "take-away";

  /**
   * formatting tools
   */
  formatUtils = formatUtils;

  /**
   * delivery type values
   */
  deliveryEnum = DeliveryType;

  constructor() {}

}
