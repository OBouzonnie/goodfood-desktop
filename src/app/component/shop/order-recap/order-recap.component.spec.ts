import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { AppModule } from 'src/app/app.module';
import { DeliveryType } from 'src/app/models/enums/Delivery';
import { OrderStatus } from 'src/app/models/enums/OrderStatus';
import { ShopModule } from 'src/app/modules/shop/shop.module';
import { MockService } from 'src/app/services/mock/mock.service';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';

import { OrderRecapComponent } from './order-recap.component';

describe('OrderRecapComponent', () => {
  let component: OrderRecapComponent;
  let fixture: ComponentFixture<OrderRecapComponent>;
  let template: HTMLElement;
  let mockService: MockService;
  let baseRepo: BaseRepositoryService;
  let route: ActivatedRoute;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule, ShopModule],
      declarations: [ OrderRecapComponent ]
    })
    .compileComponents();
    mockService = TestBed.inject(MockService);
    baseRepo = TestBed.inject(BaseRepositoryService);
    route = TestBed.inject(ActivatedRoute);
    spyOn(route.snapshot.paramMap, 'get').and.returnValue('*');
  });

  it('should create', () => {
    fixture = TestBed.createComponent(OrderRecapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should render an order placed message for a placed delivery order', waitForAsync(() => {
    const order = mockService.mockOrder(OrderStatus.PLACED, DeliveryType.DELIVER);
    spyOn(baseRepo, 'get').and.callThrough().and.returnValue(Promise.resolve({body: order}));
    fixture = TestBed.createComponent(OrderRecapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    template = fixture.nativeElement;
    clearInterval(component.interval);
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(template.textContent).toContain('order.msg.placed');
    });
  }));

  it('should render an order approved message for an approved delivery order', waitForAsync(() => {
    const order = mockService.mockOrder(OrderStatus.APPROVED, DeliveryType.TAKEAWAY);
    spyOn(baseRepo, 'get').and.callThrough().and.returnValue(Promise.resolve({body: order}));
    fixture = TestBed.createComponent(OrderRecapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    template = fixture.nativeElement;
    clearInterval(component.interval);
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(template.textContent).toContain('order.msg.approved');
    });
  }))

  it('should render an order provided message for a provided delivery order', waitForAsync(() => {
    const order = mockService.mockOrder(OrderStatus.PROVIDED, DeliveryType.TAKEAWAY);
    spyOn(baseRepo, 'get').and.callThrough().and.returnValue(Promise.resolve({body: order}));
    fixture = TestBed.createComponent(OrderRecapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    template = fixture.nativeElement;
    clearInterval(component.interval);
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(template.textContent).toContain('order.msg.provided');
    });
  }))

  it('should render an order delivered message for a delivered delivery order', waitForAsync(() => {
    const order = mockService.mockOrder(OrderStatus.DELIVERED, DeliveryType.DELIVER);
    spyOn(baseRepo, 'get').and.callThrough().and.returnValue(Promise.resolve({body: order}));
    fixture = TestBed.createComponent(OrderRecapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    template = fixture.nativeElement;
    clearInterval(component.interval);
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(template.textContent).toContain('order.msg.delivered');
    });
  }))
});
