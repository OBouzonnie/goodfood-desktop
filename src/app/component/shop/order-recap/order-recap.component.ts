import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Order } from 'src/app/models/apis/models';
import { ApiUrl } from 'src/app/models/enums/ApiUrl';
import { OrderStatus } from 'src/app/models/enums/OrderStatus';
import { CartService } from 'src/app/services/cart/cart.service';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import { environment } from 'src/environments/environment';

/**
 * order recap page
 */
@Component({
  selector: 'app-order-recap',
  templateUrl: './order-recap.component.html',
  styleUrls: ['./order-recap.component.scss']
})
export class OrderRecapComponent implements OnDestroy {

  /**
   * order status values
   */
  statusEnum = OrderStatus;

  /**
   * the processed order
   */
  order: Order|null = null;

  /**
   * the order id
   */
  id: string|null = null;

  /**
   * order api url
   */
  api: ApiUrl = ApiUrl.ORDER;

  /**
   * refresh interval
   */
  interval: any = null;

  /**
   * class constructor
   * @param baseRepo repository service
   * @param route angular activated route
   * @param router angular router
   * @param cartService allow access to cart local storage
   */
  constructor(
    private baseRepo: BaseRepositoryService,
    private route: ActivatedRoute,
    private router: Router,
    private cartService: CartService
  ) {
    this.id = this.route.snapshot.paramMap.get('id');
    if (this.id) {
      this.get(this.api, this.id);
      this.interval = setInterval(() => {
        if (this.id) this.get(this.api, this.id);
      }, 30000);
    }
  }

  ngOnDestroy(): void {
    if(this.interval) clearInterval(this.interval);
  }

  /**
   * get the order
   * @param api api url
   * @param id order id
   */
  private get(api: string, id: string){
    this.baseRepo.get(api, id)
    .then((res: any) => {
      this.order = res.body;
      if(!this.order) this.router.navigateByUrl(environment.core.ROUTES.default.modules.shop.childrens.home.url);
      if(this.order) this.cartService.emptyCart();
    })
    .catch(err => {
      console.error(err);
    });
  }

}
