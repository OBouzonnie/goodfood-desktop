import { Component } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { Recipe, Restaurant } from 'src/app/models/apis/models';
import { ApiUrl } from 'src/app/models/enums/ApiUrl';
import { RestaurantRecipeModel } from 'src/app/models/models/RestaurantRecipeModel';
import { CartService } from 'src/app/services/cart/cart.service';
import { CatalogService } from 'src/app/services/catalog/catalog.service';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';
import { environment } from 'src/environments/environment';
import { RecipeDetailComponent } from '../../dialog/recipe-detail/recipe-detail.component';

/**
 * catalog page
 */
@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.scss']
})
export class CatalogComponent {

  /**
   * image encoding
   */
  dwlImg: string = environment.core.dwlImg;

  /**
   * restaurant recipe catalog
   */
  resources: RestaurantRecipeModel[] = [];

  /**
   * catalog title
   */
  title: string|null = null;

  /**
   * restaurant id
   */
  id: string|null = null;

  /**
   * restaurant api url
   */
  api: ApiUrl = ApiUrl.RESTAURANT;

  /**
   * active franchise
   */
  franchise: Restaurant|null = null;

  /**
   * class constructor
   * @param baseRepo repository service
   * @param route angular activated route
   * @param dialog material dialog modal
   * @param catalogService the catalog service
   * @param cartService allow access to the local storage cart
   * @param spinner allow access to the spinner state
   */
  constructor(
    private baseRepo: BaseRepositoryService,
    private route: ActivatedRoute,
    protected dialog: MatDialog,
    public catalogService: CatalogService,
    public cartService: CartService,
    public spinner: SpinnerService
  ) {
    this.id = this.route.snapshot.paramMap.get('id');
    if (this.id) this.get(this.api, this.id);
  }

  /**
   * open the recipe detail modal
   * @param id recipe id
   */
  openRecipeModal(id?: string): void {
    if (id) {
      const recipe = this.resources.find(r => r.id === id);
      if(recipe) {
        const ref = this.getModalRef(recipe);
        ref.componentInstance.toCart.subscribe((recipe: RestaurantRecipeModel) => {
          if(this.franchise) this.cartService.addToCart(this.franchise, recipe);
          ref.componentInstance.toCart.unsubscribe();
        });
      }
    }
  }

  /**
   * create the recipe modal
   * @param recipe the recipe displayed
   * @returns the modal reference
   */
  getModalRef(recipe: Recipe): MatDialogRef<RecipeDetailComponent>{
    return this.dialog.open(RecipeDetailComponent, {data: recipe});
  }

  /**
   * get restaurant recipe catalog
   * @param api api url
   * @param id restaurant id
   */
  private get(api: string, id: string){
    this.spinner.increaseCount();
    this.baseRepo.get(api, id)
    .then((res: any) => {
      this.franchise = res.body;
      if(this.franchise && this.franchise.recipes) this.resources = this.catalogService.mapAllRecipes(this.franchise.recipes);
      this.spinner.decreaseCount();
    })
    .catch(err => {
      console.error(err);
      this.spinner.decreaseCount();
    });
  }

}
