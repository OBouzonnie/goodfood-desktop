import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { AppModule } from 'src/app/app.module';
import { Recipe, Restaurant } from 'src/app/models/apis/models';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { ShopModule } from 'src/app/modules/shop/shop.module';
import { MockService } from 'src/app/services/mock/mock.service';
import { BaseRepositoryService } from 'src/app/services/repository/base-repository.service';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';
import { CatalogComponent } from './catalog.component';

describe('CatalogComponent', () => {
  let component: CatalogComponent;
  let fixture: ComponentFixture<CatalogComponent>;
  let template: HTMLElement;
  let mockService: MockService;
  let resources: Recipe[];
  let baseRepo: BaseRepositoryService;
  let getAllSpy: jasmine.Spy;
  let spinner: SpinnerService;
  let route: ActivatedRoute;
  let franchise: Restaurant;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule, SharedModule, ShopModule, TranslateModule],
      declarations: [ CatalogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    spinner = TestBed.inject(SpinnerService);
    route = TestBed.inject(ActivatedRoute);
    spyOn(route.snapshot.paramMap, 'get').and.returnValue('*');
    spyOn(spinner, 'increaseCount');
    spyOn(spinner, 'decreaseCount');
    baseRepo = TestBed.inject(BaseRepositoryService);
    mockService = new MockService();
    resources = mockService.getRecipes();
    franchise = mockService.mockFranchise();
    getAllSpy = spyOn(baseRepo, 'getAll').and.callThrough().and.returnValue(Promise.resolve({body: resources}));
    spyOn(baseRepo, 'get').and.callThrough().and.returnValue(Promise.resolve({body: franchise}));
    fixture = TestBed.createComponent(CatalogComponent);
    component = fixture.componentInstance;
    component.id = '*';
    fixture.detectChanges();
    template = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render a mat-card for each recipe', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      console.log(component.resources)
      const cards = template.querySelectorAll('mat-card');
      expect(cards.length).toBe(resources.length);
      const detailBtns = template.querySelectorAll('.recipe-detail-btn');
      expect(detailBtns.length).toBe(resources.length);
      const cartBtns = template.querySelectorAll('.add-to-cart-btn');
      expect(cartBtns.length).toBe(resources.length);
      for (let i = 0; i < resources.length; i++){
        expect(cards[i].textContent).toContain(resources[i].name);
      }
    });
  }));
});
