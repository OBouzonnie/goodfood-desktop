import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AsideNavComponent } from '../../component/shared/aside-nav/aside-nav.component';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { LoginComponent } from 'src/app/component/form/login/login.component';
import { MaterialModule } from 'src/app/modules/materials/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { BackOfficePageContentComponent } from '../../component/shared/back-office-page-content/back-office-page-content.component';
import { RegisterComponent } from 'src/app/component/form/register/register.component';
import { AuthDialogComponent } from 'src/app/component/dialog/auth-dialog/auth-dialog.component';
import { ConfirmDialogComponent } from 'src/app/component/dialog/confirm-dialog/confirm-dialog.component';
import { TableComponent } from '../../component/shared/table/table.component';
import { AccordionComponent } from '../../component/shared/accordion/accordion.component';
import { CrudActionsComponent } from 'src/app/component/shared/crud-actions/crud-actions.component';
import { RestaurantComponent } from 'src/app/component/form/resource/restaurant/restaurant.component';
import { RecipeComponent } from 'src/app/component/form/resource/recipe/recipe.component';
import { ResourceComponent } from 'src/app/component/form/resource/resource.component';
import { ModalActionComponent } from 'src/app/component/dialog/modal-action/modal-action.component';
import { ModalComponent } from 'src/app/component/dialog/modal/modal.component';
import { IngredientComponent } from 'src/app/component/form/resource/ingredient/ingredient.component';
import { ShopPageContentComponent } from 'src/app/component/shared/shop-page-content/shop-page-content.component';
import { AddressComponent } from 'src/app/component/form/resource/address/address.component';
import { SupplierComponent } from 'src/app/component/form/resource/supplier/supplier.component';
import { DiscountComponent } from 'src/app/component/form/resource/discount/discount.component';
import { OrderDetailComponent } from 'src/app/component/dialog/order-detail/order-detail.component';
import { InfoTableComponent } from '../../component/shared/info-table/info-table.component';
import { BackOfficeGroupComponent } from 'src/app/component/back-office/back-office-group/back-office-group.component';
import { BackOfficeGroupRestaurantComponent } from 'src/app/component/back-office/back-office-group/back-office-group-restaurant/back-office-group-restaurant.component';
import { ChangePwdComponent } from 'src/app/component/form/change-pwd/change-pwd.component';
import { ForgottenPwdComponent } from 'src/app/component/form/forgotten-pwd/forgotten-pwd.component';

/**
 * Components shared accross the other modules
 */
@NgModule({
  declarations: [
    AsideNavComponent,
    LoginComponent,
    RegisterComponent,
    BackOfficePageContentComponent,
    AuthDialogComponent,
    ConfirmDialogComponent,
    TableComponent,
    AccordionComponent,
    CrudActionsComponent,
    RestaurantComponent,
    RecipeComponent,
    ResourceComponent,
    ModalActionComponent,
    ModalComponent,
    IngredientComponent,
    ShopPageContentComponent,
    AddressComponent,
    SupplierComponent,
    DiscountComponent,
    OrderDetailComponent,
    InfoTableComponent,
    BackOfficeGroupComponent,
    BackOfficeGroupRestaurantComponent,
    ChangePwdComponent,
    ForgottenPwdComponent
  ],
  imports: [
    CommonModule,
    TranslateModule,
    RouterModule,
    MaterialModule,
    ReactiveFormsModule,
  ],
  exports: [
    AsideNavComponent,
    LoginComponent,
    RegisterComponent,
    BackOfficePageContentComponent,
    AuthDialogComponent,
    ConfirmDialogComponent,
    TableComponent,
    AccordionComponent,
    CrudActionsComponent,
    ModalActionComponent,
    ModalComponent,
    ShopPageContentComponent,
    AddressComponent,
    SupplierComponent,
    DiscountComponent,
    InfoTableComponent,
    BackOfficeGroupComponent,
    BackOfficeGroupRestaurantComponent,
    ChangePwdComponent,
    ForgottenPwdComponent
  ]
})
export class SharedModule { }
