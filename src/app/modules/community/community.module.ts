import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CommunityRoutingModule } from './community-routing.module';
import { BackOfficeCommunityPageComponent } from 'src/app/pages/back-office-page/back-office-community-page/back-office-community-page.component';
import { SharedModule } from '../shared/shared.module';
import { BackOfficeCommunityRestaurantComponent } from 'src/app/component/back-office/back-office-community/back-office-community-restaurant/back-office-community-restaurant.component';
import { MaterialModule } from '../materials/material.module';

/**
 * Components for the community module
 * related to the community persona uses cases
 */
@NgModule({
  declarations: [
    BackOfficeCommunityPageComponent,
    BackOfficeCommunityRestaurantComponent
  ],
  imports: [
    CommonModule,
    CommunityRoutingModule,
    MaterialModule,
    SharedModule
  ]
})
export class CommunityModule { }
