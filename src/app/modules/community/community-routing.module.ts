import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BackOfficeCommunityRestaurantComponent } from 'src/app/component/back-office/back-office-community/back-office-community-restaurant/back-office-community-restaurant.component';
import { BackOfficeHomeComponent } from 'src/app/component/back-office/back-office-home/back-office-home.component';
import { BackOfficeCommunityPageComponent } from 'src/app/pages/back-office-page/back-office-community-page/back-office-community-page.component';
import { environment } from 'src/environments/environment';

const routes: Routes = [
  {
    path: '',
    component: BackOfficeCommunityPageComponent,
    children: [
      { path: '', component: BackOfficeHomeComponent },
      { path: environment.core.ROUTES.goodfood.modules.community.childrens.restaurant.url, component: BackOfficeCommunityRestaurantComponent },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommunityRoutingModule { }
