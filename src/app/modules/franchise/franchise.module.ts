import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';

import { FranchiseRoutingModule } from './franchise-routing.module';
import { BackOfficeFranchisePageComponent } from '../../pages/back-office-page/back-office-franchise-page/back-office-franchise-page.component';
import { BackOfficeFranchiseRecipeComponent } from '../../component/back-office/back-office-franchise/back-office-franchise-recipe/back-office-franchise-recipe.component';
import { BackOfficeFranchiseDiscountComponent } from '../../component/back-office/back-office-franchise/back-office-franchise-discount/back-office-franchise-discount.component';
import { BackOfficeFranchiseSupplierComponent } from '../../component/back-office/back-office-franchise/back-office-franchise-supplier/back-office-franchise-supplier.component';
import { BackOfficeFranchiseIngredientComponent } from 'src/app/component/back-office/back-office-franchise/back-office-franchise-ingredient/back-office-franchise-ingredient.component';
import { BackOfficeFranchiseComponent } from 'src/app/component/back-office/back-office-franchise/back-office-franchise.component';
import { BackOfficeFranchiseStatsComponent } from 'src/app/component/back-office/back-office-franchise/back-office-franchise-stats/back-office-franchise-stats.component';
import { BackOfficeFranchiseOrdersComponent } from 'src/app/component/back-office/back-office-franchise/back-office-franchise-orders/back-office-franchise-orders.component';
import { FranchiseOrderComponent } from 'src/app/component/dialog/franchise-order/franchise-order.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../materials/material.module';
import { TranslateModule } from '@ngx-translate/core';
import { BackOfficeFranchisePurchaseComponent } from 'src/app/component/back-office/back-office-franchise/back-office-franchise-purchase/back-office-franchise-purchase.component';
import { PurchaseComponent } from '../../component/form/resource/purchase/purchase.component';

/**
 * Components for the franchise module
 * related to the franchise persona uses cases
 */
@NgModule({
  declarations: [
    BackOfficeFranchisePageComponent,
    BackOfficeFranchiseRecipeComponent,
    BackOfficeFranchiseDiscountComponent,
    BackOfficeFranchiseSupplierComponent,
    BackOfficeFranchiseIngredientComponent,
    BackOfficeFranchiseComponent,
    BackOfficeFranchiseStatsComponent,
    BackOfficeFranchiseOrdersComponent,
    FranchiseOrderComponent,
    BackOfficeFranchisePurchaseComponent,
    PurchaseComponent
  ],
  imports: [
    CommonModule,
    FranchiseRoutingModule,
    SharedModule,
    TranslateModule,
    MaterialModule,
    ReactiveFormsModule
  ]
})
export class FranchiseModule { }
