import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BackOfficeFranchiseDiscountComponent } from 'src/app/component/back-office/back-office-franchise/back-office-franchise-discount/back-office-franchise-discount.component';
import { BackOfficeFranchiseStatsComponent } from 'src/app/component/back-office/back-office-franchise/back-office-franchise-stats/back-office-franchise-stats.component';
import { BackOfficeFranchiseIngredientComponent } from 'src/app/component/back-office/back-office-franchise/back-office-franchise-ingredient/back-office-franchise-ingredient.component';
import { BackOfficeFranchiseOrdersComponent } from 'src/app/component/back-office/back-office-franchise/back-office-franchise-orders/back-office-franchise-orders.component';
import { BackOfficeFranchisePurchaseComponent } from 'src/app/component/back-office/back-office-franchise/back-office-franchise-purchase/back-office-franchise-purchase.component';
import { BackOfficeFranchiseRecipeComponent } from 'src/app/component/back-office/back-office-franchise/back-office-franchise-recipe/back-office-franchise-recipe.component';
import { BackOfficeFranchiseSupplierComponent } from 'src/app/component/back-office/back-office-franchise/back-office-franchise-supplier/back-office-franchise-supplier.component';
import { BackOfficeHomeComponent } from 'src/app/component/back-office/back-office-home/back-office-home.component';
import { BackOfficeFranchisePageComponent } from 'src/app/pages/back-office-page/back-office-franchise-page/back-office-franchise-page.component';
import { environment } from 'src/environments/environment';

export const routes: Routes = [
  {
    path: '',
    component: BackOfficeFranchisePageComponent,
    children: [
      { path: '', component: BackOfficeHomeComponent },
      { path: environment.core.ROUTES.goodfood.modules.franchise.childrens.stats.url, component: BackOfficeFranchiseStatsComponent },
      { path: environment.core.ROUTES.goodfood.modules.franchise.childrens.orders.url, component: BackOfficeFranchiseOrdersComponent },
      { path: environment.core.ROUTES.goodfood.modules.franchise.childrens.recipe.url, component: BackOfficeFranchiseRecipeComponent },
      { path: environment.core.ROUTES.goodfood.modules.franchise.childrens.ingredient.url, component: BackOfficeFranchiseIngredientComponent },
      { path: environment.core.ROUTES.goodfood.modules.franchise.childrens.discount.url, component: BackOfficeFranchiseDiscountComponent },
      { path: environment.core.ROUTES.goodfood.modules.franchise.childrens.supplier.url, component: BackOfficeFranchiseSupplierComponent },
      { path: environment.core.ROUTES.goodfood.modules.franchise.childrens.purchase.url, component: BackOfficeFranchisePurchaseComponent }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FranchiseRoutingModule { }
