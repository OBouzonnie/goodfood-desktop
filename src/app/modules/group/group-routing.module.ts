import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BackOfficeGroupRestaurantComponent } from 'src/app/component/back-office/back-office-group/back-office-group-restaurant/back-office-group-restaurant.component';
import { BackOfficeGroupRecipeComponent } from 'src/app/component/back-office/back-office-group/back-office-group-recipe/back-office-group-recipe.component';
import { BackOfficeGroupDiscountComponent } from 'src/app/component/back-office/back-office-group/back-office-group-discount/back-office-group-discount.component';
import { BackOfficeGroupSupplierComponent } from 'src/app/component/back-office/back-office-group/back-office-group-supplier/back-office-group-supplier.component';
import { BackOfficeGroupPageComponent } from 'src/app/pages/back-office-page/back-office-group-page/back-office-group-page.component';
import { BackOfficeGroupIngredientComponent } from 'src/app/component/back-office/back-office-group/back-office-group-ingredient/back-office-group-ingredient.component';
import { environment } from 'src/environments/environment';
import { BackOfficeGroupAdminComponent } from 'src/app/component/back-office/back-office-group/back-office-group-admin/back-office-group-admin.component';
import { BackOfficeHomeComponent } from 'src/app/component/back-office/back-office-home/back-office-home.component';
import { SuperAdminGuard } from 'src/app/guards/AdminGuard/super-admin.guard';

export const routes: Routes = [
  {
    path: '',
    component: BackOfficeGroupPageComponent,
    children: [
      { path: '', component: BackOfficeHomeComponent },
      { path: environment.core.ROUTES.goodfood.modules.group.childrens.restaurant.url, component: BackOfficeGroupRestaurantComponent },
      { path: environment.core.ROUTES.goodfood.modules.group.childrens.recipe.url, component: BackOfficeGroupRecipeComponent },
      { path: environment.core.ROUTES.goodfood.modules.group.childrens.ingredient.url, component: BackOfficeGroupIngredientComponent },
      { path: environment.core.ROUTES.goodfood.modules.group.childrens.discount.url, component: BackOfficeGroupDiscountComponent },
      { path: environment.core.ROUTES.goodfood.modules.group.childrens.supplier.url, component: BackOfficeGroupSupplierComponent },
      { path: environment.core.ROUTES.goodfood.modules.group.childrens.admin.url, canActivate: [SuperAdminGuard], component: BackOfficeGroupAdminComponent }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GroupRoutingModule { }
