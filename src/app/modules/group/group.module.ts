import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GroupRoutingModule } from './group-routing.module';
import { BackOfficeGroupPageComponent } from '../../pages/back-office-page/back-office-group-page/back-office-group-page.component';
import { SharedModule } from '../shared/shared.module';
import { BackOfficeGroupRecipeComponent } from '../../component/back-office/back-office-group/back-office-group-recipe/back-office-group-recipe.component';
import { BackOfficeGroupDiscountComponent } from '../../component/back-office/back-office-group/back-office-group-discount/back-office-group-discount.component';
import { BackOfficeGroupSupplierComponent } from '../../component/back-office/back-office-group/back-office-group-supplier/back-office-group-supplier.component';
import { BackOfficeGroupIngredientComponent } from 'src/app/component/back-office/back-office-group/back-office-group-ingredient/back-office-group-ingredient.component';
import { BackOfficeGroupAdminComponent } from 'src/app/component/back-office/back-office-group/back-office-group-admin/back-office-group-admin.component';
import { AdminComponent } from '../../component/form/resource/admin/admin.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../materials/material.module';
import { TranslateModule } from '@ngx-translate/core';

/**
 * Components for the group module
 * related to the group persona uses cases
 */
@NgModule({
  declarations: [
    BackOfficeGroupPageComponent,
    BackOfficeGroupRecipeComponent,
    BackOfficeGroupDiscountComponent,
    BackOfficeGroupSupplierComponent,
    BackOfficeGroupIngredientComponent,
    BackOfficeGroupAdminComponent,
    AdminComponent
  ],
  imports: [
    CommonModule,
    GroupRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    MaterialModule,
    TranslateModule
  ]
})
export class GroupModule { }
