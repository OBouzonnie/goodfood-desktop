import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShopPageComponent } from 'src/app/pages/shop-page/shop-page.component';
import { SharedModule } from '../shared/shared.module';
import { ShopRoutingModule } from './shop-routing.module';
import { AccountComponent } from 'src/app/component/shop/account/account.component';
import { OrderProcessComponent } from 'src/app/component/shop/order-process/order-process.component';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from '../materials/material.module';
import { CustomerInfoComponent } from 'src/app/component/form/customer-info/customer-info.component';
import { ReactiveFormsModule } from '@angular/forms';
import { OrderComponent } from 'src/app/component/form/order/order.component';
import { HomeComponent } from 'src/app/component/shop/home/home.component';
import { CatalogComponent } from 'src/app/component/shop/catalog/catalog.component';
import { RecipeDetailComponent } from 'src/app/component/dialog/recipe-detail/recipe-detail.component';
import { RestaurantDetailComponent } from 'src/app/component/dialog/restaurant-detail/restaurant-detail.component';
import { ModalCartComponent } from 'src/app/component/dialog/modal-cart/modal-cart.component';
import { CartRecapComponent } from '../../component/shop/cart-recap/cart-recap.component';
import { CartComponent } from 'src/app/component/shop/cart/cart.component';
import { PaymentComponent } from '../../component/form/payment/payment.component';
import { OrderPaymentComponent } from '../../component/shop/order-payment/order-payment.component';
import { OrderRecapComponent } from '../../component/shop/order-recap/order-recap.component';
import { FeedbackComponent } from '../../component/form/feedback/feedback.component';
import { RestaurantFeedbackComponent } from 'src/app/component/shop/restaurant-feedback/restaurant-feedback.component';

/**
 * Components for the shop module
 * related to the customer persona uses cases
 */
@NgModule({
  declarations: [
    ShopPageComponent,
    AccountComponent,
    OrderProcessComponent,
    CustomerInfoComponent,
    OrderComponent,
    HomeComponent,
    CatalogComponent,
    RecipeDetailComponent,
    RestaurantDetailComponent,
    ModalCartComponent,
    CartRecapComponent,
    CartComponent,
    PaymentComponent,
    OrderPaymentComponent,
    OrderRecapComponent,
    FeedbackComponent,
    RestaurantFeedbackComponent
  ],
  imports: [
    CommonModule,
    TranslateModule,
    MaterialModule,
    SharedModule,
    ShopRoutingModule,
    ReactiveFormsModule
  ]
})
export class ShopModule { }
