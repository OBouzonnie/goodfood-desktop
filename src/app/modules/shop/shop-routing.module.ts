import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ShopPageComponent } from 'src/app/pages/shop-page/shop-page.component';
import { AccountComponent } from 'src/app/component/shop/account/account.component';
import { OrderProcessComponent } from 'src/app/component/shop/order-process/order-process.component';
import { environment } from 'src/environments/environment';
import { HomeComponent } from 'src/app/component/shop/home/home.component';
import { CatalogComponent } from 'src/app/component/shop/catalog/catalog.component';
import { CartComponent } from 'src/app/component/shop/cart/cart.component';
import { OrderPaymentComponent } from 'src/app/component/shop/order-payment/order-payment.component';
import { OrderRecapComponent } from 'src/app/component/shop/order-recap/order-recap.component';

export const routes: Routes = [
  {
    path: '',
    component: ShopPageComponent,
    children: [
      { path: environment.core.ROUTES.default.modules.shop.childrens.home.url, component: HomeComponent },
      { path: environment.core.ROUTES.default.modules.shop.childrens.catalog.url + environment.core.QUERY_PARAMS.id, component: CatalogComponent },
      { path: environment.core.ROUTES.default.modules.shop.childrens.account.url, component: AccountComponent },
      { path: environment.core.ROUTES.default.modules.shop.childrens.order.url, component: OrderProcessComponent },
      { path: environment.core.ROUTES.default.modules.shop.childrens.cart.url, component: CartComponent },
      { path: environment.core.ROUTES.default.modules.shop.childrens.payment.url + environment.core.QUERY_PARAMS.id, component: OrderPaymentComponent },
      { path: environment.core.ROUTES.default.modules.shop.childrens.recap.url + environment.core.QUERY_PARAMS.id, component: OrderRecapComponent },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShopRoutingModule { }
