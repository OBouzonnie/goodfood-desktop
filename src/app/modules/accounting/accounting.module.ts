import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountingRoutingModule } from './accounting-routing.module';
import { BackOfficeAccountingPageComponent } from 'src/app/pages/back-office-page/back-office-accounting-page/back-office-accounting-page.component';
import { SharedModule } from '../shared/shared.module';
import { BackOfficeAccountingRestaurantComponent } from '../../component/back-office/back-office-accounting/back-office-accounting-restaurant/back-office-accounting-restaurant.component';
import { RestaurantAccountingComponent } from '../../component/dialog/restaurant-accounting/restaurant-accounting.component';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from '../materials/material.module';

/**
 * Components for the accounting module
 * related to the accounting persona uses cases
 */
@NgModule({
  declarations: [
    BackOfficeAccountingPageComponent,
    BackOfficeAccountingRestaurantComponent,
    RestaurantAccountingComponent
  ],
  imports: [
    CommonModule,
    AccountingRoutingModule,
    SharedModule,
    TranslateModule,
    MaterialModule
  ]
})
export class AccountingModule { }
