import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BackOfficeAccountingRestaurantComponent } from 'src/app/component/back-office/back-office-accounting/back-office-accounting-restaurant/back-office-accounting-restaurant.component';
import { BackOfficeHomeComponent } from 'src/app/component/back-office/back-office-home/back-office-home.component';
import { BackOfficeAccountingPageComponent } from 'src/app/pages/back-office-page/back-office-accounting-page/back-office-accounting-page.component';
import { environment } from 'src/environments/environment';

const routes: Routes = [
  {
    path: '',
    component: BackOfficeAccountingPageComponent,
    children: [
      { path: '', component: BackOfficeHomeComponent },
      { path: environment.core.ROUTES.goodfood.modules.accounting.childrens.restaurant.url, component: BackOfficeAccountingRestaurantComponent },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountingRoutingModule { }
