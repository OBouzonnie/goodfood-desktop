import { Component } from '@angular/core';
import { ReturnLink } from 'src/app/models/enums/ReturnLink';
import { IPage } from 'src/app/models/interfaces/page.interface';
import { NavLink } from 'src/app/models/type/NavLink';
import { environment } from 'src/environments/environment';

/**
 * Root page for the accounting persona
 */
@Component({
  selector: 'app-back-office-accounting-page',
  templateUrl: '../back-office-page.component.html',
  styleUrls: ['../back-office-page.component.scss']
})
export class BackOfficeAccountingPageComponent implements IPage {

  /**
   * Target of the return link
   */
   home: ReturnLink = ReturnLink.ACCOUNTING;

   /**
    * navigation links for the accounting persona
    */
   nav: NavLink[] = [
     environment.core.ROUTES.goodfood.modules.accounting.childrens.restaurant
   ];
   
   constructor() { }

}
