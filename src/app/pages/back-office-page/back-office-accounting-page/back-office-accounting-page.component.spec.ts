import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppModule } from 'src/app/app.module';
import { BackOfficeAccountingPageComponent } from './back-office-accounting-page.component';

describe('BackOfficeAccountingPageComponent', () => {
  let component: BackOfficeAccountingPageComponent;
  let fixture: ComponentFixture<BackOfficeAccountingPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule],
      declarations: [ BackOfficeAccountingPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BackOfficeAccountingPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
