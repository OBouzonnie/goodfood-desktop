import { Component } from '@angular/core';
import { ReturnLink } from 'src/app/models/enums/ReturnLink';
import { IPage } from 'src/app/models/interfaces/page.interface';
import { NavLink } from 'src/app/models/type/NavLink';
import { environment } from 'src/environments/environment';

/**
 * Root page for the community management persona
 */
@Component({
  selector: 'app-back-office-community-page',
  templateUrl: '../back-office-page.component.html',
  styleUrls: ['../back-office-page.component.scss']
})
export class BackOfficeCommunityPageComponent implements IPage {

  /**
   * Target of the return link
   */
   home: ReturnLink = ReturnLink.COMMUNITY;

   /**
    * navigation links for the community management persona
    */
   nav: NavLink[] = [
     environment.core.ROUTES.goodfood.modules.community.childrens.restaurant
   ];
   
   constructor() { }

}
