import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppModule } from 'src/app/app.module';
import { BackOfficeCommunityPageComponent } from './back-office-community-page.component';

describe('BackOfficeCommunityPageComponent', () => {
  let component: BackOfficeCommunityPageComponent;
  let fixture: ComponentFixture<BackOfficeCommunityPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule],
      declarations: [ BackOfficeCommunityPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BackOfficeCommunityPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
