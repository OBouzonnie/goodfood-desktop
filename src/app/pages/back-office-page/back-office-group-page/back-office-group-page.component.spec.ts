import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import {Location} from "@angular/common";
import { AppModule } from 'src/app/app.module';
import { SharedModule } from 'src/app/modules/shared/shared.module';

import { BackOfficeGroupPageComponent } from './back-office-group-page.component';


describe('BackOfficeGroupPageComponent', () => {
  let component: BackOfficeGroupPageComponent;
  let fixture: ComponentFixture<BackOfficeGroupPageComponent>;
  let template: HTMLElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule, SharedModule],
      declarations: [ BackOfficeGroupPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BackOfficeGroupPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    template = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  /**
   * Rendering
   */
  it('should render a link for each NavLink', () => {
    const element = template.querySelector('app-aside-nav');
    const nav = element?.querySelectorAll('a');
    expect(nav?.length).toBe(component.nav.length + 1);
  });

  it('should render a link for each NavLink with the correct href', () => {
    const element = template.querySelector('app-aside-nav');
    const nav = element?.querySelectorAll('a');
    nav?.forEach((n,i) => {
      const a = n as HTMLAnchorElement;
      if (i !== 0) expect(a.href).toContain(component.nav[i - 1].url);
    })
  });
});
