import { Component } from '@angular/core';
import { ReturnLink } from 'src/app/models/enums/ReturnLink';
import { Roles } from 'src/app/models/enums/Roles';
import { NavLink } from 'src/app/models/type/NavLink';
import { TokenService } from 'src/app/services/token/token.service';
import { environment } from 'src/environments/environment';
import { IPage } from '../../../models/interfaces/page.interface';

/**
 * Root page for the group persona
 */
@Component({
  selector: 'app-group-page',
  templateUrl: '../back-office-page.component.html',
  styleUrls: ['../back-office-page.component.scss']
})
export class BackOfficeGroupPageComponent implements IPage {

  /**
   * Target of the return link
   */
  home: ReturnLink = ReturnLink.GROUP;

  /**
   * navigation links for the group persona
   */
  nav: NavLink[] = [
    environment.core.ROUTES.goodfood.modules.group.childrens.restaurant,
    environment.core.ROUTES.goodfood.modules.group.childrens.recipe,
    environment.core.ROUTES.goodfood.modules.group.childrens.ingredient,
    environment.core.ROUTES.goodfood.modules.group.childrens.discount,
    environment.core.ROUTES.goodfood.modules.group.childrens.supplier
  ];

  /**
   * class constructor
   * @param tokenService allow access to the jwt token
   */
  constructor(
    private tokenService: TokenService
  ) {
    const permissions = this.tokenService.getPermissions();
    if(permissions && permissions.includes(Roles.SUPER_ADMIN)) this.nav.push(environment.core.ROUTES.goodfood.modules.group.childrens.admin);
  }

}
