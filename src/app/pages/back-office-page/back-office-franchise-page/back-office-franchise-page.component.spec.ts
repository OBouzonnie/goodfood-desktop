import { ComponentFixture, TestBed } from '@angular/core/testing';
import {Location} from "@angular/common";
import { Router } from '@angular/router';
import { AppModule } from 'src/app/app.module';
import { SharedModule } from 'src/app/modules/shared/shared.module';

import { BackOfficeFranchisePageComponent } from './back-office-franchise-page.component';

describe('BackOfficeFranchisePageComponent', () => {
  let component: BackOfficeFranchisePageComponent;
  let fixture: ComponentFixture<BackOfficeFranchisePageComponent>;
  let template: HTMLElement;
  let location: Location;
  let router: Router;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule, SharedModule],
      declarations: [ BackOfficeFranchisePageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BackOfficeFranchisePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    template = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  /**
   * Rendering
   */
  it('should render a link for each NavLink', () => {
    const element = template.querySelector('app-aside-nav');
    const nav = element?.querySelectorAll('a');
    expect(nav?.length).toBe(component.nav.length + 1);
  });

  it('should render a link for each NavLink with the correct href', () => {
    const element = template.querySelector('app-aside-nav');
    const nav = element?.querySelectorAll('a');
    nav?.forEach((n,i) => {
      const a = n as HTMLAnchorElement;
      if (i !== 0) expect(a.href).toContain(component.nav[i - 1].url);
    })
  });
});
