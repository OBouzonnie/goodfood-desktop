import { Component } from '@angular/core';
import { ReturnLink } from 'src/app/models/enums/ReturnLink';
import { NavLink } from 'src/app/models/type/NavLink';
import { environment } from 'src/environments/environment';
import { IPage } from '../../../models/interfaces/page.interface';

/**
 * Root page for the franchise persona
 */
@Component({
  selector: 'app-franchise-page',
  templateUrl: '../back-office-page.component.html',
  styleUrls: ['../back-office-page.component.scss']
})
export class BackOfficeFranchisePageComponent implements IPage {

  /**
   * Target of the return link
   */
  home: ReturnLink = ReturnLink.FRANCHISE;

  /**
   * navigation links for the franchise persona
   */
  nav: NavLink[] = [
    environment.core.ROUTES.goodfood.modules.franchise.childrens.stats,
    environment.core.ROUTES.goodfood.modules.franchise.childrens.orders,
    environment.core.ROUTES.goodfood.modules.franchise.childrens.recipe,
    environment.core.ROUTES.goodfood.modules.franchise.childrens.ingredient,
    environment.core.ROUTES.goodfood.modules.franchise.childrens.discount,
    environment.core.ROUTES.goodfood.modules.franchise.childrens.supplier,
    environment.core.ROUTES.goodfood.modules.franchise.childrens.purchase
  ];
  
  constructor() { }

}
