import { Component } from '@angular/core';
import { ReturnLink } from 'src/app/models/enums/ReturnLink';
import { NavLink } from 'src/app/models/type/NavLink';
import { environment } from 'src/environments/environment';
import { IPage } from '../../models/interfaces/page.interface';

/**
 * Shop root page
 */
@Component({
  selector: 'app-shop-page',
  templateUrl: './shop-page.component.html',
  styleUrls: ['./shop-page.component.scss']
})
export class ShopPageComponent implements IPage {

  /**
   * Target of the return link
   */
  home: ReturnLink = ReturnLink.SHOP;

  /**
  * navigation links for the shop
  */
  nav: NavLink[] = [
  environment.core.ROUTES.default.modules.shop.childrens.account,
  environment.core.ROUTES.default.modules.shop.childrens.cart
  ];

  constructor() {}

}
