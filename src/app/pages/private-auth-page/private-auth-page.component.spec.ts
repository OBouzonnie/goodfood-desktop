import { ComponentFixture, ComponentFixtureAutoDetect, TestBed, waitForAsync } from '@angular/core/testing';

import { PrivateAuthPageComponent } from './private-auth-page.component';

import { faker } from '@faker-js/faker';
import { AppModule } from 'src/app/app.module';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { of } from 'rxjs';
import { HttpClient } from '@angular/common/http';

describe('PrivateAuthPageComponent', () => {
  let component: PrivateAuthPageComponent;
  let fixture: ComponentFixture<PrivateAuthPageComponent>;
  let template: HTMLElement;
  let http: HttpClient;
  let token: string;

  let email: string = faker.internet.email();
  let pwd: string = faker.internet.password() + Math.floor(Math.random() * 10);

  const routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule],
      declarations: [ PrivateAuthPageComponent ],
      providers: [
        { provide: ComponentFixtureAutoDetect, useValue: true },
        {provide: Router, useValue: routerSpy}
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    http = TestBed.inject(HttpClient);
    spyOn(http, 'get').and.callThrough().and.returnValue(of({status: 200, body: {token: "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJvbGJvdS5jZXNpQGdtYWlsLmNvbSIsInVzZXJJZCI6IjYyYmY1YmM2ODNkZDRiNGI1OGJkOGUzNSIsInJvbGUiOlt7ImF1dGhvcml0eSI6IlVTRVIifV0sImV4cCI6MTY1NjcxMTY1NH0.KgMoAAC6f11xhlI7K9B8XxFLlaefO46hdj5zwbthuYAGrSsWQzIAMOiqGvxRxz4pz2L4jHBfP4RiJ-gu3QR8SA"}}));
    fixture = TestBed.createComponent(PrivateAuthPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    template = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  /*
   * Integration Testing - Rendering
  **/
  it('component should render login component', () => {
    const labels = template.querySelectorAll('label');
    const btn = template.querySelector('button');
    expect(labels.length).toBe(2);
    expect(labels[0].textContent).toContain('auth.email');
    expect(labels[1].textContent).toContain('auth.password');
    expect(btn?.textContent).toContain('auth.login');
  });

  it('submit login should redirect', waitForAsync(() => {
    let btn = template.querySelector('button');
    let inputs = template.querySelectorAll('input');

    expect(inputs.length).toBe(2);
    expect(btn?.textContent).toContain('auth.login');

    inputs[0].value = email;
    inputs[0].dispatchEvent(new Event('input'));
    inputs[1].value = pwd;
    inputs[1].dispatchEvent(new Event('input'));

    btn?.click();
    fixture.whenStable().then(() => {
      expect(routerSpy.navigateByUrl).toHaveBeenCalled();
    })
  }));

  //TODO redirection test, gf/auth for user, by role for others
});
