import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Roles } from 'src/app/models/enums/Roles';
import { SnackBarCSS } from 'src/app/models/enums/SnackBarCSS';
import { Logo } from 'src/app/models/type/Logo';
import { Credential } from 'src/app/models/type/Credential';
import { AdminService } from 'src/app/services/admin/admin.service';
import { SnackbarService } from 'src/app/services/snackbar/snackbar.service';
import { TokenService } from 'src/app/services/token/token.service';
import { environment } from 'src/environments/environment';

/**
 * Private authentification page - login only - for all GoodFood personas
 */
@Component({
  selector: 'app-private-auth-page',
  templateUrl: './private-auth-page.component.html',
  styleUrls: ['./private-auth-page.component.scss']
})
export class PrivateAuthPageComponent {

  /**
   *
   * @param router angular router
   * @param adminService authentication service for admin
   * @param snackBar allow access to the material snackbar pop-up
   * @param translate internationalization tool
   * @param tokenService allow access to the jwt token
   */
  constructor(
    public router: Router,
    private adminService: AdminService,
    private snackBar: SnackbarService,
    private translate: TranslateService,
    private tokenService: TokenService
  ) { }

  /**
   * logo img attributes
   */
  logo: Logo = {
    url:'assets/logo/logo-white.svg',
    alt:'good food logo'
  }

  /**
   * process admin login
   * @param credential admin credential
   */
  login(credential: Credential){
    this.adminService.login(credential, (success: boolean) => this.logged(success));
  }

  /**
   * Redirect on successfull login
   */
   logged(event: boolean): void{
    if (event) {
      this.redirectByPermissions();
    } else {
      this.snackBar.openSnackbar(this.translate.instant('errors.auth'), SnackBarCSS.ERROR);
    }
  }

  /**
   * redirection based on admin permissions
   */
  redirectByPermissions(){
    const perm = this.tokenService.getPermissions();
    let url = environment.core.ROUTES.goodfood.url + '/' + environment.core.ROUTES.goodfood.modules.auth.url;
    if(perm && (perm.includes(Roles.GROUP) || perm.includes(Roles.ADMIN) || perm.includes(Roles.SUPER_ADMIN))) url = environment.core.ROUTES.goodfood.url + '/' + environment.core.ROUTES.goodfood.modules.group.url;
    if(perm && (perm.includes(Roles.ACCOUNTING))) url = environment.core.ROUTES.goodfood.url + '/' + environment.core.ROUTES.goodfood.modules.accounting.url;
    if(perm && (perm.includes(Roles.COMMUNITY_MANAGEMENT))) url = environment.core.ROUTES.goodfood.url + '/' + environment.core.ROUTES.goodfood.modules.community.url;
    if(perm && perm.includes(Roles.FRANCHISE)) url = environment.core.ROUTES.goodfood.url + '/' + environment.core.ROUTES.goodfood.modules.franchise.url;
    this.router.navigateByUrl(url);
  }
}
