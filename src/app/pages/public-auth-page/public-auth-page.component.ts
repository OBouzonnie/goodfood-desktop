import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SnackBarCSS } from 'src/app/models/enums/SnackBarCSS';
import { Logo } from 'src/app/models/type/Logo';
import { Credential } from 'src/app/models/type/Credential';
import { AuthService } from 'src/app/services/auth/auth.service';
import { SnackbarService } from 'src/app/services/snackbar/snackbar.service';
import { environment } from 'src/environments/environment';

/**
 * Public authentification page - login & register - for customers
 */
@Component({
  selector: 'app-public-auth-page',
  templateUrl: './public-auth-page.component.html',
  styleUrls: ['./public-auth-page.component.scss']
})
export class PublicAuthPageComponent {

  /**
   * Define which form is rendered
   * true for login and false for register
   */
  default: boolean = true;

  /**
   * logo img attributes
   */
  logo: Logo = {
    url:'assets/logo/logo-good_food-white.svg',
    alt:'good food logo'
  }

  /**
   * class constructor
   * @param router angular router
   * @param snackBar allow access to the material snackbar pop-up
   * @param translate internationalization tool
   * @param authService customer authentication service
   */
  constructor(
    public router: Router,
    private snackBar: SnackbarService,
    private translate: TranslateService,
    private authService: AuthService
  ) { }


  /**
   * Toggle between login and register form
   */
  switchForm(): void{
    this.default = !this.default;
  }

  /**
   * Render the login form on successfull registration
   *
   * @param event the boolean emitted by the register form
   */
  renderLogin(event: boolean): void{
    this.default = event;
  }

  /**
   * process customer login
   * @param credential customer credentials
   */
  login(credential: Credential){
    this.authService.login(credential, (success: boolean) => this.logged(success));
  }

  /**
   * Redirect on successfull login
   */
  logged(event: boolean): void{
    if (event) {
      this.router.navigateByUrl(environment.core.ROUTES.default.modules.shop.childrens.home.url);
    } else {
      this.snackBar.openSnackbar(this.translate.instant('errors.auth'), SnackBarCSS.ERROR);
    }
  }
}
