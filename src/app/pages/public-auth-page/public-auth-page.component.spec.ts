import { ComponentFixture, TestBed, ComponentFixtureAutoDetect, waitForAsync } from '@angular/core/testing';
import { AppModule } from 'src/app/app.module';
import { Router } from '@angular/router';

import { PublicAuthPageComponent } from './public-auth-page.component';

import { faker } from '@faker-js/faker';
import { of } from 'rxjs';
import { HttpClient } from '@angular/common/http';

describe('PublicAuthPageComponent', () => {
  let component: PublicAuthPageComponent;
  let fixture: ComponentFixture<PublicAuthPageComponent>;
  let template: HTMLElement;
  let http: HttpClient;
  let token: string;

  let email: string = faker.internet.email();
  let pwd: string = faker.internet.password() + Math.floor(Math.random() * 10);

  const routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule],
      declarations: [ PublicAuthPageComponent ],
      providers: [
        { provide: ComponentFixtureAutoDetect, useValue: true },
        {provide: Router, useValue: routerSpy}
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    http = TestBed.inject(HttpClient);
    token = faker.datatype.uuid();
    spyOn(http, 'get').and.callThrough().and.returnValue(of({status: 200, body: {token: "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJvbGJvdS5jZXNpQGdtYWlsLmNvbSIsInVzZXJJZCI6IjYyYmY1YmM2ODNkZDRiNGI1OGJkOGUzNSIsInJvbGUiOlt7ImF1dGhvcml0eSI6IlVTRVIifV0sImV4cCI6MTY1NjcxMTY1NH0.KgMoAAC6f11xhlI7K9B8XxFLlaefO46hdj5zwbthuYAGrSsWQzIAMOiqGvxRxz4pz2L4jHBfP4RiJ-gu3QR8SA"}}));
    fixture = TestBed.createComponent(PublicAuthPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    template = fixture.nativeElement;
  });

  /*
   * Test component creation
  **/
  it('should create', () => {
    expect(component).toBeTruthy();
  });

  /*
   * Integration Testing - Rendering
  **/
  it('component should render login component at init', () => {
    const link = template.querySelector('a');
    const labels = template.querySelectorAll('label');
    const btn = template.querySelector('button');
    expect(labels.length).toBe(2);
    expect(labels[0].textContent).toContain('auth.email');
    expect(labels[1].textContent).toContain('auth.password');
    expect(link?.textContent).toContain('auth.register');
    expect(btn?.textContent).toContain('auth.login');
  });

  it('component should render register component when link is clicked', () => {
    const link = template.querySelector('a');
    link?.click();
    const labels = template.querySelectorAll('label');
    const btn = template.querySelector('button');
    const checkbox = template.querySelector('mat-checkbox');
    expect(labels.length).toBe(4);
    expect(labels[0].textContent).toContain('auth.email');
    expect(labels[1].textContent).toContain('auth.password');
    expect(labels[2].textContent).toContain('auth.confirm');
    expect(checkbox?.textContent).toContain('auth.rgpd');
    expect(link?.textContent).toContain('auth.login');
    expect(btn?.textContent).toContain('auth.register');
  });

  it('component should render register then login component when link is clicked twice', () => {
    const link = template.querySelector('a');
    link?.click();
    link?.click();
    const labels = template.querySelectorAll('label');
    const btn = template.querySelector('button');
    expect(labels.length).toBe(2);
    expect(labels[0].textContent).toContain('auth.email');
    expect(labels[1].textContent).toContain('auth.password');
    expect(link?.textContent).toContain('auth.register');
    expect(btn?.textContent).toContain('auth.login');
  });

  /*
   * Integration Testing - Register
  **/

  it('submit register should return to login for a 200', waitForAsync(() => {
    spyOn(http, 'post').and.callThrough().and.returnValue(of({status: 200}));
    const link = template.querySelector('a');
    link?.click();
    let btn = template.querySelector('button');
    let inputs = template.querySelectorAll('input');

    expect(inputs.length).toBe(4);
    expect(link?.textContent).toContain('auth.login');
    expect(btn?.textContent).toContain('auth.register');

    inputs[0].value = email;
    inputs[0].dispatchEvent(new Event('input'));
    inputs[1].value = pwd;
    inputs[1].dispatchEvent(new Event('input'));
    inputs[2].value = pwd;
    inputs[2].dispatchEvent(new Event('input'));
    inputs[3].click();

    btn?.click();

    fixture.whenStable().then(() => {
      inputs = template.querySelectorAll('input');
      btn = template.querySelector('button');

      expect(inputs.length).toBe(2);
      expect(btn?.textContent).toContain('auth.login');
      expect(link?.textContent).toContain('auth.register');
    });
  }));

  it('submit register should not return to login for a 409', waitForAsync(() => {
    spyOn(http, 'post').and.callThrough().and.returnValue(of({status: 409}));
    const link = template.querySelector('a');
    link?.click();
    let btn = template.querySelector('button');
    let inputs = template.querySelectorAll('input');

    expect(inputs.length).toBe(4);
    expect(link?.textContent).toContain('auth.login');
    expect(btn?.textContent).toContain('auth.register');

    inputs[0].value = email;
    inputs[0].dispatchEvent(new Event('input'));
    inputs[1].value = pwd;
    inputs[1].dispatchEvent(new Event('input'));
    inputs[2].value = pwd;
    inputs[2].dispatchEvent(new Event('input'));
    inputs[3].click();

    btn?.click();

    fixture.whenStable().then(() => {
      inputs = template.querySelectorAll('input');
      btn = template.querySelector('button');

      expect(inputs.length).toBe(4);
      expect(btn?.textContent).toContain('auth.register');
      expect(link?.textContent).toContain('auth.login');
    });
  }));

  /*
   * Integration Testing - Login
  **/

  it('submit login should redirect', waitForAsync(() => {
    const link = template.querySelector('a');
    let btn = template.querySelector('button');
    let inputs = template.querySelectorAll('input');

    expect(inputs.length).toBe(2);
    expect(link?.textContent).toContain('auth.register');
    expect(btn?.textContent).toContain('auth.login');

    inputs[0].value = email;
    inputs[0].dispatchEvent(new Event('input'));
    inputs[1].value = pwd;
    inputs[1].dispatchEvent(new Event('input'));

    btn?.click();
    fixture.whenStable().then(() => {
      expect(routerSpy.navigateByUrl).toHaveBeenCalledWith('home');
    })
  }));
});
