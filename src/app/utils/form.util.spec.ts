import { TestBed } from '@angular/core/testing';
import faker from '@faker-js/faker';
import FormUtil from './form.util';

describe('FormUtil', () => {
  let str: string;
  let i: number;
  let digit: string;
  let pwd: string;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    str = faker.word.noun().replace(/-/g, '');
    pwd = faker.word.noun().replace(/-/g, '');
    digit = Math.floor(Math.random() * 1000000).toString();
  });

  it('should be instanciated', () => {
    expect(FormUtil).toBeTruthy();
  });

  it('#forbidCharsAndSpace should reject char <', () => {
    i = Math.floor(Math.random() * str.length);
    str = [str.slice(0, i), '<', str.slice(i)].join('');
    expect(FormUtil.testForbidCharsAndSpace(str)).toBeFalse();
  });

  it('#forbidCharsAndSpace should reject char >', () => {
    i = Math.floor(Math.random() * str.length);
    str = [str.slice(0, i), '>', str.slice(i)].join('');
    expect(FormUtil.testForbidCharsAndSpace(str)).toBeFalse();
  });

  it('#forbidCharsAndSpace should reject char *', () => {
    i = Math.floor(Math.random() * str.length);
    str = [str.slice(0, i), '*', str.slice(i)].join('');
    expect(FormUtil.testForbidCharsAndSpace(str)).toBeFalse();
  });

  it('#forbidCharsAndSpace should reject char %', () => {
    i = Math.floor(Math.random() * str.length);
    str = [str.slice(0, i), '%', str.slice(i)].join('');
    expect(FormUtil.testForbidCharsAndSpace(str)).toBeFalse();
  });

  it('#forbidCharsAndSpace should reject char :', () => {
    i = Math.floor(Math.random() * str.length);
    str = [str.slice(0, i), ':', str.slice(i)].join('');
    expect(FormUtil.testForbidCharsAndSpace(str)).toBeFalse();
  });

  it('#forbidCharsAndSpace should reject char &', () => {
    i = Math.floor(Math.random() * str.length);
    str = [str.slice(0, i), '&', str.slice(i)].join('');
    expect(FormUtil.testForbidCharsAndSpace(str)).toBeFalse();
  });

  it('#forbidCharsAndSpace should reject char ;', () => {
    i = Math.floor(Math.random() * str.length);
    str = [str.slice(0, i), ';', str.slice(i)].join('');
    expect(FormUtil.testForbidCharsAndSpace(str)).toBeFalse();
  });

  it('#forbidCharsAndSpace should reject char -', () => {
    i = Math.floor(Math.random() * str.length);
    str = [str.slice(0, i), '-', str.slice(i)].join('');
    expect(FormUtil.testForbidCharsAndSpace(str)).toBeFalse();
  });

  it('#forbidCharsAndSpace should reject space', () => {
    i = Math.floor(Math.random() * str.length);
    str = [str.slice(0, i), ' ', str.slice(i)].join('');
    expect(FormUtil.testForbidCharsAndSpace(str)).toBeFalse();
  });

  it('#forbidCharsAndSpace should allow clean string', () => {
    console.log(str)
    expect(FormUtil.testForbidCharsAndSpace(str)).toBeTrue();
  });

  it('#forbiddenChars should reject char <', () => {
    i = Math.floor(Math.random() * str.length);
    str = [str.slice(0, i), '<', str.slice(i)].join('');
    expect(FormUtil.testForbiddenChars(str)).toBeFalse();
  });

  it('#forbiddenChars should reject char >', () => {
    i = Math.floor(Math.random() * str.length);
    str = [str.slice(0, i), '>', str.slice(i)].join('');
    expect(FormUtil.testForbiddenChars(str)).toBeFalse();
  });

  it('#forbiddenChars should reject char *', () => {
    i = Math.floor(Math.random() * str.length);
    str = [str.slice(0, i), '*', str.slice(i)].join('');
    expect(FormUtil.testForbiddenChars(str)).toBeFalse();
  });

  it('#forbiddenChars should reject char %', () => {
    i = Math.floor(Math.random() * str.length);
    str = [str.slice(0, i), '%', str.slice(i)].join('');
    expect(FormUtil.testForbiddenChars(str)).toBeFalse();
  });

  it('#forbiddenChars should reject char :', () => {
    i = Math.floor(Math.random() * str.length);
    str = [str.slice(0, i), ':', str.slice(i)].join('');
    expect(FormUtil.testForbiddenChars(str)).toBeFalse();
  });

  it('#forbiddenChars should reject char &', () => {
    i = Math.floor(Math.random() * str.length);
    str = [str.slice(0, i), '&', str.slice(i)].join('');
    expect(FormUtil.testForbiddenChars(str)).toBeFalse();
  });

  it('#forbiddenChars should reject char ;', () => {
    i = Math.floor(Math.random() * str.length);
    str = [str.slice(0, i), ';', str.slice(i)].join('');
    expect(FormUtil.testForbiddenChars(str)).toBeFalse();
  });

  it('#forbiddenChars should reject char -', () => {
    i = Math.floor(Math.random() * str.length);
    str = [str.slice(0, i), '-', str.slice(i)].join('');
    expect(FormUtil.testForbiddenChars(str)).toBeFalse();
  });

  it('#forbiddenChars should reject char $', () => {
    i = Math.floor(Math.random() * str.length);
    str = [str.slice(0, i), '-', str.slice(i)].join('');
    expect(FormUtil.testForbiddenChars(str)).toBeFalse();
  });

  it('#forbiddenChars should allow space', () => {
    i = Math.floor(Math.random() * (str.length - 2)) + 1;
    str = [str.slice(0, i), ' ', str.slice(i)].join('');
    console.log(str)
    expect(FormUtil.testForbiddenChars(str)).toBeTrue();
  });

  it('#forbiddenChars should allow clean string', () => {
    expect(FormUtil.testForbiddenChars(str)).toBeTrue();
  });

  it('#testDigits should allow stringyfied int digits', () => {
    expect(FormUtil.testDigits(digit)).toBeTrue();
  });

  it('#testDigits should not allow stringyfied dotted decimals', () => {
    i = Math.floor(Math.random() * (digit.length - 2)) + 1;
    digit = [digit.slice(0, i), '.', digit.slice(i)].join('');
    console.log(digit)
    expect(FormUtil.testDigits(digit)).toBeFalse();
  });

  it('#testDigits should not allow stringyfied coma decimals', () => {
    i = Math.floor(Math.random() * (digit.length - 2)) + 1;
    digit = [digit.slice(0, i), ',', digit.slice(i)].join('');
    console.log(digit)
    expect(FormUtil.testDigits(digit)).toBeFalse();
  });

  it('#testDigits should reject mixed alphanumericals', () => {
    expect(FormUtil.testDigits(faker.datatype.uuid())).toBeFalse();
  });

  it('#testDigits should reject non-digits', () => {
    expect(FormUtil.testDigits(str)).toBeFalse();
  });

  it('#testPrices should allow stringyfied int digits', () => {
    expect(FormUtil.testPrices(digit)).toBeTrue();
  });

  it('#testPrices should allow stringyfied one int digits', () => {
    expect(FormUtil.testPrices(Math.floor(Math.random() * 10).toString())).toBeTrue();
  });

  it('#testPrices should allow stringyfied dotted double digits', () => {
    digit = [digit.slice(0, digit.length - 2), '.', digit.slice(digit.length - 2)].join('');
    console.log(digit)
    expect(FormUtil.testPrices(digit)).toBeTrue();
  });

  it('#testPrices should allow stringyfied coma double digits', () => {
    digit = [digit.slice(0, digit.length - 2), ',', digit.slice(digit.length - 2)].join('');
    console.log(digit)
    expect(FormUtil.testPrices(digit)).toBeTrue();
  });

  it('#testPrices should not allow more than 2 decimals with dot', () => {
    i = Math.floor(Math.random() * (digit.length - 3)) + 1;
    digit = [digit.slice(0, i), '.', digit.slice(i)].join('');
    console.log(digit)
    expect(FormUtil.testPrices(digit)).toBeFalse();
  });

  it('#testPrices should not allow more than 2 decimals with coma', () => {
    i = Math.floor(Math.random() * (digit.length - 3)) + 1;
    digit = [digit.slice(0, i), ',', digit.slice(i)].join('');
    console.log(digit)
    expect(FormUtil.testPrices(digit)).toBeFalse();
  });

  it('#testPrices should not allow . as first char', () => {
    expect(FormUtil.testPrices('.' + digit)).toBeFalse();
  });

  it('#testPrices should not allow . as last char', () => {
    expect(FormUtil.testPrices(digit + '.')).toBeFalse();
  });

  it('#testPrices should not allow , as first char', () => {
    expect(FormUtil.testPrices(',' + digit)).toBeFalse();
  });

  it('#testPrices should not allow , as last char', () => {
    expect(FormUtil.testPrices(digit + ',')).toBeFalse();
  });

  it('#testPrices should reject mixed alphanumericals', () => {
    expect(FormUtil.testPrices(faker.datatype.uuid())).toBeFalse();
  });

  it('#testPrices should reject non-digits', () => {
    expect(FormUtil.testPrices(str)).toBeFalse();
  });

  it('#testPwd should reject no digits', () => {
    str = str.toLowerCase();
    pwd = pwd.toUpperCase();
    expect(FormUtil.testPwd(str + pwd)).toBeFalse();
  });

  it('#testPwd should reject no lowercase', () => {
    str = str.toUpperCase();
    expect(FormUtil.testPwd(str + digit)).toBeFalse();
  });

  it('#testPwd should reject no uppercase', () => {
    str = str.toLowerCase();
    expect(FormUtil.testPwd(str + digit)).toBeFalse();
  });

  it('#testPwd should allow lowercase + uppercase + digit', () => {
    str = str.toLowerCase();
    pwd = pwd.toUpperCase();
    expect(FormUtil.testPwd(str + pwd + digit)).toBeTrue();
  });
});
