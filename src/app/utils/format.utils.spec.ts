import { TestBed } from '@angular/core/testing';
import { environment } from 'src/environments/environment';
import FormatUtils from './format.utils';

describe('FormatUtils', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({});
  });

  it('should be instanciated', () => {
    expect(FormatUtils).toBeTruthy();
  });

  it('toCentInteger should convert prices integer from euro string to cent integer', () => {
    expect(FormatUtils.toCentInteger("1")).toBe(100);
    expect(FormatUtils.toCentInteger("100")).toBe(10000);
    expect(FormatUtils.toCentInteger("999")).toBe(99900);
  })

  it('toCentInteger should convert euro prices decimals to cent integer', () => {

    expect(FormatUtils.toCentInteger("1.01")).toBe(101);
    expect(FormatUtils.toCentInteger("999.09")).toBe(99909);

    expect(FormatUtils.toCentInteger("1.23")).toBe(123);
    expect(FormatUtils.toCentInteger("1,23")).toBe(123);

    expect(FormatUtils.toCentInteger("45.23")).toBe(4523);
    expect(FormatUtils.toCentInteger("72.56")).toBe(7256);

    expect(FormatUtils.toCentInteger("999.99")).toBe(99999);
    expect(FormatUtils.toCentInteger("999,99")).toBe(99999);
  })

  it('toCentInteger should not take into account more than two decimals', () => {

    expect(FormatUtils.toCentInteger("1.23456")).toBe(123);
    expect(FormatUtils.toCentInteger("1,23456")).toBe(123);

    expect(FormatUtils.toCentInteger("123.999999")).toBe(12399);
    expect(FormatUtils.toCentInteger("123,456789")).toBe(12345);
  })

  it('toDecimalString should convert cent integers to euro string', () => {
    expect(FormatUtils.toDecimalString(1)).toBe("0.01");
    expect(FormatUtils.toDecimalString(100)).toBe("1");
    expect(FormatUtils.toDecimalString(99999)).toBe("999.99");

    expect(FormatUtils.toDecimalString(1.01)).toBe("0.01");
    expect(FormatUtils.toDecimalString(999.99)).toBe("9.99");
  })

  it('toDecimalString should convert with to two decimal max', () => {
    expect(FormatUtils.toDecimalString(1.01)).toBe("0.01");
    expect(FormatUtils.toDecimalString(999.99)).toBe("9.99");
  })

  it('toEuroString should convert and add euro symbol', () => {
    expect(FormatUtils.toEuroString(1)).toBe("0.01" + environment.core.currency);
    expect(FormatUtils.toEuroString(100)).toBe("1" + environment.core.currency);
    expect(FormatUtils.toEuroString(99999)).toBe("999.99" + environment.core.currency);

    expect(FormatUtils.toEuroString(1.01)).toBe("0.01" + environment.core.currency);
    expect(FormatUtils.toEuroString(999.99)).toBe("9.99" + environment.core.currency);
  })
});
