import { Validators } from '@angular/forms';
import {environment} from 'src/environments/environment';

/**
 * Utilities for form
 */
class FormUtil{

  /**
   * Environment const
   */
 private env = environment;

  /**
   * Forbidden chars regexp, including space
   */
  private forbidCharsAndSpace: RegExp = new RegExp(this.env.core.VALIDATOR.REGEXP.forbidCharsAndSpace);

  /**
   * Forbidden chars regexp without space
   */
   private forbiddenChars: RegExp = new RegExp(this.env.core.VALIDATOR.REGEXP.forbiddenChars);

  /**
   * Only digits regexp
   */
  private digits: RegExp = new RegExp(this.env.core.VALIDATOR.REGEXP.digit);

  /**
   * Only decimal regexp
   */
  private decimals: RegExp = new RegExp(this.env.core.VALIDATOR.REGEXP.decimal);

  /**
   * Password regexp
   */
  private pwd: RegExp = new RegExp(this.env.core.VALIDATOR.REGEXP.pwd);

  /**
   * Credit Card numbers regexp
   */
   private creditCardNbrsRegexp: RegExp = new RegExp(this.env.core.VALIDATOR.REGEXP.creditCard.number);

  /**
   * Credit Card numbers regexp
   */
  private creditCardExpsRegexp: RegExp = new RegExp(this.env.core.VALIDATOR.REGEXP.creditCard.expiration);

  /**
   * Credit Card code regexp
   */
  private creditCardCodeRegexp: RegExp = new RegExp(this.env.core.VALIDATOR.REGEXP.creditCard.code);

  /**
   * Validators for email inputs
   */
  public email: Validators[] = [
    Validators.required,
    Validators.maxLength(this.env.core.VALIDATOR.INPUTS_MAX_LENGTH),
    Validators.email,
    Validators.pattern(this.forbidCharsAndSpace)
  ];

  /**
   * Validators for text inputs
   */
   public words: Validators[] = [
    Validators.required,
    Validators.maxLength(this.env.core.VALIDATOR.INPUTS_MAX_LENGTH),
    Validators.pattern(this.forbidCharsAndSpace)
  ];

  /**
   * Validator for checkbox
   */
  public bool: Validators[] = [
    Validators.required
  ];

  /**
   * Validators for phone inputs
   */
  public phone: Validators[] = [
    Validators.required,
    Validators.maxLength(this.env.core.VALIDATOR.PHONE_MAX_LENGTH),
    Validators.pattern(this.forbidCharsAndSpace)
  ];

  /**
   * Validators for digit inputs
   */
  public nbrs: Validators[] = [
    Validators.required,
    Validators.pattern(this.digits),
  ];

  /**
   * Validators for prices inputs
   */
   public prices: Validators[] = [
    Validators.required,
    Validators.pattern(this.decimals),
  ];

  /**
   * Validators for credit card numbers
   */
   public creditCardNbrs: Validators[] = [
    Validators.required,
    Validators.pattern(this.creditCardNbrsRegexp),
  ];

  /**
   * Validators for credit card numbersexpiration
   */
  public creditCardExp: Validators[] = [
    Validators.required,
    Validators.pattern(this.creditCardExpsRegexp),
  ];

  /**
   * Validators for credit card code
   */
   public creditCardCode: Validators[] = [
    Validators.required,
    Validators.pattern(this.creditCardCodeRegexp),
  ];

  /**
   * Validators for text inputs
   */
   public txt: Validators[] = [
    Validators.maxLength(this.env.core.VALIDATOR.INPUTS_MAX_LENGTH),
    Validators.pattern(this.forbiddenChars)
  ];

  /**
   * Validators for textarea
   */
  public textarea: Validators[] = [
    Validators.maxLength(this.env.core.VALIDATOR.TXT_MAX_LENGTH),
    Validators.pattern(this.forbiddenChars)
  ];

  /**
   * Validators for passwords
   */
  public password: Validators[] = [
    Validators.required,
    Validators.minLength(this.env.core.VALIDATOR.PWD_MIN_LENGTH),
    Validators.maxLength(this.env.core.VALIDATOR.INPUTS_MAX_LENGTH),
    Validators.pattern(this.pwd),
    Validators.pattern(this.forbidCharsAndSpace)
  ];

  /**
   * check if the string include any forbidden char or space
   * @param str string to test
   * @returns test result
   */
  testForbidCharsAndSpace(str: string): boolean {
    return this.forbidCharsAndSpace.test(str);
  }

  /**
   * check if the string include any forbidden char
   * @param str string to test
   * @returns test result
   */
  testForbiddenChars(str: string): boolean {
    return this.forbiddenChars.test(str);
  }

  /**
   * check if the string contains only digits chars
   * @param str string to test
   * @returns test result
   */
  testDigits(str: string): boolean {
    return this.digits.test(str);
  }

  /**
   * check if the string respect price formatting
   * @param str string to test
   * @returns test result
   */
  testPrices(str: string): boolean {
    return this.decimals.test(str);
  }

  /**
   * check if the string respect password constraints
   * @param str string to test
   * @returns test result
   */
  testPwd(str: string): boolean {
    return this.pwd.test(str);
  }
}

export default new FormUtil();
