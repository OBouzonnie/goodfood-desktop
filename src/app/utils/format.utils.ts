import * as moment from "moment";
import { environment } from "src/environments/environment";
import { DeliveryType } from "../models/enums/Delivery";
import { OrderStatus } from "../models/enums/OrderStatus";
import { CartDetail } from "../models/interfaces/cart-detail.interface";

/**
 * formatting tools
 */
class FormatUtils {

  /**
   * Transform a price string in euro into a price integer in cents
   * @param price price as a string
   * @returns the absolute price value in cents
   */
  toCentInteger(price: string): number{
    return Math.trunc(parseFloat(price.replace(/,/g, '.')) * 100);
  }

  /**
   * Transform an absolute cents integer into a price string
   * @param price price as an absolute cents integer
   * @returns a price string
   */
  toDecimalString(price: number): string{
    return (Math.trunc(price) / 100).toString();
  }

  /**
   * Transform an absolute cents integer into a price string suffixed by its currency
   * @param price price as an absolute integer cents
   * @returns a price string suffix with its currency
   */
  toEuroString(price: number): string{
    return this.toDecimalString(price) + environment.core.currency;
  }

  /**
   * Fetch a cart item price and format it into a price currency string
   * @param item cart detail item
   * @returns the price string
   */
  formatPrice(item: CartDetail): string{
    const price = this.getCartItemActivePrice(item);
    return this.toEuroString(price);
  }

  /**
   * Fetch a cart item price and format its total into a price currency string
   * @param item cart detail item
   * @returns the total price string
   */
  formatItemTotal(item: CartDetail){
    const price = this.getCartItemActivePrice(item);
    return this.toEuroString(price * item.quantity);
  }

  /**
   * Get the active price of a cart item
   * @param item cart detail item
   * @returns the active price of the item
   */
  private getCartItemActivePrice(item: CartDetail): number{
    let price = item.activePrice ? item.activePrice : item.price;
    if(item.discountValue) price = price * (1 - item.discountValue/100);
    return Math.trunc(price);
  }

  /**
   * format a discount value to a string
   * @param value the discount value
   * @returns the discount value as a string
   */
  formatDiscountPercent(value: number|null): string{
    if(!value) return '';
    return '-' + value + '%';
  }

  /**
   * Provide the displayable order status of an order
   * @param status the order status
   * @returns the displayed order status
   */
  formatOrderStatus(status: OrderStatus): string{
    switch(status){
      case OrderStatus.PLACED : return 'enum.placed';
      case OrderStatus.APPROVED : return 'enum.approved';
      case OrderStatus.PROVIDED : return 'enum.provided';
      case OrderStatus.DELIVERED : return 'enum.delivered';
      default: return '';
    }
  }

  /**
   * Provide the displayable delivery type of an order
   * @param type the order delivery type
   * @returns the displayed delivery type
   */
  formatDeliveryType(type : DeliveryType): string{
    switch(type){
      case DeliveryType.DELIVER : return 'enum.deliver';
      case DeliveryType.TAKEAWAY : return 'enum.takeaway';
      default: return '';
    }
  }

  /**
   * Transform a string date to a utc date
   * @param date date to format
   * @returns the date in utc format
   */
  formatUTC(date: string): string{
    return moment(date).format('DD/MM/YYYY');
  }

  /**
   * format a current millisecond number into a minute string
   * @param time the current millisecond
   * @returns time in minutes
   */
  formatStatsUTC(time: number): string{
    return Math.trunc(time/60) + ' min.';
  }

  /**
   * Transform a formatted price to its cent integer value
   * @param str the price suffixed by its currency
   * @returns an absolute cent integer
   */
  fromEuroStringToInteger(str: string): number|void{
    str.replace('€', '');
    const int = this.toCentInteger(str);
    if(!isNaN(int)) return int;
  }

  /**
   * check is the discount is still active
   * @param end the end date of the discount
   * @returns the active state of the discount
   */
  isDiscountOver(end: string): boolean{
    return moment() <= moment(end);
  }
}

export default new FormatUtils();
