import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PublicAuthPageComponent } from './pages/public-auth-page/public-auth-page.component';
import { environment } from 'src/environments/environment';
import { PrivateAuthPageComponent } from './pages/private-auth-page/private-auth-page.component';
import { AppGuard } from './guards/AppGuard/app.guard';

const routes: Routes = [
  {
    path: environment.core.ROUTES.default.modules.auth.url,
    component: PublicAuthPageComponent
  },
  {
    path: environment.core.ROUTES.goodfood.url,
    canActivate: [AppGuard],
    children: [
      {
        path: environment.core.ROUTES.goodfood.modules.auth.url,
        component: PrivateAuthPageComponent
      },
      {
        path: environment.core.ROUTES.goodfood.modules.group.url,
        loadChildren: () => import('./modules/group/group.module').then(m => m.GroupModule),
        canLoad: [AppGuard]
      },
      {
        path: environment.core.ROUTES.goodfood.modules.franchise.url,
        loadChildren: () => import('./modules/franchise/franchise.module').then(m => m.FranchiseModule),
        canLoad: [AppGuard]
      },
      {
        path: environment.core.ROUTES.goodfood.modules.accounting.url,
        loadChildren: () => import('./modules/accounting/accounting.module').then(m => m.AccountingModule),
        canLoad: [AppGuard]
      },
      {
        path: environment.core.ROUTES.goodfood.modules.community.url,
        loadChildren: () => import('./modules/community/community.module').then(m => m.CommunityModule),
        canLoad: [AppGuard]
      }
    ]
  },
  {
    path: environment.core.ROUTES.default.modules.shop.url,
    canActivate: [AppGuard],
    loadChildren: () => import('./modules/shop/shop.module').then(m => m.ShopModule)
  },
  {
    path: "**",
    redirectTo: environment.core.ROUTES.default.modules.auth.url
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
