export type ChangePwd = {
  oldPassword: "string";
  newPassword: "string";
}
