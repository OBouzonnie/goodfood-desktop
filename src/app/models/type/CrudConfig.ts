import { CRUD } from "../enums/CRUD";

export type CrudConfig = CRUD[];
