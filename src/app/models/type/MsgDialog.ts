export type MsgDialog = {
  errors?: string[],
  msg?: string
}
