import { Roles } from "../enums/Roles"

export type Token = {
  exp: string,
  userId: string,
  role: {authority: Roles}[],
  franchise?: string
}
