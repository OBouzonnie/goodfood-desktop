export type ApiResponse = {
  status: number,
  message?: string,
  body?: any
}
