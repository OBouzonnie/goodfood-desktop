export type OpeningHours = {
  day: string;
  open: string;
  close: string;
}
