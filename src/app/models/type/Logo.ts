export type Logo = {
  url: string,
  alt: string
}
