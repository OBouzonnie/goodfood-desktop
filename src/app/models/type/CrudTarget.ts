import { CRUD } from "../enums/CRUD";
import { BaseModel } from 'src/app/models/apis/models';

export type CrudTarget = {
  action: CRUD,
  uid: string,
  resource?: BaseModel,
  franchise?: string,
  actives?: string[]
}
