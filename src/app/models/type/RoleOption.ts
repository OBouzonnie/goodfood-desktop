import { Roles } from "../enums/Roles"

export type RoleOption = {
  label: string,
  role: Roles
}
