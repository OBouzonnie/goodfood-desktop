/**
 * application roles
 */
export enum Roles {
  USER = "USER",
  SUPER_ADMIN = "superAdmin",
  ADMIN = "admin",
  GROUP = "group",
  FRANCHISE = "franchise",
  COMMUNITY_MANAGEMENT = "communityManager",
  ACCOUNTING = "accounting"
}

/**
 * return all application roles
 * @returns all possible roles
 */
export function rolesValues(): Roles[]{
  return [
    Roles.SUPER_ADMIN,
    Roles.ADMIN,
    Roles.GROUP,
    Roles.FRANCHISE,
    Roles.COMMUNITY_MANAGEMENT,
    Roles.ACCOUNTING
  ]
}
