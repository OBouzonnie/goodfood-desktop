/**
 * Local Storage Keys
 */
export enum LocalStorageKey {
  TOKEN = 'token',
  VALIDITY = 'tokenValidity',
  USER = 'user',
  PERMISSIONS = "permissions",
  FRANCHISE_ID = "franchiseID",
  CART = "cart",
  FRANCHISE = "franchise",
}
