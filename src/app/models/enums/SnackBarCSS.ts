/**
 * SnackBar CSS values
 */
export enum SnackBarCSS {
  ERROR = 'error-snackbar',
  SUCCESS = 'success-snackbar'
}
