/**
 * Delivery Type values
 */
export enum DeliveryType {
  DELIVER = "deliver",
  TAKEAWAY = "take-away"
}
