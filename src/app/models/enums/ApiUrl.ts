/**
 * API URL values
 */
export enum ApiUrl{
  NONE = '',
  ADDRESS = '/address',
  RECIPE = '/recipe',
  INGREDIENT = '/ingredient',
  SUPPLIER = '/supplier',
  COMPLAINT = '/complaint',
  FEEDBACK = '/feedback',
  PURCHASE = '/purchase',

  LOGIN = '/login',
  LOGOUT = '/logout',
  REGISTER = '/register',

  AUTH = '/auth',
  ADMIN = '/admin',
  ADMIN_PWD = '/admin/changePassword/',
  ADMIN_PERMISSION = '/permission',
  ADMIN_LOGIN = '/admin/login',
  ADMIN_RESET_PWD = '/admin/forgotPassword',

  CUSTOMER = '/customer',
  CUSTOMER_ORDER = '/customer/{id}/order',
  CUSTOMER_ADDRESS = '/customer/{id}/address',
  CUSTOMER_INFORMATION = '/information',
  CUSTOMER_RGPD = '/rgpd',
  CUSTOMER_PWD = '/customer/changePassword/',
  CUSTOMER_RESET_PWD = '/customer/forgotPassword',

  RESTAURANT = '/restaurant',
  RESTAURANT_ORDER = '/order/restaurant/',
  RESTAURANT_RECIPE = '/restaurant/{id}/recipe',
  RESTAURANT_FEEDBACK = '/restaurant/{id}/feedback',
  RESTAURANT_PURCHASE = '/restaurant/{id}/purchase',
  RESTAURANT_STATS = '/restaurant/{id}/stats',
  RESTAURANT_GPS = '/gps',

  ORDER = '/order',
  ORDER_PAY = '/order/{id}/pay',

  DISCOUNT = '/discount',
  DISCOUNT_ACTIVE = '/active',
  DISCOUNT_PAST = '/past',

  ACTION_ADD = '/add',
  ACTION_REMOVE = '/remove',
  ACTION_STANDARDISE = '/standardise',
  ACTION_DELIVERY = '/delivery',
  ACTION_APPROVE = '/approve',
  ACTION_DENY = '/deny',
  ACTION_PROVIDE = '/provide',
  ACTION_DELIVER = '/deliver',
  ACTION_PAY = '/pay',
  ACTION_RECEIVE = '/receive',
}
