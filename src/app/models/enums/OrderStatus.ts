/**
 * Order Status values
 */
export enum OrderStatus {
  PLACED = 'placed',
  APPROVED = 'approved',
  PROVIDED = 'provided',
  DELIVERED = 'delivered'
}
