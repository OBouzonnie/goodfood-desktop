/**
 * CRUD actions
 */
export enum CRUD {
  CREATE,
  READ,
  UPDATE,
  DELETE,
  STANDARDISE,
  ADD,
  REMOVE,
  MANAGE
}
