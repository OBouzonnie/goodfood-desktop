/**
 * All access restricted modules
 */
export enum SecuredModules
{
  GROUP = 'group',
  FRANCHISE = 'franchise',
  ACCOUNTING = 'accounting',
  COMMUNITY = 'community'
}
