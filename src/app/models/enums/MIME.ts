/**
 * MiMe type used
 */
export enum MIME {
  JPG = 'image/jpeg'
}
