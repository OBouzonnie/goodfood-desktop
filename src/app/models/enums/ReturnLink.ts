/**
 * Return route for each module
 */
export enum ReturnLink
{
  GROUP = '/gf/group',
  FRANCHISE = '/gf/franchise',
  ACCOUNTING = '/gf/accounting',
  COMMUNITY = '/gf/community',
  SHOP = '/home',
  BACK_OFFICE_ACCESS = '/gf/auth',
  LOGIN = 'auth'
}
