/* tslint:disable */
import { BaseModel } from './base-model';
import { AppliedDiscount } from './applied-discount';
import { Feedback } from './feedback';

/**
 * restaurant recipe model
 */
export interface RestaurantRecipe extends BaseModel{

  /**
   * the restaurant active discount if available
   */
  discount?: AppliedDiscount;

  /**
   * customer feedback for this recipe
   */
  feedbacks?: Array<Feedback>;

  /**
   * the recipe name
   */
  name?: string;

  /**
   * the filenames of the photo of the recipe
   */
  photo?: string;

  /**
   * the recipe price
   */
  price?: number;
}
