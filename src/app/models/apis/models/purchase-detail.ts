/* tslint:disable */

/**
 * Purchase detail model
 */
export interface PurchaseDetail {

  /**
   * the id of the ingredient supplier ordered
   */
  ingredientSupplierId: string;

  /**
   * the price of the purchase at the date of the order, potential discount included
   */
  price: number;

  /**
   * quantity ordered for this purchase
   */
  quantity: number;
}
