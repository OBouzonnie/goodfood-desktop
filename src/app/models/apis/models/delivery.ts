/* tslint:disable */

import { DeliveryType } from "../../enums/Delivery";

/**
 * order delivery related informations
 */
export interface Delivery {

  /**
   * the estimated time of arrival of the order
   */
  arrivalTime?: string;

  /**
   * the time the order is ready to be delivered
   */
  readyTime?: string;

  /**
   * either take-away or deliver
   */
  type: DeliveryType;
}
