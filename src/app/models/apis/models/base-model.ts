/* tslint:disable */

/**
 * base model
 */
export interface BaseModel {

  /**
   * base model identifier
   */
  id?: string;
}
