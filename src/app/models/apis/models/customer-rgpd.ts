/* tslint:disable */

/**
 * customer RGPD model
 */
export interface CustomerRGPD {

  /**
   * database storage consent date
   */
  dbConsentDate: string;

  /**
   * has customer consent for his personal data to be stored in a database
   */
  hasConsentForDB: boolean;
}
