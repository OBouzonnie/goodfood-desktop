/* tslint:disable */

import { BaseModel } from "./base-model";

/**
 * order detail model
 */
export interface OrderDetail extends BaseModel {

  /**
   * the price of the recipe at the date of the order, potential discount included
   */
  price: number;

  /**
   * quantity ordered for this recipe
   */
  quantity: number;

  /**
   * the id of the recipe ordered
   */
  recipe: string;

  /**
   * the name of the recipe ordered
   */
  recipeName?: string;

  /**
 * the value of the discount if applied
 */
  discountValue?: number;
}
