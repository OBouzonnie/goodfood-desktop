/* tslint:disable */
import { BaseModel } from './base-model';

/**
 * discount model
 */
export interface Discount extends BaseModel{

  /**
   * the date the discount end
   */
  end?: string;

  /**
   * the id of the recipe concern by the discount if needed
   */
  recipe?: string;

  /**
   * the name of the recipe concern by the discount if needed
   */
  recipeName?: string;

  /**
   * the date the discount start
   */
  start?: string;

  /**
   * the discount value
   */
  value?: number;
}
