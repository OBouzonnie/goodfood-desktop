/* tslint:disable */
import { BaseModel } from './base-model';

/**
 * applied discount model
 */
export interface AppliedDiscount extends BaseModel{

  /**
   * the date the discount end
   */
  end: string;

  /**
   * the date the discount start
   */
  start: string;

  /**
   * the discount value
   */
  value: number;
}
