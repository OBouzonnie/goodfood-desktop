/* tslint:disable */

/**
 * feedback model
 */
export interface Feedback {

  /**
   * the comment of the feedback
   */
  comment?: string;

  /**
   * the customer evaluation of the service quality
   */
  eval: number;
}
