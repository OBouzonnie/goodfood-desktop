/* tslint:disable */
import { DisactivableModel } from './disactivable-model';
import { SupplierIngredient } from './supplier-ingredient';

/**
 * supplier model
 */
export interface Supplier extends DisactivableModel{

  /**
   * if the supplier was created by a restaurant, provide its id
   */
  creator?: string;

  /**
   * the supplier email
   */
  email?: string;

  /**
   * the ingredients provided by the supplier
   */
  ingredients?: Array<SupplierIngredient>;

  /**
   * is the supplier part of the official good food suppliers ?
   */
  isStandard?: boolean;

  /**
   * the supplier name
   */
  name?: string;

  /**
   * the supplier phone number
   */
  phone?: string;
}
