/* tslint:disable */
import { Address } from './address';
import { AppliedDiscount } from './applied-discount';
import { RestaurantRecipe } from './restaurant-recipe';
import { Feedback } from '../models';
import { DisactivableModel } from './disactivable-model';

/**
 * restaurant model
 */
export interface Restaurant extends DisactivableModel{

  /**
   * the restaurant address
   */
  address?: Address;

  /**
   * the closing hours of the restaurant
   */
  closeDateTime?: Array<string>;

  /**
   * the restaurant active discount if available
   */
  discount?: AppliedDiscount;

  /**
   * the restaurant name
   */
  name?: string;

  /**
   * the opening hours of the restaurant
   */
  openDateTime?: Array<string>;

  /**
   * the restaurant owner
   */
  owner?: {email?: string, firstName?: string, lastName?: string};

  /**
   * the restaurant phone number
   */
  phone?: string;

  /**
   * the recipes provided by the restaurant
   */
  recipes?: Array<RestaurantRecipe>;

  /**
   * The customers feedbacks about the restaurant
   */
  feedbacks?: Array<Feedback>

  /**
   * the restaurant average evaluation by customers
   */
   averageMark?: number;
}
