/* tslint:disable */
import { BaseModel } from './base-model';
import { Address } from './address';
import { Delivery } from './delivery';
import { OrderDetail } from './order-detail';
import { OrderStatus } from '../../enums/OrderStatus';

/**
 * customer order model
 */
export interface Order extends BaseModel{

  /**
   * the adress the order will be delivered to
   */
  address?: Address;

  /**
   * the id of the customer who passes the order
   */
  customer?: string;

  /**
   * the delivery informations of the order if needed
   */
  delivery?: Delivery;

  /**
   * the details of the order for each recipe ordered
   */
  details?: Array<OrderDetail>;

  /**
   * set to true when the customer payment is confirm
   */
  isPayed?: boolean;

  /**
   * the date of the order
   */
  orderDate?: string;

  /**
   * the reference of the payment from BNB
   */
  paymentRef?: string;

  /**
   * the id of the restaurant which receive the order
   */
  restaurant?: string;

  /**
   * the name of the restaurant which receive the order
   */
   restaurantName?: string;

  /**
   * the status of the order in its process
   */
  status?: OrderStatus;

  /**
   * the total amount for the order, sum of the order details amounts
   */
  total?: number;

  /**
   * the global discount if applied
   */
  discountValue?: number;
}
