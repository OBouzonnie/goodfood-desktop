/* tslint:disable */
import { Permission } from './permission';

/**
 * token + validity timestamp + permission
 */
export interface Authentication {

  /**
   * admin permission
   */
  permission?: Permission;

  /**
   * user's email
   */
  token: string;

  /**
   * user's id
   */
  userID: string;

  /**
   * token validity timestamp
   */
  validUntil: string;
}
