/* tslint:disable */
import { BaseModel } from './base-model';

/**
 * address model
 */
export interface Address extends BaseModel{

  /**
   * the city part of the address
   */
  city?: string;

  /**
   * the street number of the address
   */
  number?: string;

  /**
   * the postal code
   */
  postal?: string;

  /**
   * the street part of the address
   */
  street?: string;
}
