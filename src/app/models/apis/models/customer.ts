/* tslint:disable */
import { BaseModel } from './base-model';
import { CustomerInformation } from './customer-information';
import { CustomerRGPD } from './customer-rgpd';

/**
 * customer model
 */
export interface Customer extends BaseModel{

  /**
   * the customer email, used as login
   */
  email?: string;

  /**
   * the personal informations for this customer
   */
  information?: CustomerInformation;

  /**
   * the customer password, encrypted
   */
  password?: string;

  /**
   * the customer rgpd consents
   */
  rgpd?: CustomerRGPD;
}
