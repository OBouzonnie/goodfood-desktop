/* tslint:disable */
import { BaseModel } from './base-model';

/**
 * ingredient model
 */
export interface Ingredient extends BaseModel{

  /**
   * the ingredient known allergen
   */
  allergen?: Array<string>;

  /**
   * the ingredient name
   */
  name?: string;

  /**
   * the ingredient quantity inside a recipe
   */
  quantity?: number;
}
