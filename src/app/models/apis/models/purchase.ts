/* tslint:disable */
import { BaseModel } from './base-model';
import { PurchaseDetail } from './purchase-detail';
import { Supplier } from './supplier';

/**
 * purchase model
 */
export interface Purchase extends BaseModel {

  /**
   * the details of the purchase for each ingredients
   */
  PurchaseDetails: Array<PurchaseDetail>;

  /**
   * the date of the purchase
   */
  orderDate: string;

  /**
   * the supplier receiving the purchase
   */
  supplier: Supplier;

  /**
   * the total amount for the purchase
   */
  total: number;
}
