/* tslint:disable */

/**
 * customer information model
 */
export interface CustomerInformation {

  /**
   * customer birthdate
   */
  birthDate?: string;

  /**
   * customer firstname
   */
  firstName?: string;

  /**
   * customer lastname
   */
  lastName?: string;

  /**
   * customer phone
   */
  phone: string;
}
