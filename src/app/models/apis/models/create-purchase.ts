/* tslint:disable */
import { PurchaseDetail } from './purchase-detail';

/**
 * purchase model for creation
 */
export interface CreatePurchase {

  /**
   * the details of the Purchase for each ingredient and supplier ordered
   */
  PurchaseDetails: Array<PurchaseDetail>;

  /**
   * the supplier receiving the purchase
   */
  supplier: string;
}
