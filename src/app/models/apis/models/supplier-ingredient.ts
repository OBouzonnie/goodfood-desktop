/* tslint:disable */
import { BaseModel } from './base-model';

/**
 * supplier's ingredient model
 */
export interface SupplierIngredient extends BaseModel{

  /**
   * the ingredient name
   */
  name?: string;

  /**
   * the ingredient price
   */
  price?: number;
}
