/* tslint:disable */
import { BaseModel } from './base-model';

/**
 * admin model
 */
export interface Admin extends BaseModel{

  /**
   * the admin login
   */
  login: string;

  /**
   * the admin password
   */
  password: string;

  /**
   * the admin permissions
   */
  permission: Array<string>;

  /**
   * the franchise id this admin is related too, for franchise roles
   */
  franchise?:	string

  /**
   * the franchise name this admin is related too, for franchise roles
   */
  franchiseName?:	string
}
