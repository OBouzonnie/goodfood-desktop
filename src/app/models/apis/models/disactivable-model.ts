/* tslint:disable */

import { BaseModel } from "./base-model";

/**
 * disactivable model
 */
export interface DisactivableModel extends BaseModel {

  /**
   * if the resource is still active in the database
   */
  isActive?: boolean;
}
