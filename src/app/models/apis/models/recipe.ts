/* tslint:disable */
import { AppliedDiscount } from './applied-discount';
import { Ingredient } from './ingredient';
import { DisactivableModel } from './disactivable-model';

/**
 * recipe model
 */
export interface Recipe extends DisactivableModel{

  /**
   * if the recipe was created by a restaurant, provide its id
   */
  creator?: string;

  /**
   * the recipe active discount if available
   */
  discount?: AppliedDiscount;

  /**
   * the ingredients of the recipe
   */
  ingredients?: Array<Ingredient>;

  /**
   * is this recipe part of the good food officials recipes
   */
  isStandard?: boolean;

  /**
   * the recipe name
   */
  name?: string;

  /**
   * the filenames of the photo of the recipe
   */
  photo?: string;

  /**
   * the basic price of the recipe
   */
  standardPrice?: number;

  /**
   * the number os restaurant using this recipe
   */
  restaurantUsageCount?: number;
}
