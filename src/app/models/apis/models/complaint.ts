/* tslint:disable */
import { BaseModel } from './base-model';
import { OrderDetail } from './order-detail';

/**
 * complaint model
 */
export interface Complaint extends BaseModel{

  /**
   * action taken for approved complaints
   */
  action?: 'refund' | 'voucher';

  /**
   * the id of the customer who made the complaint
   */
  customer?: string;

  /**
   * the explanation of the customer about the complaint
   */
  description?: string;

  /**
   * the details of the order for each recipe ordered
   */
  details?: Array<OrderDetail>;

  /**
   * the id of the order concern by the complaint
   */
  order?: string;

  /**
   * the filename of the photo provided with complaint
   */
  photo?: string;

  /**
   * community management Response to the complaint
   */
  status?: 'processing' | 'approved' | 'denied' | 'provided';

  /**
   * the type of the complaint
   */
  type?: 'not_delivered' | 'late' | 'missing_product' | 'wrong_product' | 'bad_food_quality';

  /**
   * the monetary refund provided to the customer
   */
  value?: number;
}
