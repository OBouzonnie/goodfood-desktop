/* tslint:disable */

/**
 * statistics of the restaurant
 */
export interface RestaurantStats{

  /**
   * this restaurant's name
   */
  restaurant: string;

  /**
   * total number of order for this restaurant
   */
  orderCount: number;

  /**
   * turnover of this restaurant
   */
  turnover: number;

  /**
   * averageBasket for this restaurant
   */
  averageBasket: number;

  /**
   * the average the order is ready to be delivered
   */
  averageReadyTime: number;

  /**
   * the average time of arrival of the order
   */
  averageArrivalTime: number;
}
