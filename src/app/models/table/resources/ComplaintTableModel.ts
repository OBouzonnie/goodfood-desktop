import { TableModel } from "./TableModel";

/**
 * table model for complaint resource
 */
export interface ComplaintTableModel extends TableModel{

  /**
   * complaint type
   */
  type: string,

  /**
   * complaint status
   */
  status: string,

  /**
   * related order id
   */
  order: string,
}
