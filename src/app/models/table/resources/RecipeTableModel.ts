import { TableModel } from "./TableModel";

/**
 * table model for recipe resource
 */
export interface RecipeTableModel extends TableModel{

  /**
   * recipe photo
   */
  photo: string,

  /**
   * recipe price
   */
  price: string,

  /**
   * recipe usage count
   */
  count: string,

  /**
   * good food logo
   */
  logo?: string,
}
