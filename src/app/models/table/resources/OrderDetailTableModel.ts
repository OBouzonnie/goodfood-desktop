import { TableModel } from "./TableModel";

/**
 * table model for order details
 */
export interface OrderDetailTableModel extends TableModel{

  /**
   * resource quantity
   */
  qty: string,

  /**
   * resource price
   */
  price: string
}
