import { TableModel } from "./TableModel";

/**
 * table model for restaurant resource
 */
export interface RestaurantTableModel extends TableModel{

  /**
   * restaurant email
   */
  email: string;

  /**
   * restaurant phone
   */
  phone: string;
}

