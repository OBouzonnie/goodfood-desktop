import { TableModel } from "./TableModel";

/**
 * table model for admin resource
 */
export interface AdminTableModel extends TableModel{

  /**
   * admin permissions
   */
  permissions: string

  /**
   * admin related franchise - for admin franchise only
   */
  restaurant: string
}
