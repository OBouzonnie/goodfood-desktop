import { TableModel } from "./TableModel";

/**
 * table model for feedback resource
 */
export interface FeedbackTableModel extends TableModel{

  /**
   * type of feedback
   */
  type: string,

  /**
   * target of the feedback
   */
  target: string,

  /**
   * customer evaluation
   */
  eval: string,
}
