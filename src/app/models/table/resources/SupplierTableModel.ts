import { TableModel } from "./TableModel";

/**
 * table model for supplier resource
 */
export interface SupplierTableModel extends TableModel{
  /**
   * supplier email
   */
  email: string;

  /**
   * supplier phone
   */
  phone: string;

  /**
   * good food logo
   */
  logo?: string;
}

