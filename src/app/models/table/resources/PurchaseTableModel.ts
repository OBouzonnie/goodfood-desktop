import { TableModel } from "./TableModel";

/**
 * table model for purchase resource
 */
export interface PurchaseTableModel extends TableModel{

  /**
   * related supplier
   */
  supplier: string;

  /**
   * total amount of the purchase
   */
  total: string;
}
