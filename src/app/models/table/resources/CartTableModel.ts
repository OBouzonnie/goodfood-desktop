import { TableModel } from "./TableModel";

/**
 * table model for cart
 */
export interface CartTableModel extends TableModel{

  /**
   * recipe price
   */
  price: string,

  /**
   * recipe quantity
   */
  qty: string,

  /**
   * total amount for the recipe
   */
  total: string
}
