import { TableModel } from "./TableModel";

/**
 * table model for ingredient resource
 */
export interface IngredientTableModel extends TableModel{

  /**
   * ingredient allergens
   */
  allergens: string
}
