import { OrderDetailTableModel } from "./OrderDetailTableModel";

/**
 * table model for purchase details
 */
export interface PurchaseDetailTableModel extends OrderDetailTableModel{

  /**
   * total amount for the purchase detail
   */
  total: string
}
