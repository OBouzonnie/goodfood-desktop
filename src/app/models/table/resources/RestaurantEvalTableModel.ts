import { TableModel } from "./TableModel";

/**
 * table model for restaurant evaluations
 */
export interface RestaurantEvalTableModel extends TableModel{

  /**
   * evaluation count
   */
  count: string;

  /**
   * average evaluation
   */
  eval: string;
}
