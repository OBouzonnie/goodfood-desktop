import { TableModel } from "./TableModel";

/**
 * table model for discount resource
 */
export interface DiscountTableModel extends TableModel{

  /**
   * discount value
   */
  value: string,

  /**
   * discount start date
   */
  start: string,

  /**
   * discount end date
   */
  end: string,

  /**
   * discount target
   */
  target: string
}
