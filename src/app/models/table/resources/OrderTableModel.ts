import { TableModel } from "./TableModel";

/**
 * table model for order resource
 */
export interface OrderTableModel extends TableModel{

  /**
   * order status
   */
  status?: string,

  /**
   * total amount for the order
   */
  total: string,

  /**
   * delivery mode
   */
  delivery?: string
}
