/**
 * basic parent table model
 */
export interface TableModel {
  /**
   * resource id
   */
  uid?: string;

  /**
   * collapse state of the accordeon row
   */
  collapse?: boolean;

  /**
   * resource name
   */
  name?: string;

  /**
   * resource image
   */
  photo?: string;

  /**
   * resource description
   */
  desc?: string;

  /**
   * related franchise
   */
  franchise?: boolean;

  /**
   * standard state of the resource
   */
  isStandard?: boolean;

  /**
   * resource creator
   */
  creator?: string;

  /**
   * resource date
   */
  date?:string;
}
