import { SupplierIngredient } from "src/app/models/apis/models";
import formatUtils from "src/app/utils/format.utils";
import { environment } from "src/environments/environment";
import { PurchaseDetailTableModel } from "../../resources/PurchaseDetailTableModel";

/**
 * map purchase details for back office
 */
export class PurchaseDetailTableMapper {

  /**
   * display for no data
   */
  noData: string = environment.core.noData;

  /**
   * table head row
   */
  head: PurchaseDetailTableModel = {
    name: 'resource.ingredients',
    price: 'table.price',
    qty: 'table.qty',
    total: 'table.total'
  };

  /**
   * table rows
   */
  rows: PurchaseDetailTableModel[] = [];

  constructor() {}

  /**
   * get table head titles
   * @returns table head titles
   */
  getColumns(){
    return Object.keys(this.head);
  }

  /**
   * add several rows at once
   * @param resources purchase details to add
   */
  addMultipleRows(resources: {ingId: SupplierIngredient, ingQty: number}[]){
    this.rows = [];
    resources.forEach(r => {
      this.addRow(r);
    })
  }

  /**
   * add one row to the table
   * @param detail purchase detail to add
   */
  addRow(detail: {ingId: SupplierIngredient, ingQty: number}){
    this.rows.push({
      name: detail.ingId.name ? detail.ingId.name : this.noData,
      price: detail.ingId.price ? formatUtils.toEuroString(detail.ingId.price) : this.noData,
      qty: detail.ingQty ? detail.ingQty.toString() : this.noData,
      total: detail.ingId.price && detail.ingQty ? formatUtils.toEuroString(detail.ingId.price * detail.ingQty) : this.noData
    });
  }

  /**
   * return the total amount for this purchase
   * @returns total amount for this purchase
   */
  getTotal(): string{
    let total: number = 0;
    this.rows.forEach(r => {
      if(r.total) {
        const tot = formatUtils.fromEuroStringToInteger(r.total);
        if(tot) total += tot;
      }
    });
    return formatUtils.toEuroString(total);
  }
}
