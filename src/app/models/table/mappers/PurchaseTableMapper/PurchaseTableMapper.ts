import { Purchase } from "src/app/models/apis/models";
import formatUtils from "src/app/utils/format.utils";
import { PurchaseTableModel } from "../../resources/PurchaseTableModel";
import { TableMapper } from "../TableMapper";

/**
 * map purchase for back office
 */
export class PurchaseTableMapper extends TableMapper {


  /**
   * table head row
   */
  override head: PurchaseTableModel = {
    date: 'table.date',
    name: 'table.purchase',
    supplier: 'resource.supplier',
    total: 'table.total',
    uid: ''
  };

  /**
   * table rows
   */
  override rows: PurchaseTableModel[] = [];

  constructor() {super();}

  /**
   * add one row to the table
   * @param purchase purchase to add
   */
  override addRow(purchase: Purchase){
    this.rows.push({
      date: purchase.orderDate ? purchase.orderDate : this.noData,
      name: purchase.id ? purchase.id : this.noData,
      supplier: purchase.supplier && purchase.supplier.name ? purchase.supplier.name : this.noData,
      total: purchase.total ? formatUtils.toEuroString(purchase.total) : this.noData,
      uid: purchase.id,
      collapse: true
    });
  }
}
