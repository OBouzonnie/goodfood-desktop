import { Admin } from "../../../apis/models";
import { AdminTableModel } from "../../resources/AdminTableModel";
import { TableMapper } from "../TableMapper";

/**
 * map admin for back office
 */
export class AdminTableMapper extends TableMapper {

  /**
   * table head row
   */
  override head: AdminTableModel = {
    name: 'table.email',
    restaurant: 'table.franchise',
    permissions: 'table.perm',
    uid: ''
  };

  /**
   * table rows
   */
  override rows: AdminTableModel[] = [];

  constructor() {super();}

  /**
   * add one row to the table
   * @param admin admin to add
   */
  override addRow(admin: Admin){
    this.rows.push({
      name: admin.login ? admin.login : this.noData,
      restaurant: admin.franchiseName ? admin.franchiseName : this.noData,
      permissions: admin.permission ? admin.permission.toString() : this.noData,
      uid: admin.id,
      collapse: true
    });
  }
}
