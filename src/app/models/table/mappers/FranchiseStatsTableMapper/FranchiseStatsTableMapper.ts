import { RestaurantStats } from "src/app/models/apis/models/restaurant-stats";
import formatUtils from "src/app/utils/format.utils";
import { InfoTableMapper } from "../InfoTableMapper";

/**
 * map franchise stats for back office
 */
export class FranchiseStatsTableMapper extends InfoTableMapper {

  /**
   * class constructor
   * @param data restaurant stats that are displayed
   */
  constructor(data: RestaurantStats) {
    super();
    if(data.turnover) this.dataSource.push({key: 'stats.turnover', value: formatUtils.toEuroString(data.turnover)});
    if(data.orderCount) this.dataSource.push({key: 'stats.orderCount', value: data.orderCount});
    if(data.averageBasket) this.dataSource.push({key: 'stats.averageBasket', value: formatUtils.toEuroString(data.averageBasket)});
    if(data.averageReadyTime) this.dataSource.push({key: 'stats.averageReadyTime', value: formatUtils.formatStatsUTC(data.averageReadyTime)});
    if(data.averageArrivalTime) this.dataSource.push({key: 'stats.averageArrivalTime', value: formatUtils.formatStatsUTC(data.averageArrivalTime)});
  }
}
