import { TableModel } from 'src/app/models/table/resources/TableModel';
import { BaseModel } from 'src/app/models/apis/models';
import { environment } from 'src/environments/environment';

/**
 * Table Mapper original parent
 */
export class TableMapper {

  /**
   * table head row
   */
  head: TableModel = {uid: ''};

  /**
   * table rows
   */
  rows: TableModel[] = [];

  /**
   * display for no data
   */
  noData: string = environment.core.noData;

  constructor() {
  }

  /**
   * get columns names
   * @returns columns name
   */
  getColumns(){
    return Object.keys(this.head);
  }

  /**
   * add one row to the table
   * @param resource resource to add
   */
  addRow(resource: BaseModel){
    this.rows.push({uid: resource.id});
  }

  /**
   * add several row to the table
   * @param resources resources to add
   */
  addMultipleRows(resources: BaseModel[]){
    this.rows = [];
    resources.reverse();
    resources.forEach(r => {
      this.addRow(r);
    })
  }
}
