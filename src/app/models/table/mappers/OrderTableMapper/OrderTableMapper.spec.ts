import { MockService } from "src/app/services/mock/mock.service";
import { Recipe } from "../../../apis/models";
import { OrderTableMapper } from "./OrderTableMapper";

const tableMapper: OrderTableMapper = new OrderTableMapper(true);
const mockService: MockService = new MockService();
let resource: Recipe[] = mockService.getRecipes();

describe('OrderTableMapper', () => {

  it('should create an instance', () => {
    expect(new OrderTableMapper(true)).toBeTruthy();
  });

  it('#addRow should add a row', () => {
    tableMapper.addRow(resource[0]);
    const row = tableMapper.rows.pop();
    expect(row?.uid).toEqual(resource[0].id);
    expect(row?.collapse).toBeTrue();
  });

  it('#getColumns should return a string array', () => {
    const columns = tableMapper.getColumns();
    expect(columns.length).toBe(6);
    expect(columns[0]).toBe('name');
    expect(columns[1]).toBe('date');
    expect(columns[2]).toBe('total');
    expect(columns[3]).toBe('delivery');
    expect(columns[4]).toBe('status');
    expect(columns[5]).toBe('uid');
  });
});
