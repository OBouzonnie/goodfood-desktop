import formatUtils from "src/app/utils/format.utils";
import { Order } from "../../../apis/models";
import { OrderTableModel } from "../../resources/OrderTableModel";
import { TableMapper } from "../TableMapper";

/**
 * map order for back office and shop
 */
export class OrderTableMapper extends TableMapper {

  /**
   * show order reference or franchise name as first column ?
   */
  showRef: boolean = false;

  /**
   * table head row
   */
  override head: OrderTableModel = {
    name: '',
    date: 'table.date',
    total: 'table.total',
    delivery: 'table.delivery',
    status: 'table.status',
    uid: ''
  };

  /**
   * table rows
   */
  override rows: OrderTableModel[] = [];

  /**
   * class constructor
   * @param showRef show franchise name or order reference ?
   */
  constructor(showRef: boolean) {
    super();
    this.showRef = showRef;
    this.head.name = this.showRef ? 'table.franchiseOrder' : 'table.customerOrder';
  }

  /**
   * add one row to the table
   * @param order order to add
   */
  override addRow(order: Order){
    this.rows.push({
      name: order.restaurantName && order.id ? this.showRef ? order.id : order.restaurantName : this.noData,
      date: order.orderDate ? order.orderDate : this.noData,
      total: order.total ? formatUtils.toEuroString(order.total) : this.noData,
      delivery: order.delivery && order.delivery.type ? formatUtils.formatDeliveryType(order.delivery.type) : this.noData,
      status: order.status ? formatUtils.formatOrderStatus(order.status) : this.noData,
      uid: order.id,
      collapse: true
    });
  }
}
