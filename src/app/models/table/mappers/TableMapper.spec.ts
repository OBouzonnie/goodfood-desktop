import { MockService } from "src/app/services/mock/mock.service";
import { BaseModel } from 'src/app/models/apis/models';
import { TableMapper } from "./TableMapper";

const tableMapper: TableMapper = new TableMapper();
const mockService: MockService = new MockService();

const rand = Math.round(Math.random());
let resource: BaseModel[];
if (rand !== 0){
  resource = mockService.getRecipes();
} else {
  resource = mockService.getRestaurants();
}

describe('TableMapper', () => {

  it('should create an instance', () => {
    expect(new TableMapper()).toBeTruthy();
  });

  it('#addRow should add a row', () => {
    tableMapper.addRow(resource[0]);
    const row = tableMapper.rows.pop();
    expect(row?.uid).toEqual(resource[0].id);
  });

  it('#addMultipleRows should add multiple rows', () => {
    tableMapper.addMultipleRows(resource);
    expect(tableMapper.rows.length).toBe(resource.length);
    tableMapper.rows.forEach((row, i) => {
      expect(row.uid).toBe(resource[i].id);
    })
  });

  it('#getColumns should return a string array', () => {
    const columns = tableMapper.getColumns();
    expect(columns.length).toBe(1);
    expect(columns[0]).toBe('uid');
  });
});
