import { Discount } from "../../../apis/models";
import { DiscountTableModel } from "../../resources/DiscountTableModel";
import { TableMapper } from "../TableMapper";

/**
 * map discount for back office
 */
export class DiscountTableMapper extends TableMapper {

  /**
   * table head row
   */
  override head: DiscountTableModel = {
    target: 'resource.recipe',
    start: 'table.start',
    end: 'table.end',
    value: 'table.value',
    uid: ''
  };

  /**
   * table rows
   */
  override rows: DiscountTableModel[] = [];

  constructor() {super();}

  /**
   * add one row to the table
   * @param discount discount to add
   */
  override addRow(discount: Discount){
    this.rows.push({
      end: discount.end ? discount.end : this.noData,
      target: discount.recipeName ? discount.recipeName : this.noData,
      start: discount.start ? discount.start : this.noData,
      value: discount.value ? discount.value.toString() : this.noData,
      uid: discount.id,
      isStandard: true,
      collapse: true
    });
  }
}
