/**
 * Map informational tables
 */
export class InfoTableMapper {

  /**
   * key, value columns
   */
  displayedColumns = ['key', 'value'];

  /**
   * data to display in the table
   */
  dataSource: any[] = []

  constructor(){}
}
