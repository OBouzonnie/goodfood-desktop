import { Order } from "src/app/models/apis/models";
import formatUtils from "src/app/utils/format.utils";
import { InfoTableMapper } from "../InfoTableMapper";

/**
 * map order infos
 */
export class OrderInfoTableMapper extends InfoTableMapper {

  /**
   * class constructor
   * @param data order which info are displayed
   */
  constructor(data: Order) {
    super();
    if(data.id) this.dataSource.push({key: 'dialog.order.ref', value: data.id});
    if(data.restaurantName) this.dataSource.push({key: 'dialog.order.restaurant', value: data.restaurantName});
    if(data.orderDate) this.dataSource.push({key: 'dialog.order.date', value: data.orderDate});
    if(data.status) this.dataSource.push({key: 'dialog.order.status', value: formatUtils.formatOrderStatus(data.status)});
    if(data.discountValue) this.dataSource.push({key: 'dialog.order.discount', value: formatUtils.formatDiscountPercent(data.discountValue)});
    if(data.total) this.dataSource.push({key: 'dialog.order.total', value: formatUtils.toEuroString(data.total)});
    if(data.paymentRef) this.dataSource.push({key: 'dialog.order.payment', value: data.paymentRef});
    if(data.delivery && data.delivery.type) this.dataSource.push({key: 'dialog.order.deliveryMode', value: formatUtils.formatDeliveryType(data.delivery.type)});
    if(data.address) this.dataSource.push({key: 'dialog.order.address', value: data.address.number + ', ' + data.address.street + ' ' + data.address.city});
  }
}
