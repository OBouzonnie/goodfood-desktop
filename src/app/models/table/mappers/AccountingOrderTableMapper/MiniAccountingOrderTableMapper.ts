import formatUtils from "src/app/utils/format.utils";
import { Order } from "../../../apis/models";
import { OrderTableModel } from "../../resources/OrderTableModel";
import { TableMapper } from "../TableMapper";

/**
 * map orders for accounting - reduced size
 */
export class MiniAccountingOrderTableMapper extends TableMapper {

  /**
   * table head row
   */
  override head: OrderTableModel = {
    date: 'table.date',
    total: 'table.total'
  };

  /**
   * table rows
   */
  override rows: OrderTableModel[] = [];

  constructor() {
    super();
  }

  /**
   * add one row to the table
   * @param order order to add
   */
  override addRow(order: Order){
    this.rows.push({
      date: order.orderDate ? order.orderDate : this.noData,
      total: order.total ? formatUtils.toEuroString(order.total) : this.noData
    });
  }
}
