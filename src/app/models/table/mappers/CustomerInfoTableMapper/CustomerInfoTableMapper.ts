import { CustomerInformation } from "src/app/models/apis/models";
import { InfoTableMapper } from "../InfoTableMapper";

/**
 * map customer info for account
 */
export class CustomerInfoTableMapper extends InfoTableMapper {

  /**
   * class constructor
   * @param data customer info that are displayed
   */
  constructor(data: CustomerInformation) {
    super();
    if(data.firstName) this.dataSource.push({key: 'account.firstname', value: data.firstName});
    if(data.lastName) this.dataSource.push({key: 'account.lastname', value: data.lastName});
    if(data.phone) this.dataSource.push({key: 'account.phone', value: data.phone});
  }
}
