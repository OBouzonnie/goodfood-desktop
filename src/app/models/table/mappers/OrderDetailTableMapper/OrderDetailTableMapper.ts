import { OrderDetail } from "src/app/models/apis/models";
import formatUtils from "src/app/utils/format.utils";
import { OrderDetailTableModel } from "../../resources/OrderDetailTableModel";
import { TableMapper } from "../TableMapper";

/**
 * map order details for shop and back office
 */
export class OrderDetailTableMapper extends TableMapper {

  /**
   * table head row
   */
  override head: OrderDetailTableModel = {
    name: 'resource.recipe',
    price: 'table.price',
    qty: 'table.qty'
  };

  /**
   * table rows
   */
  override rows: OrderDetailTableModel[] = [];

  constructor() {super();}

  /**
   * add one row to the table
   * @param detail order detail to add
   */
  override addRow(detail: OrderDetail){
    const price = detail.discountValue ? detail.price * (1 - detail.discountValue/100) : detail.price;
    this.rows.push({
      name: detail.recipeName ? detail.recipeName : this.noData,
      price: formatUtils.toEuroString(price),
      qty: 'x' + detail.quantity.toString()
    });
  }
}
