import { Supplier } from "src/app/models/apis/models";
import { SupplierTableModel } from "../../resources/SupplierTableModel";
import { TableMapper } from "../TableMapper";

/**
 * map suppliers for back office
 */
export class SupplierTableMapper extends TableMapper {

  /**
   * table head row
   */
  override head: SupplierTableModel = {
    name: 'resource.supplier',
    logo: '',
    email: 'table.email',
    phone:'table.phone',
    uid: ''
  };

  /**
   * table rows
   */
  override rows: SupplierTableModel[] = [];

  constructor() {super();}

  /**
   * add one row to the table
   * @param supplier supplier to add
   */
  override addRow(supplier: Supplier){
    if(supplier.isActive) this.rows.push({
      name: supplier.name ? supplier.name : this.noData,
      email: supplier.email ? supplier.email : this.noData,
      phone: supplier.phone ? supplier.phone : this.noData,
      uid: supplier.id,
      isStandard: supplier.isStandard,
      creator: supplier.creator,
      collapse: true
    });
  }
}
