import { MockService } from 'src/app/services/mock/mock.service';
import { Ingredient } from '../../../apis/models';
import { IngredientTableMapper } from './IngredientTableMapper';

const tableMapper: IngredientTableMapper = new IngredientTableMapper();
const mockService: MockService = new MockService();
let resource: Ingredient[] = mockService.getRecipes();

describe('IngredientTableMapper', () => {

  it('should create an instance', () => {
    expect(new IngredientTableMapper()).toBeTruthy();
  });

  it('#addRow should add a row', () => {
    tableMapper.addRow(resource[0]);
    const row = tableMapper.rows.pop();
    expect(row?.name).toEqual(resource[0].name);
    expect(row?.uid).toEqual(resource[0].id);
    expect(row?.collapse).toBeTrue();
  });

  it('#getColumns should return a string array', () => {
    const columns = tableMapper.getColumns();
    expect(columns.length).toBe(3);
    expect(columns[0]).toBe('name');
    expect(columns[1]).toBe('allergens');
    expect(columns[2]).toBe('uid');
  });
});
