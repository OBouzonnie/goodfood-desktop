import { Ingredient } from "../../../apis/models";
import { IngredientTableModel } from "../../resources/IngredientTableModel";
import { TableMapper } from "../TableMapper";

/**
 * map ingredients for back office
 */
export class IngredientTableMapper extends TableMapper {

  /**
   * table head row
   */
  override head: IngredientTableModel = {
    name: 'resource.ingredients',
    allergens: 'table.allergens',
    uid: ''
  };

  /**
   * table rows
   */
  override rows: IngredientTableModel[] = [];

  constructor() {super();}

  /**
   * add one row to the table
   * @param ingredient ingredient to add
   */
  override addRow(ingredient: Ingredient){
    this.rows.push({
      name: ingredient.name ? ingredient.name : this.noData,
      allergens: ingredient.allergen ? ingredient.allergen.join(', '): this.noData,
      uid: ingredient.id ? ingredient.id : '',
      collapse: true
    });
  }
}
