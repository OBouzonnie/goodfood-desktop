import { Restaurant } from "../../../apis/models";
import { RestaurantTableModel } from "../../resources/RestaurantTableModel";
import { TableMapper } from "../TableMapper";

/**
 * map restaurants for back office
 */
export class RestaurantTableMapper extends TableMapper {

  /**
   * show all restaurant or only actives ones ?
   */
  showAll: boolean = false;

  /**
   * table head row
   */
  override head: RestaurantTableModel = {
    name: 'resource.restaurant',
    email: 'table.email',
    phone: 'table.phone',
    uid: ''
  };

  /**
   * table rows
   */
  override rows: RestaurantTableModel[] = [];

  constructor(show?: boolean) {
    super();
    if (show) this.showAll = show;
  }

  /**
   * add one row to the table
   * @param restaurant restaurant to add
   */
  override addRow(restaurant: Restaurant){
    if (this.showAll || restaurant.isActive) this.rows.push({
      name: restaurant.name ? restaurant.name : this.noData,
      email: restaurant.owner && restaurant.owner.email ? restaurant.owner.email : this.noData,
      phone: restaurant.phone ? restaurant.phone : this.noData,
      uid: restaurant.id,
      collapse: true
    });
  }
}
