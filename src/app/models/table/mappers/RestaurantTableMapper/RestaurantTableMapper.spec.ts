import { MockService } from "src/app/services/mock/mock.service";
import { Restaurant } from "../../../apis/models";
import { RestaurantTableMapper } from "./RestaurantTableMapper";

const tableMapper: RestaurantTableMapper = new RestaurantTableMapper();
const mockService: MockService = new MockService();
let resource: Restaurant[] = mockService.getRestaurants();

describe('RestaurantTableMapper', () => {

  it('should create an instance', () => {
    expect(new RestaurantTableMapper()).toBeTruthy();
  });

  it('#addRow should add a row', () => {
    tableMapper.addRow(resource[0]);
    const row = tableMapper.rows.pop();
    expect(row?.name).toEqual(resource[0].name);
    expect(row?.email).toEqual(resource[0].owner?.email);
    expect(row?.phone).toEqual(resource[0].phone);
    expect(row?.uid).toEqual(resource[0].id);
    expect(row?.collapse).toBeTrue();
  });

  it('#getColumns should return a string array', () => {
    const columns = tableMapper.getColumns();
    expect(columns.length).toBe(4);
    expect(columns[0]).toBe('name');
    expect(columns[1]).toBe('email');
    expect(columns[2]).toBe('phone');
    expect(columns[3]).toBe('uid');
  });
});
