import formatUtils from "src/app/utils/format.utils";
import { Recipe } from "../../../apis/models";
import { RecipeTableModel } from "../../resources/RecipeTableModel";
import { TableMapper } from "../TableMapper";

/**
 * map recipe for back office
 */
export class RecipeTableMapper extends TableMapper {

  /**
   * table head row
   */
  override head: RecipeTableModel = {
    name: 'resource.recipe',
    logo: '',
    price: 'table.price',
    photo: 'table.photo',
    count: 'table.usage',
    uid: ''
  };

  /**
   * table rows
   */
  override rows: RecipeTableModel[] = [];

  constructor() {super();}

  /**
   * add one row to the table
   * @param recipe recipe to add
   */
  override addRow(recipe: Recipe){
    if (recipe.isActive) this.rows.push({
      name: recipe.name ? recipe.name : this.noData,
      price: recipe.standardPrice ? formatUtils.toEuroString(recipe.standardPrice) : this.noData,
      photo: recipe.photo ? recipe.photo : this.noData,
      count: recipe.restaurantUsageCount != undefined ?  recipe.restaurantUsageCount.toString() : this.noData,
      uid: recipe.id,
      isStandard: recipe.isStandard,
      creator: recipe.creator,
      collapse: true
    });
  }
}
