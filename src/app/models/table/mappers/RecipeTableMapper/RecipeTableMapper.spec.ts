import { MockService } from "src/app/services/mock/mock.service";
import formatUtils from "src/app/utils/format.utils";
import { Recipe } from "../../../apis/models";
import { RecipeTableMapper } from "./RecipeTableMapper";

const tableMapper: RecipeTableMapper = new RecipeTableMapper();
const mockService: MockService = new MockService();
let resource: Recipe[] = mockService.getRecipes();

describe('RecipeTableMapper', () => {

  it('should create an instance', () => {
    expect(new RecipeTableMapper()).toBeTruthy();
  });

  it('#addRow should add a row', () => {
    tableMapper.addRow(resource[0]);
    const row = tableMapper.rows.pop();
    expect(row?.name).toEqual(resource[0].name);
    expect(row?.photo).toEqual(resource[0].photo);
    expect(row?.price).toEqual(resource[0].standardPrice ? formatUtils.toEuroString(resource[0].standardPrice) : '');
    expect(row?.uid).toEqual(resource[0].id);
    expect(row?.collapse).toBeTrue();
  });

  it('#getColumns should return a string array', () => {
    const columns = tableMapper.getColumns();
    expect(columns.length).toBe(6);
    expect(columns[0]).toBe('name');
    expect(columns[1]).toBe('logo');
    expect(columns[2]).toBe('price');
    expect(columns[3]).toBe('photo');
    expect(columns[4]).toBe('count');
    expect(columns[5]).toBe('uid');
  });
});
