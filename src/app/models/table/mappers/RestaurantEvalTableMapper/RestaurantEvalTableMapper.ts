import { Restaurant } from "../../../apis/models";
import { RestaurantEvalTableModel } from "../../resources/RestaurantEvalTableModel";
import { TableMapper } from "../TableMapper";

/**
 * map restaurant evaluations
 */
export class RestaurantEvalTableMapper extends TableMapper {

  /**
   * table head row
   */
  override head: RestaurantEvalTableModel = {
    name: 'resource.restaurant',
    count: 'table.feedback',
    eval: 'table.average',
    uid: ''
  };

  /**
   * table rows
   */
  override rows: RestaurantEvalTableModel[] = [];

  constructor() {super();}

  /**
   * add one row to the table
   * @param restaurant restaurant to add
   */
  override addRow(restaurant: Restaurant){
    if (restaurant.isActive) this.rows.push({
      name: restaurant.name ? restaurant.name : this.noData,
      count: restaurant.feedbacks ? restaurant.feedbacks.length.toString() : this.noData,
      eval: restaurant.averageMark ? restaurant.averageMark.toString() : this.noData,
      uid: restaurant.id,
      collapse: true
    });
  }
}
