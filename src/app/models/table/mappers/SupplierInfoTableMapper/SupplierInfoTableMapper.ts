import { Supplier } from "src/app/models/apis/models";
import { InfoTableMapper } from "../InfoTableMapper";

/**
 * map supplier informations
 */
export class SupplierInfoTableMapper extends InfoTableMapper {

  constructor() {
    super();
  }

  /**
   * update table informations
   * @param data supplier which info are displayed
   */
  updateData(data: Supplier){
    this.dataSource = [];
    if(data.name) this.dataSource.push({key: 'dialog.purchase.supplier', value: data.name});
    if(data.phone) this.dataSource.push({key: 'dialog.purchase.phone', value: data.phone});
    if(data.email) this.dataSource.push({key: 'dialog.purchase.email', value: data.email});
  }
}
