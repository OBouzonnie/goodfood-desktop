import * as moment from "moment";
import { AppliedDiscount, Feedback, RestaurantRecipe } from "../apis/models";

/**
 * Restaurant Recipe Model
 */
export class RestaurantRecipeModel implements RestaurantRecipe {
  /**
   * recipe id
   */
  id?: string;
  /**
   * recipe active price
   */
  activePrice?: number;
  /**
   * recipe discount
   */
  discount?: AppliedDiscount;
  /**
   * feedback for this recipe
   */
  feedbacks?: Array<Feedback>;
  /**
   * recipe name
   */
  name?: string;
  /**
   * recipe image access
   */
  photo?: string;
  /**
   * recipe base price
   */
  price?: number;

  /**
   * class constructor
   * @param recipe the complete recipe from catalog
   */
  constructor(recipe: RestaurantRecipe){
    this.id = recipe.id;
    this.discount = recipe.discount;
    this.feedbacks = recipe.feedbacks;
    this.name = recipe.name;
    this.photo = recipe.photo;
    this.price = recipe.price ? Math.trunc(recipe.price): undefined;
    if (this.price && this.discount && moment(this.discount.end) > moment()){
      this.activePrice = Math.trunc(this.price * (1 - this.discount.value/100));
    }
  }
}
