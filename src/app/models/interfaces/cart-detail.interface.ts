import { OrderDetail } from "../apis/models";

/**
 * Cart Details
 */
export interface CartDetail extends OrderDetail {
  /**
   * recipe active price
   */
  activePrice?: number;
  /**
   * recipe discount
   */
  discountValue?: number;
}
