import { ReturnLink } from "src/app/models/enums/ReturnLink";
import { NavLink } from "src/app/models/type/NavLink";

/**
 * Page interface
 */
export interface IPage
{
  /**
   * Target of the return link
   */
  home: ReturnLink;

  /**
    * navigation links for the franchise persona
    */
  nav: NavLink[];
}
