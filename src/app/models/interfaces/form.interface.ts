import { EventEmitter } from "@angular/core"
import { FormBuilder, FormGroup } from "@angular/forms"
import { TranslateService } from "@ngx-translate/core"
import { SnackbarService } from "src/app/services/snackbar/snackbar.service"

/**
 * Form interface
 */
export interface IForm
{
  /**
   * form title
   */
  title: string,
  /**
   * readonly state
   */
  readonly?: boolean,
  /**
   * submission event
   */
  submitted: EventEmitter<any>,
  /**
   * provided data
   */
  data?: any,
  /**
   * allow acces to the material snackbar
   */
  snackBarService: SnackbarService,
  /**
   * form controls
   */
  f: Function,
  /**
   * angular form builder
   */
  fb: FormBuilder,
  /**
   * angular form
   */
  form: FormGroup,
  /**
   * internationalization tool
   */
  translate?: TranslateService,
  /**
   * modal confirmation handling
   */
  handleModalConfirm?: Function,
  /**
   * set form values for a read or update crud state
   */
  setFormValues?: Function,
  /**
   * form submission
   */
  submitForm: Function,
  /**
   * error messages compilation
   */
  getErrorsMsg: Function,
  /**
   * request body creation
   */
  getBody: Function
}
