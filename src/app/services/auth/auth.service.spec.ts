import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import faker from '@faker-js/faker';
import * as moment from 'moment';
import { of } from 'rxjs';
import { AppModule } from 'src/app/app.module';
import { TokenService } from '../token/token.service';

import { AuthService } from './auth.service';

describe('AuthService', () => {
  let service: AuthService;
  let uid: string;
  let email: string;
  let password: string;
  let http: HttpClient;
  let tokenService: TokenService;
  let userID: string;
  let validUntil: moment.Moment;
  let token: string;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AppModule],
    });
    service = TestBed.inject(AuthService);
    http = TestBed.inject(HttpClient);
    tokenService = TestBed.inject(TokenService);
    uid = faker.datatype.uuid();
    email = faker.internet.email();
    password = faker.internet.password();
    userID = faker.datatype.uuid();
    validUntil = moment().add(1, 'hour');
    token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJvbGJvdS5jZXNpQGdtYWlsLmNvbSIsInVzZXJJZCI6IjYyYmY1YmM2ODNkZDRiNGI1OGJkOGUzNSIsInJvbGUiOlt7ImF1dGhvcml0eSI6IlVTRVIifV0sImV4cCI6MTY1NjcxMTY1NH0.KgMoAAC6f11xhlI7K9B8XxFLlaefO46hdj5zwbthuYAGrSsWQzIAMOiqGvxRxz4pz2L4jHBfP4RiJ-gu3QR8SA";
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('#handleAuthResponse execute callback with a true argument for a 200', () => {
    let test: boolean;
    const callback = (confirm: boolean) => {
      test = confirm;
      expect(test).toBeTrue();
      expect(service.timer).toBeTruthy();
      clearTimeout(service.timer);
    };
    const resp = {status: 200, body: {token: token}};
    service.handleAuthResponse(resp, callback);
  });

  it('#handleAuthResponse execute callback with a false argument for a 500', () => {
    let test: boolean;
    const callback = (confirm: boolean) => {
      test = confirm;
      expect(test).toBeFalse();
      expect(service.timer).toBeFalsy();
    };
    const resp = {status: 500, body: {token: ""}};
    service.handleAuthResponse(resp, callback);
  });

  it('#handleAuthResponse execute callback with a false argument for a 404', () => {
    let test: boolean;
    const callback = (confirm: boolean) => {
      test = confirm;
      expect(test).toBeFalse();
      expect(service.timer).toBeFalsy();
    };
    const resp = {status: 404, body: {token: ""}};
    service.handleAuthResponse(resp, callback);
  });

  it('#login should set token and timer for a 200', () => {
    spyOn(http, 'get').and.callThrough().and.returnValue(of({status: 200, body: {token: token}}));
    let test: boolean;
    let credential = {username: email, password: password};
    const callback = (confirm: boolean) => {
      test = confirm;
      expect(test).toBeTrue();
      expect(tokenService.getToken()).toBe(token);
      expect(tokenService.getUser()).not.toBeNull();
      expect(service.timer).toBeTruthy();
      clearTimeout(service.timer);
    };
    service.login(credential, callback);
    expect(http.get).toHaveBeenCalled();
  });

  it('#login should not set token and timer for a 500', () => {
    spyOn(http, 'get').and.callThrough().and.returnValue(of({status: 500, body: {token: ""}}));
    let test: boolean;
    let credential = {username: email, password: password};
    const callback = (confirm: boolean) => {
      test = confirm;
      expect(test).toBeFalse();
      expect(tokenService.getToken()).toBeNull();
      expect(tokenService.getUser()).toBeNull();
      expect(service.timer).toBeFalsy();
    };
    service.login(credential, callback);
    expect(http.get).toHaveBeenCalled();
  });

  it('#login should not set token and timer for a 404', () => {
    spyOn(http, 'get').and.callThrough().and.returnValue(of({status: 404, body: {token: ""}}));
    let test: boolean;
    let credential = {username: email, password: password};
    const callback = (confirm: boolean) => {
      test = confirm;
      expect(test).toBeFalse();
      expect(tokenService.getToken()).toBeNull();
      expect(tokenService.getUser()).toBeNull();
      expect(service.timer).toBeFalsy();
    };
    service.login(credential, callback);
    expect(http.get).toHaveBeenCalled();
  });

  it('#auth should set token and timer for a 200', () => {
    const newToken = "aaaabGciOiJIUzUxMiJ9.eyJzdWIiOiJvbGJvdS5jZXNpQGdtYWlsLmNvbSIsInVzZXJJZCI6IjYyYmY1YmM2ODNkZDRiNGI1OGJkOGUzNSIsInJvbGUiOlt7ImF1dGhvcml0eSI6IlVTRVIifV0sImV4cCI6MTY1NjcxMTY1NH0.KgMoAAC6f11xhlI7K9B8XxFLlaefO46hdj5zwbthuYAGrSsWQzIAMOiqGvxRxz4pz2L4jHBfP4RiJ-gu3QR8SA";
    tokenService.saveToken({token: token});
    spyOn(http, 'get').and.callThrough().and.returnValue(of({status: 200, body: {token: newToken}}));
    let test: boolean;
    const callback = (confirm: boolean) => {
      test = confirm;
      expect(test).toBeTrue();
      expect(tokenService.getToken()).toBe(newToken);
      expect(service.timer).toBeTruthy();
      clearTimeout(service.timer);
    };
    service.auth(callback);
    expect(http.get).toHaveBeenCalled();
  });

  it('#auth should not set token and timer for a 500', () => {
    const oldToken = token;
    const newToken = "aaaabGciOiJIUzUxMiJ9.eyJzdWIiOiJvbGJvdS5jZXNpQGdtYWlsLmNvbSIsInVzZXJJZCI6IjYyYmY1YmM2ODNkZDRiNGI1OGJkOGUzNSIsInJvbGUiOlt7ImF1dGhvcml0eSI6IlVTRVIifV0sImV4cCI6MTY1NjcxMTY1NH0.KgMoAAC6f11xhlI7K9B8XxFLlaefO46hdj5zwbthuYAGrSsWQzIAMOiqGvxRxz4pz2L4jHBfP4RiJ-gu3QR8SA";
    tokenService.saveToken({token: oldToken});
    spyOn(http, 'get').and.callThrough().and.returnValue(of({status: 500, body: {token: newToken}}));
    let test: boolean;
    const callback = (confirm: boolean) => {
      test = confirm;
      expect(test).toBeFalse();
      expect(tokenService.getToken()).toBe(oldToken);
      expect(service.timer).toBeFalsy();
    };
    service.auth(callback);
    expect(http.get).toHaveBeenCalled();
  });

  it('#auth should not set token and timer for a 404', () => {
    const oldToken = token;
    const newToken = "aaaabGciOiJIUzUxMiJ9.eyJzdWIiOiJvbGJvdS5jZXNpQGdtYWlsLmNvbSIsInVzZXJJZCI6IjYyYmY1YmM2ODNkZDRiNGI1OGJkOGUzNSIsInJvbGUiOlt7ImF1dGhvcml0eSI6IlVTRVIifV0sImV4cCI6MTY1NjcxMTY1NH0.KgMoAAC6f11xhlI7K9B8XxFLlaefO46hdj5zwbthuYAGrSsWQzIAMOiqGvxRxz4pz2L4jHBfP4RiJ-gu3QR8SA";
    tokenService.saveToken({token: oldToken});
    spyOn(http, 'get').and.callThrough().and.returnValue(of({status: 404, body: {token: newToken}}));
    let test: boolean;
    const callback = (confirm: boolean) => {
      test = confirm;
      expect(test).toBeFalse();
      expect(tokenService.getToken()).toBe(oldToken);
      expect(service.timer).toBeFalsy();
    };
    service.auth(callback);
    expect(http.get).toHaveBeenCalled();
  });

  it('#auth should not set token and timer for a 200 if no previous token is set', () => {
    tokenService.removeToken();
    spyOn(http, 'get').and.callThrough().and.returnValue(of({status: 200, body: {token: token}}));
    let test: boolean;
    const callback = (confirm: boolean) => {
      test = confirm;
      expect(test).toBeFalse();
      expect(tokenService.getToken()).toBeNull();
      expect(tokenService.getUser()).toBeNull();
      expect(service.timer).toBeFalsy();
    };
    service.auth(callback);
    expect(http.get).not.toHaveBeenCalled();
  });

  it('#register should execute callback with a 200 for a 200', () => {
    const newUser: any = {
      email: faker.internet.email(),
      password: faker.internet.password(),
      rgpd: {
        hasConsentForDB: true,
        dbConsentDate: moment().format('DD/MM/YYYY')
      }
    }
    spyOn(http, 'post').and.callThrough().and.returnValue(of({status: 200}));
    let test: number;
    const callback = (confirm: number) => {
      test = confirm;
      expect(test).toBe(200);
    };
    service.register(newUser, callback);
    expect(http.post).toHaveBeenCalled();
  });

  it('#register should execute callback with a 409 for a 409', () => {
    const newUser: any = {
      email: faker.internet.email(),
      password: faker.internet.password(),
      rgpd: {
        hasConsentForDB: true,
        dbConsentDate: moment().format('DD/MM/YYYY')
      }
    }
    spyOn(http, 'post').and.callThrough().and.returnValue(of({status: 409}));
    let test: number;
    const callback = (confirm: number) => {
      test = confirm;
      expect(test).toBe(409);
    };
    service.register(newUser, callback);
    expect(http.post).toHaveBeenCalled();
  });

  it('#register should execute callback with nothing for a 500', () => {
    const newUser: any = {
      email: faker.internet.email(),
      password: faker.internet.password(),
      rgpd: {
        hasConsentForDB: true,
        dbConsentDate: moment().format('DD/MM/YYYY')
      }
    }
    spyOn(http, 'post').and.callThrough().and.returnValue(of({status: 500}));
    let test: number;
    const callback = (confirm: number) => {
      test = confirm;
      expect(test).toBeUndefined();
    };
    service.register(newUser, callback);
    expect(http.post).toHaveBeenCalled();
  });
});
