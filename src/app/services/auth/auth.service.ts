import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { firstValueFrom } from 'rxjs';
import { Customer } from 'src/app/models/apis/models';
import { ApiUrl } from 'src/app/models/enums/ApiUrl';
import { Credential } from 'src/app/models/type/Credential';
import { environment } from 'src/environments/environment';
import { RequestService } from '../request/request.service';
import { TokenService } from '../token/token.service';

/**
 * customer authentication service
 */
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  /**
   * api url
   */
  readonly API: string = environment.api;

  /**
   * refresh auth timer
   */
  timer: any;

  /**
   * maximum number of refresh
   */
  max = 5;

  /**
   * class constructor
   * @param http angular hhtp request client
   * @param tokenService allow access to the token local storage
   * @param requestService tools for http requests
   */
  constructor(
    public http: HttpClient,
    private tokenService: TokenService,
    private requestService: RequestService
  ) { }

  /**
   * create a new customer
   * @param newUser new customer credentials
   * @param callback request handler callback
   */
  register(newUser: Customer, callback: Function){
    const callAPI: string = this.API + ApiUrl.REGISTER;
    const options = this.requestService.getHttpOptions();
    firstValueFrom(this.http.post<any>(callAPI, newUser, options))
                                                        .then(res => this.handleRegisterResponse(res, callback))
                                                        .catch(error => this.handleRegisterResponse(error, callback));
  }

  /**
   * user login
   * @param credentials user credentials
   * @param callback request handler callback
   */
  login(credentials: Credential, callback: Function){
    console.log('login user');
    this.tokenService.removeToken();
    const callAPI: string = this.API + ApiUrl.LOGIN;
    const params = new HttpParams().set('username', credentials.username).set('password', credentials.password);
    const options = this.requestService.getHttpOptions(params);
    this.request(callAPI, options, callback);
  }

  /**
   * refresh user authentication
   * @param callback request handler callback
   */
  auth(callback: Function){
    this.max--;
    if (this.max <= 0) return this.logout();
    console.log('refreshing auth');
    const token = this.tokenService.getToken();
    clearTimeout(this.timer);
    if (token) {
      const callAPI: string = this.API + ApiUrl.AUTH;
      const params = new HttpParams().set('token', token);
      const options = this.requestService.getHttpOptions(params);
      this.request(callAPI, options, callback);
    }
  }

  /**
   * user logout
   */
  logout(){
    console.log('logout user');
    clearTimeout(this.timer);
    this.tokenService.removeToken();
    console.log('confirm token cleared', this.tokenService.getToken());
  }

  /**
   * perform the request
   * @param callAPI api url
   * @param options request options
   * @param callback request handler callback
   */
  request(callAPI: string, options: any, callback: Function){
    firstValueFrom(this.http.get<any>(callAPI, options))
                                                        .then(res => this.handleAuthResponse(res, callback))
                                                        .catch(error => this.handleAuthError(error, callback));
  }

  /**
   * handle a successful login or auth response
   * @param res request response
   * @param callback request handler callback
   * @returns
   */
  handleAuthResponse(res: any, callback: Function){
    if(res.status === 200) {
      this.tokenService.saveToken(res.body);
      const deadline = this.tokenService.getTokenValidity();
      if (deadline) {
        const refresh = moment.unix(deadline).subtract(5, 'minutes').diff(moment());
        this.timer = setTimeout(() => {
          this.auth(callback);
        }, refresh);
        callback(true);
        return;
      }
      callback(false);
      return;
    }
    callback(false);
  }

  /**
   * handle successfull register response
   * @param res request response
   * @param callback request handler callback
   */
  handleRegisterResponse(res: any, callback: Function){
    console.log(res)
    if(res.status === 200 || res.status === 409) {
      callback(res.status);
      return;
    }
    callback();
  }

  /**
   * handle failed request response
   * @param err request error
   * @param callback request handler callback
   */
  handleAuthError(err: any, callback: Function){
    console.error(err);
    callback(false);
  }
}
