import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { AppliedDiscount, Restaurant, RestaurantRecipe } from 'src/app/models/apis/models';
import { RestaurantRecipeModel } from 'src/app/models/models/RestaurantRecipeModel';
import { OpeningHours } from 'src/app/models/type/OpeningHours';
import formatUtils from 'src/app/utils/format.utils';
import { environment } from 'src/environments/environment';

/**
 * catalog tools
 */
@Injectable({
  providedIn: 'root'
})
export class CatalogService {

  /**
   * no data display
   */
  noData: string = environment.core.noData;

  /**
   * class constructor
   * @param translate internationalization tool
   */
  constructor(private translate: TranslateService) { }

  /**
   * map restaurant recipe to a displayable state
   * @param recipe a restaurant recipe
   * @returns a mapped for display recipe
   */
  mapRecipe(recipe: RestaurantRecipe): RestaurantRecipeModel{
    return new RestaurantRecipeModel(recipe);
  }

  /**
   * map several recipe to displayable states at once
   * @param recipes the restaurant recipes
   * @returns an array of mapped recipes
   */
  mapAllRecipes(recipes: RestaurantRecipe[]): RestaurantRecipeModel[]{
    const mappedRecipes: RestaurantRecipeModel[] = [];
    recipes.forEach(r => mappedRecipes.push(this.mapRecipe(r)));
    return mappedRecipes;
  }

  /**
   * check if the recipe discount is still active
   * @param discount the recipe discount
   * @returns the discount active state
   */
  isDiscountActive(discount: AppliedDiscount|undefined): boolean {
    if(!discount) return false;
    return moment(discount.end) > moment();
  }

  /**
   * format recipe price to a displayable state
   * @param recipe the restaurant recipe
   * @returns the recipe displayable price
   */
  formatPrice(recipe: RestaurantRecipeModel): string{
    if(recipe.price){
      if(recipe.activePrice) return formatUtils.toEuroString(recipe.activePrice);
      return formatUtils.toEuroString(recipe.price);
    }
    return this.noData;
  }

  /**
   * format a recipe discount to a displayable state
   * @param recipe the restaurant recipe
   * @returns the displayable discount
   */
  formatDiscount(recipe: RestaurantRecipeModel): string{
    if(recipe.price && recipe.activePrice) {
      return formatUtils.toEuroString(recipe.price);
    }
    return '';
  }

  /**
   * format a restaurant opening hours to a displayable state
   * @param restaurant a restaurant
   * @returns the displayable opening hours of the restaurant
   */
  formatOpeningHours(restaurant: Restaurant): OpeningHours[]{
    const openingHours: OpeningHours[] = [];
    for(let i = 0; i <7; i++){
      openingHours.push({
        day: this.translate.instant('week.' + i.toString()),
        open: restaurant.openDateTime && restaurant.openDateTime[i] ? restaurant.openDateTime[i] : this.noData,
        close: restaurant.closeDateTime && restaurant.closeDateTime[i]? restaurant.closeDateTime[i] : this.noData,
      })
    }
    return openingHours;
  }
}
