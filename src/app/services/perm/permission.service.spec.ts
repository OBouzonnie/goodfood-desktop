import { TestBed } from '@angular/core/testing';
import * as moment from 'moment';
import { Roles } from 'src/app/models/enums/Roles';
import { SecuredModules } from 'src/app/models/enums/SecuredModules';
import { TokenService } from '../token/token.service';

import { PermissionService } from './permission.service';

describe('PermissionService', () => {
  let service: PermissionService;
  let tokenService: TokenService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PermissionService);
    tokenService = TestBed.inject(TokenService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('no role CANNOT access any back office module', () => {
    spyOn(tokenService, 'getPermissions').and.returnValue([])
    expect(service.canAccess(SecuredModules.GROUP)).toBeFalse();
    expect(service.canAccess(SecuredModules.FRANCHISE)).toBeFalse();
    expect(service.canAccess(SecuredModules.ACCOUNTING)).toBeFalse();
    expect(service.canAccess(SecuredModules.COMMUNITY)).toBeFalse();
  });

  it('user role CANNOT access any back office module', () => {
    spyOn(tokenService, 'getPermissions').and.returnValue([Roles.USER])
    expect(service.canAccess(SecuredModules.GROUP)).toBeFalse();
    expect(service.canAccess(SecuredModules.FRANCHISE)).toBeFalse();
    expect(service.canAccess(SecuredModules.ACCOUNTING)).toBeFalse();
    expect(service.canAccess(SecuredModules.COMMUNITY)).toBeFalse();
  });

  it('superadmin role can access all back office module', () => {
    spyOn(tokenService, 'getPermissions').and.returnValue([Roles.SUPER_ADMIN])
    expect(service.canAccess(SecuredModules.GROUP)).toBeTrue();
    expect(service.canAccess(SecuredModules.FRANCHISE)).toBeTrue();
    expect(service.canAccess(SecuredModules.ACCOUNTING)).toBeTrue();
    expect(service.canAccess(SecuredModules.COMMUNITY)).toBeTrue();
  });

  it('admin role can access all back office module', () => {
    spyOn(tokenService, 'getPermissions').and.returnValue([Roles.ADMIN])
    expect(service.canAccess(SecuredModules.GROUP)).toBeTrue();
    expect(service.canAccess(SecuredModules.FRANCHISE)).toBeTrue();
    expect(service.canAccess(SecuredModules.ACCOUNTING)).toBeTrue();
    expect(service.canAccess(SecuredModules.COMMUNITY)).toBeTrue();
  });

  it('group role can only access group back office module', () => {
    spyOn(tokenService, 'getPermissions').and.returnValue([Roles.GROUP])
    expect(service.canAccess(SecuredModules.GROUP)).toBeTrue();
    expect(service.canAccess(SecuredModules.FRANCHISE)).toBeFalse();
    expect(service.canAccess(SecuredModules.ACCOUNTING)).toBeFalse();
    expect(service.canAccess(SecuredModules.COMMUNITY)).toBeFalse();
  });

  it('franchise role can only access franchise back office module', () => {
    spyOn(tokenService, 'getPermissions').and.returnValue([Roles.FRANCHISE])
    expect(service.canAccess(SecuredModules.GROUP)).toBeFalse();
    expect(service.canAccess(SecuredModules.FRANCHISE)).toBeTrue();
    expect(service.canAccess(SecuredModules.ACCOUNTING)).toBeFalse();
    expect(service.canAccess(SecuredModules.COMMUNITY)).toBeFalse();
  });

  it('accounting role can only access accounting back office module', () => {
    spyOn(tokenService, 'getPermissions').and.returnValue([Roles.ACCOUNTING])
    expect(service.canAccess(SecuredModules.GROUP)).toBeFalse();
    expect(service.canAccess(SecuredModules.FRANCHISE)).toBeFalse();
    expect(service.canAccess(SecuredModules.ACCOUNTING)).toBeTrue();
    expect(service.canAccess(SecuredModules.COMMUNITY)).toBeFalse();
  });

  it('community role can only access community back office module', () => {
    spyOn(tokenService, 'getPermissions').and.returnValue([Roles.COMMUNITY_MANAGEMENT])
    expect(service.canAccess(SecuredModules.GROUP)).toBeFalse();
    expect(service.canAccess(SecuredModules.FRANCHISE)).toBeFalse();
    expect(service.canAccess(SecuredModules.ACCOUNTING)).toBeFalse();
    expect(service.canAccess(SecuredModules.COMMUNITY)).toBeTrue();
  });

  it('superadmin is super', () => {
    spyOn(tokenService, 'getPermissions').and.returnValue([Roles.SUPER_ADMIN])
    expect(service.isSuper()).toBeTrue();
  });

  it('admin is NOT super', () => {
    spyOn(tokenService, 'getPermissions').and.returnValue([Roles.ADMIN])
    expect(service.isSuper()).toBeFalse();
  });

  it('group is NOT super', () => {
    spyOn(tokenService, 'getPermissions').and.returnValue([Roles.GROUP])
    expect(service.isSuper()).toBeFalse();
  });

  it('franchise is NOT super', () => {
    spyOn(tokenService, 'getPermissions').and.returnValue([Roles.FRANCHISE])
    expect(service.isSuper()).toBeFalse();
  });

  it('accounting is NOT super', () => {
    spyOn(tokenService, 'getPermissions').and.returnValue([Roles.ACCOUNTING])
    expect(service.isSuper()).toBeFalse();
  });

  it('community is NOT super', () => {
    spyOn(tokenService, 'getPermissions').and.returnValue([Roles.COMMUNITY_MANAGEMENT])
    expect(service.isSuper()).toBeFalse();
  });

  it('user is NOT super', () => {
    spyOn(tokenService, 'getPermissions').and.returnValue([Roles.USER])
    expect(service.isSuper()).toBeFalse();
  });

  it('nothing is NOT super', () => {
    spyOn(tokenService, 'getPermissions').and.returnValue([])
    expect(service.isSuper()).toBeFalse();
  });

  it('user is loggued in when he has a token and a unexpired token validity', () => {
    spyOn(tokenService, 'getToken').and.returnValue('*');
    spyOn(tokenService, 'getTokenValidity').and.returnValue(moment().add(1, 'hour').unix());
    expect(service.isLoggedIn('*')).toBeTrue();
  });

  it('user is NOT loggued in when he does not have a token', () => {
    spyOn(tokenService, 'getToken').and.returnValue(null);
    spyOn(tokenService, 'getTokenValidity').and.returnValue(moment().add(1, 'hour').unix());
    expect(service.isLoggedIn('*')).toBeFalse();
  });

  it('user is NOT loggued in when is token is not valid anymore', () => {
    spyOn(tokenService, 'getToken').and.returnValue(null);
    spyOn(tokenService, 'getTokenValidity').and.returnValue(moment().subtract(1, 'hour').unix());
    expect(service.isLoggedIn('*')).toBeFalse();
  });
});
