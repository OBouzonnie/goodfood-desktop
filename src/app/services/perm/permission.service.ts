import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { ReturnLink } from 'src/app/models/enums/ReturnLink';
import { Roles } from 'src/app/models/enums/Roles';
import { SecuredModules } from 'src/app/models/enums/SecuredModules';
import { environment } from 'src/environments/environment';
import { TokenService } from '../token/token.service';

/**
 * guard tool
 */
@Injectable({
  providedIn: 'root'
})
export class PermissionService {

  /**
   * class constructor
   * @param tokenService allow access to the jwt token
   */
  constructor(
    private tokenService: TokenService
  ) { }

  /**
   * check if the user have the right to access this application url
   * @param url application url
   * @returns the access state
   */
  canAccess(url: string): boolean{
    const permissions = this.tokenService.getPermissions();
    if (permissions == null) return false;
    let eligibleRoles: Roles[] = [];
    switch(url){
      case SecuredModules.GROUP:
        eligibleRoles = environment.core.ROUTES.goodfood.modules.group.permissions;
        break;
      case SecuredModules.FRANCHISE:
        eligibleRoles = environment.core.ROUTES.goodfood.modules.franchise.permissions;
        break;
      case SecuredModules.ACCOUNTING:
        eligibleRoles = environment.core.ROUTES.goodfood.modules.accounting.permissions;
        break;
      case SecuredModules.COMMUNITY:
        eligibleRoles = environment.core.ROUTES.goodfood.modules.community.permissions;
        break;
      default:
        eligibleRoles = [];
    }
    return permissions.some(item => eligibleRoles.includes(item));
  }

  /**
   * check if the user is logged in
   * @param url the active application url
   * @returns the logged in state
   */
  isLoggedIn(url: string): boolean{
    const token = this.tokenService.getToken();
    const deadline = this.tokenService.getTokenValidity();
    if (!deadline) return false;
    return token && moment.unix(deadline) > moment() || url == ReturnLink.BACK_OFFICE_ACCESS;
  }

  /**
   * check if a user is a super admin
   * @returns the isSuper state
   */
  isSuper(): boolean{
    const permissions = this.tokenService.getPermissions();
    if(permissions) return permissions.includes(Roles.SUPER_ADMIN);
    return false;
  }
}
