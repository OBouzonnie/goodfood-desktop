import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

/**
 * material snackbar manager
 */
@Injectable({
  providedIn: 'root'
})
export class SnackbarService {

  /**
   * class constructor
   * @param _snackBar material snackbar
   */
  constructor(protected _snackBar: MatSnackBar,) { }

  /**
   * open the material snackbar
   * @param msg message displayed in the snackbar
   * @param css snacbar css style to apply
   */
  openSnackbar(msg: string, css: string): void{
    this._snackBar.open(msg, '', {
      horizontalPosition: 'center',
      verticalPosition: 'top',
      panelClass: [css],
      duration: 5000
    })
  }
}
