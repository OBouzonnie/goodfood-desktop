import { Injectable } from '@angular/core';
import jwtDecode from 'jwt-decode';
import { LocalStorageKey } from 'src/app/models/enums/LocalStorageKey';
import { Roles } from 'src/app/models/enums/Roles';
import { Token } from 'src/app/models/type/Token';

/**
 * jwt token manegement
 */
@Injectable({
  providedIn: 'root'
})
export class TokenService {

  /**
   * token key
   */
  readonly token: string = LocalStorageKey.TOKEN;

  /**
   * validity key
   */
  readonly validity: string = LocalStorageKey.VALIDITY;

  /**
   * user key
   */
  readonly user: string = LocalStorageKey.USER;

  /**
   * permission key
   */
  readonly permissions: string = LocalStorageKey.PERMISSIONS;

  /**
   * franchise key
   */
  readonly franchise: string = LocalStorageKey.FRANCHISE_ID;

  constructor() { }

  /**
   * decode jwt token
   * @returns a decoded token
   */
  decodeToken(): Token|null{
    const token = this.getToken();
    if (token == null) return null;
    console.log('decode', jwtDecode(token));
    return jwtDecode(token);
  }

  /**
   * get token form local storage
   * @returns the encoded token
   */
  getToken(): string|null {
    console.log('getToken', localStorage.getItem(this.token));
    return localStorage.getItem(this.token);
  }

  /**
   * provide the token validity
   * @returns the token validity
   */
  getTokenValidity(): number|null {
    const token = this.decodeToken();
    if (token == null) return null;
    console.log('getTokenValidity', token.exp);
    return parseInt(token.exp);
  }

  /**
   * provide the user ID
   * @returns the user ID
   */
  getUser(): string|null {
    const token = this.decodeToken();
    if (token == null) return null;
    console.log('getUser', token.userId);
    return token.userId;
  }

  /**
   * provide the franchise ID
   * @returns the franchise ID
   */
  getFranchise(): string|null {
    const token = this.decodeToken();
    if (token == null) return null;
    if (!token.franchise) return null;
    console.log('getFranchise', token.franchise);
    return token.franchise;
  }

  /**
   * provide user permissions
   * @returns user roles
   */
  getPermissions(): Roles[]|null {
    const token = this.decodeToken();
    if (token == null) return null;
    console.log('getPermissions', token.role);
    return token.role.map(r => r.authority);
  }

  /**
   * provide the jwt token to the local storage
   * @param token jwt token
   */
  saveToken(token: any): void {
    localStorage.setItem(this.token, token.token);
  }

  /**
   * remove token from the local storage
   */
  removeToken(): void {
    console.log('clearing token');
    localStorage.removeItem(this.token);
  }
}
