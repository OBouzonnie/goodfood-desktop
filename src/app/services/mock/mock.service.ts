import { Injectable } from '@angular/core';
import faker from '@faker-js/faker';
import * as moment from 'moment';
import { Address, AppliedDiscount, CustomerInformation, Discount, Ingredient, Order, OrderDetail, Recipe, Restaurant, RestaurantRecipe, Supplier } from 'src/app/models/apis/models';
import { DeliveryType } from 'src/app/models/enums/Delivery';
import { OrderStatus } from 'src/app/models/enums/OrderStatus';
import { CartDetail } from 'src/app/models/interfaces/cart-detail.interface';

/**
 * mock service for test purpose
 */
@Injectable({
  providedIn: 'root'
})
export class MockService {

  /**
   * mocked restaurants
   */
  private restaurants: Restaurant[];

  /**
   * mocked recipes
   */
  private recipes: Recipe[] = [];

  /**
   * mocked ingredients
   */
  private ingredients: Ingredient[] = [];

  /**
   * mocked suppliers
   */
  private suppliers: Supplier[] = [];

  /**
   * mocked discount
   */
  private discounts: Discount[] = [];

  /**
   * recipes exemples
   */
  private api: any[] = [
    {"meals":[{"idMeal":"52902","strMeal":"Parkin Cake","strDrinkAlternate":null,"strCategory":"Dessert","strArea":"British","strInstructions":"Heat oven to 160C\/140C fan\/gas 3. Grease a deep 22cm\/9in square cake tin and line with baking parchment. Beat the egg and milk together with a fork.\r\n\r\nGently melt the syrup, treacle, sugar and butter together in a large pan until the sugar has dissolved. Remove from the heat. Mix together the oatmeal, flour and ginger and stir into the syrup mixture, followed by the egg and milk.\r\n\r\nPour the mixture into the tin and bake for 50 mins - 1 hr until the cake feels firm and a little crusty on top. Cool in the tin then wrap in more parchment and foil and keep for 3-5 days before eating if you can \u2013 it\u2019ll become softer and stickier the longer you leave it, up to 2 weeks.\r\n","strMealThumb":"https:\/\/www.themealdb.com\/images\/media\/meals\/qxuqtt1511724269.jpg","strTags":"Caramel","strYoutube":"https:\/\/www.youtube.com\/watch?v=k1lG4vk2MQA","strIngredient1":"Butter","strIngredient2":"Egg","strIngredient3":"Milk","strIngredient4":"Golden Syrup","strIngredient5":"Black Treacle","strIngredient6":"Brown Sugar","strIngredient7":"Oatmeal","strIngredient8":"Self-raising Flour","strIngredient9":"Ground Ginger","strIngredient10":"","strIngredient11":"","strIngredient12":"","strIngredient13":"","strIngredient14":"","strIngredient15":"","strIngredient16":"","strIngredient17":"","strIngredient18":"","strIngredient19":"","strIngredient20":"","strMeasure1":"200g","strMeasure2":"1 large","strMeasure3":"4 tbs","strMeasure4":"200g","strMeasure5":"85g","strMeasure6":"85g","strMeasure7":"100g ","strMeasure8":"250g","strMeasure9":"1 tbs","strMeasure10":"","strMeasure11":"","strMeasure12":"","strMeasure13":"","strMeasure14":"","strMeasure15":"","strMeasure16":"","strMeasure17":"","strMeasure18":"","strMeasure19":"","strMeasure20":"","strSource":"https:\/\/www.bbcgoodfood.com\/recipes\/1940684\/parkin","strImageSource":null,"strCreativeCommonsConfirmed":null,"dateModified":null}]},
    {"meals":[{"idMeal":"52978","strMeal":"Kumpir","strDrinkAlternate":null,"strCategory":"Side","strArea":"Turkish","strInstructions":"If you order kumpir in Turkey, the standard filling is first, lots of butter mashed into the potato, followed by cheese. There\u2019s then a row of other toppings that you can just point at to your heart\u2019s content \u2013 sweetcorn, olives, salami, coleslaw, Russian salad, allsorts \u2013 and you walk away with an over-stuffed potato because you got ever-excited by the choices on offer.\r\n\r\nGrate (roughly \u2013 you can use as much as you like) 150g of cheese.\r\nFinely chop one onion and one sweet red pepper.\r\nPut these ingredients into a large bowl with a good sprinkling of salt and pepper, chilli flakes (optional).","strMealThumb":"https:\/\/www.themealdb.com\/images\/media\/meals\/mlchx21564916997.jpg","strTags":"SideDish","strYoutube":"https:\/\/www.youtube.com\/watch?v=IEDEtZ4UVtI","strIngredient1":"Potatoes","strIngredient2":"Butter","strIngredient3":"Cheese","strIngredient4":"Onion","strIngredient5":"Red Pepper","strIngredient6":"Red Chile Flakes","strIngredient7":"","strIngredient8":"","strIngredient9":"","strIngredient10":"","strIngredient11":"","strIngredient12":"","strIngredient13":"","strIngredient14":"","strIngredient15":"","strIngredient16":"","strIngredient17":"","strIngredient18":"","strIngredient19":"","strIngredient20":"","strMeasure1":"2 large","strMeasure2":"2 tbs","strMeasure3":"150g","strMeasure4":"1 large","strMeasure5":"1 large","strMeasure6":"Pinch","strMeasure7":" ","strMeasure8":" ","strMeasure9":" ","strMeasure10":" ","strMeasure11":" ","strMeasure12":" ","strMeasure13":" ","strMeasure14":" ","strMeasure15":" ","strMeasure16":" ","strMeasure17":" ","strMeasure18":" ","strMeasure19":" ","strMeasure20":" ","strSource":"http:\/\/www.turkeysforlife.com\/2013\/10\/firinda-kumpir-turkish-street-food.html","strImageSource":null,"strCreativeCommonsConfirmed":null,"dateModified":null}]},
    {"meals":[{"idMeal":"53043","strMeal":"Fish fofos","strDrinkAlternate":null,"strCategory":"Seafood","strArea":"Portuguese","strInstructions":"STEP 1\r\n\r\nPut the fish into a lidded pan and pour over enough water to cover. Bring to a simmer and gently poach for 10 minutes over a low heat with the lid on. Drain and flake the fish.\r\n\r\nSTEP 2\r\n\r\nPut the fish, potato, green chilli, coriander, cumin, black pepper, garlic and ginger in a large bowl. Season, add the rice flour, mix well and break in 1 egg. Stir the mixture and divide into 15, then form into small logs. Break the remaining eggs into a bowl and whisk lightly. Put the breadcrumbs into another bowl. Dip each fofo in the beaten egg followed by the breadcrumb mixture. Chill for 20 minutes.\r\n\r\nSTEP 3\r\n\r\nHeat 1cm of oil in a large frying pan over a medium heat. Fry the fofos in batches for 2 minutes on each side, turning gently to get an even golden brown colour all over. Drain on kitchen paper and repeat with the remaining fofos.\r\n\r\nSTEP 4\r\n\r\nFor the onion salad, mix together the onion, coriander and lemon juice with a pinch of salt. Serve with the fofos and mango chutney.","strMealThumb":"https:\/\/www.themealdb.com\/images\/media\/meals\/a15wsa1614349126.jpg","strTags":null,"strYoutube":"https:\/\/www.youtube.com\/watch?v=YXUZHK2PrV8","strIngredient1":"Haddock","strIngredient2":"Potatoes","strIngredient3":"Green Chilli","strIngredient4":"Coriander","strIngredient5":"Cumin Seeds","strIngredient6":"Pepper","strIngredient7":"Garlic","strIngredient8":"Ginger","strIngredient9":"Flour","strIngredient10":"Eggs","strIngredient11":"Breadcrumbs","strIngredient12":"Vegetable Oil","strIngredient13":"","strIngredient14":"","strIngredient15":"","strIngredient16":"","strIngredient17":"","strIngredient18":"","strIngredient19":"","strIngredient20":"","strMeasure1":"600g","strMeasure2":"300g","strMeasure3":"1 chopped","strMeasure4":"3 tbs","strMeasure5":"1 tsp ","strMeasure6":"1\/2 tsp","strMeasure7":"3 cloves","strMeasure8":"2 pieces ","strMeasure9":"2 tbs","strMeasure10":"3","strMeasure11":"75g","strMeasure12":"For frying","strMeasure13":" ","strMeasure14":" ","strMeasure15":" ","strMeasure16":" ","strMeasure17":" ","strMeasure18":" ","strMeasure19":" ","strMeasure20":" ","strSource":"https:\/\/www.olivemagazine.com\/recipes\/fish-and-seafood\/fish-fofos\/","strImageSource":null,"strCreativeCommonsConfirmed":null,"dateModified":null}]},
    {"meals":[{"idMeal":"52873","strMeal":"Beef Dumpling Stew","strDrinkAlternate":null,"strCategory":"Beef","strArea":"British","strInstructions":"Preheat the oven to 180C\/350F\/Gas 4.\r\n\r\nFor the beef stew, heat the oil and butter in an ovenproof casserole and fry the beef until browned on all sides.\r\n\r\nSprinkle over the flour and cook for a further 2-3 minutes.\r\n\r\nAdd the garlic and all the vegetables and fry for 1-2 minutes.\r\n\r\nStir in the wine, stock and herbs, then add the Worcestershire sauce and balsamic vinegar, to taste. Season with salt and freshly ground black pepper.\r\n\r\nCover with a lid, transfer to the oven and cook for about two hours, or until the meat is tender.\r\n\r\nFor the dumplings, sift the flour, baking powder and salt into a bowl.\r\nAdd the suet and enough water to form a thick dough.\r\n\r\nWith floured hands, roll spoonfuls of the dough into small balls.\r\n\r\nAfter two hours, remove the lid from the stew and place the balls on top of the stew. Cover, return to the oven and cook for a further 20 minutes, or until the dumplings have swollen and are tender. (If you prefer your dumplings with a golden top, leave the lid off when returning to the oven.)\r\n\r\nTo serve, place a spoonful of mashed potato onto each of four serving plates and top with the stew and dumplings. Sprinkle with chopped parsley.","strMealThumb":"https:\/\/www.themealdb.com\/images\/media\/meals\/uyqrrv1511553350.jpg","strTags":"Stew,Baking","strYoutube":"https:\/\/www.youtube.com\/watch?v=6NgheY-r5t0","strIngredient1":"Olive Oil","strIngredient2":"Butter","strIngredient3":"Beef","strIngredient4":"Plain Flour","strIngredient5":"Garlic","strIngredient6":"Onions","strIngredient7":"Celery","strIngredient8":"Carrots","strIngredient9":"Leek","strIngredient10":"Swede","strIngredient11":"Red Wine","strIngredient12":"Beef Stock","strIngredient13":"Bay Leaf","strIngredient14":"Thyme","strIngredient15":"Parsley","strIngredient16":"Plain Flour","strIngredient17":"Baking Powder","strIngredient18":"Suet","strIngredient19":"Water","strIngredient20":null,"strMeasure1":"2 tbs","strMeasure2":"25g","strMeasure3":"750g","strMeasure4":"2 tblsp ","strMeasure5":"2 cloves minced","strMeasure6":"175g","strMeasure7":"150g","strMeasure8":"150g","strMeasure9":"2 chopped","strMeasure10":"200g","strMeasure11":"150ml","strMeasure12":"500g","strMeasure13":"2","strMeasure14":"3 tbs","strMeasure15":"3 tblsp chopped","strMeasure16":"125g","strMeasure17":"1 tsp ","strMeasure18":"60g","strMeasure19":"Splash","strMeasure20":"","strSource":"https:\/\/www.bbc.co.uk\/food\/recipes\/beefstewwithdumpling_87333","strImageSource":null,"strCreativeCommonsConfirmed":null,"dateModified":null}]},
    {"meals":[{"idMeal":"53012","strMeal":"Gigantes Plaki","strDrinkAlternate":null,"strCategory":"Vegetarian","strArea":"Greek","strInstructions":"Soak the beans overnight in plenty of water. Drain, rinse, then place in a pan covered with water. Bring to the boil, reduce the heat, then simmer for approx 50 mins until slightly tender but not soft. Drain, then set aside.\r\n\r\nHeat oven to 180C\/160C fan\/gas 4. Heat the olive oil in a large frying pan, tip in the onion and garlic, then cook over a medium heat for 10 mins until softened but not browned. Add the tomato pur\u00e9e, cook for a further min, add remaining ingredients, then simmer for 2-3 mins. Season generously, then stir in the beans. Tip into a large ovenproof dish, then bake for approximately 1 hr, uncovered and without stirring, until the beans are tender. The beans will absorb all the fabulous flavours and the sauce will thicken. Allow to cool, then scatter with parsley and drizzle with a little more olive oil to serve.","strMealThumb":"https:\/\/www.themealdb.com\/images\/media\/meals\/b79r6f1585566277.jpg","strTags":null,"strYoutube":"https:\/\/www.youtube.com\/watch?v=e-2K2iyPASA","strIngredient1":"Butter Beans","strIngredient2":"Olive Oil","strIngredient3":"Onion","strIngredient4":"Garlic Clove","strIngredient5":"Tomato Puree","strIngredient6":"Tomatoes","strIngredient7":"Sugar","strIngredient8":"Dried Oregano","strIngredient9":"Cinnamon","strIngredient10":"Chopped Parsley","strIngredient11":"","strIngredient12":"","strIngredient13":"","strIngredient14":"","strIngredient15":"","strIngredient16":"","strIngredient17":"","strIngredient18":"","strIngredient19":"","strIngredient20":"","strMeasure1":"400g","strMeasure2":"3 tbs","strMeasure3":"1 chopped","strMeasure4":"2 chopped","strMeasure5":"2 tbs","strMeasure6":"800g","strMeasure7":"1 tbs","strMeasure8":"1 tbs","strMeasure9":"Pinch","strMeasure10":"2 tbs","strMeasure11":" ","strMeasure12":" ","strMeasure13":" ","strMeasure14":" ","strMeasure15":" ","strMeasure16":" ","strMeasure17":" ","strMeasure18":" ","strMeasure19":" ","strMeasure20":" ","strSource":"https:\/\/www.bbcgoodfood.com\/recipes\/gigantes-plaki","strImageSource":null,"strCreativeCommonsConfirmed":null,"dateModified":null}]},
    {"meals":[{"idMeal":"52850","strMeal":"Chicken Couscous","strDrinkAlternate":null,"strCategory":"Chicken","strArea":"Moroccan","strInstructions":"Heat the olive oil in a large frying pan and cook the onion for 1-2 mins just until softened. Add the chicken and fry for 7-10 mins until cooked through and the onions have turned golden. Grate over the ginger, stir through the harissa to coat everything and cook for 1 min more.\r\n\r\nTip in the apricots, chickpeas and couscous, then pour over the stock and stir once. Cover with a lid or tightly cover the pan with foil and leave for about 5 mins until the couscous has soaked up all the stock and is soft. Fluff up the couscous with a fork and scatter over the coriander to serve. Serve with extra harissa, if you like.","strMealThumb":"https:\/\/www.themealdb.com\/images\/media\/meals\/qxytrx1511304021.jpg","strTags":null,"strYoutube":"https:\/\/www.youtube.com\/watch?v=GZQGy9oscVk","strIngredient1":"Olive Oil","strIngredient2":"Onion","strIngredient3":"Chicken Breast","strIngredient4":"Ginger","strIngredient5":"Harissa Spice","strIngredient6":"Dried Apricots","strIngredient7":"Chickpeas","strIngredient8":"Couscous","strIngredient9":"Chicken Stock","strIngredient10":"Coriander","strIngredient11":"","strIngredient12":"","strIngredient13":"","strIngredient14":"","strIngredient15":"","strIngredient16":"","strIngredient17":"","strIngredient18":"","strIngredient19":"","strIngredient20":"","strMeasure1":"1 tbsp","strMeasure2":"1 chopped","strMeasure3":"200g","strMeasure4":"pinch","strMeasure5":"2 tblsp ","strMeasure6":"10","strMeasure7":"220g","strMeasure8":"200g","strMeasure9":"200ml","strMeasure10":"Handful","strMeasure11":"","strMeasure12":"","strMeasure13":"","strMeasure14":"","strMeasure15":"","strMeasure16":"","strMeasure17":"","strMeasure18":"","strMeasure19":"","strMeasure20":"","strSource":"https:\/\/www.bbcgoodfood.com\/recipes\/13139\/onepan-chicken-couscous","strImageSource":null,"strCreativeCommonsConfirmed":null,"dateModified":null}]},
    {"meals":[{"idMeal":"52868","strMeal":"Kidney Bean Curry","strDrinkAlternate":null,"strCategory":"Vegetarian","strArea":"Indian","strInstructions":"Heat the oil in a large frying pan over a low-medium heat. Add the onion and a pinch of salt and cook slowly, stirring occasionally, until softened and just starting to colour. Add the garlic, ginger and coriander stalks and cook for a further 2 mins, until fragrant.\r\n\r\nAdd the spices to the pan and cook for another 1 min, by which point everything should smell aromatic. Tip in the chopped tomatoes and kidney beans in their water, then bring to the boil.\r\n\r\nTurn down the heat and simmer for 15 mins until the curry is nice and thick. Season to taste, then serve with the basmati rice and the coriander leaves.","strMealThumb":"https:\/\/www.themealdb.com\/images\/media\/meals\/sywrsu1511463066.jpg","strTags":"Curry","strYoutube":"https:\/\/www.youtube.com\/watch?v=Tp_PMWvIKzo","strIngredient1":"Vegetable Oil","strIngredient2":"Onion","strIngredient3":"Garlic","strIngredient4":"Ginger","strIngredient5":"Coriander","strIngredient6":"Cumin","strIngredient7":"Paprika","strIngredient8":"Garam Masala","strIngredient9":"Chopped Tomatoes","strIngredient10":"Kidney Beans","strIngredient11":"Basmati Rice","strIngredient12":"","strIngredient13":"","strIngredient14":"","strIngredient15":"","strIngredient16":"","strIngredient17":"","strIngredient18":"","strIngredient19":"","strIngredient20":"","strMeasure1":"1 tbls","strMeasure2":"1 finely chopped ","strMeasure3":"2 cloves chopped","strMeasure4":"1 part ","strMeasure5":"1 Packet","strMeasure6":"1 tsp ","strMeasure7":"1 tsp ","strMeasure8":"2 tsp","strMeasure9":"400g","strMeasure10":"400g","strMeasure11":"to serve","strMeasure12":"","strMeasure13":"","strMeasure14":"","strMeasure15":"","strMeasure16":"","strMeasure17":"","strMeasure18":"","strMeasure19":"","strMeasure20":"","strSource":"https:\/\/www.bbcgoodfood.com\/recipes\/kidney-bean-curry","strImageSource":null,"strCreativeCommonsConfirmed":null,"dateModified":null}]},
    {"meals":[{"idMeal":"52826","strMeal":"Braised Beef Chilli","strDrinkAlternate":null,"strCategory":"Beef","strArea":"Mexican","strInstructions":"Preheat the oven to 120C\/225F\/gas mark 1.\r\n\r\nTake the meat out of the fridge to de-chill. Pulse the onions and garlic in a food processor until finely chopped. Heat 2 tbsp olive oil in a large casserole and sear the meat on all sides until golden.\r\n\r\nSet to one side and add another small slug of oil to brown the chorizo. Remove and add the onion and garlic, spices, herbs and chillies then cook until soft in the chorizo oil. Season with salt and pepper and add the vinegar, tomatoes, ketchup and sugar.\r\n\r\nPut all the meat back into the pot with 400ml water (or red wine if you prefer), bring up to a simmer and cook, covered, in the low oven.\r\n\r\nAfter 2 hours, check the meat and add the beans. Cook for a further hour and just before serving, pull the meat apart with a pair of forks.","strMealThumb":"https:\/\/www.themealdb.com\/images\/media\/meals\/uuqvwu1504629254.jpg","strTags":null,"strYoutube":"https:\/\/www.youtube.com\/watch?v=z4kSoJgsu6Y","strIngredient1":"Beef","strIngredient2":"Onions","strIngredient3":"Garlic","strIngredient4":"Olive oil","strIngredient5":"Chorizo","strIngredient6":"Cumin","strIngredient7":"All spice","strIngredient8":"Cloves","strIngredient9":"Cinnamon stick","strIngredient10":"Bay Leaves","strIngredient11":"Oregano","strIngredient12":"Ancho Chillies","strIngredient13":"Balsamic Vinegar","strIngredient14":"Plum Tomatoes","strIngredient15":"Tomato Ketchup","strIngredient16":"Dark Brown Sugar","strIngredient17":"Borlotti Beans","strIngredient18":null,"strIngredient19":null,"strIngredient20":null,"strMeasure1":"1kg","strMeasure2":"3","strMeasure3":"4 cloves","strMeasure4":"Dash","strMeasure5":"300g","strMeasure6":"2 tsp","strMeasure7":"2 tsp","strMeasure8":"1 tsp","strMeasure9":"1 large","strMeasure10":"3","strMeasure11":"2 tsp dried","strMeasure12":"2 ancho","strMeasure13":"3 tbsp","strMeasure14":"2 x 400g","strMeasure15":"2 tbsp","strMeasure16":"2 tbsp","strMeasure17":"2 x 400g tins","strMeasure18":"","strMeasure19":"","strMeasure20":"","strSource":"http:\/\/www.telegraph.co.uk\/food-and-drink\/recipes\/braised-beef-chilli-con-carne\/","strImageSource":null,"strCreativeCommonsConfirmed":null,"dateModified":null}]},
    {"meals":[{"idMeal":"52957","strMeal":"Fruit and Cream Cheese Breakfast Pastries","strDrinkAlternate":null,"strCategory":"Breakfast","strArea":"American","strInstructions":"Preheat oven to 400\u00baF (200\u00baC), and prepare two cookie sheets with parchment paper. In a bowl, mix cream cheese, sugar, and vanilla until fully combined. Lightly flour the surface and roll out puff pastry on top to flatten. Cut each sheet of puff pastry into 9 equal squares. On the top right and bottom left of the pastry, cut an L shape approximately \u00bd inch (1 cm) from the edge.\r\nNOTE: This L shape should reach all the way down and across the square, however both L shapes should not meet at the ends. Your pastry should look like a picture frame with two corners still intact.\r\nTake the upper right corner and fold down towards the inner bottom corner. You will now have a diamond shape.\r\nPlace 1 to 2 teaspoons of the cream cheese filling in the middle, then place berries on top.\r\nRepeat with the remaining pastry squares and place them onto the parchment covered baking sheet.\r\nBake for 15-20 minutes or until pastry is golden brown and puffed.\r\nEnjoy!\r\n","strMealThumb":"https:\/\/www.themealdb.com\/images\/media\/meals\/1543774956.jpg","strTags":"Breakfast,Summer","strYoutube":"","strIngredient1":"Cream Cheese","strIngredient2":"Sugar","strIngredient3":"Vanilla Extract","strIngredient4":"Flour","strIngredient5":"Puff Pastry","strIngredient6":"Strawberries","strIngredient7":"Raspberries","strIngredient8":"Blackberries","strIngredient9":"","strIngredient10":"","strIngredient11":"","strIngredient12":"","strIngredient13":"","strIngredient14":"","strIngredient15":"","strIngredient16":"","strIngredient17":"","strIngredient18":"","strIngredient19":"","strIngredient20":"","strMeasure1":"1 1\/4 oz ","strMeasure2":"1 1\/4 cup","strMeasure3":"1 teaspoon","strMeasure4":"","strMeasure5":"2","strMeasure6":"","strMeasure7":"","strMeasure8":"","strMeasure9":"","strMeasure10":"","strMeasure11":"","strMeasure12":"","strMeasure13":"","strMeasure14":"","strMeasure15":"","strMeasure16":"","strMeasure17":"","strMeasure18":"","strMeasure19":"","strMeasure20":"","strSource":"","strImageSource":null,"strCreativeCommonsConfirmed":null,"dateModified":null}]},
    {"meals":[{"idMeal":"52937","strMeal":"Jerk chicken with rice & peas","strDrinkAlternate":null,"strCategory":"Chicken","strArea":"Jamaican","strInstructions":"To make the jerk marinade, combine all the ingredients in a food processor along with 1 tsp salt, and blend to a pur\u00e9e. If you\u2019re having trouble getting it to blend, just keep turning off the blender, stirring the mixture, and trying again. Eventually it will start to blend up \u2013 don\u2019t be tempted to add water, as you want a thick paste.\r\n\r\nTaste the jerk mixture for seasoning \u2013 it should taste pretty salty, but not unpleasantly, puckering salty. You can now throw in more chillies if it\u2019s not spicy enough for you. If it tastes too salty and sour, try adding in a bit more brown sugar until the mixture tastes well balanced.\r\n\r\nMake a few slashes in the chicken thighs and pour the marinade over the meat, rubbing it into all the crevices. Cover and leave to marinate overnight in the fridge.\r\n\r\nIf you want to barbecue your chicken, get the coals burning 1 hr or so before you\u2019re ready to cook. Authentic jerked meats are not exactly grilled as we think of grilling, but sort of smoke-grilled. To get a more authentic jerk experience, add some wood chips to your barbecue, and cook your chicken over slow, indirect heat for 30 mins. To cook in the oven, heat to 180C\/160C fan\/gas 4. Put the chicken pieces in a roasting tin with the lime halves and cook for 45 mins until tender and cooked through.\r\n\r\nWhile the chicken is cooking, prepare the rice & peas. Rinse the rice in plenty of cold water, then tip it into a large saucepan with all the remaining ingredients except the kidney beans. Season with salt, add 300ml cold water and set over a high heat. Once the rice begins to boil, turn it down to a medium heat, cover and cook for 10 mins.\r\n\r\nAdd the beans to the rice, then cover with a lid. Leave off the heat for 5 mins until all the liquid is absorbed. Squeeze the roasted lime over the chicken and serve with the rice & peas, and some hot sauce if you like it really spicy.","strMealThumb":"https:\/\/www.themealdb.com\/images\/media\/meals\/tytyxu1515363282.jpg","strTags":"Chilli,Curry","strYoutube":"https:\/\/www.youtube.com\/watch?v=qfchrS2D_v4","strIngredient1":"Chicken Thighs","strIngredient2":"Lime","strIngredient3":"Spring Onions","strIngredient4":"Ginger","strIngredient5":"Garlic","strIngredient6":"Onion","strIngredient7":"Red Chilli","strIngredient8":"Thyme","strIngredient9":"Lime","strIngredient10":"Soy Sauce","strIngredient11":"Vegetable Oil","strIngredient12":"Brown Sugar","strIngredient13":"Allspice","strIngredient14":"Basmati Rice","strIngredient15":"Coconut Milk","strIngredient16":"Spring Onions","strIngredient17":"Thyme","strIngredient18":"Garlic","strIngredient19":"Allspice","strIngredient20":"Kidney Beans","strMeasure1":"12","strMeasure2":"1\/2 ","strMeasure3":"1  bunch","strMeasure4":"1 tbs chopped","strMeasure5":"3 cloves","strMeasure6":"1\/2 ","strMeasure7":"3 chopped","strMeasure8":"1\/2 teaspoon","strMeasure9":"Juice of 1","strMeasure10":"2 tbs","strMeasure11":"2 tbs","strMeasure12":"3 tbs","strMeasure13":"1 tbs","strMeasure14":"200g","strMeasure15":"400g","strMeasure16":"1  bunch","strMeasure17":"2 sprigs","strMeasure18":"2 cloves chopped","strMeasure19":"1 tbs","strMeasure20":"800g","strSource":"https:\/\/www.bbcgoodfood.com\/recipes\/2369635\/jerk-chicken-with-rice-and-peas","strImageSource":null,"strCreativeCommonsConfirmed":null,"dateModified":null}]}
  ]

  /**
   * table model mcoked content
   */
  private testers: any[] = [];

  constructor() {
    this.mockApiRecipes();
    this.restaurants = this.mockRestaurants();
    this.testers = this.mockTesters();
    this.discounts = this.mockDiscounts();
  }

  /**
   * mock a recipe discount
   * @param index recipe index
   * @returns a mocked discount
   */
  mockDiscount(index : number){
    let i = null;
    if(index % 2 == 0){
      let i = Math.floor(Math.random() * this.recipes.length);
    }
    return {
      id: faker.datatype.uuid(),
      recipe: i ? this.recipes[i].id : undefined,
      recipeName : i ? this.recipes[i].name : undefined,
      start: moment().subtract(1, 'week').format('MM/DD/YYYY'),
      end: moment().add(1, 'week').format('MM/DD/YYYY'),
      value: (Math.random() * 100)
    }
  }

  /**
   * mock 10 discounts
   * @returns an array of mocked discount
   */
  mockDiscounts(){
    const count = 10;
    const discounts = [];
    for (let i = 0; i < count; i++) {
      discounts.push(this.mockDiscount(i));
    }
    return discounts;
  }

  /**
   * generate a mocked item
   * @returns a mocked item
   */
  mockTester(){
    return {
      id: faker.datatype.uuid(),
      name: faker.random.word(),
      photo: faker.image.image(),
      text: faker.random.word(),
    }
  }

  /**
   * provide the mocked discounts
   * @returns mocked discounts
   */
  getDiscounts(){
    return this.discounts;
  }

  /**
   * mock 10 test item
   * @returns an array of mocked test item
   */
  mockTesters(){
    const count = 10;
    const testers = [];
    for (let i = 0; i < count; i++) {
      testers.push(this.mockTester());
    }
    return testers;
  }

  /**
   * provide mocked test items
   * @returns mocked test items
   */
  getTesters(){
    return this.testers;
  }

  /**
   * mock a restaurant
   * @returns a mocked restaurant
   */
  mockRestaurant(){
    const restaurant: Restaurant = {
      isActive: true,
      averageMark: Math.trunc(Math.random() * 5),
      address: {
        city: faker.address.city(),
        id: faker.datatype.uuid(),
        number: Math.floor(Math.random() * 100).toString(),
        postal: faker.address.zipCode(),
        street: faker.address.streetName()
      },
      closeDateTime: [moment().add(10, 'hour').format('HH:mm')],
      id: faker.datatype.uuid(),
      name: faker.company.companyName(),
      openDateTime: [moment().format('HH:mm')],
      owner: {email: faker.internet.email(), firstName: faker.name.firstName(), lastName: faker.name.lastName()},
      phone: faker.phone.phoneNumber(),
      recipes: []
    };
    for (let i = 1; i < 7; i++){
      restaurant.openDateTime?.push(moment().add(i, 'hour').format('HH:mm'));
      restaurant.closeDateTime?.push(moment().add(i+10, 'hour').format('HH:mm'));
    }
    return restaurant;
  }

  /**
   * mock several restaurant
   * @returns an array of mocked restaurant
   */
  mockRestaurants(){
    const count = 10;
    const restaurants = [];
    for (let i = 0; i < count; i++) {
      restaurants.push(this.mockRestaurant());
    }
    return restaurants;
  }

  /**
   * provide the mocked restaurant
   * @returns the mocked restaurants
   */
  getRestaurants(){
    return this.restaurants
  }

  /**
   * mock a franchise
   * @returns a mocked franchise
   */
  mockFranchise(): Restaurant{
    const franchise = this.mockRestaurant();
    franchise.recipes = this.getRecipes();
    return franchise;
  }

  /**
   * mock a recipe
   * @returns a mocked recipe
   */
  mockRecipe(){
    const ing = Math.floor(Math.random() * 10);
    const ingredients = [];
    for (let i = 0 ; i < ing; i++){
      ingredients.push({
        id: faker.datatype.uuid(),
        name: faker.name.lastName(),
        quantity: Math.floor(Math.random() * 10)
      })
    }
    return {
      isActive: true,
      id: faker.datatype.uuid(),
      ingredients: ingredients,
      isStandard: faker.datatype.boolean(),
      name: faker.random.word(),
      photo: [faker.image.food()],
      standardPrice: Math.random() * 100
    }
  }

  /**
   * mock several recipes
   * @returns an array of mocked recipes
   */
  mockRecipes(){
    const count = 10;
    const recipes = [];
    for (let i = 0; i < count; i++) {
      recipes.push(this.mockRecipe());
    }
    return recipes;
  }

  /**
   * mock several recipes
   * @returns an array of mocked recipes
   */
  mockApiRecipes(){
    const recipes = JSON.parse(JSON.stringify(this.api));
    recipes.forEach((e: any) => {
      let ingredients = [
        e.meals[0].strIngredient1,
        e.meals[0].strIngredient2,
        e.meals[0].strIngredient3,
        e.meals[0].strIngredient4,
        e.meals[0].strIngredient5,
        e.meals[0].strIngredient6,
        e.meals[0].strIngredient7,
        e.meals[0].strIngredient8,
        e.meals[0].strIngredient9,
        e.meals[0].strIngredient10,
        e.meals[0].strIngredient11,
        e.meals[0].strIngredient12,
        e.meals[0].strIngredient13,
        e.meals[0].strIngredient14,
        e.meals[0].strIngredient15,
        e.meals[0].strIngredient16,
        e.meals[0].strIngredient17,
        e.meals[0].strIngredient18,
        e.meals[0].strIngredient19,
        e.meals[0].strIngredient20,
      ];
      ingredients = ingredients.filter(e => e != '' && e != null);
      const ing: any[] = [];
      ingredients.forEach(e => {
        const id = faker.datatype.uuid();
        ing.push(
          {
            id: id,
            name: e,
            quantity: Math.floor(Math.random() * 10 + 1)
          }
        );
        this.ingredients.push(
          {
            id: id,
            name : e
          }
        );
      });
      this.recipes.push({
        isActive: true,
        id: faker.datatype.uuid(),
        ingredients: ing,
        isStandard: faker.datatype.boolean(),
        name: e.meals[0].strMeal,
        photo: e.meals[0].strMealThumb,
        standardPrice: Math.random() * 100
      });
      this.ingredients.forEach(ing => {
        const count: number = Math.floor(Math.random() * 10) + 1;
        ing.allergen = [];
        for (let i = 0; i < count; i++){
          ing.allergen.push(faker.name.lastName());
        }
      });
    });
  }

  /**
   * provide the mocked recipes
   * @returns the mocked recipes
   */
  getRecipes(){
    return this.recipes;
  }

  /**
   * provide the mocked ingredients
   * @returns the mocked ingredients
   */
  getIngredients(){
    return this.ingredients;
  }

  /**
   * provide the mocked customer informations
   * @returns the mocked customer informations
   */
  getCustomerInfo(): CustomerInformation{
    return {
      birthDate: moment().add(-20, 'year').format('DD/MM/YYYY'),
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      phone: faker.phone.phoneNumber()
    }
  }

  /**
   * provide a mocked date
   * @returns the mocked date
   */
  getDate(): string {
    const date = [
      Math.floor(Math.random() * 11 + 1).toString(),
      Math.floor(Math.random() * 27 + 1).toString(),
      Math.floor(Math.random() * 100 + 1980).toString()
    ]
    return date.join('/');
  }

  /**
   * mock an address
   * @returns a mocked address
   */
  mockAddress(): Address {
    return {
      number: Math.floor(Math.random() * 110).toString(),
      street: faker.address.streetName(),
      postal: (Math.floor(Math.random() * 9999) + 10000).toString(),
      city: faker.address.city(),
      id: faker.datatype.uuid()
    }
  }

  /**
   * provide the mocked addresses
   * @returns the mocked addresses
   */
  getAddresses(): Address[] {
    const addresses: Address[] = [];
    const count = Math.ceil(Math.random() * 6);
    for (let i = 0; i < count; i++) {
      addresses.push(this.mockAddress());
    }
    return addresses;
  }

  /**
   * mock several suppliers
   * @returns an array of mocked suppliers
   */
  mockSuppliers(): Supplier[] {
    const count = Math.ceil(Math.random() * 6);
    for (let i = 0; i < count; i++) {
        this.suppliers.push(this.mockSupplier());
    }
    return this.suppliers;
  }

  /**
   * mock a supplier
   * @returns a mocked supplier
   */
  mockSupplier(){
    const ing = Math.floor(Math.random() * 10 + 1);
    const ingredients = [];
    for (let i = 0 ; i < ing; i++){
      const j = Math.floor(Math.random() * this.ingredients.length);
      ingredients.push({
        id: this.ingredients[j].id,
        price: Math.floor(Math.random() * 10)
      })
    }
    return {
      isActive: true,
      id: faker.datatype.uuid(),
      ingredients: ingredients,
      isStandard: faker.datatype.boolean(),
      name: faker.random.word(),
      phone: faker.phone.phoneNumber(),
      email: faker.internet.email()
    }
  }

  /**
   * provide the mocked suppliers
   * @returns the mocked suppliers
   */
  getSuppliers()
  {
    this.mockSuppliers()
    return this.suppliers;
  }

  /**
   * mock an order
   * @returns a mocked order
   */
  mockOrder(status: OrderStatus, delivery: DeliveryType, isPayed: boolean = true): Order{
    return {
      id: faker.datatype.uuid(),
      address: this.mockAddress(),
      customer: faker.datatype.uuid(),
      restaurant: faker.datatype.uuid(),
      restaurantName: faker.company.companyName(),
      orderDate: this.getDate(),
      total: Math.trunc(Math.random() * 100000),
      status: status,
      delivery: {
        type: delivery,
        arrivalTime: moment().utc().format(),
        readyTime: moment().utc().format()
      },
      details: this.mockOrderDetails(),
      isPayed: isPayed
    }
  }

  /**
   * mock an order details
   * @returns a mocked order details
   */
  mockOrderDetail(): OrderDetail{
    return {
      price: Math.trunc(Math.random()* 10000 + 1),
      quantity: Math.trunc(Math.random() * 10 + 1),
      recipe: faker.datatype.uuid(),
      recipeName: faker.word.noun()
    }
  }

  /**
   * mock several order details
   * @returns an array of mocked order details
   */
  mockOrderDetails(): OrderDetail[]{
    const j = Math.trunc(Math.random() * 10 + 1);
    const details: OrderDetail[] = [];
    for(let i = 0; i < j; i++){
      details.push(this.mockOrderDetail());
    }
    return details;
  }

  /**
   * mock a cart detail
   * @returns a mocked cart detail
   */
  mockCartDetail(i: number): CartDetail{
    return {
      price: i,
      quantity: i,
      recipe: faker.datatype.uuid(),
      recipeName: faker.word.noun()
    }
  }

  /**
   * mock a cart
   * @returns a mocked cart
   */
  mockCart(): CartDetail[]{
    const cart: CartDetail[] = [];
    for(let i = 0; i < 10; i++){
      cart.push(this.mockCartDetail(i));
    }
    return cart;
  }

  /**
   * mock a restaurant recipe
   * @returns a mocked restaurant recipe
   */
  mockRestaurantRecipe(hasDiscount?: boolean): RestaurantRecipe{
    return {
      id: faker.datatype.uuid(),
      name: faker.word.noun(),
      price: Math.trunc(Math.random() * 10000 + 1),
      discount: hasDiscount ? this.mockAppliedDiscount() : undefined
    }
  }

  /**
   * mock an applied discount
   * @returns a mocked applied discount
   */
  mockAppliedDiscount(): AppliedDiscount{
    return {
      start: moment().subtract(1, 'day').format('dd/MM/yyyy'),
      end: moment().add(2, 'day').format('dd/MM/yyyy'),
      value: Math.trunc(Math.random() * 100 + 1)
    }
  }
}
