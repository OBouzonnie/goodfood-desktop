import { Injectable } from '@angular/core';

/**
 * material spinner state manager
 */
@Injectable({
  providedIn: 'root'
})
export class SpinnerService {

  constructor() { }

  /**
   * number of processing request
   */
  count: number = 0;

  /**
   * check if tehere is request processing
   * @returns the spinner state
   */
  isLoading(): boolean{
    return this.count > 0;
  }

  /**
   * add a processing request to the count
   */
  increaseCount(){
    this.count++;
    this.log();
  }

  /**
   * remove a resolved request from the count
   */
  decreaseCount(){
    if (this.count > 0) this.count--;
    this.log();
  }

  /**
   * log the request calls and resolution in the console
   */
  log(){
    console.log(this.count + ' request(s) processing');
  }
}
