import { Injectable } from '@angular/core';
import faker from '@faker-js/faker';

/**
 * mock a payment service
 */
@Injectable({
  providedIn: 'root'
})
export class PaymentService {

  constructor() { }

  /**
   * mock the payment with a timeout
   * @returns a mocked payment response promise
   */
  pay(): Promise<string>{
    return new Promise<string>((resolve, reject) => {
      setTimeout(() => {
        resolve(faker.datatype.uuid());
      }, 3000);
    });
  }
}
