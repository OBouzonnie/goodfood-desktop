import { TestBed } from '@angular/core/testing';
import { AppModule } from 'src/app/app.module';
import { PaymentService } from './payment-service.service';

describe('PaymentServiceService', () => {
  let service: PaymentService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AppModule],
    });
    service = TestBed.inject(PaymentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
