import { TestBed } from '@angular/core/testing';
import { AppModule } from 'src/app/app.module';
import { Restaurant } from 'src/app/models/apis/models';
import { CartDetail } from 'src/app/models/interfaces/cart-detail.interface';
import { RestaurantRecipeModel } from 'src/app/models/models/RestaurantRecipeModel';
import { MockService } from '../mock/mock.service';
import { CartService } from './cart.service';

describe('CartService', () => {
  let service: CartService;
  let mockService: MockService;
  let cart: CartDetail[];
  let franchise: Restaurant;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AppModule],
    });
    service = TestBed.inject(CartService);
    mockService = TestBed.inject(MockService);
    service.emptyCart();
    franchise = mockService.mockRestaurant();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('processAdditionToCart should add recipe to cart <with quantity to one> if recipe was not already included', () => {
    const recipe = mockService.mockRestaurantRecipe();
    service.processAdditionToCart(new RestaurantRecipeModel(recipe));
    const cart = service.getCart();
    expect(cart.length).toBe(1);
    const cartRecipe = cart[0];
    expect(cartRecipe.recipeName).toBe(recipe.name);
    expect(cartRecipe.price).toBe(recipe.price ? recipe.price : 0);
    expect(cartRecipe.quantity).toBe(1);
  });

  it('processAdditionToCart should add another recipe with quantity to one if other recipe was not already included', () => {
    const recipe = mockService.mockRestaurantRecipe();
    service.processAdditionToCart(new RestaurantRecipeModel(recipe));
    service.processAdditionToCart(new RestaurantRecipeModel(recipe));
    const cart = service.getCart();
    expect(cart.length).toBe(1);
    const cartRecipe1 = cart[0];
    const cartRecipe2 = cart[1];
    expect(cartRecipe2).toBeFalsy();
    expect(cartRecipe1.recipeName).toBe(recipe.name);
    expect(cartRecipe1.price).toBe(recipe.price ? recipe.price : 0);
    expect(cartRecipe1.quantity).toBe(2);
  });

  it('removeFromCart should remove recipe from cart if it is included', () => {
    const recipes = [
      mockService.mockRestaurantRecipe(),
      mockService.mockRestaurantRecipe(),
      mockService.mockRestaurantRecipe()
    ]
    service.processAdditionToCart(new RestaurantRecipeModel(recipes[0]));
    service.processAdditionToCart(new RestaurantRecipeModel(recipes[1]));
    service.processAdditionToCart(new RestaurantRecipeModel(recipes[2]));
    const cart = service.getCart();
    expect(cart.length).toBe(3);
    const recipe = recipes[Math.trunc(Math.random() * 3)];
    service.removeFromCart(recipe.id ? recipe.id : '');
    expect(cart.length).toBe(2);
  });

  it('removeFromCart should NOT remove recipe from cart if it is not included', () => {
    const recipes = [
      mockService.mockRestaurantRecipe(),
      mockService.mockRestaurantRecipe(),
      mockService.mockRestaurantRecipe()
    ]
    const recipe = mockService.mockRestaurantRecipe();
    service.processAdditionToCart(new RestaurantRecipeModel(recipes[0]));
    service.processAdditionToCart(new RestaurantRecipeModel(recipes[1]));
    service.processAdditionToCart(new RestaurantRecipeModel(recipes[2]));
    const cart = service.getCart();
    expect(cart.length).toBe(3);
    service.removeFromCart(recipe.id ? recipe.id : '');
    expect(cart.length).toBe(3);
  });

  it('updateQuantity should update recipe quantity if it is included', () => {
    const recipes = [
      mockService.mockRestaurantRecipe(),
      mockService.mockRestaurantRecipe(),
      mockService.mockRestaurantRecipe()
    ]
    service.processAdditionToCart(new RestaurantRecipeModel(recipes[0]));
    service.processAdditionToCart(new RestaurantRecipeModel(recipes[1]));
    service.processAdditionToCart(new RestaurantRecipeModel(recipes[2]));
    const cart = service.getCart();
    const recipe = recipes[1];
    expect(cart[0].quantity).toBe(1);
    expect(cart[1].quantity).toBe(1);
    expect(cart[2].quantity).toBe(1);
    service.updateQuantity(recipe.id ? recipe.id : '', {target: {value: 3}});
    expect(cart[0].quantity).toBe(1);
    expect(cart[1].quantity).toBe(3);
    expect(cart[2].quantity).toBe(1);
  });

  it('updateQuantity should NOT update recipe quantity if it is included', () => {
    const recipes = [
      mockService.mockRestaurantRecipe(),
      mockService.mockRestaurantRecipe(),
      mockService.mockRestaurantRecipe()
    ]
    service.processAdditionToCart(new RestaurantRecipeModel(recipes[0]));
    service.processAdditionToCart(new RestaurantRecipeModel(recipes[1]));
    service.processAdditionToCart(new RestaurantRecipeModel(recipes[2]));
    const cart = service.getCart();
    const recipe = mockService.mockRestaurantRecipe();
    expect(cart[0].quantity).toBe(1);
    expect(cart[1].quantity).toBe(1);
    expect(cart[2].quantity).toBe(1);
    service.updateQuantity(recipe.id ? recipe.id : '', {target: {value: 3}});
    expect(cart[0].quantity).toBe(1);
    expect(cart[1].quantity).toBe(1);
    expect(cart[2].quantity).toBe(1);
  });

  it('getDetail should return an item if it is included', () => {
    const recipes = [
      mockService.mockRestaurantRecipe(),
      mockService.mockRestaurantRecipe(),
      mockService.mockRestaurantRecipe()
    ]
    service.processAdditionToCart(new RestaurantRecipeModel(recipes[0]));
    service.processAdditionToCart(new RestaurantRecipeModel(recipes[1]));
    service.processAdditionToCart(new RestaurantRecipeModel(recipes[2]));
    const cart = service.getCart();
    const recipe = recipes[1];
    expect(service.getDetail(recipe.id ? recipe.id : '')).toBeTruthy();
  });

  it('getDetail should NOT return an item if it is not included', () => {
    const recipes = [
      mockService.mockRestaurantRecipe(),
      mockService.mockRestaurantRecipe(),
      mockService.mockRestaurantRecipe()
    ]
    service.processAdditionToCart(new RestaurantRecipeModel(recipes[0]));
    service.processAdditionToCart(new RestaurantRecipeModel(recipes[1]));
    service.processAdditionToCart(new RestaurantRecipeModel(recipes[2]));
    const cart = service.getCart();
    const recipe = mockService.mockRestaurantRecipe();
    expect(service.getDetail(recipe.id ? recipe.id : '')).toBeFalsy();
  });

  it('getTotal should return correct amount', () => {
    const recipes = [
      mockService.mockRestaurantRecipe(),
      mockService.mockRestaurantRecipe(),
      mockService.mockRestaurantRecipe()
    ]
    service.processAdditionToCart(new RestaurantRecipeModel(recipes[0]));
    service.processAdditionToCart(new RestaurantRecipeModel(recipes[1]));
    service.processAdditionToCart(new RestaurantRecipeModel(recipes[2]));
    const cart = service.getCart();
    let tot = 0;
    cart.forEach((cd: CartDetail) => {
      if(cd.activePrice) {
        tot += cd.activePrice * cd.quantity
      } else {
        tot += cd.price * cd.quantity
      }
    });
    expect(service.getTotal()).toBe(tot);
  });

  it('getTotal should return correct amount including recipe discount', () => {
    const recipes = [
      mockService.mockRestaurantRecipe(),
      mockService.mockRestaurantRecipe(true),
      mockService.mockRestaurantRecipe()
    ]
    service.processAdditionToCart(new RestaurantRecipeModel(recipes[0]));
    service.processAdditionToCart(new RestaurantRecipeModel(recipes[1]));
    service.processAdditionToCart(new RestaurantRecipeModel(recipes[2]));
    const cart = service.getCart();
    let tot = 0;
    cart.forEach((cd: CartDetail) => {
      if(cd.activePrice) {
        tot += cd.activePrice * cd.quantity
      } else {
        tot += cd.price * cd.quantity
      }
    });
    expect(service.getTotal()).toBe(tot);
  });

  it('getDiscount return null for a franchise without global discount', () => {
    const franchise = mockService.mockRestaurant();
    service.setFranchise(franchise);
    expect(service.getDiscount()).toBeNull();
  });

  it('getDiscount return discount value for a franchise with global discount', () => {
    const franchise = mockService.mockRestaurant();
    franchise.discount = mockService.mockDiscount(1);
    service.setFranchise(franchise);
    expect(service.getDiscount()).toBe(franchise.discount.value);
  });


});
