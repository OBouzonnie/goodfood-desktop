import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import * as moment from 'moment';
import { ModalCartComponent } from 'src/app/component/dialog/modal-cart/modal-cart.component';
import { Restaurant } from 'src/app/models/apis/models';
import { LocalStorageKey } from 'src/app/models/enums/LocalStorageKey';
import { CartDetail } from 'src/app/models/interfaces/cart-detail.interface';
import { RestaurantRecipeModel } from 'src/app/models/models/RestaurantRecipeModel';
import formatUtils from 'src/app/utils/format.utils';

/**
 * cart locale storage tools
 */
@Injectable({
  providedIn: 'root'
})
export class CartService {

  /**
   * cart locale storage key
   */
  readonly cartKey: string = LocalStorageKey.CART;

  /**
   * franchise locale storage key
   */
  readonly franchiseKey: string = LocalStorageKey.FRANCHISE;

  /**
   * cart content
   */
  private cart: CartDetail[];

  /**
   * franchiseconcern
   */
  private franchise: Restaurant|null = null;

  /**
   * class constructor
   * @param dialog material modal
   */
  constructor(
    protected dialog: MatDialog
  ) {
    const storedCart = localStorage.getItem(this.cartKey);
    this.cart = storedCart ? JSON.parse(storedCart) : [];
    const storedFranchise = localStorage.getItem(this.franchiseKey);
    this.franchise = storedFranchise ? JSON.parse(storedFranchise) : null;
  }

  /**
   * provide the actual cart
   * @returns the actual cart
   */
  getCart(): CartDetail[]{
    return this.cart;
  }

  /**
   * provide the franchise concern by the cart
   * @returns the franchise
   */
  getFranchise(): Restaurant|null{
    return this.franchise ? this.franchise : null;
  }

  /**
   * provide a specific cart detail
   * @param id recipe id
   * @returns the cart detail for this recipe
   */
  getDetail(id: string): CartDetail|undefined{
    return this.cart.find(d => d.recipe == id);
  }

  /**
   * provide the displayable total amount for the cart
   * @returns the total amount as currency
   */
  renderTotal(): string{
    return formatUtils.toEuroString(this.getTotal());
  }

  /**
   * calculate the total amount for the cart
   * @returns the total amount
   */
  getTotal(): number{
    let tot = 0;
    this.cart.forEach((cd: CartDetail) => {
      if(cd.activePrice) {
        tot += cd.activePrice * cd.quantity
      } else {
        tot += cd.price * cd.quantity
      }
    });
    const franchise = this.getFranchise();
    if(franchise && franchise.discount) {
      const isActive = moment() < moment(franchise.discount.end);
      if(isActive) tot = tot * (1 - franchise.discount.value/100);
    }
    return tot
  }

  /**
   * provide the active discount of the cart
   * @returns the discount value
   */
  getDiscount(): number|null{
    const franchise = this.getFranchise();
    if(franchise && franchise.discount){
      const isActive = moment() <= moment(franchise.discount.end);
      return isActive ?  franchise.discount.value : null;
    }
    return null;
  }

  /**
   * erase cart from local storage
   */
  emptyCart(): void{
    this.cart = [];
    this.franchise = null;
    localStorage.removeItem(this.cartKey);
    localStorage.removeItem(this.franchiseKey);
  }

  /**
   * store the cart franchise in local storage
   * @param franchise the franchise concern by the cart
   */
  setFranchise(franchise: Restaurant){
    this.franchise = franchise;
    localStorage.setItem(this.franchiseKey, JSON.stringify(franchise));
  }

  /**
   * add a recipe to the cart
   * @param franchise the franchise owning this recipe
   * @param recipe the recipe to add to the cart
   */
  addToCart(franchise: Restaurant|null, recipe: RestaurantRecipeModel){
    if(franchise && this.franchise && franchise.id != this.franchise.id) {
      const ref = this.openCartmodal();
      ref.componentInstance.confirm.subscribe((confirm: boolean) => {
        if (confirm) {
          this.emptyCart();
          this.setFranchise(franchise);
          this.processAdditionToCart(recipe);
        }
        ref.componentInstance.confirm.unsubscribe();
        ref.close();
      });
    } else {
      if(!this.franchise && franchise) this.setFranchise(franchise);
      this.processAdditionToCart(recipe);
    }
  }

  /**
   * open a confirmation modal
   * @returns the modal reference
   */
  openCartmodal(): MatDialogRef<ModalCartComponent> {
    return this.dialog.open(ModalCartComponent);
  }

  /**
   * manage the inclusion state of the recipe in the cart when it's added to the cart
   * @param recipe recipe to add to the cart
   */
  processAdditionToCart(recipe: RestaurantRecipeModel){
    if(recipe.id && recipe.name && recipe.price){
      const cartItem = this.cart.find(d => d.recipe == recipe.id);
      if(cartItem) this.cart[this.cart.indexOf(cartItem)].quantity++;
      if(!cartItem) this.cart.push({
        recipe: recipe.id,
        recipeName: recipe.name,
        price: recipe.price,
        activePrice: recipe.activePrice,
        quantity: 1
      });
      localStorage.setItem(this.cartKey, JSON.stringify(this.cart));
    }
  }

  /**
   * remove a recipe from the cart
   * @param id recipe id
   */
  removeFromCart(id: string){
    const recipe = this.cart.find(d => d.recipe == id);
    if(recipe) {
      this.cart.splice(this.cart.indexOf(recipe), 1);
      localStorage.setItem(this.cartKey, JSON.stringify(this.cart));
    }
  }

  /**
   * change a recipe quantity in the cart
   * @param id recipe id
   * @param qty new quantity
   */
  updateQuantity(id: string, qty: any){
    if(id && qty){
      const recipe = this.cart.find(d => d.recipe == id);
      const quantity = parseInt(qty.target.value);
      if(recipe && !isNaN(quantity) && quantity > 0){
        console.log(quantity)
        recipe.quantity = quantity;
        this.cart.splice(this.cart.indexOf(recipe), 1, recipe);
        localStorage.setItem(this.cartKey, JSON.stringify(this.cart));
      }
    }
  }
}
