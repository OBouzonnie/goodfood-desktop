import { HttpHeaders, HttpParams } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import faker from '@faker-js/faker';
import { AppModule } from 'src/app/app.module';

import { RequestService } from './request.service';

describe('RequestService', () => {
  let service: RequestService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AppModule],
    });
    service = TestBed.inject(RequestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('getHttpOptions return options w/o params', () => {
    const options = service.getHttpOptions();
    const content = options.headers.get('content-type');
    const accept = options.headers.get('accept');
    const authorize = options.headers.get('Authorization');
    expect(content).toBe('application/json');
    expect(accept).toBe('application/json');
    expect(authorize).toContain('Bearer');
    expect(options.observe).toBe('response');
  });

  it('getHttpOptions return options with params', () => {
    const uid = faker.datatype.uuid();
    const params = new HttpParams().set('id', uid);
    const options = service.getHttpOptions(params);
    const content = options.headers.get('content-type');
    const accept = options.headers.get('accept');
    const authorize = options.headers.get('Authorization');
    const id = options.params.get('id');
    expect(content).toBe('application/json');
    expect(accept).toBe('application/json');
    expect(authorize).toContain('Bearer');
    expect(options.observe).toBe('response');
    expect(id).toBe(uid);
  });
});
