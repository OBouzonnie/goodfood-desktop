import { HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { TokenService } from '../token/token.service';

/**
 * request tools
 */
@Injectable({
  providedIn: 'root'
})
export class RequestService {

  /**
   * class constructor
   * @param tokenService allow access to the jwt token
   */
  constructor(
    private tokenService: TokenService,
  ) { }

  /**
   * handle request errors
   * @param badResponse
   * @returns an error observable
   */
  public handleServerError(badResponse: HttpErrorResponse): Observable<never> {
    return throwError(() => badResponse);
  }

  /**
   * create http request options
   * @param params http params
   * @returns the request http options
   */
  public getHttpOptions(params?: HttpParams): any {
    const options: any = {
      headers: this.httpHeaders()
    };
    if (params) options.params = params;
    options.observe = 'response';
    return options;
  }

  /**
   * create http request headers
   * @returns the http headers of the request
   */
  private httpHeaders(): HttpHeaders {
    const token = this.tokenService.getToken();
    return new HttpHeaders()
                            .set('content-type', 'application/json')
                            .set('accept', 'application/json')
                            .set('Authorization', `Bearer ${token}`);
  }
}
