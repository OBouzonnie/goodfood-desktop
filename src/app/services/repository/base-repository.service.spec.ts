import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { BaseRepositoryService } from './base-repository.service';
import { MockService } from '../mock/mock.service';
import { Observable } from 'rxjs';
import faker from '@faker-js/faker';
import { RequestService } from '../request/request.service';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

describe('BaseRepositoryService', () => {
  let service: BaseRepositoryService;
  let mock: MockService;
  let http: HttpClient;
  let api: string  = environment.api;
  let url: string  = faker.datatype.uuid();
  let objID: string  = faker.datatype.uuid();
  let requestService: RequestService;
  let bodyID: string = faker.datatype.uuid();

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    requestService = TestBed.inject(RequestService);
    service = TestBed.inject(BaseRepositoryService);
    http = TestBed.inject(HttpClient);
    mock = new MockService();
    url = faker.datatype.uuid();
    objID = faker.datatype.uuid();
    bodyID = faker.datatype.uuid();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('getAll should call http.get', waitForAsync(() => {
    const obs = new Observable();
    spyOn(http, 'get').and.callThrough().and.returnValue(obs);
    const resp = service.getAll(url);
    expect(http.get).toHaveBeenCalledWith(api + url, requestService.getHttpOptions());
    expect(resp.then).toBeTruthy();
  }));

  it('get should call http.get', waitForAsync(() => {
    const obs = new Observable();
    spyOn(http, 'get').and.callThrough().and.returnValue(obs);
    const resp = service.get(url, objID);
    expect(http.get).toHaveBeenCalledWith(api + url + '/' + objID, requestService.getHttpOptions());
    expect(resp.then).toBeTruthy();
  }));

  it('delete should call http.delete', waitForAsync(() => {
    const obs = new Observable();
    spyOn(http, 'delete').and.callThrough().and.returnValue(obs);
    const resp = service.delete(url, objID);
    expect(http.delete).toHaveBeenCalledWith(api + url + '/' + objID, requestService.getHttpOptions());
    expect(resp.then).toBeTruthy();
  }));

  it('post should call http.post', waitForAsync(() => {
    const obs = new Observable();
    const body = {id: bodyID};
    spyOn(http, 'post').and.callThrough().and.returnValue(obs);
    const resp = service.post(url, body);
    expect(http.post).toHaveBeenCalledWith(api + url, body, requestService.getHttpOptions());
    expect(resp.then).toBeTruthy();
  }));

  it('put should call http.put', waitForAsync(() => {
    const obs = new Observable();
    const body = {id: bodyID};
    spyOn(http, 'put').and.callThrough().and.returnValue(obs);
    const resp = service.put(url, body);
    expect(http.put).toHaveBeenCalledWith(api + url + '/' + body.id, body, requestService.getHttpOptions());
    expect(resp.then).toBeTruthy();
  }));

});
