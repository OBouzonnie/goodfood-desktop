import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, firstValueFrom } from 'rxjs';
import { environment } from 'src/environments/environment';
import { RequestService } from '../request/request.service';

/**
 * the application repository
 * handle all api calls, except authentication related ones
 */
@Injectable({
  providedIn: 'root'
})
export class BaseRepositoryService {

  /**
   * api url
   */
  readonly API: string = environment.api;

  /**
   * class constructor
   * @param http angular http request client
   * @param requestService request tools
   */
  constructor(
    protected http: HttpClient,
    protected requestService: RequestService
  ) { }

  /**
   * perform a get request for all record of a resource
   * @param url api url
   * @returns a response promise
   */
  getAll(url: string): Promise<any>{
    const callAPI = this.API + url;
    const options = this.requestService.getHttpOptions();
    return firstValueFrom(this.http.get<any>(callAPI, options).pipe(catchError(this.requestService.handleServerError)));
  }

  /**
   * perform a get request for one record of a resource
   * @param url api url
   * @param id resource id
   * @returns a response promise
   */
  get(url: string, id?: string): Promise<any>{
    let callAPI = this.API + url;
    if(id) callAPI += '/' + id;
    const options = this.requestService.getHttpOptions();
    return firstValueFrom(this.http.get<any>(callAPI, options).pipe(catchError(this.requestService.handleServerError)));
  }

  /**
   * perform a delete request for one record of a resource
   * @param url api url
   * @param id resource id
   * @returns a response promise
   */
  delete(url: string, id: string): Promise<any>{
    const callAPI = this.API + url + '/' + id;
    const options = this.requestService.getHttpOptions();
    return firstValueFrom(this.http.delete<any>(callAPI, options).pipe(catchError(this.requestService.handleServerError)));
  }

  /**
   * perform a post request for one record of a resource
   * @param url api url
   * @param body resource to create as a request body
   * @returns a response promise
   */
  post(url: string, body: any): Promise<any>{
    const callAPI = this.API + url;
    const options = this.requestService.getHttpOptions();
    return firstValueFrom(this.http.post<any>(callAPI, body, options).pipe(catchError(this.requestService.handleServerError)));
  }

  /**
   * perform a put request for one record of a resource
   * @param url api url
   * @param body resource to update as a request body
   * @returns a response promise
   */
  put(url: string, body: any): Promise<any>{
    let callAPI = this.API + url;
    if(body.id) callAPI += '/' + body.id;
    const options = this.requestService.getHttpOptions();
    return firstValueFrom(this.http.put<any>(callAPI, body, options).pipe(catchError(this.requestService.handleServerError)));
  }

  /**
   * perform a post request for a rest action on one resource
   * @param url api url
   * @returns a response promise
   */
  action(url: string): Promise<any>{
    const callAPI = this.API + url;
    const options = this.requestService.getHttpOptions();
    return firstValueFrom(this.http.post<any>(callAPI, null, options).pipe(catchError(this.requestService.handleServerError)));
  }
}
