import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';
import {TranslateService} from '@ngx-translate/core';
import { environment } from 'src/environments/environment';

/**
 * App entry point - render the app
 */
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  /**
   * Handle injected services and instanciation
   */
  constructor(
    /**
     * ngx translate translation service
     */
    public translate: TranslateService,
    private title: Title
  ) {
    translate.addLangs(['en', 'fr']);
    translate.setDefaultLang('fr');

    const browserLang = translate.getBrowserLang();
    translate.use(browserLang?.match(/en|fr/) ? browserLang : 'en');
    title.setTitle(environment.title);
  }
}
