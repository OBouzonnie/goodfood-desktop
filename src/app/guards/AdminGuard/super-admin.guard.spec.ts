import { TestBed } from '@angular/core/testing';
import { AppModule } from 'src/app/app.module';
import { SuperAdminGuard } from './super-admin.guard';

describe('SuperAdminGuard', () => {
  let guard: SuperAdminGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AppModule],
    });
    guard = TestBed.inject(SuperAdminGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
