import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { PermissionService } from 'src/app/services/perm/permission.service';

/**
 * protect super-admin restricted areas
 */
@Injectable({
  providedIn: 'root'
})
export class SuperAdminGuard implements CanActivate {

  constructor(
    private permService: PermissionService,
    private router: Router
  ){}

  /**
   * allow super admin to activate their restricted resources
   * @param route angular activated route
   * @param state angular router state
   * @returns a boolean access permission
   */
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.permService.isSuper();
  }

}
