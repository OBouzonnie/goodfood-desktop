import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanLoad, Route, Router, RouterStateSnapshot, UrlSegment, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { ReturnLink } from 'src/app/models/enums/ReturnLink';
import { PermissionService } from 'src/app/services/perm/permission.service';
import { environment } from 'src/environments/environment';

/**
 * global application guard
 */
@Injectable({
  providedIn: 'root'
})
export class AppGuard implements CanActivate, CanLoad {

  /**
   * unrestricted routes
   */
  whiteList = ['/' + environment.core.ROUTES.goodfood.url + '/' + environment.core.ROUTES.goodfood.modules.auth.url];

  /**
   * class constructor
   * @param permService permission checking tool
   * @param router
   */
  constructor(
    private permService: PermissionService,
    private router: Router
  ){}

  /**
   * check user permission to access any role restricted routes
   * @param route angular activated route
   * @param state angular router state
   * @returns a boolean access permission state
   */
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if(this.whiteList.includes(state.url)) return true;
    if (this.permService.isLoggedIn(state.url)) return true;
    return this.router.createUrlTree([ReturnLink.LOGIN]);
  }

  /**
   * check user permission to lead a specific role restricted module
   * @param route angular route
   * @param segments urls segments
   * @returns a boolean access permission state
   */
  canLoad(
    route: Route,
    segments: UrlSegment[]): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (route.path && this.permService.canAccess(route.path)) return true;
    return this.router.createUrlTree([ReturnLink.LOGIN]);
  }
}
