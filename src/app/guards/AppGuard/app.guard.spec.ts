import { TestBed } from '@angular/core/testing';
import { AppModule } from 'src/app/app.module';
import { AppGuard } from './app.guard';

describe('AppGuard', () => {
  let guard: AppGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AppModule],
    });
    guard = TestBed.inject(AppGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
