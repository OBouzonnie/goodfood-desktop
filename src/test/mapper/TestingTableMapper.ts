import faker from "@faker-js/faker";
import { TableModel } from "../../app/models/table/resources/TableModel";
import { TableMapper } from "../../app/models/table/mappers/TableMapper";

/**
 * table mapper for test purpose
 */
export class TestingTableMapper extends TableMapper {

  /**
   * table head
   */
  override head: TableModel = {
    name: 'name',
    photo: 'photo',
    desc: 'description',
    uid: ''
  };

  /**
   * table length
   */
  private length: number = Math.floor((Math.random() + 1) * 100);

  constructor() {
    super();
    this.addMultipleRows();
  }

  /**
   * add one mocked row
   */
  override addRow(){
    this.rows.push({
      name: faker.commerce.productName(),
      photo: faker.image.image(),
      desc: faker.commerce.productDescription(),
      uid: faker.datatype.uuid(),
      collapse: false
    });
  }

  /**
   * add several mocked row
   */
  override addMultipleRows(): void {
    for (let i = 0; i < this.length; i++){
      this.addRow();
    }
  }
}
