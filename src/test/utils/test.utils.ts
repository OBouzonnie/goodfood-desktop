import faker from '@faker-js/faker';
import { NavLink } from 'src/app/models/type/NavLink';
import {environment} from 'src/environments/environment';
import {TestSpecialChar} from '../type/TestSpecialChar';

/**
 * Test utilities
 */
class TestUtil{

  /**
   * api localhost regexp
   */
  appUrlRegExp = new RegExp(/^http:\/\/localhost:[0-9]+\//);

  /**
   * Environment const
   */
  private env = environment;

  /**
   * Forbidden chars
   */
  private specialChars: string[] = this.env.core.VALIDATOR.REGEXP.forbidCharsAndSpace.toString().replace('\\s', '').slice(4, 12).split('');

  /**
   * Insert a forbidden char in a string
   *
   * @param str
   * @return an object providing the modified string and the char inserted
   */
  public useSpecialChar(str: string): TestSpecialChar{
    let i = Math.floor(Math.random() * this.specialChars.length);
    let j = Math.floor(Math.random() * str.length);
    return {
      char: this.specialChars[i],
      str: [str.slice(0, j), this.specialChars[i], str.slice(j)].join('')
    }
  }

  /**
   * Insert a space in a string
   *
   * @param str
   * @return the space inserted string
   */
  public useSpace(str: string): string{
    let i = Math.floor(Math.random() * str.length);
    return [str.slice(0, i), ' ', str.slice(i)].join('');
  }

  /**
   * Reduce a password string length
   *
   * @param str
   * @return the sliced string
   */
  public cutPwd(str: string): string{
    let i = Math.floor(Math.random() * this.env.core.VALIDATOR.PWD_MIN_LENGTH);
    return str.slice(0, i);
  }

  /**
   * A basic script
   *
   * @return a string of a basic console.log
   */
  public useScript(): string{
    return "<script>console.log('injected script')</script>"
  }

  public mockNavLinks(length: number = 10): any[] {
    const links: NavLink[] = [];
    for (let i = 0; i < length; i++){
      links.push({
        url: faker.datatype.uuid(),
        label: faker.random.word(),
      });
    }
    return links;
  }

  /**
   * generate a random datetime
   * @returns a random datetime
   */
  public randomTime(): string{
    return (Math.floor(Math.random() * 10) + 10).toString() + ':' + (Math.floor(Math.random() * 50) + 10).toString();
  }

  /**
   * generate a random string number
   * @param x random range
   * @returns a random number as a string
   */
  public randomStringifiedNumber(x: number): string{
    return Math.floor(Math.random() * x).toString()
  }
}

export default new TestUtil();
