/**
 * Return type for string modified with specials chars
 */
export type TestSpecialChar = {
  /**
   * tested char
   */
  char: string,

  /**
   * mocked string
   */
  str: string
}
