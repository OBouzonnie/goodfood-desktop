import { Roles } from "src/app/models/enums/Roles";

/**
 * App shared const for all environments
 */
export const core = {

  //application currency
  currency: '€',

  // use for no data display
  noData: '--',
  //image encoding
  dwlImg: 'data:image/jpeg;base64,',
  idParam: '{id}',

  //table commun field name
  TABLE: {
    uid: 'uid',
    photo: 'photo',
    name: 'name',
    std: 'logo',
    eval: 'eval'
  },

  //form validators
  VALIDATOR: {
    REGEXP: {
      forbidCharsAndSpace: /^[^<>*%:\s$&;-]+$/,
      forbiddenChars: /^[^<>*%:&;-]+$/,
      pwd: /^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])([^\s]){1,}$/,
      digit: /^\d+$/,
      decimal: /^(\d+((\.|,)?\d{1,2}){0,1})$/,
      creditCard: {
        number: /^(\d{4}\s){3}\d{4}$/,
        expiration: /^(0[1-9]|1[0-2])\/[0-9]{2}$/,
        code: /^\d{3}$/
      }
    },
    PWD_MIN_LENGTH: 8,
    INPUTS_MAX_LENGTH: 50,
    PHONE_MAX_LENGTH: 15,
    TXT_MAX_LENGTH: 250
  },

  QUERY_PARAMS: {
    id : '/:id'
  },

  //application routes configuration
  ROUTES: {
    default: {
      modules: {
        auth: {
          url: 'auth'
        },
        shop: {
          url: '',
          childrens: {
            home : {
              url: 'home',
              label: 'shop.home',
              icon: 'home'
            },
            catalog : {
              url: 'catalog',
              label: '',
              icon: ''
            },
            account : {
              url: 'account',
              label: 'shop.account',
              icon: 'account_circle'
            },
            cart : {
              url: 'cart',
              label: 'shop.cart',
              icon: 'shopping_cart'
            },
            order : {
              url: 'order',
              label: 'shop.order',
            },
            payment: {
              url: 'payment'
            },
            recap: {
              url: 'recap'
            }
          }
        }
      }
    },
    goodfood: {
      url: 'gf',
      modules: {
        auth :{
          url: 'auth'
        },
        group: {
          url: 'group',
          permissions: [Roles.GROUP, Roles.ADMIN, Roles.SUPER_ADMIN],
          childrens: {
            restaurant : {
              url: 'restaurant',
              label: 'resource.restaurant',
              icon: 'restaurant'
            },
            recipe : {
              url: 'recipe',
              label: 'resource.recipe',
              icon: 'menu_book'
            },
            ingredient : {
              url: 'ingredients',
              label: 'resource.ingredients',
              icon: 'egg'
            },
            discount : {
              url: 'discount',
              label: 'resource.discount',
              icon: 'discount'
            },
            supplier : {
              url: 'supplier',
              label: 'resource.supplier',
              icon: 'storefront'
            },
            admin : {
              url: 'admin',
              label: 'resource.admin',
              icon: 'admin_panel_settings'
            },
          }
        },
        franchise: {
          url: 'franchise',
          permissions: [Roles.FRANCHISE, Roles.ADMIN, Roles.SUPER_ADMIN],
          childrens: {
            stats : {
              url: 'stats',
              label: 'resource.stats',
              icon: 'analytics'
            },
            orders : {
              url: 'orders',
              label: 'resource.order',
              icon: 'reorder'
            },
            recipe : {
              url: 'recipe',
              label: 'resource.recipe',
              icon: 'menu_book'
            },
            ingredient : {
              url: 'ingredients',
              label: 'resource.ingredients',
              icon: 'egg'
            },
            discount : {
              url: 'discount',
              label: 'resource.discount',
              icon: 'discount'
            },
            supplier : {
              url: 'supplier',
              label: 'resource.supplier',
              icon: 'storefront'
            },
            purchase : {
              url: 'purchase',
              label: 'resource.purchase',
              icon: 'payments'
            },
          }
        },
        accounting: {
          url: 'accounting',
          permissions: [Roles.ACCOUNTING, Roles.ADMIN, Roles.SUPER_ADMIN],
          childrens: {
            restaurant : {
              url: 'restaurant',
              label: 'resource.restaurant',
              icon: 'restaurant'
            }
          }
        },
        community: {
          url: 'community',
          permissions: [Roles.COMMUNITY_MANAGEMENT, Roles.ADMIN, Roles.SUPER_ADMIN],
          childrens: {
            restaurant : {
              url: 'restaurant',
              label: 'resource.restaurant',
              icon: 'restaurant'
            }
          }
        }
      }
    }
  }
};
