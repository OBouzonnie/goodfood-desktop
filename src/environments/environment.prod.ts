import {core} from './core/core.environment';

/**
 * App shared const as set for production
 */
export const environment = {
  production: true,

  title: 'GOOD FOOD',

  core: core,
  api: "http://5.196.74.128:8082"
};
